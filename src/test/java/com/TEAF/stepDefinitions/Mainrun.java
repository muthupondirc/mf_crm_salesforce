package com.TEAF.stepDefinitions;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Mainrun {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int corePoolSize = 15;
		int maximumPoolSize = 15;
		int keepAliveTime = 15;
		int queueSize = 15;

		ThreadFactory threadfactory = Executors.defaultThreadFactory();
		ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime,
				TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(queueSize), threadfactory);
		for (int i = 0; i < 15; i++) {
			Runnable runnable = new Test();
			poolExecutor.execute(runnable);
		}
	}

}

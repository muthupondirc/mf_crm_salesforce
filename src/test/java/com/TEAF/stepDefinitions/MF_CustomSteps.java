package com.TEAF.stepDefinitions;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import java.awt.Robot;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import cucumber.runtime.CucumberException;
import java.util.TimeZone;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import org.junit.Assert;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.bcel.generic.Select;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import cucumber.api.java.en.And;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
//import com.gargoylesoftware.htmlunit.javascript.host.Screen;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.search.FlagTerm;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @ScriptName : Utilities
 * @Description : This class contains Commonly used Keyword for Mobile/Web
 *              application automation using Cucumber framework
 * @Author : Swathin Ratheendren
 * @Creation Date : September 2016 @Modified Date:
 */
public class MF_CustomSteps {

	static Logger log = Logger.getLogger(MF_CustomSteps.class.getName());
	private static int elementWaitTime = Integer.parseInt(System.getProperty("test.implicitlyWait"));

	private static WebDriver driver = StepBase.getDriver();
	// private static int elementWaitTime =
	// Integer.parseInt(System.getProperty("test.implicitlyWait"));

	// private static int elementWaitTime =
	// Integer.parseInt(System.getProperty("test.implicitlyWait"));
	
	@Given("^I login to Mattress Firm Sales Force with valid '(.*)' User Credentials$")
	public static void I_Login_to_MF_SF_ValidCreds(String userType) throws Exception {
	try {
	Utilities util = new Utilities();
	String host = "outlook.office365.com";// change accordingly
	String mailStoreType = "imaps";
	String username = "sindhu.r@royalcyber.com";// change accordingly
	String password = "wzcqvckqhyzzkhfz";// change accordingly
	log.info("Logging into SF with " + userType + " Credential");
	CommonSteps.I_enter_in_field(Utilities.getTestData(userType + "_username"), "login_username");
	CommonSteps.I_enter_in_field(Utilities.getTestData(userType + "_password"), "login_password");
	CommonSteps.I_click("login_submitBtn");
	CommonSteps.I_pause_for_seconds(4);
	} catch (Exception e) {
	log.error(e);
	throw new CucumberException(e.getMessage(), e);
	} }


	
	/*@Given("^I login to Mattress Firm Sales Force with valid '(.*)' User Credentials$")
	public static void I_Login_to_MF_SF_ValidCreds(String userType) throws Exception {
		try {			
			Utilities util = new Utilities();
			String host = "outlook.office365.com";// change accordingly
			String mailStoreType = "imaps";
			String username = "sindhu.r@royalcyber.com";// change accordingly
			String password = "wzcqvckqhyzzkhfz";// change accordingly

			if(System.getProperty("test_env_URL").equals("MF_SF_UAT")) 
			{
				log.info("Env : "+System.getProperty("test_env_URL"));
				if(userType.equalsIgnoreCase("Agent_UAT") || userType.equalsIgnoreCase("CCO Agent User 2_UAT")) 
				{
					//if(userType.equalsIgnoreCase("Kim Henry-Nelson_QA")) {
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccoagent2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("Lead_UAT") || userType.equalsIgnoreCase("CCO Lead User 2_UAT"))
					//else if(userType.equalsIgnoreCase("Lautrice Tillman_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccolead2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("ConsumerRelations_UAT") || userType.equalsIgnoreCase("CCO CR User 2_UAT"))
					//else if(userType.equalsIgnoreCase("Stenio Joseph_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccocr2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("Telesales_UAT") || userType.equalsIgnoreCase("IS Mgr User 2_UAT"))
					//else if(userType.equalsIgnoreCase("RICHARD LOPEZ_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ismgr2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("TelesalesRep_UAT") || userType.equalsIgnoreCase("IS Agent User 2_UAT"))
					//else if(userType.equalsIgnoreCase("Kelly Bonner_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("isagent2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("eCommSupport_UAT") || userType.equalsIgnoreCase("CCO Ecomm user 2_UAT"))
					//else if(userType.equalsIgnoreCase("Christiana Adams_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccoecomm2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("HybridAgent_UAT") || userType.equalsIgnoreCase("Retention Agent User 2_UAT"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("retentionagent2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("ConsumerRelationsManagment_UAT") || userType.equalsIgnoreCase("CCO Sr Mgr User 2_UAT"))
					//else if(userType.equalsIgnoreCase("Vanessa Guevara"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccosrmgr2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("ConsumerRelations_UAT") || userType.equalsIgnoreCase("CCO CR User 2_UAT"))
					//else if(userType.equalsIgnoreCase("Vanessa Guevara"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccocr2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("Logistics Mgr User 2_UAT") || userType.equalsIgnoreCase("Logistics Mgr User 2_UAT"))
					//else if(userType.equalsIgnoreCase("Vanessa Guevara"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("logisticsmgr2@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}

				else {
					log.info("CCO Agent User 3_UAT");
					CommonSteps.I_enter_in_field("ccoagent3@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
			}
			//credentials for QA 
			else
			{
				if(userType.equalsIgnoreCase("Agent_QA") || userType.equalsIgnoreCase("CCO Agent User 2_QA")) 
				{
					//if(userType.equalsIgnoreCase("Kim Henry-Nelson_QA")) {
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccoagent2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("Lead_QA") || userType.equalsIgnoreCase("CCO Lead User 2_QA"))
					//else if(userType.equalsIgnoreCase("Lautrice Tillman_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccolead2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("ConsumerRelations_QA") || userType.equalsIgnoreCase("CCO CR User 2_QA"))
					//else if(userType.equalsIgnoreCase("Stenio Joseph_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccocr2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("Telesales_QA") || userType.equalsIgnoreCase("IS Mgr User 2_QA"))
					//else if(userType.equalsIgnoreCase("RICHARD LOPEZ_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ismgr2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("TelesalesRep_QA") || userType.equalsIgnoreCase("IS Agent User 2_QA"))
					//else if(userType.equalsIgnoreCase("Kelly Bonner_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("isagent2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("eCommSupport_QA") || userType.equalsIgnoreCase("CCO Ecomm user 2_QA"))
					//else if(userType.equalsIgnoreCase("Christiana Adams_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccoecomm2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("HybridAgent_QA") || userType.equalsIgnoreCase("Retention Agent User 2_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("retentionagent2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("HybridMgr_QA") || userType.equalsIgnoreCase("Retention Mgr User 1_QA"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("muhammad.hasan@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Hasan123!@#", "login_password");
				}
				else if(userType.equalsIgnoreCase("ConsumerRelationsManagment_QA") || userType.equalsIgnoreCase("CCO Sr Mgr User 2_QA"))
					//else if(userType.equalsIgnoreCase("Vanessa Guevara"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccosrmgr2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("ConsumerRelations_QA") || userType.equalsIgnoreCase("CCO CR User 2_QA"))
					//else if(userType.equalsIgnoreCase("Vanessa Guevara"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("ccocr2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else if(userType.equalsIgnoreCase("Logistics Mgr User 2_QA") || userType.equalsIgnoreCase("Logistics Mgr User 2_QA"))
					//else if(userType.equalsIgnoreCase("Vanessa Guevara"))
				{
					log.info("Logging into SF with Agent Credential");
					CommonSteps.I_enter_in_field("logisticsmgr2@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}
				else {
					log.info("CCO Agent User 3_QA");
					CommonSteps.I_enter_in_field("ccoagent3@royalcyber.com.qa", "login_username");
					CommonSteps.I_enter_in_field("Divya@123", "login_password");
				}

			}

			CommonSteps.I_click("login_submitBtn");
			CommonSteps.I_pause_for_seconds(4);
			//String body = util.readRecentMail_byCount_and_Subject(host, mailStoreType, username, password, 5, "[EXTERNAL]Sandbox: Verify your identity in Salesforce", "noreply@salesforce.com");
			//String code = util.get_SF_VerificationCode(body);
			//CommonSteps.I_should_see_on_page("VerifiyIdentityHeader");
			//CommonSteps.I_enter_in_field(code, "verificationCodeField");
			//CommonSteps.I_click("Verify_Button");
			//CommonSteps.I_pause_for_seconds(10);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	/*@Given("^I login to Mattress Firm Sales Force with valid '(.*)' User Credentials$")
	public static void I_Login_to_MF_SF_ValidCreds(String userType) throws Exception {
		try {
			Utilities util = new Utilities();
			String host = "outlook.office365.com";// change accordingly
			String mailStoreType = "imaps";
			String username = "sindhu.r@royalcyber.com";// change accordingly
			String password = "wzcqvckqhyzzkhfz";// change accordingly
			log.info("Logging into SF with " + userType + " Credential");
			CommonSteps.I_enter_in_field(Utilities.getTestData(userType + "_username"), "login_username");
			CommonSteps.I_enter_in_field(Utilities.getTestData(userType + "_password"), "login_password");
			CommonSteps.I_click("login_submitBtn");
			CommonSteps.I_pause_for_seconds(4);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}

	}*/
		
		    /*  if(System.getProperty("test_env_URL").equals("MF_SF_UAT")) 
		      {
		    	  log.info("Env : "+System.getProperty("test_env_URL"));
		    	  if(userType.equalsIgnoreCase("Agent_UAT") || userType.equalsIgnoreCase("Kim Henry-Nelson_UAT")) {
		     //if(userType.equalsIgnoreCase("Kim Henry-Nelson")) {
		    	 log.info("Logging into SF with Agent Credential");
		    	CommonSteps.I_enter_in_field("ccoagent3@royalcyber.com.uat", "login_username");
				CommonSteps.I_enter_in_field("Test@123", "login_password");
		     }
		      else if(userType.equalsIgnoreCase("Lead_UAT") || userType.equalsIgnoreCase("Lautrice Tillman_UAT"))
		     //else if(userType.equalsIgnoreCase("Lautrice Tillman"))
		     {
		    	 log.info("Logging into SF with Agent Credential");
			    	CommonSteps.I_enter_in_field("182096632@mfrm.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Sindhu@456", "login_password");
		     }
		      else if(userType.equalsIgnoreCase("ConsumerRelations_UAT") || userType.equalsIgnoreCase("Stenio Joseph_UAT"))
		     //else if(userType.equalsIgnoreCase("Stenio Joseph"))
		     {
		    	 log.info("Logging into SF with Agent Credential");
			    	CommonSteps.I_enter_in_field("118945878@mfrm.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Sindhu@345", "login_password");
		     }
		      else if(userType.equalsIgnoreCase("Telesales_UAT") || userType.equalsIgnoreCase("RICHARD LOPEZ_UAT"))
		     //else if(userType.equalsIgnoreCase("RICHARD LOPEZ"))
		     {
		    	 log.info("Logging into SF with Agent Credential");
			    	CommonSteps.I_enter_in_field("ismgr3@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Test@123", "login_password");
		     }
		      else if(userType.equalsIgnoreCase("TelesalesRep_UAT") || userType.equalsIgnoreCase("Kelly Bonner_UAT"))
		     //else if(userType.equalsIgnoreCase("Kelly Bonner"))
		     {
		    	 log.info("Logging into SF with Agent Credential");
			    	CommonSteps.I_enter_in_field("isagent3@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Test@123", "login_password");
		     }
		      else if(userType.equalsIgnoreCase("eCommSupport_UAT") || userType.equalsIgnoreCase("Christiana Adams_UAT"))
		     //else if(userType.equalsIgnoreCase("Christiana Adams"))
		     {
		    	 log.info("Logging into SF with Agent Credential");
			    	CommonSteps.I_enter_in_field("ccoecomm3@royalcyber.com.uat", "login_username");
					CommonSteps.I_enter_in_field("Test@123", "login_password");
		     }
		      else if(userType.equalsIgnoreCase("ConsumerRelationsManagment_UAT") || userType.equalsIgnoreCase("Vanessa Guevara_UAT"))
				     //else if(userType.equalsIgnoreCase("Vanessa Guevara"))
				     {
				    	 log.info("Logging into SF with Agent Credential");
					    	CommonSteps.I_enter_in_field("100241908@mfrm.com.uat", "login_username");
							CommonSteps.I_enter_in_field("Sindhu@444", "login_password");
				     }
		      else if(userType.equalsIgnoreCase("HybridAgent_UAT") || userType.equalsIgnoreCase("Jarely Cruz_UAT"))
			     {
			    	 log.info("Logging into SF with Agent Credential");
				    	CommonSteps.I_enter_in_field("118570682@mfrm.com.uat", "login_username");
						CommonSteps.I_enter_in_field("Sindhu@222", "login_password");
			     }
		     else {
		    	 log.info("Ashley Bailey_UAT");
		    	CommonSteps.I_enter_in_field("139190684@mfrm.com.uat", "login_username");
				CommonSteps.I_enter_in_field("Akshaya@123", "login_password");
		     }
		      }
		      //credentials for QA 
		      else
		      {
		    	  if(userType.equalsIgnoreCase("Agent_QA") || userType.equalsIgnoreCase("CCO Agent User 2_QA")) 
		    	  {
		    	  //if(userType.equalsIgnoreCase("Kim Henry-Nelson_QA")) {
				    	 log.info("Logging into SF with Agent Credential");
				    	CommonSteps.I_enter_in_field("ccoagent2@royalcyber.com.qa", "login_username");
						CommonSteps.I_enter_in_field("Sindhu@111", "login_password");
				     }
		    	  else if(userType.equalsIgnoreCase("Lead_QA") || userType.equalsIgnoreCase("CCO Lead User 2_QA"))
				     //else if(userType.equalsIgnoreCase("Lautrice Tillman_QA"))
				     {
				    	 log.info("Logging into SF with Agent Credential");
					    	CommonSteps.I_enter_in_field("ccolead2@royalcyber.com.qa", "login_username");
							CommonSteps.I_enter_in_field("Sindhu@444", "login_password");
				     }
		    	  else if(userType.equalsIgnoreCase("ConsumerRelations_QA") || userType.equalsIgnoreCase("CCO CR User 2_QA"))
				     //else if(userType.equalsIgnoreCase("Stenio Joseph_QA"))
				     {
				    	 log.info("Logging into SF with Agent Credential");
					    	CommonSteps.I_enter_in_field("ccocr2@royalcyber.com.qa", "login_username");
							CommonSteps.I_enter_in_field("Sindhu@222", "login_password");
				     }
		    	  else if(userType.equalsIgnoreCase("Telesales_QA") || userType.equalsIgnoreCase("IS Mgr User 2_QA"))
				     //else if(userType.equalsIgnoreCase("RICHARD LOPEZ_QA"))
				     {
				    	 log.info("Logging into SF with Agent Credential");
					    	CommonSteps.I_enter_in_field("ismgr2@royalcyber.com.qa", "login_username");
							CommonSteps.I_enter_in_field("Sindhu@888", "login_password");
				     }
		    	  else if(userType.equalsIgnoreCase("TelesalesRep_QA") || userType.equalsIgnoreCase("IS Agent User 2_QA"))
				     //else if(userType.equalsIgnoreCase("Kelly Bonner_QA"))
				     {
				    	 log.info("Logging into SF with Agent Credential");
					    	CommonSteps.I_enter_in_field("isagent2@royalcyber.com.qa", "login_username");
							CommonSteps.I_enter_in_field("Sindhu@777", "login_password");
				     }
		    	  else if(userType.equalsIgnoreCase("eCommSupport_QA") || userType.equalsIgnoreCase("CCO Ecomm user 2_QA"))
				     //else if(userType.equalsIgnoreCase("Christiana Adams_QA"))
				     {
				    	 log.info("Logging into SF with Agent Credential");
					    	CommonSteps.I_enter_in_field("ccoecomm2@royalcyber.com.qa", "login_username");
							CommonSteps.I_enter_in_field("Sindhu@333", "login_password");
				     }
				     else if(userType.equalsIgnoreCase("HybridAgent_QA") || userType.equalsIgnoreCase("Retention Agent User 2_QA"))
				     {
				    	 log.info("Logging into SF with Agent Credential");
					    	CommonSteps.I_enter_in_field("retentionagent2@royalcyber.com.qa", "login_username");
							CommonSteps.I_enter_in_field("Sindhu@121", "login_password");
				     }
				     else if(userType.equalsIgnoreCase("ConsumerRelationsManagment_QA") || userType.equalsIgnoreCase("CCO Sr Mgr User 2_QA"))
					     //else if(userType.equalsIgnoreCase("Vanessa Guevara"))
					     {
					    	 log.info("Logging into SF with Agent Credential");
						    	CommonSteps.I_enter_in_field("ccosrmgr2@royalcyber.com.qa", "login_username");
								CommonSteps.I_enter_in_field("Sindhu@666", "login_password");
					     }
				     else if(userType.equalsIgnoreCase("ConsumerRelations_QA") || userType.equalsIgnoreCase("CCO CR User 2_QA"))
					     //else if(userType.equalsIgnoreCase("Vanessa Guevara"))
					     {
					    	 log.info("Logging into SF with Agent Credential");
						    	CommonSteps.I_enter_in_field("ccocr2@royalcyber.com.qa", "login_username");
								CommonSteps.I_enter_in_field("Sindhu@222", "login_password");
					     }
				     else if(userType.equalsIgnoreCase("Logistics Mgr User 2_QA") || userType.equalsIgnoreCase("Logistics Mgr User 2_QA"))
					     //else if(userType.equalsIgnoreCase("Vanessa Guevara"))
					     {
					    	 log.info("Logging into SF with Agent Credential");
						    	CommonSteps.I_enter_in_field("logisticsmgr2@royalcyber.com.qa", "login_username");
								CommonSteps.I_enter_in_field("Sindhu@000", "login_password");
					     }
				     else {
				    	 log.info("CCO Agent User 1_QA");
				    	CommonSteps.I_enter_in_field("ccoagent1@royalcyber.com.qa", "login_username");
						CommonSteps.I_enter_in_field("Hasan123!@#", "login_password");
				     }
		    	  
		      }
			
			CommonSteps.I_click("login_submitBtn");
			CommonSteps.I_pause_for_seconds(4);
			//String body = util.readRecentMail_byCount_and_Subject(host, mailStoreType, username, password, 5, "[EXTERNAL]Sandbox: Verify your identity in Salesforce", "noreply@salesforce.com");
			//String code = util.get_SF_VerificationCode(body);
			//CommonSteps.I_should_see_on_page("VerifiyIdentityHeader");
			//CommonSteps.I_enter_in_field(code, "verificationCodeField");
			//CommonSteps.I_click("Verify_Button");
			//CommonSteps.I_pause_for_seconds(10);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}*/


	@Given("^I extract password reset link from gmail inbox & store to '(.*)'$")
	public static void I_Login_to_GmailAccount_ValidCreds(String var) throws Exception {
		try {
			String mailContent = Utilities.Fetch_Last_email_MatchingSubject_FromGmail("hasantestuser@gmail.com",
					"Hasan123!@#", "Sandbox: Let's reset your password.");
			System.out.println(mailContent);
			Document doc = Jsoup.parse(mailContent);
			Elements p_tags = doc.select("a");
			String attr = null;
			for (Element p : p_tags) {
				attr = p.attr("href");
				List<Element> sibs = p.siblingElements();

				if (sibs.size() < 1 && attr.contains("forgotpassword")) {
					System.out.println("link: " + attr);
					HashMapContainer.add("$$" + var, attr);
					break;
				} else {
					throw new Exception("Password Link extraction failed!");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
//	 public static void main(String[] args) throws Exception {
//		 I_Login_to_GmailAccount_ValidCreds("test");
//	}

	@Given("^I extract the Identification Code from gmail inbox '(.*)'$")
	public static void I_Login_to_GmailAccount_Toget_IdentificationCode(String var) throws Exception {
		try {
			String mailContent = Utilities.Fetch_Last_email_MatchingSubject_FromGmail("hasantestuser@gmail.com",
					"Hasan123!@#", "Sandbox: Verify your new account in Mattress Firm Customer Portal");
			System.out.println(mailContent);

			// String x = "Just enter the following code where prompted: 1286<br></br>";

			String otp = StringUtils.substringBetween(mailContent, "where prompted: ", "<br></br>");

			System.out.println("Otp output:" + otp);

			CommonSteps.I_should_see_on_page("IdentificationHeader");
			CommonSteps.I_clickJS("IdentityVerificationCode");
			CommonSteps.I_enter_in_field(otp, "IdentityVerificationCode");
			CommonSteps.I_hit_tab_key_on_element("IdentityVerificationCode");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
//	 public static void main(String[] args) throws Exception {
//		 I_Login_to_GmailAccount_Toget_IdentificationCode("Identification Number");
//	}

	@Given("^I switch to Contact '(.*)' tab$")
	public static void I_switch_to_contact_Tab(String contactName) throws Exception {
		try {
			driver = StepBase.getDriver();
			WebElement contactTabelement = driver
					.findElement(By.xpath("//span[contains(text(),'" + contactName + "')]"));
			contactTabelement.click();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I verify if Contact is Portal User '(.*)'$")
	public static void I_verify_if_SF_PortalUser(String isPortalUser) throws Exception {
		try {
			driver = StepBase.getDriver();
			if (isPortalUser.equalsIgnoreCase("yes")) {
				WebElement isPortalUserFlag = driver.findElement(GetPageObjectRead.OR_GetElement("PortalUserFlag"));
				WrapperFunctions.highLightElement(isPortalUserFlag);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I close '(.*)' contact tab$")
	public static void I_close_MF_SF_ContactTab(String contactName) throws Exception {
		try {
			driver = StepBase.getDriver();
			WebElement closeContact = driver
					.findElement(By.xpath("//span[contains(text(),'Close " + contactName + "')]"));
			WrapperFunctions.clickByJS(closeContact);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I should not see element '(.*)' present on page$")
	public static void I_should_not_see_on_page(String element) throws Exception {

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			// WebElement wElement = WrapperFunctions.getElementByLocator(element,
			// GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			WebElement wElement = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			log.info("Element is displayed: " + wElement.isDisplayed());
			if (wElement.isDisplayed()) {
				StepBase.embedScreenshot();
				WrapperFunctions.highLightElement(driver.findElement(GetPageObjectRead.OR_GetElement(element)));
				throw new Exception("Element is found on page!");
			}

		} catch (Exception e) {

			System.out.println("Element not found"); // throw new Exception();
		}
	}

//	@And("^I Switch between Tab '(.*)'")
//	public static void I_switch_between_tab(int tab) {
//	
//		driver=StepBase.getDriver();
//	
//	ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
//	driver.switchTo().window(tabs.get(tab));
//	}

	@Given("^I get latest warranty case$")
	public static void I_get_latest_warranty_case() throws Exception {
		List<WebElement> caserow = driver.findElements(
				By.xpath("//div[@class='listViewContent slds-table--header-fixed_container']//table//tbody//tr"));
		int size = caserow.size();
		System.out.println(size);

//	     int size=caserow.size();
//	     for(WebElement caserowfind : caserow) {
//	    	 boolean displayed = caserowfind.isDisplayed();
//	    	 System.out.println(displayed);

//	     }//div[@class=\"maincontent\"]//table//tbody//tr)
//	     System.out.println(size);
		WebElement caserowfind = driver.findElement(
				By.xpath("(//div[@class='listViewContent slds-table--header-fixed_container']//table//tbody//tr)["
						+ size + "]"));
		caserowfind.click();
	}
	// (//div[@class='main-content active fullLeft']//table//tbody//tr
//		List<WebElement> allElement=(driver.findElements(By.xpath("//tbody//tr[last()]")));
//		int count=allElement.size();
//		allElement.get(count-1).click();

	@Given("^I reply email from gmail inbox$")
	public static void I_Login_to_GmailAccount_to_Reply() throws Exception {
		Utilities util = new Utilities();
		try {
		
			String mailContent = Utilities.Fetch_Last_email_MatchingSubject_FromGmail_To_Reply(
					"hasantestuser@gmail.com", "Hasan123!@#", "Sandbox: Case");
			System.out.println(mailContent);
			Utilities.sendEmail(util.getTestData("email"),
					"Closed");

		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws Exception {
		I_Login_to_GmailAccount_to_Reply();

	}

	@Given("I send email from gmail inbox with subject {string} {string} {string}")
	public static void I_Login_to_GmailAccount_to_SendEmail_With_Subject(String emailID, String subject, String content)
			throws Exception {

		try {

			String email = Utilities.getTestData(emailID);
			System.out.println(email);
			Utilities.sendEmailToRecipient(email, content, subject);

		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Given("I select the last case from the list '(.*)'")
	public static void I_Select_Last_Case(String element) throws Exception {

		List<WebElement> wElement = driver.findElements(GetPageObjectRead.OR_GetElement(element));
		int totalCases = wElement.size();
		driver.findElement(By.xpath("(//th[contains(@class,'slds-cell-edit')])[" + totalCases + "]//a")).click();

	}

	@Then("^I send '(.*)' message in omni channel$")
	public static void i_verify_if_enter_key_is_pressed(String element) {
		try {
			CommonSteps.i_clear_and_enter_the_text(element, "Textarea_Field_Omni");
			CommonSteps.I_pause_for_seconds(10);
			CommonSteps.I_clickJS("EnterMessage_Field_Omni");
			CommonSteps.I_clickJS("Textarea_Field_Omni");
			CommonSteps.I_hit_enter_key_on_element("Textarea_Field_Omni");

		} catch (Exception e) {
			CommonSteps.I_should_see_on_page("Textarea_Field_Omni");
			CommonSteps.I_pause_for_seconds(70);
			CommonSteps.i_clear_and_enter_the_text(element, "Textarea_Field_Omni");
			CommonSteps.I_pause_for_seconds(10);
			CommonSteps.I_clickJS("EnterMessage_Field_Omni");
			CommonSteps.I_clickJS("Textarea_Field_Omni");
			CommonSteps.I_hit_enter_key_on_element("Textarea_Field_Omni");
		}
	}

	@Then("^I click '(.*)' Tab$")
	public static void I_Click_Tab(String element) {
		try {
			CommonSteps.I_should_see_on_page("Edit_Button_Close");
			CommonSteps.I_click("Edit_Button_Close");
		} catch (Exception e) {
			CommonSteps.I_should_see_on_page("Edit_Button_Close");
			CommonSteps.I_pause_for_seconds(30);
			CommonSteps.I_should_see_on_page("Edit_Button_Close");
			CommonSteps.I_click("Edit_Button_Close");
		}
	}

	@Then("^I upload reciept files$")
	public static void I_upload_reciept_files() throws InterruptedException {
		driver = StepBase.getDriver();
		WebElement uploadelement = driver
				.findElement(By.xpath("//p[contains(text(),'Please upload a copy')]//..//span[text()='Upload Files']"));
		uploadelement.click();
		Thread.sleep(2000);
		// uploadFile("file:///C:/Users/Sindhu/Desktop/Customer_Receipt.pdf");
		uploadFile(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\Customer_Receipt.pdf");
		Thread.sleep(2000);
	}

	public static void setClipboardData(String string) {
		// StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	@Then("^I upload Mainarea files$")
	public static void I_upload_Mainarea_files() throws InterruptedException {
		driver = StepBase.getDriver();
		WebElement uploadelement = driver.findElement(By.xpath(
				"//span[text()='1']//..//span[contains(text(),'Main Affected Area')]//..//..//span[text()='Upload Files']"));
		uploadelement.click();
		Thread.sleep(2000);
		uploadFile(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\MainAffectedArea.gif");
		Thread.sleep(2000);
	}

	public static void setClipboardData1(String string) {
//StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile1(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	@Then("^I upload CloseUp Of Main Affected Area files$")
	public static void I_upload_CloseUp_files() throws InterruptedException {
		driver = StepBase.getDriver();
		WebElement uploadelement = driver.findElement(By.xpath(
				"//span[text()='2']//..//span[contains(text(),'Close-Up')]//..//..//span[text()='Upload Files']"));
		uploadelement.click();
		Thread.sleep(2000);
		uploadFile(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\Close-Up.jpg");
		Thread.sleep(2000);
	}

	public static void setClipboardData2(String string) {
//StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile2(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	@Then("^I upload Photo Of Law Tag Attached files$")
	public static void I_upload_Photo_files() throws InterruptedException {
		driver = StepBase.getDriver();
		WebElement uploadelement = driver.findElement(By.xpath(
				"(//span[text()='7']//..//..//label[contains(text(),'Photo Of')]//..//..//span[text()='Upload Files'])[1]"));
		uploadelement.click();
		Thread.sleep(2000);
		uploadFile(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\LawTag.png");
		Thread.sleep(2000);
	}

	public static void setClipboardData3(String string) {
//StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile3(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	@Then("^I upload Frame Support files$")
	public static void I_upload_Frame_files() throws InterruptedException {
		driver = StepBase.getDriver();
		WebElement uploadelement = driver.findElement(By.xpath(
				"//span[text()='11']//..//..//label[contains(text(),' Photo of the Support*')]//..//..//span[text()='Upload Files']"));
		uploadelement.click();
		Thread.sleep(2000);
		uploadFile(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\FrameSupport.bmp");
		Thread.sleep(2000);
	}

	public static void setClipboardData4(String string) {
//StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile4(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	@Then("^I click popup close button$")
	public static void I_click_popup_close() throws Exception {
		// try {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		WebElement popup = driver.findElement(By.xpath("//button[@id='email-modal-close']//img"));
		// WebElement popup =
		// driver.findElement(GetPageObjectRead.OR_GetElement("MattressSite_CloseButton"));
		boolean flag = popup.isDisplayed();
		System.out.println(flag);

		// WebElement help =
		// driver.findElement(GetPageObjectRead.OR_GetElement("HelpButton_Case"));
		// Actions ac=new Actions(driver);
		// CommonSteps.I_wait_for_visibility_of_element("MattressSite_CloseButton");
		// CommonSteps.I_should_see_on_page("MattressSite_CloseButton");
		// CommonSteps.I_clickJS("MattressSite_CloseButton");
		// wait.until(ExpectedConditions.elementToBeClickable(popup));
		// ac.click(popup).build().perform();
		// CommonSteps.I_click("MattressSite_CloseButton");
		// Thread.sleep(10000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", popup);
		Thread.sleep(1000);
		// CommonSteps.I_clickJS("HelpButton_Case");
		// wait.until(ExpectedConditions.visibilityOf(help));
		// CommonSteps.I_wait_for_visibility_of_element("HelpButton_Case");
		// CommonSteps.I_should_see_on_page("HelpButton_Case");
		// CommonSteps.I_mouse_over("HelpButton_Case");

	}
	// } catch (Exception e) {
	// CommonSteps.I_should_see_on_page("HelpButton_Case");
	// CommonSteps.I_mouse_over("HelpButton_Case");
	// e.printStackTrace();
	// }}

	@Given("^I get latest warranty case and store$")
	public static void I_get_latest_warranty_case_and_store() throws Exception {
		WebElement casenumber = driver.findElement(By.xpath(
				"//li[position()=last()]//ul[@class='secondaryFields truncate']//span[@class='uiOutputTextArea']"));
		// tbody/tr[position()=last()]//td//span[@class='uiOutputTextArea
		// String num = casenumber.getText();
		// System.out.println(num);
		// List<WebElement> caserow=driver.findElements(By.xpath("//tbody//tr"));
		// int size=caserow.size();
		// WebElement warantycase =
		// driver.findElement(By.xpath("//tbody//tr["+(size)+"]"));
		String value = casenumber.getText();
		log.info("*****Case ID: " + value);
		HashMapContainer.add("$$value1", value);
		StepBase.embedScreenshot();
	}

	@Given("^I click popup close icon$")
	public static void I_click_popup_close_icon() throws Exception {
		try {
			WebElement close = driver.findElement(GetPageObjectRead.OR_GetElement("MattressSite_CloseButton"));
			boolean flag = close.isDisplayed();
			if (flag) {

				CommonSteps.I_should_see_on_page("MattressSite_CloseButton");
				CommonSteps.I_clickJS("MattressSite_CloseButton");
				Thread.sleep(10000);
				CommonSteps.I_should_see_on_page("ChatwithanExpert_Button_Omni");
				CommonSteps.I_clickJS("ChatwithanExpert_Button_Omni");
			} else {
				CommonSteps.I_should_see_on_page("ChatwithanExpert_Button_Omni");
				CommonSteps.I_clickJS("ChatwithanExpert_Button_Omni");
			}
		} catch (Exception e) {
			CommonSteps.I_should_see_on_page("ChatwithanExpert_Button_Omni");
			CommonSteps.I_clickJS("ChatwithanExpert_Button_Omni");
			e.printStackTrace();
		}
	}

	@Given("^I compare customer names with contact record type as customer$")
	public static void I_compare_customer_names_with_contact_record_type() throws Exception {
		driver = StepBase.getDriver();
		List<WebElement> ContactNames = driver
				.findElements(By.xpath("//th[@class='slds-cell-edit cellContainer']//span//..//a"));
		List<WebElement> CustomerOptions = driver
				.findElements(By.xpath("//span[text()='Customer' and @class='slds-truncate uiOutputText']"));
		// comparing the customer names with contact record type and clicking it.
		for (WebElement name : ContactNames) {
			CommonSteps.I_get_text_from("name");
			System.out.println(name);
			for (WebElement customer : CustomerOptions) {
				System.out.println(customer);
				if (name.equals(customer)) {
					CommonSteps.I_get_text_from("customer");
					// CommonSteps.I_should_see_on_page("UnderReviewStutus_NewCaseNumber");
					// CommonSteps.I_get_text_from("name");
					System.out.println(name);
					CommonSteps.I_clickJS("name");
					break;
				}
			}
		}
	}

	@Given("^I Validate Red Color Field$")
	public static void I_validate_Field_Red_color() throws Exception {
		driver = StepBase.getDriver();
		String ExpectedgreenColor = "#c23934";
		WebElement Field = driver.findElement(By.xpath("//li[contains(@class,'tabItem slds-context-bar__item')]"));
		String bgcolor1 = Field.getCssValue("background-color");
		System.out.println(bgcolor1);// rgba format rgba(194, 57, 52, 1)
		String hexcolor1 = Color.fromString(bgcolor1).asHex();
		System.out.println(hexcolor1);// hex value #c23934
		Assert.assertEquals(ExpectedgreenColor, hexcolor1);
	}

	@Given("^I Validate Green Color Field$")
	public static void I_validate_Field_Green_color() throws Exception {
		driver = StepBase.getDriver();
		String ExpectedredColor = "#04844b";
		WebElement Field = driver.findElement(By.xpath("//li[contains(@class,'tabItem slds-context-bar__item')]"));
		String bgcolor2 = Field.getCssValue("background-color");
		System.out.println(bgcolor2);// rgba format rgba(4, 132, 75)
		String hexcolor2 = Color.fromString(bgcolor2).asHex();
		System.out.println(hexcolor2);// hex value #04844b
		Assert.assertEquals(ExpectedredColor, hexcolor2);
	}

	@Given("^I Validate All Dropdown Values$")
	public static void I_validate_All_Dropdown_Values() throws Exception {
		driver = StepBase.getDriver();
		org.openqa.selenium.support.ui.Select dropdown = new org.openqa.selenium.support.ui.Select(
				driver.findElement(By.xpath("//select[contains(@class,'slds-select select')]")));
		ArrayList<String> actualvalues = new ArrayList<String>(); // adding values to array list
		List<WebElement> options = dropdown.getOptions();// list of web elements
		for (WebElement element : options) {
			String values = element.getText();
			actualvalues.add(values);
			System.out.println(values);// print all text from dropdown
		}
		ArrayList<String> expectedvalues = new ArrayList<String>();
		expectedvalues.add("Please select a disposition");
		expectedvalues.add("Abandoned - Pre Chat");
		expectedvalues.add("120 Day Price Match Guarantee Request");
		expectedvalues.add("Cancel My Entire Order");
		expectedvalues.add("Cancel Part Of My Order");
		expectedvalues.add("Certificate of Insurance (COI)");
		expectedvalues.add("Change Product(s) on My Existing Order");
		expectedvalues.add("Incorrect/Missing/Damaged Item(s) Received");
		expectedvalues.add("None of the Provided Options Meet my Needs");
		expectedvalues.add("Pre-Warranty Claim Assistance");
		expectedvalues.add("Request an Invoice");
		expectedvalues.add("Request Comfort Exchange");
		expectedvalues.add("Request to Return Product Already Recevied");
		expectedvalues.add("Reschedule My Delivery");
		expectedvalues.add("Status of My Delivery");
		expectedvalues.add("Status of Open Warranty Claim");
		expectedvalues.add("Status of Pending Refund");
		expectedvalues.add("Update My Shipping / Billing Information");

		for (int i = 0; i < actualvalues.size(); i++) {
			Assert.assertEquals(expectedvalues.get(i), actualvalues.get(i));
		}
	}

	@Then("^I should see Window Authontication popup on page$")
	public static void I_should__see_Window_Authontication_popup_on_page() throws Exception {

		Runtime.getRuntime().exec("E:\\AutoIT\\HandleAuthonticationWindow.exe");
//driver.get("https://dev.mattressfirm.com/");
	}

	@Then("^I should click PDF Download button on page$")
	public static void I_should__click_PDF_Download_button_on_page() throws Exception {

		Runtime.getRuntime().exec("D:\\AutoIT\\ClickPDFDownloadButton.exe");
//driver.get("https://dev.mattressfirm.com/");
	}

	@Then("^I should click pdf Download button on page using sikulixapi$")
	public static void I_should__click_PDF_Download_button_on_page_using_sikulixapi() throws Exception {
//Screen screen=new Screen();
//Pattern p=new Pattern("D:\\defef.png");
	}

	@Given("I close all the opened tabs '(.*)'")
	public void closeAllOpenTabs(String element) throws InterruptedException {
		try {
			WrapperFunctions.waitForElementVisibility(GetPageObjectRead.OR_GetElement(element),10);
			List<WebElement> wElement = driver.findElements(GetPageObjectRead.OR_GetElement(element));

			for (WebElement closeIcon : wElement) {
				Thread.sleep(2000);
				closeIcon.click();
			}

		} catch (NoSuchElementException e) {
			log.info("All tabs already closed");
		} catch (StaleElementReferenceException e) {
			log.info("All tabs already closed");
		}

	}

	@Then("^I should see profile name '(.*)' present on page at '(.*)'$")
	public static void I_should_see_profilename_present_on_page_At(String pName, String element) {
		String profileName = Utilities.getTestData(pName);

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			WrapperFunctions.highLightElement(wElement);
			Assert.assertEquals(profileName, actualText);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I should see CaseOwner name '(.*)' present on page$")
	public static void I_should_see_caseownername_present_on_page_At(String displayName) {
		displayName = Utilities.getTestData(displayName);

		try {
			WebElement wElement = driver.findElement(
					By.xpath("//span[text()='Case Owner']//..//..//div//a//span[text()='" + displayName + "']"));
			WrapperFunctions.highLightElement(wElement);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	@Then("^I should see CaseOwner label '(.*)' present on page$")
	public static void I_should_see_caseownerlabel_present_on_page_At(String displayName) {
		displayName = Utilities.getTestData(displayName);

		try {
			WebElement wElement = driver.findElement(
					By.xpath("//span[@class='displayLabel' and text()='" + displayName + "']"));
			WrapperFunctions.highLightElement(wElement);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("I check the sort status to be {string}")
	public void checkSortStatus(String sortStatus) throws Exception {
		WebElement wElement = WrapperFunctions.getElementByLocator("LastModifiedColumnSortStatus",
				GetPageObjectRead.OR_GetElement("LastModifiedColumnSortStatus"), 20);
		String actualText = wElement.getText();
		if (sortStatus.equalsIgnoreCase(actualText)) {
			log.info("Already in descending order");
		} else {
			WebElement wEle = WrapperFunctions.getElementByLocator("LastModifiedColumn",
					GetPageObjectRead.OR_GetElement("LastModifiedColumn"), 20);
			Thread.sleep(5000);
			wEle.click();
		}

	}
	@Then("I Check all 10 cases have the values {string} {string} {string} {string}")
	public void checkCases(String caseOwner,String caseOrigin,String caseRecordType,String resolutionDetails) throws Exception {
		List<WebElement> wElement = driver.findElements(By.xpath("(//th[contains(@class,'slds-cell-edit')])//a"));
		int iteration =0;
		if (wElement.size() > 10) {
			iteration = 10;
		} else {
			iteration = wElement.size();
		}
		for(int i=0;i<iteration;i++) {
			Thread.sleep(5000);
			wElement.get(i).click();		
			WrapperFunctions.waitForElementVisibility(By.xpath("//div[text()='Case']/..//lightning-formatted-text"));
			WrapperFunctions.highLightElement(driver.findElement(By.xpath("//div[text()='Case']/..//lightning-formatted-text")));

			WrapperFunctions.highLightElement(driver.findElement(By.xpath("//span[text()='Total COGS']/../..//lightning-formatted-text")));
			
			if(caseOwner.equalsIgnoreCase("nationwide")) {
				String value=driver.findElement(By.xpath("//span[text()='Total COGS']/../..//lightning-formatted-text")).getText().replace("$","").replace(" ", "").replace(",", "");
				System.out.println(value);			
				//int TotalCOGS=Integer.parseInt(value[0]);  
				int TotalCOGS = (int) Double.parseDouble(value);
				System.out.println(TotalCOGS);
			Assert.assertTrue(TotalCOGS>1000);
			System.out.println(i);
			}
			else if(caseOwner.equalsIgnoreCase("nationwide cutoff")) {
				String value=driver.findElement(By.xpath("//span[text()='Order Total Amount']/../..//lightning-formatted-number")).getText().replace(" ", "").replace(",", "");;
				int TotalCOGS=Integer.parseInt(value);  
				Assert.assertTrue(TotalCOGS<=1000);
			}
			WrapperFunctions.highLightElement(driver.findElement(By.xpath("//span[text()='Case Owner']/../..//*[text()='"+caseOwner+"']")));
			Assert.assertTrue(driver.findElement(By.xpath("//span[text()='Case Owner']/../..//*[text()='"+caseOwner+"']")).isDisplayed());
			WrapperFunctions.highLightElement(driver.findElement(By.xpath("//span[text()='Case Origin']/../..//*[text()='"+caseOrigin+"']")));
			Assert.assertTrue(driver.findElement(By.xpath("//span[text()='Case Origin']/../..//*[text()='"+caseOrigin+"']")).isDisplayed());
			WrapperFunctions.highLightElement(driver.findElement(By.xpath("//span[text()='Case Record Type']/../..//*[text()='"+caseRecordType+"']")));
			Assert.assertTrue(driver.findElement(By.xpath("//span[text()='Case Record Type']/../..//*[text()='"+caseRecordType+"']")).isDisplayed());
			WrapperFunctions.highLightElement(driver.findElement(By.xpath("//span[text()='Resolution Details']/../..//lightning-formatted-text")));

			Assert.assertEquals(driver.findElement(By.xpath("//span[text()='Resolution Details']/../..//lightning-formatted-text")).getText(),resolutionDetails);
			closeAllOpenTabs("iconClose");
		}
		

	}
	@Then("I should see text containing {string} in all the knowledge articles")
	public void verifyKnowledeArticlesContent(String keywordText) throws Exception {
		List<WebElement> articleLinks=driver.findElements(By.xpath("//a[contains(@class,'itemTitle textUnderline')]"));
		for(int article=1;article<articleLinks.size();article++) {
			boolean isTextPresentInLink=driver.findElement(By.xpath("(//a[contains(@class,'itemTitle textUnderline')])["+article+"]")).getText().contains(keywordText);
			WrapperFunctions.mouseHover(By.xpath("(//a[contains(@class,'itemTitle textUnderline')])["+article+"]"));
			Thread.sleep(3000);
			WebElement txtArticleDetails = WrapperFunctions.getElementByLocator("txtArticleDetails",
					GetPageObjectRead.OR_GetElement("txtArticleDetails"), elementWaitTime);
			WebElement btnArticleClose = WrapperFunctions.getElementByLocator("btnArticleClose",
					GetPageObjectRead.OR_GetElement("btnArticleClose"), elementWaitTime);
			//Assert.assertTrue(txtArticleDetails.getText().toLowerCase().contains(keywordText) || isTextPresentInLink);
			btnArticleClose.click();
			Thread.sleep(2000);
		}
	}
	@Given("^I verify welcome mail from gmail inbox '(.*)'$")
	public static void I(String text) throws Exception {
		try {
			String mailContent = Utilities.Fetch_Last_email_MatchingSubject_FromGmail("hasantestuser@gmail.com",
					"Hasan123!@#", "Sandbox: Welcome to the Mattress Firm Customer Portal");
			System.out.println("mailContent"+mailContent);
			Assert.assertTrue(mailContent.contains(text));
}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@When("I select value {string} from the dropdown")
	public void selectValueFromDropdown(String value) {
		
		try {
			WrapperFunctions.waitForElementPresence(By.xpath("//a[@title='"+value+"']"));
			driver.findElement(By.xpath("//a[@title='"+value+"']")).click();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
		
	}
	@Given("I reply email from gmail inbox {string}")
	public static void I_Login_to_GmailAccount_to_Reply(String email) throws Exception {
		Utilities util = new Utilities();
		try {
		
			String mailContent = Utilities.Fetch_Last_email_MatchingSubject_FromGmail_To_Reply(
					"hasantestuser@gmail.com", "Hasan123!@#", "Sandbox: Case");
			System.out.println(mailContent);
			Utilities.sendEmail(util.getTestData(email),
					"Closed");

		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

}
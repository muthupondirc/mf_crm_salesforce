package com.TEAF.stepDefinitions;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.runtime.CucumberException;

public class Test implements Runnable {
	private  WebDriver driver;
	
	//@Test(threadPoolSize = 4, invocationCount = 4, timeOut = 1000)
	
	@org.junit.Test
	public void run() {
		try {

			System.setProperty("webdriver.chrome.driver", "C://Users//MuthupondiS//Documents//MFRMSalesforce//mattressfirm_salesforce//src//test//java//com//Resources//chromedriver.exe");
			driver = new ChromeDriver();
			Wait<WebDriver> wait = new WebDriverWait(driver, 90);

			

			driver.get("https://mattress--qa.my.salesforce.com/");
			driver.manage().window().maximize();
			String envUrl = "MF_SF_QA";
			System.setProperty("test_env_URL", envUrl);
			System.setProperty("test.implicitlyWait", "10");

			driver.findElement(By.id("username")).sendKeys("ccoagent3@royalcyber.com.qa");
			driver.findElement(By.id("password")).sendKeys("Test@123");
			driver.findElement(By.id("Login")).click();
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			
			Thread.sleep(20000);

			for (int i = 0; i < 5; i++) {
				Thread.sleep(3000);
				JavascriptExecutor executor = (JavascriptExecutor)driver;

				WebElement dropdown = driver.findElement(
						By.xpath("//button[@title='Show Navigation Menu']//span[text()='Show Navigation Menu']"));
				//wait.until(ExpectedConditions.elementToBeClickable(dropdown));
				dropdown.isEnabled();
				dropdown.isDisplayed();
				executor.executeScript("arguments[0].click();", dropdown);
				WebElement menuCases = driver
						.findElement(By.xpath("//span[@class='slds-media__body']//span[text()='Cases']"));
				wait.until(ExpectedConditions.elementToBeClickable(menuCases));
				executor.executeScript("arguments[0].click();", menuCases);
				Thread.sleep(3000);
				System.out.println("Header Status"+driver.findElement(By.xpath("//lst-breadcrumbs//span[text()='Cases']")).isDisplayed());
				dropdown.isEnabled();
				dropdown.isDisplayed();
				executor.executeScript("arguments[0].click();", dropdown);
				Thread.sleep(3000);
				WebElement menuContacts = driver
						.findElement(By.xpath("//span[@class='slds-media__body']//span[text()='Contacts']"));
				wait.until(ExpectedConditions.elementToBeClickable(menuContacts));
				menuContacts.isEnabled();
				menuContacts.isDisplayed();
				executor.executeScript("arguments[0].click();", menuContacts);
				System.out.println("Header Status :"+driver.findElement(By.xpath("//lst-breadcrumbs//span[text()='Contacts']")).isDisplayed());


			}
			System.out.println(">>>>>Execution completed<<<<");
			

		} catch (Exception e) {
			throw new CucumberException(e.getMessage(), e);
		}

	}

}

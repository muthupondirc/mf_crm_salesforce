package com.TEAF.stepDefinitions;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.LocatorGenerator;
import com.TEAF.framework.StepBase;
import cucumber.runtime.CucumberException;

import static org.junit.Assert.assertEquals;
import java.awt.Desktop.Action;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
//import org.apache.commons.mail.DefaultAuthenticator;
//import org.apache.commons.mail.Email;
//import org.apache.commons.mail.EmailException;
//import org.apache.commons.mail.SimpleEmail;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @ScriptName : Utilities
 * @Description : This class contains Commonly used Keyword for Mobile/Web
 *              application automation using Cucumber framework
 * @Author : Swathin Ratheendren
 * @Creation Date : September 2016 @Modified Date:
 */
public class CommonSteps {

	static Logger log = Logger.getLogger(CommonSteps.class.getName());

//	static StepBase sb = new StepBase();
	// static StepBase_D2 sb = new StepBase_D2();
	// public static WebDriver driver = StepBase.getDriver();
	private static WebDriver driver;
	private static BufferedImage bufferedImage;
	private static byte[] imageInByte = null;
	private static int elementWaitTime = Integer.parseInt(System.getProperty("test.implicitlyWait"));
	public static String appName = null;

	@Given("^My WebApp '(.*)' is open$")
	public static void my_webapp_is_open(String url) throws Exception {
		try {
			appName = url;
			driver = StepBase.getDriver();
			log.info("Driver value: " + driver);
			String URL = GetPageObjectRead.OR_GetURL(url);
			System.out.println("CommonSteps: URL: " + URL);
			driver.get(URL);
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I get all element locators under '(.*)' : '(.*)' on section : '(.*)' to file name: '(.*)'$")
	public static void i_capture_locators_in_the_page(String key, String value, String section, String fileName)
			throws Throwable {
		try {
			log.info(driver.getCurrentUrl());
			LocatorGenerator.createExcel(appName, section, fileName, driver.getCurrentUrl());
			LocatorGenerator.getTagChildElementsbyAttributeKeyValue(driver.getPageSource(), key, value, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^My NativeApp '(.*)' is open$")
	public static void my_nativeapp_is_open(String app) throws Exception {
		try {
			driver = StepBase.getDriver();
			if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("csv")) {
				GetPageObjectRead.ReadCSV(app);

			} else if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("class")) {
				GetPageObjectRead.ReadPOClass(app);
			} else if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("xlsx")) {
				GetPageObjectRead.ReadExcel(app);
			}
			Thread.sleep(15000);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I navigate to '(.*)' page$")
	public static void I_NavigateTo(String url) {
		try {
			driver.navigate().to(HashMapContainer.getPO(url));
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I refresh the WebPage$")
	public static void I_Refresh_WebPage() {
		try {
			driver.navigate().refresh();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I focus and click '(.*)'$")
	public static void I_focus_click(String element) {
		try {
			Actions ac = new Actions(driver);
			// WebElement findElement =
			// driver.findElement(GetPageObjectRead.OR_GetElement(element));
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			ac.moveToElement(wElement).click().build().perform();

		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I wait for visibility of element '(.*)'$")
	public static void I_wait_for_visibility_of_element(String element) {

		WrapperFunctions.waitForElementVisibility(GetPageObjectRead.OR_GetElement(element));
	}

	@Then("^I enter '(.*)' in field '(.*)'$")
	public static void I_enter_in_field(String value, String element) {
		try {
			if (value.length() > 3) {
				if (value.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					value = HashMapContainer.get(value);
				} else if (value.substring(0, 3).equals("$XS")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomString(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XE")) {
					String randomVal = Utilities.GetRandomEmail(Character.getNumericValue(value.charAt(3)));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XN")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomNum(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XA")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomAlphaNumeric(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(value);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I generate random email id and enter in field '(.*)'$")
	public static void i_generate_random_emailid_in_field(String element) {

		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(2000);
		String email = "hasantestuser+" + randomInt + "@gmail.com";
		System.setProperty("randomemail", email);
		System.out.println("Email"+email);
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.clear();
			wElement.sendKeys(email);
		
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}

	}

	@Then("^I enter '(.*)' in the feild '(.*)' using actions$")
	public static void i_enter_in_the_feild_using_actions(String value, String element) {

		try {
			WebElement inputElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (inputElement.isDisplayed() && inputElement.isEnabled()) {
				Actions ac = new Actions(driver);
				ac.sendKeys(inputElement, value).build().perform();

			} else {
				throw new RuntimeException();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e);
			throw new RuntimeException();

		}
	}

	@Then("^I clear the text and enter '(.*)' in field '(.*)'$")
	public static void i_clear_and_enter_the_text(String value, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.clear();
			wElement.sendKeys(value);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I clear field '(.*)'$")
	public static void I_clear_Field(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.clear();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

//	  @Given("^I scroll till element '(.*)'$")
//	    public static void i_should_scroll_till_the_element(String locator) throws Exception {
//	        WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
//	        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
//	        Thread.sleep(500);
//	    }

	@Then("^I hit enter-key on element '(.*)'$")
	public static void I_hit_enter_key_on_element(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(Keys.ENTER);
			// wElement.sendKeys(Keys.RETURN);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I hit tab-key on element '(.*)'$")
	public static void I_hit_tab_key_on_element(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(Keys.TAB);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I hit down Arrow key on element '(.*)'$")
	public static void I_hit_Downkey_on_element(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(Keys.ARROW_DOWN);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I mouse over '(.*)'$")
	public static void I_mouse_over(String element) {
		try {
			Actions action = new Actions(driver);
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			action.moveToElement(wElement).build().perform();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I verify tool tip message '(.*)' for element '(.*)'$")
	public static void I_Verify_ToolTip_Message(String expectedMessage, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			/*
			 * Actions action = new Actions(driver); WebElement we =
			 * driver.findElement(GetPageObject.OR_GetElement(element));
			 * action.moveToElement(we).build().perform();
			 */
			String ToolTipMessage = wElement.getAttribute("title");
			Assert.assertEquals(expectedMessage, ToolTipMessage);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I verify value '(.*)' in field '(.*)'$")
	public static void I_Verify_value_inField(String expectedValue, String element) {
		try {
			if (expectedValue.length() > 1) {
				if (expectedValue.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedValue = HashMapContainer.get(expectedValue);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);

			String ActualValue = wElement.getAttribute("value");
			Assert.assertEquals(expectedValue, ActualValue,
					"Actual value does not match the expected value in specified field!");
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I verify element '(.*)' is disabled$")
	public static void I_Verify_Element_isDisabled(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (wElement.isEnabled()) {
				throw new Exception("Element is enabled!");
			}
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I enter '(.*)' into RichText Editor '(.*)'$")
	public static void I_enter_into_RichTextEditor(String value, String element) {
		try {
			driver.switchTo().frame("tinymce");
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.clear();
			wElement.sendKeys(value);
			driver.switchTo().defaultContent();
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I click '(.*)'$")
	public static void I_click(String element) {
		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.click();
			StepBase.embedScreenshot();
			// Utilities.takeScreenshot(driver);
			// Thread.sleep(5000);
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I double-click '(.*)'$")
	public static void I_doubleClick(String element) {
		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			Actions act = new Actions(driver);
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			act.doubleClick(wElement).build().perform();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I take screenshot$")
	public static void take_Screenshot() {
		try {
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I click link '(.*)'$")
	public static void I_click_link(String linkText) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(linkText, By.linkText(linkText),
					elementWaitTime);
			wElement.click();
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^Blur '(.*)'$")
	public static void Blur_JS(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].focus(); arguments[0].blur(); return true", wElement);
			// WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I drag field '(.*)' and drop '(.*)'$")
	public static void I_drag_Field(String element, String element2) {
		try {
			WebElement source = WrapperFunctions.getElementByLocator(element, GetPageObjectRead.OR_GetElement(element),
					elementWaitTime);
			WebElement target = WrapperFunctions.getElementByLocator(element2,
					GetPageObjectRead.OR_GetElement(element2), elementWaitTime);
			Actions ac = new Actions(driver);
			ac.dragAndDrop(source, target).build().perform();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I click alert accept$")
	public static void I_click_alert_accept() {
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I click by JS '(.*)'$")
	public static void I_clickJS(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.isEnabled();
			wElement.isDisplayed();
			WrapperFunctions.clickByJS(wElement);
			StepBase.embedScreenshot();
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I should see element '(.*)' present on page$")
	public static void I_should_see_on_page(String element) {

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (wElement.isDisplayed()) {
				WrapperFunctions.highLightElement(wElement);
				StepBase.embedScreenshot();
			} else {
				throw new Exception("Element is not found! :" + element);
			}

		} catch (Exception e) {
			log.error(e);
			log.error(e.getMessage() + " " + element);
			throw new CucumberException(e.getMessage());
		}
	}

	@Then("^I should see element '(.*)' is diabled on page$")
	public static void I_should_see_disbaled_on_page(String element) {

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (!wElement.isEnabled()) {
				WrapperFunctions.highLightElement(wElement);
			} else {
				throw new Exception("Element is not disabled! :" + element);
			}

		} catch (Exception e) {
			log.error(e);
			log.error(e.getMessage() + " " + element);
			throw new CucumberException(e.getMessage());
		}
	}

	@Then("^I should see element '(.*)' present on page_$")
	public static void I_should_see_on_page_AndScreenshot(String element) {

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (wElement.isDisplayed()) {
				WrapperFunctions.highLightElement(driver.findElement(GetPageObjectRead.OR_GetElement(element)));
				StepBase.embedScreenshot();
				// Utilities.takeScreenshot(driver);
			} else {
				throw new Exception("Element is not found!");
			}

		} catch (Exception e) {
			log.error(e);
			log.info("Element is not found");
			throw new ElementNotFoundException(element, "", "");
		}
	}

/*	@Then("^I should not see element '(.*)' present on page$")
	public static void I_should_not_see_on_page(String element) throws Exception{

		try {
			if(element.length()>1){
				if(element.substring(0,2).equals("$$"))
				{
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element, GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			log.info("Element is displayed: "+wElement.isDisplayed());
			if(wElement.isEnabled()){
				StepBase.embedScreenshot(); 
				WrapperFunctions.highLightElement(driver.findElement(GetPageObjectRead.OR_GetElement(element)));
				throw new Exception("Element is found on page!");
			}

		} catch (Exception e) {
			log.error(e);
			throw new Exception("Element is found on Page!");
		}
	}*/

	@Then("^I should see text '(.*)' present on page$")
	public static void I_should_see_text_present_on_page(String expectedText) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			if (driver.getPageSource().contains(expectedText)) {
				log.info("Text " + expectedText + " found on page!");
				StepBase.embedScreenshot();
			} else {
				throw new ElementNotFoundException(expectedText, " ", " ");
			}
		} catch (Exception e) {
			log.error(e);
			throw new ElementNotFoundException(expectedText, " ", " ");
		}
	}

	@Then("^I verify checkbox '(.*)' is '(.*)'$")
	public static void I_verify_checkBox_is_Checked(String element, String status) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (status.equalsIgnoreCase("checked")) {
				if (wElement.isSelected()) {
					throw new Exception("Specified Checkbox is checked!");
				}
			} else if (status.equalsIgnoreCase("unchecked")) {
				if (!wElement.isSelected()) {
					throw new Exception("Specified Checkbox is not checked!");
				}
			} else if (status.equalsIgnoreCase("unchecked")) {
				if (wElement.isSelected()) {
					throw new Exception("Specified Checkbox is checked!");
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I switch to iFrame '(.*)'$")
	public static void I_switchTo_iFrame(String FrameID) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(FrameID,
					GetPageObjectRead.OR_GetElement(FrameID), elementWaitTime);
			driver.switchTo().frame(wElement);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}

	}

	@Then("^I switch to default content$")
	public static void I_switchTo_DefaultContent() {
		try {
			driver.switchTo().defaultContent();
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}

	}

	@Then("^I switch back to Main Window$")
	public static void I_switchTo_MainWindow() {
		try {
			driver.switchTo().window(driver.getWindowHandle());
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}

	}

	@Then("^I should see text '(.*)' present on page at '(.*)'$")
	public static void I_should_see_text_present_on_page_At(String expectedText, String element) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			WrapperFunctions.highLightElement(wElement);
			Assert.assertEquals(expectedText, actualText);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I should see text '(.*)' contained on page at '(.*)'$")
	public static void I_should_see_text_contained_on_page_At(String expectedText, String element) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer! " + HashMapContainer.get(expectedText));

					expectedText = HashMapContainer.get(expectedText);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			WrapperFunctions.highLightElement(wElement);
			log.info("Actual " + actualText + " Expected: " + expectedText);

			try {
				Assert.assertTrue(expectedText.toLowerCase().contains((actualText.toLowerCase())));
			} catch (AssertionError e) {
				Assert.assertEquals(expectedText, actualText);
			}
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("I should see element - '(.*)' contains text '(.*)'")
	public static void i_should_see_contains_text_present_on_page_at(String element, String text) {
		try {
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			WrapperFunctions.waitForElementVisibility(GetPageObjectRead.OR_GetElement(element));
			String ac = driver.findElement(GetPageObjectRead.OR_GetElement(element)).getText();
			if (ac.contains(text)) {
				Assert.assertTrue(true);
				WrapperFunctions.highLightElement(driver.findElement(GetPageObjectRead.OR_GetElement(element)));
			} else {
				Assert.fail("Expected = " + text + " but Actual = " + ac);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e);
			throw new CucumberException(e.getMessage(), e);

		}
	}

	@Then("^I should see text matching regx '(.*)' present on page at '(.*)'$")
	public static void I_should_see_text_matching_regularExpression(String regx, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			// Assert.assertTrue(actualText.matches(regx),"Actual Text Found:
			// "+actualText+"|");
			Assert.assertTrue(actualText.matches(regx));
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}

	}

	@Then("^I wait '(.*)' seconds for presence of element '(.*)'$")
	public static void I_wait_for_presence_of_element(int seconds, String element) {
		try {
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element), seconds);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I wait for '(.*)' seconds$")
	public static void I_pause_for_seconds(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I switch to window with title '(.*)'$")
	public static void I_switch_to_window_with_title(String title) {
		try {
			if (title.length() > 1) {
				if (title.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					title = HashMapContainer.get(title);
				}
			}
			WrapperFunctions.switchToWindowUsingTitle(title);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}

	}

	@Then("^I select option '(.*)' in dropdown '(.*)' by '(.*)'$")
	public static void I_select_option_in_dd_by(String option, String element, String optionType) {
		Assert.assertTrue(
				WrapperFunctions.selectDropDownOption(GetPageObjectRead.OR_GetElement(element), option, optionType));
	}

	@Then("^I scroll to '(.*)' - '(.*)'$")
	public static void I_scroll_to_element(String scrolltype, String element) {
		try {
			WrapperFunctions.scroll(scrolltype, element);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I scroll through page$")
	public static void I_scroll_thru_page() {
		try {
			for (int i = 0; i <= 16; i++) {
				WrapperFunctions.scroll("coordinates", "0,200");
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I scroll through axis$")
	public static void I_scroll_axis() {
		try {
			for (int i = 0; i <= 16; i++) {
				WrapperFunctions.scroll("coordinates", "0,50");
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I scroll to top of the page$")
	public static void i_scroll_through_top_page() {
		try {
			WrapperFunctions.scroll("top", "");
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I store text '(.*)' as '(.*)'$")
	public static void I_get_text_from(String TextValue, String ValueName) {
		try {
			HashMapContainer.add("$$" + ValueName, TextValue);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I get text from '(.*)' and store$")
	public static void I_get_text_from(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String value = wElement.getText();
			log.info("*****Case ID: " + value);
			HashMapContainer.add("$$" + element, value);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I enter from stored variable '(.*)' into feild '(.*)'$")
	public static void I_enter_from_StoredValue(String StoredValue, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(HashMapContainer.get(StoredValue));
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I compare '(.*)' with stored value '(.*)'$")
	public static void I_compare_with_StoredValue(String element, String storedValue) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualValue = wElement.getText();
			storedValue = HashMapContainer.get(storedValue);
			Assert.assertEquals(storedValue, actualValue,
					"I Compare " + actualValue + " with expected value " + storedValue);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I verify screenshot to Baseline of page '(.*)'$")
	public static void I_verify_Screenshot_toBaselined(String ImageBaseline) throws Exception {
		try {
			File f1 = new File(Utilities.takeScreenshot(driver));
			log.info("ActualSS Path: " + f1);
			File f2 = new File(ImageBaseline);
			Float PercentageVariation = Utilities.compareImage(f1, f2);
			log.info("% of variation in image comparison with Baseline image: " + PercentageVariation + "%");
			if (PercentageVariation != 0.0) {
				bufferedImage = Utilities.getDifferenceImage(f1, f2);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(bufferedImage, "jpg", baos);
				baos.flush();
				imageInByte = baos.toByteArray();
				baos.close();
				throw new Exception("Compared images do not match!" + imageInByte);
			}
		} catch (Exception e) {
			log.error(e);
			// StepBase.embedProvidedScreenshot(imageInByte);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I close browser$")
	public static void I_Close_Browser() {
		try {
			driver.close();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@When("^I navigate to '(.*)' application$")
	public static void i_navigate_to_application(String url) {
		try {
			if (url.substring(0, 2).equals("$$")) {
				log.info("Fetching from HMcontainer!");
				url = HashMapContainer.get(url);
			}
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.location.replace('" + url + "')");
			driver.navigate().to(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I clear the text and enter '(.*)' in field '(.*)' by JS$")
	public static void i_clear_and_enter_the_text_js(String value, String element) {

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('value', '" + value + "')", wElement);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Given("^I scroll till element '(.*)'$")
	public static void i_should_scroll_till_the_element(String locator) throws Exception {
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(500);
	}

	@Then("^I validate the table$")
	public static void i_validate_the_table() {
		driver = StepBase.getDriver();
		WebElement Table = driver.findElement(By.xpath("//table[@class='data-grid-table data-grid-full-table']"));
		List<WebElement> tableRows = Table
				.findElements(By.xpath("//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr"));
		System.out.println(tableRows.size() + "--------------");
		for (int i = 2; i <= 4; i += 2) {
			WebElement row = tableRows.get(i);
			CommonSteps.I_scroll_axis();

			String createdby = driver
					.findElement(By.xpath("//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr[" + i
							+ "]//th//div[2]//label//a"))
					.getText();
			System.out.println("Created By " + createdby);
			String firstname = driver.findElement(By.xpath(
					"//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr[" + i + "]//td[1]//div//a"))
					.getText();
			System.out.println(firstname);
			String lastname = driver.findElement(By.xpath(
					"//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr[" + i + "]/td[2]//div//a"))
					.getText();
			String leaddatetime = driver.findElement(By.xpath(
					"//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr[" + i + "]//td[3]//div//div"))
					.getText();
			String email = driver.findElement(By.xpath(
					"//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr[" + i + "]/td[4]//div//a"))
					.getText();
			String phone = driver
					.findElement(By.xpath("//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr[" + i
							+ "]/td[5]//div//div//div"))
					.getText();
			String storephonenumber = driver.findElement(By.xpath(
					"//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr[" + i + "]/td[6]//div//span"))
					.getText();
			String storeemailaddress = driver.findElement(By.xpath(
					"//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr[" + i + "]/td[7]//div//span"))
					.getText();
			String storenumber = driver.findElement(By.xpath(
					"//div//table[@class='data-grid-table data-grid-full-table']/tbody/tr[" + i + "]/td[8]//div"))
					.getText();
			System.out.println(createdby + " " + firstname + " " + lastname + " " + leaddatetime + " " + email + " "
					+ phone + " " + storephonenumber + " " + storeemailaddress + " " + storenumber + " ");

			// Assert.assertEquals("Hasan", colmn1);
		}

	}

	@Then("^I should see empty value in the Input feild '(.*)'$")
	public static void i_should_see_emptyvalue_in_input_feild(String locator) throws Exception {

		try {
			WebElement emptyfield = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
			String blank = emptyfield.getAttribute("value");
			Assert.assertTrue(blank.length() == 0 || blank == null || blank.equals("null"));
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

//	  @Then("^I send an email through gamil account$") public static void
//	  i_send_an_email() throws EmailException{
//	  
//	  System.out.println("======Email Started======"); Email email = new
//	  SimpleEmail(); email.setHostName("smtp.gmail.com"); email.setSmtpPort(465);
//	  email.setAuthenticator(new DefaultAuthenticator("hasantestuser@gmail.com",
//	  "Hasan123!@#")); email.setSSLOnConnect(true);
//	  email.setFrom("hasantestuser@gmail.com"); email.setSubject("TestMail");
//	  email.setMsg("test mail"); email.addTo(
//	  "ra1481520347861@nwupdji7r1e8fwkfryom3d2m2pasbv2qk3ylc6nklyvmcdiv9.6s-4ajheaa.cs165.case.sandbox.salesforce.com"
//	  ); email.send(); // email.addReplyTo(
//	 "ra1481520347861@nwupdji7r1e8fwkfryom3d2m2pasbv2qk3ylc6nklyvmcdiv9.6s-4ajheaa.cs165.case.sandbox.salesforce.com"); // email.getSubject().contains("Sandbox: Case#");
//	  System.out.println("======Email Sent======"); 
//	  }

	@Then("^I validate PDF file - filename: '(.*)' & path: '(.*)' content - Field Name '(.*)' & Field Value: '(.*)'$")
	public static void I_validate_PDF_file_content(String filePath, String fileName, String fieldName, String Value) {
		Utilities util = new Utilities();
		util.CheckPDFContent(filePath, fileName, fieldName, Value);
	}

	@When("^I download PDP file for MF$")
	public static void i_download_pdf_file_mf() {
		WebElement viewer = driver.findElement(By.xpath("//*[@id='toolbar']"));
		WebElement ele = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot",
				viewer);
		WebElement download = ele.findElement(By.id("download"));
		Actions action = new Actions(driver);
		action.moveToElement(download).build().perform();
		download.click();
	}


	public static void setClipboardData(String string) {
		// StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	@When("^I Close Window Tab$")
	public static void i_Close_Window_Tab() {
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		driver.switchTo().window(tabs2.get(0));
	}

	@When("^I navigate to PDF url$")
	public static void I_navigate_to_PDF_url() {
		String url = "https://mattress--uat--c.visualforce.com/apex/WarrantyCasePDF?ltn_app_id=06m0f000000ffKzAAI&caseID=500P000000925Q6IAI&sfdcIFrameHost=web&isdtp=p1&clc=1&sfdcIFrameOrigin=https%3A%2F%2Fmattress--uat.lightning.force.com&nonce=880606529f11141121e9981c200f1e5d29fd4956858a4e7c261a1d27c8e0d4fc&tour=";
		driver.navigate().to(url);
	}

	

	public static void setClipboardData1(String string) {
//StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	@Then("^I remove opened tab$")
	public static void i_remove_openedTab() {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		int tabsOpen = tabs.size();
		for (int i = 1; i <= tabsOpen - 1; i++) {
			driver.switchTo().window(tabs.get(i));
			driver.close();
			log.info("Closed Tabs");
		}
		driver.switchTo().window(tabs.get(0));
	}

	@When("^I Switch the Tab and enter the '(.*)'$")
	public static void I_switch_to_new_browser_tab(String url) {
		driver = StepBase.getDriver();
		// driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1)); // switches to new tab
		//driver.get(url);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.location.replace('" + url + "')");
		driver.navigate().to(url);
	}

	@When("^I Switch between Tab '(.*)'$")
	public static void I_switch_between_tab(int tab) {

		driver = StepBase.getDriver();

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(tab));
	}

	@When("^I press key: enter$")
	public static void i_press_enter_key_() throws Exception {
		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.ENTER).build().perform();
			// driver.findElement(By.xpath("//textarea[@class='slds-textarea
			// runtime_service_liveagentChatInput' and
			// @id='textarea']")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			// Robot r = new Robot();
			// r.keyPress(KeyEvent.VK_ENTER);
			// r.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();

		}
	}

	@When("^I change the screen dimension to '(.*)' - '(.*)'$")
	public static void i_change_the_screen_dimension(int width, int height) throws Exception {
		try {
			driver.manage().window().setSize(new Dimension(width, height));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	public static void uploadFile1(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}


	public static void setClipboardData2(String string) {
//StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile2(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}


	public static void setClipboardData3(String string) {
//StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile3(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	
	public static void setClipboardData4(String string) {
//StringSelection is a class that can be used for copy and paste operations.
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public static void uploadFile4(String fileLocation) {
		try {
			// Setting clipboard with file location
			setClipboardData(fileLocation);
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	

	@Given("^I click by JS latest warranty case$")
	public static void I_click_latest_warranty_case() throws Exception {
		WebElement wElement = driver.findElement(By
				.xpath("//li[position()=last()]//ul[@class='secondaryFields truncate']//..//a[text()='Under Review']"));
		wElement.isEnabled();
		wElement.isDisplayed();
		WrapperFunctions.clickByJS(wElement);
		StepBase.embedScreenshot();
		WrapperFunctions.waitForPageToLoad();
	}

	@Then("^I get Case Number from Chat '(.*)'$")
	public static void I_get_CaseNumber_from_Chat(String element) {
//	String CaseNum="";
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String value = wElement.getText();
			log.info("*****Case ID: " + value);

			String[] split = value.split("is");
			String value1 = split[1].replace(".", "").trim();
			System.out.println(value1);
			HashMapContainer.add("$$" + element, value1);

			StepBase.embedScreenshot();

		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}

	}

	@Then("^I get Case Number from Chat Transcript '(.*)'$")
	public static void I_get_CaseNumber_from_Chat_Transcript(String element) {
//	String CaseNum="";
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String value = wElement.getText();
			log.info("*****Case ID: " + value);

			String[] split = value.split("number ");
			String value1 = split[1].replace(" has been created.", "").trim();
			System.out.println(value1);
			HashMapContainer.add("$$" + element, value1);

			StepBase.embedScreenshot();

		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("^I validate css property '(.*)' should contain value '(.*)' for element '(.*)'$")
	public static void CSS_Assertion(String cssPropertyName, String cssPropertyValue, String element) {
//		String CaseNum="";
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualcssvalue = wElement.getCssValue(cssPropertyName);
			Assert.assertEquals("Css Value Mismatch", cssPropertyValue, actualcssvalue);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}

	}

	@When("^I execute JavaScript to change button text color$")
	public static void execute_Js_changeButtonTextColor() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.getElementById(\"checkout-button-ID\").style.color = \"blue\";");
	}
	@Then("^I enter from system variable '(.*)' into feild '(.*)'$")
	public static void I_enter_from_StoredValue1(String StoredValue, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(System.getProperty("randomemail"));
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}
	@When("^I Switch the Tab and enter the url '(.*)'$")
	public static void I_switch_to_new_browser_tab_enter_url(String urlKey) {
		String url=Utilities.getTestData(urlKey);
		driver = StepBase.getDriver();
		// driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1)); // switches to new tab
		//driver.get(url);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.location.replace('" + url + "')");
		driver.navigate().to(url);
	}
	@When("^I navigate to application url '(.*)'$")
	public static void i_navigate_to_application_url(String urlKey) {
		String url=Utilities.getTestData(urlKey);
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.location.replace('" + url + "')");
			driver.navigate().to(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	@Then("^I should see the url '(.*)'$")
	public static void I_enter_from_StoredValue1(String urlKey) {
		try {
			String url=Utilities.getTestData(urlKey);
			Assert.assertTrue(driver.getCurrentUrl().contains(url));
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}
}

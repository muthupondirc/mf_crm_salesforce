package com.TEAF.framework;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.IOUtils;

import com.TEAF.framework.HashMapContainer;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAssuredUtility {
	Response response;

	public Response Get(String path, RequestSpecification spec) {
		response = spec.get(path);
		return response;
	}

	public Response Post(String url, RequestSpecification spec) {
		response = spec.post(url);
		return response;
	}

	public Response Put(String url, RequestSpecification spec) {

		response = spec.patch(url);
		return response;
	}

	public Response Delete(String url, RequestSpecification spec) {
		response = spec.delete(url);
		return response;
	}

	public Response SOAP(String url, String file) throws Throwable {
		FileInputStream fileinputstream = new FileInputStream(file);
		RestAssured.baseURI = url;
		Response response = given().header("Content-Type", "text/xml").and()
				.body(IOUtils.toString(fileinputstream, "UTF-8")).when().post("");
		return response;
	}
	
	

	public static File updateJSONforPOSTRequest(File f, String key, String store) throws Exception {
		DocumentContext parse = JsonPath.parse(f);
		if (store.startsWith("$$")) {
			store = HashMapContainer.get(store);
		}

		DocumentContext set = parse.set(key, store);
		String jsonString = set.jsonString();
		Files.write(Paths.get(f.getAbsolutePath()), jsonString.getBytes());
		return f;
	}

	public static void retriveJSONforPOSTRequest(Response res, String key, String store) throws Exception {
		String value = JsonPath.parse(res.getBody().asString()).read(key);
		HashMapContainer.add("$$" + store, value);
		System.out.println(value);
	}
}

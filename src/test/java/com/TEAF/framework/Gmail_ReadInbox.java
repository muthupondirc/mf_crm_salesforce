package com.TEAF.framework;

import java.io.IOException;
import java.util.Properties;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import com.sun.mail.imap.IMAPBodyPart;
import com.sun.mail.imap.IMAPFolder;

public class Gmail_ReadInbox {

	public static void I_Login_to_GmailAccount_ValidCreds() throws Exception {

		try {            

			String host = "imap.gmail.com";// change accordingly

			String username = "hasantestuser@gmail.com";// change accordingly

			String password = "Hasan123!@#";// change accordingly

			Properties props = new Properties();

			props.setProperty("mail.imap.starttls.enable", "true");

			// set any other needed mail.imap.* properties here

			Session session = Session.getInstance(props);

			Store store = session.getStore("imaps");

			store.connect(host, username, password);

			System.out.println(store.isConnected());

			store.getDefaultFolder();

			System.out.println();

			IMAPFolder  folder =(IMAPFolder) store.getFolder("Inbox");

			folder.open(Folder.READ_WRITE);

			Message[] messages = folder.getMessages();
			String mailBodyContent=null;
			for(Message message : messages ) {
				System.out.println(message.getSubject());   
				if(message.getSubject().equals("Sandbox: Let's reset your password.")) {
					System.out.println("Content Type: "+message.getContentType());
					Multipart Multipart = (Multipart) message.getContent();
					int count = Multipart.getCount();
					for (int i = 0; i < count; i++)
					{
						//System.out.println( "Message body: "+Multipart.getBodyPart(i));
						if (message.isMimeType("multipart/*")) {
					    mailBodyContent = getContentFromMultipartMessage(Multipart);
				        System.out.println("i= "+i+" Result: "+mailBodyContent);
				        
						}
					}
				}
			}
			store.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String getContentFromMultipartMessage(Multipart mimeMultipart)  throws MessagingException, IOException{
	    String result = "";
	    int count = mimeMultipart.getCount();
	    for (int i = 0; i < count; i++) {
	        IMAPBodyPart bodyPart = (IMAPBodyPart)mimeMultipart.getBodyPart(i);
	        //System.out.println(i+" Content Type: "+bodyPart.getContentType());
	        if (bodyPart.isMimeType("TEXT/HTML; charset=utf-8")) {
	            result = result + "\n" + bodyPart.getContent();
	            break; // without break same text appears twice in my tests
	        } else if (bodyPart.isMimeType("text/html")) {
	            String html = (String) bodyPart.getContent();
	            result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
	        } else if (bodyPart.getContent() instanceof MimeMultipart){
	            result = result + getContentFromMultipartMessage((MimeMultipart)bodyPart.getContent());
	        }
	    }
	    return result;
	}
	
	public static void main(String[] args) throws Exception {
		I_Login_to_GmailAccount_ValidCreds();
	}

}

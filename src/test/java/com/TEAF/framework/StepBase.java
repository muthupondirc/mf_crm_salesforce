package com.TEAF.framework;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import org.apache.log4j.Logger;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import cucumber.api.Scenario;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;


/**
 * @ScriptName : StepBase
 * @Description : This class contains generic functionalities like
 *              setup/teardown test environment
 * @Author : Swathin Ratheendren
 * @Creation Date : September 2016 @Modified Date:
 */
public class StepBase {
	// Local Variables
	static WebDriver driver;
	static Process WiniumProcess;
	static Process process;
	static Process webkitprocess;
	static WebDriverWait webDriverWait;
	public static Scenario crScenario;
	static DesiredCapabilities capabilities = null;
	public static String testPlatform;
	public static String testBrowser;
	static AppiumDriverLocalService service;
	static String service_url;
	// static WindowsDriver wDriver;
	// BrowserStack credentials
	String USERNAME = System.getProperty("test.bstack.username");
	String AUTOMATE_KEY = System.getProperty("test.bstack.automatekey");
	public static AppiumServiceBuilder appiumServiceBuilder;

	static String appiumUrl = System.getProperty("test.appium.url");
	String bstackURL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	static String huburl;

	static String BrowserStackURL = null;

	
	static Logger log = Logger.getLogger(StepBase.class.getName());

	/**
	 * @Method: setUp
	 * @Description: This method is used to initialize test execution environment
	 *               i.e. driver initialization, setting capabilities for selected
	 *               device
	 * @author Swathin Ratheendren
	 * @throws Exception
	 * @Creation Date: September 2016 @Modified Date:
	 */

	public static void setScenario(Scenario cScenario) throws Exception {
		crScenario = cScenario;
	}

	public static void setUp(String Platform, String Browser) throws Exception {
		try {
			testPlatform = Platform;
			testBrowser = Browser;

			if (Platform.equalsIgnoreCase("desktop")
					&& !System.getProperty("test.appType").equalsIgnoreCase("windowsapp")) {
				if (Browser.toLowerCase().equals("chrome")) {
					System.setProperty("webdriver.chrome.driver",
							System.getProperty("user.dir") + "/src/test/java/com/Resources/chromedriver.exe");
					ChromeOptions Options = new ChromeOptions();
					HashMap<String,Object> chromePrefs = new HashMap<String, Object>();
					chromePrefs.put("plugins.always_open_pdf_externally", true);
					String dpath = "/output/PDFData";
					File downloadfilespath = new File(dpath);
					if(!downloadfilespath.exists()) {
					downloadfilespath.mkdirs();
					}
					chromePrefs.put("download.default_directory", System.getProperty("user.dir") + File.separator + "output" + File.separator + "downloadfiles");
					Options.setExperimentalOption("prefs", chromePrefs);
					Options.addArguments("--disable-notifications");
					Options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					driver = new ChromeDriver(Options);
					// Dimension dime = new Dimension(1440, 900);
					// driver.manage().window().setSize(dime);
					driver.manage().window().maximize();
				} else if (Browser.toLowerCase().equals("node-chrome")) {
					// huburl = "http://192.168.99.100:4444/wd/hub";
					huburl = System.getProperty("test.hubUrl");
					DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
					ChromeOptions options = new ChromeOptions();
					options.setCapability("platform", "LINUX");
					desiredCapabilities.setCapability("idleTimeout", 500);
					desiredCapabilities.setCapability("name", System.getProperty("test.projectName"));
					desiredCapabilities.setCapability("build", System.getProperty("test.projectName"));
					desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
					driver = new RemoteWebDriver(new URL(System.getProperty("test.hubUrl")), desiredCapabilities);
					driver.manage().window().maximize();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					log.info("Driver=" + driver);
				} else if (Browser.toLowerCase().equals("grid-chrome")) {
					huburl = System.getProperty("test.hubUrl");
					DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
					ChromeOptions options = new ChromeOptions();
					desiredCapabilities.setPlatform(org.openqa.selenium.Platform.WINDOWS);
					desiredCapabilities.setBrowserName("chrome");
					desiredCapabilities.setCapability("idleTimeout", 500);
					desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
					driver = new RemoteWebDriver(new URL(huburl), options);
					driver.manage().window().maximize();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					log.info("Driver=" + driver);
				} else if (Browser.toLowerCase().equals("chrome-headless-linux")) {
					log.info("Running tests on Chrome Browser Headlessly on Linux");
					String chromeBinaryPath = "/usr/bin/google-chrome";
					String driverPath = System.getProperty("user.dir") + "/src/test/java/com/Resources/chromedriver";
					System.setProperty("webdriver.chrome.driver", driverPath);
					// System.setProperty("webdriver.chrome.verboseLogging","true");
					// System.setProperty("webdriver.chrome.logfile","/data/Package/log/chromedriver.log");
					System.setProperty("test.disableScreenshotCapture", "true");
					ChromeOptions optionsLinux64 = new ChromeOptions();
					optionsLinux64.setAcceptInsecureCerts(true);
					optionsLinux64.setBinary(chromeBinaryPath);
					// optionsLinux64.addArguments("port=8087");
					optionsLinux64.addArguments("--headless");
					optionsLinux64.addArguments("--no-sandbox");
					optionsLinux64.addArguments("window-size=1980,1080");
					optionsLinux64.addArguments("--disable-gpu");
					optionsLinux64.addArguments("--headless");
					driver = new ChromeDriver(optionsLinux64);
				} else if (Browser.toLowerCase().equals("rd-chrome-headless-linux")) {
					log.info("Running tests on Remote Driver Chrome Browser Headlessly");
					URL serverurl = new URL("http://localhost:9515");
					log.info("Running tests on Chrome Browser Headlessly");
					String chromeBinaryPath = "/usr/bin/google-chrome";
					String driverPath = System.getProperty("user.dir") + "/src/test/java/com/Resources/chromedriver";
					System.setProperty("webdriver.chrome.driver", driverPath);
					System.setProperty("webdriver.chrome.verboseLogging", "true");
					System.setProperty("webdriver.chrome.logfile", "/data/Package/log/chromedriver.log");
					ChromeOptions optionsLinux64 = new ChromeOptions();
					optionsLinux64.setBinary(chromeBinaryPath);
					optionsLinux64.addArguments("port=8087");
					optionsLinux64.addArguments("--headless");
					optionsLinux64.addArguments("--no-sandbox");
					optionsLinux64.addArguments("--disable-gpu");
					optionsLinux64.addArguments("--headless");
					driver = new RemoteWebDriver(serverurl, capabilities);
				} else if (Browser.toLowerCase().equals("firefox")) {
					System.setProperty("webdriver.gecko.driver",
							System.getProperty("user.dir") + "/src/test/java/com/Resources/geckodriver.exe");
					log.info("Executing test on Firefox browser");
					driver = new FirefoxDriver();
					driver.manage().window().maximize();
				} else if (Browser.toLowerCase().equals("ie")) {
					System.setProperty("webdriver.ie.driver",
							System.getProperty("user.dir") + "/src/test/java/com/Resources/IEDriverServer.exe");
					log.info("Executing test on Internet Explorer browser");
					driver = new InternetExplorerDriver();
					driver.manage().window().maximize();
				} else if (Browser.toLowerCase().equals("htmlunit")) {
					log.info("Executing test Headlessly on HtmlUnit");
					driver = new HtmlUnitDriver(false);
				} else if (Browser.toLowerCase().equals("edge")) {
					System.setProperty("webdriver.edge.driver",
							System.getProperty("user.dir") + "/src/test/java/com/Resources/msedgedriver.exe");
					log.info("Executing test on Internet Explorer browser");
					driver = new EdgeDriver();
					driver.manage().window().maximize();
				} else if (Browser.toLowerCase().equals("safari")) {
					SafariOptions Options = new SafariOptions();
					Options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					log.info("Executing test on Safari browser");
					driver = new SafariDriver(Options);
					driver.manage().window().maximize();
				} else if (Browser.toLowerCase().equals("browserstack-desktop-browser")) {
					DesiredCapabilities caps = new DesiredCapabilities();
					caps.setCapability("browser", System.getProperty("test.bstack.browser"));
//					caps.setCapability("browserstack.local", System.getProperty("test.bstack.local"));
//					caps.setCapability("browserstack.localIdentifier",
//							System.getProperty("test.bstack.localIdentifier"));
					caps.setCapability("browserstack.debug", true);
					caps.setCapability("browser_version", System.getProperty("test.bstack.browserVersion"));
					caps.setCapability("os", System.getProperty("test.bstack.os"));
					caps.setCapability("os_version", System.getProperty("test.bstack.osVersion"));
					caps.setCapability("resolution", System.getProperty("test.bstack.resolution"));
					caps.setCapability("autoGrantPermissions", "true");
					caps.setCapability("autoAcceptAlerts", "true");
					//caps.setCapability("browserstack.local", "true");
					String USERNAME = System.getProperty("test.bstack.userName");
					String AUTOMATE_KEY = System.getProperty("test.bstack.automateKey");
					String bstackURL = "https://" + USERNAME + ":" + AUTOMATE_KEY
							+ "@hub-cloud.browserstack.com/wd/hub";
					driver = new RemoteWebDriver(new URL(bstackURL), caps);
					driver.manage().window().maximize();

				} else if (Browser.toLowerCase().equals("browserstack-desktop-browser1")) {
					DesiredCapabilities caps = new DesiredCapabilities();
					caps.setCapability("browser", System.getProperty("test.bstack.browser"));
					caps.setCapability("browserstack.debug", true);
					caps.setCapability("browser_version", System.getProperty("test.bstack.browserVersion"));
					caps.setCapability("os", System.getProperty("test.bstack.os"));
					caps.setCapability("os_version", System.getProperty("test.bstack.osVersion"));
					caps.setCapability("resolution", System.getProperty("test.bstack.resolution"));
					caps.setCapability("autoGrantPermissions", "true");
					caps.setCapability("autoAcceptAlerts", "true");
					caps.setCapability("browserstack.local", "true");
					String USERNAME = System.getProperty("test.bstack.userName");
					String AUTOMATE_KEY = System.getProperty("test.bstack.automateKey");
					String bstackURL = "https://" + USERNAME + ":" + AUTOMATE_KEY
							+ "@hub-cloud.browserstack.com/wd/hub";
					log.info(bstackURL);
					driver = new RemoteWebDriver(new URL(bstackURL), caps);
					driver.manage().window().maximize();

				}
				driver.manage().timeouts().implicitlyWait(Integer.parseInt(System.getProperty("test.implicitlyWait")),
						TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(Integer.parseInt(System.getProperty("test.pageLoadTimeout")),
						TimeUnit.SECONDS);

			} else if (Platform.equalsIgnoreCase("mobile")) {
				if (Browser.toLowerCase().equals("appium-nativeapp")) {
					// Set up desired capabilities and pass the Android app-activity and app-package
					// to Appium
					DesiredCapabilities capabilities = new DesiredCapabilities();
					capabilities.setCapability("browserName", "Android");
					capabilities.setCapability("VERSION", "9.0");
					capabilities.setCapability("deviceName", "OnePlus5");
					capabilities.setCapability("platformName", "Android");
					capabilities.setCapability("appPackage", "in.amazon.mShop.android.shopping");
					capabilities.setCapability("appActivity", "com.amazon.mShop.home.HomeActivity");
					driver = new RemoteWebDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
				} else if (Browser.toLowerCase().equals("bstack-droid-nativeapp")) {
					DesiredCapabilities caps = new DesiredCapabilities();
					caps.setCapability("device", System.getProperty("test.bstack.device"));
					caps.setCapability("os_version", System.getProperty("test.bstack.mobOsVersion"));
					caps.setCapability("name", System.getProperty("test.bstack.testName"));
					caps.setCapability("app", System.getProperty("test.bstack.bs"));
					caps.setCapability("autoGrantPermissions", "true");
					caps.setCapability("autoAcceptAlerts", "true");
					String USERNAME = System.getProperty("test.bstack.userName");
					String AUTOMATE_KEY = System.getProperty("test.bstack.automateKey");
					String bstackURL = "https://" + USERNAME + ":" + AUTOMATE_KEY
							+ "@hub-cloud.browserstack.com/wd/hub";

					driver = new AppiumDriver<MobileElement>(new URL(bstackURL), caps);

				} else if (Browser.toLowerCase().equals("bstack-ios-nativeapp")) {
					DesiredCapabilities caps = new DesiredCapabilities();
					caps.setCapability("device", System.getProperty("test.bstack.device"));
					caps.setCapability("os_version", System.getProperty("test.bstack.mobOsVersion"));
					caps.setCapability("name", System.getProperty("test.bstack.testName"));
					caps.setCapability("app", System.getProperty("test.bstack.bs"));
					caps.setCapability("autoGrantPermissions", "true");
					// caps.setCapability("autoAcceptAlerts", "true");
					String USERNAME = System.getProperty("test.bstack.userName");
					String AUTOMATE_KEY = System.getProperty("test.bstack.automateKey");
					String bstackURL = "https://" + USERNAME + ":" + AUTOMATE_KEY
							+ "@hub-cloud.browserstack.com/wd/hub";

					driver = new IOSDriver<IOSElement>(
							new URL("http://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub"),
							caps);
				} else if (Browser.toLowerCase().equals("bstack-mob-webapp-local")) {
					DesiredCapabilities caps = new DesiredCapabilities();
					caps.setCapability("device", System.getProperty("test.bstack.device"));
					caps.setCapability("os_version", System.getProperty("test.bstack.mobOsVersion"));
					caps.setCapability("name", System.getProperty("test.bstack.testName"));
					caps.setCapability("browserName", System.getProperty("test.bstack.browserName"));
					caps.setCapability("realMobile", "true");
					caps.setCapability("browserstack.local", System.getProperty("test.bstack.local"));
					caps.setCapability("browserstack.localIdentifier",
							System.getProperty("test.bstack.localIdentifier"));
					caps.setCapability("autoGrantPermissions", "true");
					caps.setCapability("autoAcceptAlerts", "true");
					caps.setCapability("browserstack.debug", true);
					String USERNAME = System.getProperty("test.bstack.userName");
					String AUTOMATE_KEY = System.getProperty("test.bstack.automateKey");
					String bstackURL = "https://" + USERNAME + ":" + AUTOMATE_KEY
							+ "@hub-cloud.browserstack.com/wd/hub";

					driver = new AppiumDriver<MobileElement>(new URL(bstackURL), caps);
				} else if (Browser.toLowerCase().equals("bstack-droid-webapp")) {
					DesiredCapabilities caps = new DesiredCapabilities();
					caps.setCapability("device", System.getProperty("test.bstack.device"));
					caps.setCapability("os_version", System.getProperty("test.bstack.mobOsVersion"));
					caps.setCapability("name", System.getProperty("test.bstack.testName"));
					caps.setCapability("browserName", System.getProperty("test.bstack.browserName"));
					caps.setCapability("realMobile", "true");
					caps.setCapability("browserstack.debug", true);
					caps.setCapability("autoGrantPermissions", "true");
					caps.setCapability("autoAcceptAlerts", "true");
					String USERNAME = System.getProperty("test.bstack.userName");
					String AUTOMATE_KEY = System.getProperty("test.bstack.automateKey");
					String bstackURL = "https://" + USERNAME + ":" + AUTOMATE_KEY
							+ "@hub-cloud.browserstack.com/wd/hub";

					driver = new AppiumDriver<MobileElement>(new URL(bstackURL), caps);
				}

				else if (Browser.toLowerCase().equals("real-droid-nativeapp")) {
					DesiredCapabilities caps = new DesiredCapabilities();
					caps.setCapability(MobileCapabilityType.DEVICE_NAME, System.getProperty("test.real.deviceName"));
					caps.setCapability(MobileCapabilityType.PLATFORM_NAME, org.openqa.selenium.Platform.ANDROID);
					caps.setCapability(MobileCapabilityType.PLATFORM_VERSION,
							System.getProperty("test.real.platformVersion"));
					caps.setCapability("noReset", true);
					caps.setCapability("adbExecTimeout", 70000);
					caps.setCapability("–session-override", true);
					caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE,
							System.getProperty("test.real.app.package"));
					caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,
							System.getProperty("test.real.app.activity"));
					String appiumUrl = System.getProperty("test.appium.url");
					driver = new AppiumDriver<MobileElement>(new URL(appiumUrl), caps);
				} else if (Browser.toLowerCase().equals("real-ios-nativeapp")) {
					capabilities = new DesiredCapabilities();
					capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, org.openqa.selenium.Platform.IOS);
					capabilities.setCapability(MobileCapabilityType.VERSION,
							System.getProperty("test.real.platformVersion"));
					capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,
							System.getProperty("test.real.deviceName"));
					capabilities.setCapability(MobileCapabilityType.UDID, System.getProperty("test.udid"));
					// capabilities.setCapability("autoAcceptAlerts", true);
					// capabilities.setCapability("autoWebview", true);
					capabilities.setCapability("autoDismissAlerts", true);
					capabilities.setCapability("showIOSLog", true);
					capabilities.setCapability("xcodeOrgId", System.getProperty("test.xcodeOrgId"));
					capabilities.setCapability("xcodeSigningId", System.getProperty("test.xcodeSigningId"));
					capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
					capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "480");
					// capabilities.setCapability(MobileCapabilityType.LAUNCH_TIMEOUT, "480000");
					capabilities.setCapability("bundleId", System.getProperty("test.appBundleID"));
					driver = new IOSDriver(new URL(appiumUrl), capabilities);
					Thread.sleep(10000);
				}

				else if (Browser.toLowerCase().equals("real-droid-chrome")) {
					DesiredCapabilities caps = new DesiredCapabilities();
					caps.setCapability(MobileCapabilityType.DEVICE_NAME, System.getProperty("test.real.deviceName"));
					caps.setCapability(MobileCapabilityType.PLATFORM_NAME, org.openqa.selenium.Platform.ANDROID);
					caps.setCapability(MobileCapabilityType.PLATFORM_VERSION,
							System.getProperty("test.real.platformVersion"));
					caps.setCapability("noReset", true);
					caps.setCapability(MobileCapabilityType.BROWSER_NAME, "CHROME");
					caps.setBrowserName(BrowserType.CHROME);
					String appiumUrl = System.getProperty("test.appium.url");
					driver = new AppiumDriver<MobileElement>(new URL(appiumUrl), caps);
				}
				driver.manage().timeouts().implicitlyWait(Integer.parseInt(System.getProperty("test.implicitlyWait")),
						TimeUnit.SECONDS);
				if (System.getProperty("test.appType").equalsIgnoreCase("webapp")) {
					driver.manage().timeouts().pageLoadTimeout(
							Integer.parseInt(System.getProperty("test.pageLoadTimeout")), TimeUnit.SECONDS);

				}
			} else if (Platform.equalsIgnoreCase("desktop")
					&& System.getProperty("test.appType").equalsIgnoreCase("windowsapp")) {
				log.info("Running Tests on Windows Application");
				WiniumProcess = Runtime.getRuntime().exec(
						System.getProperty("user.dir") + "/src/test/java/com/Resources/Winium.Desktop.Driver.exe");
			} else if (Platform.equalsIgnoreCase("desktop")
					&& System.getProperty("test.appType").equalsIgnoreCase("winapp")) {
				log.info("Running Tests on Win Application");

			}

			else {
				log.info("Enter Valid - Platform|Browser Values");
			}
		} catch (Exception e) {
			log.error(e);
			throw e;
		}
	}

	public static void browserStackStart(String Browser, String browserName, String browserVersion)
			throws MalformedURLException {
		String USERNAME = System.getProperty("test.bstack.userName");
		String AUTOMATE_KEY = System.getProperty("test.bstack.automateKey");
		String bstackURL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
		if (Browser.toLowerCase().equals("browserstack-desktop-browser")) {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("browser", browserName);
//			caps.setCapability("browserstack.local", System.getProperty("test.bstack.local"));
//			caps.setCapability("browserstack.localIdentifier",
//					System.getProperty("test.bstack.localIdentifier"));
			caps.setCapability("browserstack.debug", true);
			caps.setCapability("browser_version", browserVersion);
			caps.setCapability("os", System.getProperty("test.bstack.os"));
			caps.setCapability("os_version", System.getProperty("test.bstack.osVersion"));
			if (System.getProperty("resolution") != null && !System.getProperty("resolution").isEmpty()) {
				System.out.println(System.getProperty("resolution"));
				caps.setCapability("resolution", System.getProperty("resolution"));
			} else {
				System.out.println(System.getProperty("test.bstack.resolution"));
				caps.setCapability("resolution", System.getProperty("test.bstack.resolution"));

			}
			caps.setCapability("autoGrantPermissions", "true");
			caps.setCapability("autoAcceptAlerts", "true");

			driver = new RemoteWebDriver(new URL(bstackURL), caps);
			driver.manage().window().maximize();

		} else if (Browser.toLowerCase().equals("bstack-mobile-browser")) {
			System.out.println(System.getProperty("resolution"));
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("browserName", browserName);
			if (System.getProperty("resolution") != null && !System.getProperty("resolution").isEmpty()) {
				System.out.println(System.getProperty("resolution"));
				caps.setCapability("device", System.getProperty("resolution"));
			} else if (browserName.equalsIgnoreCase("iphone")) {
				System.out.println(System.getProperty("test.bstack.iosmresolution"));
				caps.setCapability("device", System.getProperty("test.bstack.iosmresolution"));

			} else {
				caps.setCapability("device", System.getProperty("test.bstack.androidmresolution"));

			}
			caps.setCapability("realMobile", "true");
			if (System.getProperty("osversion") != null && !System.getProperty("osversion").isEmpty()) {
				System.out.println(System.getProperty("osversion"));
				caps.setCapability("os_version", System.getProperty("osversion"));
				} else if (browserName.equalsIgnoreCase("iphone")) {
				caps.setCapability("resolution", System.getProperty("test.bstack.iosmversion"));

			} else {
				caps.setCapability("resolution", System.getProperty("test.bstack.androidmversion"));

			}
			

			driver = new RemoteWebDriver(new URL(bstackURL), caps);
		}
	}

	// public static void appiumStart(String portNo, String appiumNodePath, String
	// appiumJSPath, String appName) throws Exception {
	public static void appiumStart() throws Exception {
		// Set Capabilities
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("noReset", "true");
		cap.setCapability("--session-override", true);
		cap.setCapability("--log-no-colors", false);
		cap.setCapability("--relaxed-security", true);

		// Build the Appium service
		appiumServiceBuilder = new AppiumServiceBuilder();
		appiumServiceBuilder.withIPAddress("0.0.0.0");
		appiumServiceBuilder.usingPort(4723);
		appiumServiceBuilder.withCapabilities(cap);
		appiumServiceBuilder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
		appiumServiceBuilder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");

		// Start the server with the builder
		service = AppiumDriverLocalService.buildService(appiumServiceBuilder);
		service.start();
		log.info("Appium Server Started");
	}

	public static void appiumStop() throws Exception {
		service.stop();
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static void tearDown() {
		try {
			
			if (driver != null) {
				if (System.getProperty("test.appType").equalsIgnoreCase("webapp")) {
					driver.manage().deleteAllCookies();
				}
				driver.quit();
				driver = null;
				Thread.sleep(3000);
			}
			Utilities.stopVideoRecorder();
		} catch (Exception e) {
			log.error(e);
		}
	}

	public static void embedScreenshot() {
		try {

			if (System.getProperty("test.disableScreenshotCapture").equalsIgnoreCase("false")
					&& !System.getProperty("test.appType").equalsIgnoreCase("windowsapp")) {
				Thread.sleep(1000);
				final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				crScenario.embed(screenshot, "image/png"); // Stick it to HTML report
			} else {
				log.info("Test Property - test.ScreenShotCapture is disabled!");
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}

package com.TEAF.framework;

import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.SearchTerm;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.TEAF.Hooks.Hooks;
import com.sun.mail.imap.IMAPBodyPart;
import com.sun.mail.imap.IMAPFolder;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @ScriptName : Utilities
 * @Description : This class contains
 * @Author : Swathin Ratheendren
 * @Creation Date : September 2016 @Modified Date:2019
 */
public class Utilities {

	/**
	 * Method: takeScreenshot Description:
	 * 
	 * @param timeInMilliseconds
	 * @author Swathin Ratheendren
	 * @Creation Date: September 2016 Modified Date:
	 */
	static Logger log = Logger.getLogger(Utilities.class.getName());

	static ATUTestRecorder AtuTestRecorder = null;

	public static WebDriver driver = StepBase.getDriver();

	public static Properties objConfig = new Properties();

	public static byte[] takeScreenshotByte(WebDriver driver) {
		try {
			byte[] scrFile = null;
			if (System.getProperty("test.disableScreenshotCapture").equalsIgnoreCase("false")) {
				Thread.sleep(1000);
				scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);

			}
			return scrFile;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);

		}
	}

	public static String takeScreenshot(WebDriver driver) {
		try {
			String SSPath = "";
			if (System.getProperty("test.disableScreenshotCapture").equalsIgnoreCase("false")) {
				Thread.sleep(1000);
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				File directory = new File(String.valueOf("Screenshots"));
				if (!directory.exists()) {
					directory.mkdir();
				}
				SSPath = "Screenshots/" + getRequiredDate(0, "yyyy_MM_dd_hh", null) + "/screenshot_"
						+ getRequiredDate(0, "yyyy_MM_dd_hh_mm_ss", null) + ".png";
				FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/output/" + SSPath));
			}
			return SSPath;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public static String startVideoRecorder() throws ATUTestRecorderException, InterruptedException {
		DateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm_ss");
		Date date = new Date();

		String RootPath = System.getProperty("user.dir") + "/output/ScreenCaptureVideos/";
		File file = new File(RootPath);
		if (!file.exists()) {
			file.mkdir();
		}
		String VidFileName = "TestVideo-" + dateFormat.format(date);

		AtuTestRecorder = new ATUTestRecorder(RootPath, VidFileName, false);
		AtuTestRecorder.start();
		return RootPath + VidFileName;
	}

	public static void stopVideoRecorder() throws ATUTestRecorderException, InterruptedException {
		if (AtuTestRecorder != null) {
			AtuTestRecorder.stop();
		}
	}

	/**
	 * Method: waitFor Description: Waits for the specified amount of
	 * [timeInMilliseconds].
	 * 
	 * @param timeInMilliseconds
	 * @author Swathin Ratheendren
	 * @Creation Date: September 2016 Modified Date:
	 */
	public void waitFor(final Long timeInMilliseconds) {
		try {
			Thread.sleep(timeInMilliseconds);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public static void waittextToBePresentInElement(String location, String expectedText) {
		try {
			WebDriverWait wb = new WebDriverWait(driver, 50);

			wb.until(ExpectedConditions.textToBePresentInElement(
					driver.findElement(GetPageObjectRead.OR_GetElement(location)), expectedText));
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException("expected text");

		}
	}

	public static void scrollToTopOfPage() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0,-document.body.scrollHeight);");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);

		}
	}

	/**
	 * Method: getRequiredDate Description: This method will give require date
	 * 
	 * @param incrementDateByDays Number by which user want increase date
	 * @param sExpectedDateFormat - User expected date format eg. 9 april 2014 ---
	 *                            dd/MM/yyyy -> 09/04/2015, dd-MM-yyyy -> 09-04-2015
	 * @param timeZoneId          - Time Zone
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public static String getRequiredDate(int incrementDays, String expectedDateFormat, String timeZoneId) {
		try {
			DateFormat dateFormat;
			Calendar calendar = Calendar.getInstance();
			dateFormat = new SimpleDateFormat(expectedDateFormat);
			if (timeZoneId != null && !timeZoneId.equals(""))
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneId));
			calendar.add(Calendar.DAY_OF_MONTH, incrementDays);
			Date tomorrow = calendar.getTime();
			String formattedDate = dateFormat.format(tomorrow);
			return formattedDate;
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}

	/**
	 * Method: copyFileUsingStream Description:
	 * 
	 * @param timeInMilliseconds
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			is.close();
			os.close();
		}
	}

	/**
	 * Method: waitForPageLoad Description: timeInSeconds for the specified amount
	 * of [timeInSeconds].
	 * 
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public void waitForPageLoad() {
		try {
			WebDriverWait wait = new WebDriverWait(StepBase.getDriver(), 180);
			final JavascriptExecutor javascript = (JavascriptExecutor) (StepBase
					.getDriver() instanceof JavascriptExecutor ? StepBase.getDriver() : null);
			/*
			 * wait.until(new ExpectedCondition<Boolean>() {
			 * 
			 * @Override public Boolean apply(WebDriver d) { boolean outcome =
			 * Boolean.parseBoolean(javascript .executeScript("return jQuery.active == 0")
			 * .toString()); return outcome; } });
			 * 
			 */
			wait.until(new ExpectedCondition<Boolean>() {
				// @Override
				public Boolean apply(WebDriver d) {
					return javascript.executeScript("return document.readyState").equals("complete");
				}
			});

			StepBase.getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			// objStepBase.getDriver().manage().timeouts().pageLoadTimeout(90,
			// TimeUnit.SECONDS);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	/**
	 * Method: waitForAjaxCallComplete Description: timeInSeconds for the specified
	 * amount of [timeInSeconds].
	 * 
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public void waitForAjaxCallComplete() {
		try {
			WebDriverWait wait = new WebDriverWait(StepBase.getDriver(), 180);
			final JavascriptExecutor javascript = (JavascriptExecutor) (StepBase
					.getDriver() instanceof JavascriptExecutor ? StepBase.getDriver() : null);
			wait.until(new ExpectedCondition<Boolean>() {
				// @Override
				public Boolean apply(WebDriver d) {
					boolean outcome = Boolean
							.parseBoolean(javascript.executeScript("return jQuery.active == 0").toString());
					return outcome;
				}
			});
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public static void resizeImage(File inputImagePath, File outputImagePath, int scaledWidth, int scaledHeight)
			throws IOException {
		// reads input image
		BufferedImage inputImage = ImageIO.read(inputImagePath);

		// creates output image
		BufferedImage outputImage = new BufferedImage(scaledWidth, scaledHeight, inputImage.getType());

		// scales the input image to the output image
		Graphics2D g2d = outputImage.createGraphics();
		g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
		g2d.dispose();

		log.info("Resize completed : " + outputImagePath);

		/*
		 * // extracts extension of output file String formatName =
		 * outputImagePath.substring(outputImagePath.lastIndexOf(".") + 1);
		 */

		// writes to output file
		ImageIO.write(outputImage, "png", outputImagePath);
	}

	/**
	 * Method: ImageComparison Description: timeInSeconds for the specified amount
	 * of [timeInSeconds].
	 * 
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public static float compareImage(File ActualImage, File BaseLinedImage) {
		float percentage = 0;
		try {
			// take buffer data from both image files //
			BufferedImage biA = ImageIO.read(ActualImage);
			DataBuffer dbA = biA.getData().getDataBuffer();
			int sizeA = dbA.getSize();
			BufferedImage biB = ImageIO.read(BaseLinedImage);
			DataBuffer dbB = biB.getData().getDataBuffer();

			int sizeB = dbB.getSize();
			int count = 0;
			// compare data-buffer objects //
			if (sizeA == sizeB) {
				for (int i = 0; i < sizeA; i++) {
					if (dbA.getElem(i) == dbB.getElem(i)) {
						count = count + 1;
					}
				}
				percentage = (count * 100) / sizeA;
				percentage = 100 - percentage;
			} else {
				log.info("Both the images are not of same size");
				throw new Exception("Both the images are not of same size");
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return percentage;
	}

	public static BufferedImage getDifferenceImage(File ActualImage, File BaseLinedImage) throws Exception {
		BufferedImage img1 = ImageIO.read(ActualImage);
		// DataBuffer dbA = img1.getData().getDataBuffer();
		BufferedImage img2 = ImageIO.read(BaseLinedImage);
		// DataBuffer dbB = img2.getData().getDataBuffer();

		// convert images to pixel arrays...
		final int w = img1.getWidth(), h = img1.getHeight(), highlight = Color.RED.getRGB();
		final int[] p1 = img1.getRGB(0, 0, w, h, null, 0, w);
		final int[] p2 = img2.getRGB(0, 0, w, h, null, 0, w);
		// compare pixel by pixel of img1 to img2. Highlight img1's pixels which are
		// different.
		for (int i = 0; i < p1.length; i++) {
			if (p1[i] != p2[i]) {
				p1[i] = highlight;
			}
		}
		// save img1's pixels to a new BufferedImage, and return it...
		// (May require TYPE_INT_ARGB)
		final BufferedImage out = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		out.setRGB(0, 0, w, h, p1, 0, w);
		return out;
	}

	/*
	 * public static void main(String[] args) throws Exception { File f1 = new
	 * File("C:\\Users\\user\\Desktop\\image1.png"); File f2 = new
	 * File("C:\\Users\\user\\Desktop\\image2.png"); log.info(compareImage(f1,f2));
	 * 
	 * BufferedImage bufferedImage = getDifferenceImage(f1,f2);
	 * 
	 * File outputfile = new
	 * File("C:\\Users\\user\\Desktop\\output_differenceimage.png");
	 * ImageIO.write(bufferedImage, "jpg", outputfile);
	 * 
	 * }
	 */

	public static String getDBData(String DBUrl, String DBName, String DBUserName, String DBPassword,
			String QueryStatement, int columnIndex) throws SQLException {
		String data = null;

		String connectionString = "jdbc:sqlserver://" + DBUrl + ";" + "database=" + DBName + ";" + "user=" + DBUserName
				+ ";" + "password=" + DBPassword + ";" + "encrypt=true;" + "trustServerCertificate=true;"
				+ "hostNameInCertificate=*.database.windows.net;" + "loginTimeout=30;";
		Connection connObj = DriverManager.getConnection(connectionString);

		Statement stmtObj = connObj.createStatement();
		ResultSet resObj = stmtObj.executeQuery(QueryStatement);
		while (resObj.next()) {
			data = resObj.getString(columnIndex);
			log.info(data);
		}
		return data;
	}

	public static String getDBData(String DBUrl, String DBName, String DBUserName, String DBPassword,
			String QueryStatement, String columnName) throws SQLException {
		String data = null;

		String connectionString = "jdbc:sqlserver://" + DBUrl + ";" + "database=" + DBName + ";" + "user=" + DBUserName
				+ ";" + "password=" + DBPassword + ";" + "encrypt=true;" + "trustServerCertificate=true;"
				+ "hostNameInCertificate=*.database.windows.net;" + "loginTimeout=30;";
		Connection connObj = DriverManager.getConnection(connectionString);

		Statement stmtObj = connObj.createStatement();
		ResultSet resObj = stmtObj.executeQuery(QueryStatement);
		if (columnName != null) {
			while (resObj.next()) {
				data = resObj.getString(columnName);
				log.info(data);
			}
		}
		return data;
	}

	public static void ExecuteQuery(String DBUrl, String DBName, String DBUserName, String DBPassword,
			String QueryStatement) throws SQLException {
		// String data=null;

		String connectionString = "jdbc:sqlserver://" + DBUrl + ";" + "database=" + DBName + ";" + "user=" + DBUserName
				+ ";" + "password=" + DBPassword + ";" + "encrypt=true;" + "trustServerCertificate=true;"
				+ "hostNameInCertificate=*.database.windows.net;" + "loginTimeout=30;";
		Connection connObj = DriverManager.getConnection(connectionString);

		Statement stmtObj = connObj.createStatement();
		stmtObj.execute(QueryStatement);
	}

	/* Generates Random String */
	public static String getRandomString(int stringSize) {
		// 'E' changed to 'a'
		byte[] stringarray1 = { 'a' };
		byte[] stringarray2 = new byte[stringSize - 1];
		for (int strsizeCounter = 0; strsizeCounter < stringSize - 1; strsizeCounter++) {
			stringarray2[strsizeCounter] = (byte) rand('a', 'z');
		}
		String temprandom = new String(ArrayUtils.addAll(stringarray1, stringarray2));
		// log.debug(temprandom);
		return temprandom;
	}

	public static Random rn = new Random();

	public static int rand(int lo, int hi) {
		int n = hi - lo + 1;
		if (n > 9)
			n = 9;
		int i = rn.nextInt() % n;
		if (i < 0)
			i = -i;
		return lo + i;
	}

	/* Generates Random Numbers */
	public static String getRandomNum(int stringSize) {
		int[] strnumarray = new int[stringSize];
		StringBuffer buf = new StringBuffer();
		for (int strsizeCounter = 0; strsizeCounter < stringSize; strsizeCounter++) {
			strnumarray[strsizeCounter] = rand(0, 9);
			if (strnumarray[0] == 0)
				strnumarray[0] = 1;
			buf.append(strnumarray[strsizeCounter]);
		}
		String temprandom = buf.toString();
		return temprandom;
	}

	/* Generates Random AlphaNumeric Characters */
	public static String getRandomAlphaNumeric(int size) {
		Random rand = new Random();
		String[] charset = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
				"s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		String[] numset = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		StringBuffer sb = new StringBuffer();
		for (int n = 0; n < size - 1; n++) {
			sb = sb.append(charset[rand.nextInt(30)]);
		}

		sb = sb.append(numset[rand.nextInt(9)]);

		return (sb.toString());
	}

	/* Generate Random Email */
	public static String ReEnterEmail;

	public static String GetRandomEmail(int size) {

		String Value = "email";
		if (Value.equalsIgnoreCase("email")) {
			Random rand = new Random();
			String[] charset = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q",
					"r", "s", "t", "u", "v", "w", "x", "y", "z" };
			StringBuffer sb = new StringBuffer();
			for (int n = 0; n < size; n++) {
				sb = sb.append(charset[rand.nextInt(26)]);
			}
			// if(flag==1)
			{
				StringBuffer sb1 = sb.append("@datagenerator.com");
				ReEnterEmail = sb1.toString();
				return (sb1.toString());
			} // end of if
		} else
			return (ReEnterEmail);
	}

	public static void deleteZipFiles(String desFN) {
		try {
			String zipName = System.getProperty("user.dir") + "/" + desFN + ".zip";
			String folderName = System.getProperty("user.dir") + "/" + desFN + ".zip";

			FileUtils.forceDelete(new File(zipName));
			FileUtils.forceDelete(new File(folderName));

		} catch (Exception e) {
			// log.info("No zip folder found to delete" + desFN);
			// log.error(e.getMessage());

		}
	}

	public static void deleteFiles(String desFN) {
		try {
			String zipName = System.getProperty("user.dir") + "/" + desFN;
			FileUtils.deleteDirectory(new File(zipName));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// log.info("No dir found to delete");
			// log.error(e.getMessage());

		}
	}

	public static void reportstoZipFile(String srcfolder, String desFN) throws Exception {

		try {

			// Use the following paths for windows
			String folderToZip = System.getProperty("user.dir") + "/" + srcfolder;
			String zipName = System.getProperty("user.dir") + "/" + desFN + ".zip";

			File f = new File(folderToZip);
			if (f.isDirectory()) {

				final Path sourceFolderPath = Paths.get(folderToZip);
				Path zipPath = Paths.get(zipName);
				final ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipPath.toFile()));
				Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>() {
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						zos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));
						Files.copy(file, zos);
						zos.closeEntry();
						return FileVisitResult.CONTINUE;
					}
				});
				zos.close();
			}
		} catch (Exception e) {
			// log.error(e.getMessage());
		}

	}

	public static void auto_generation_Email(String mailId, String ccMail) throws Exception {

		// Create object of Property file
		Properties props = new Properties();
		props.put("mail.smtp.host", System.getProperty("email.smtp.server"));
		props.put("mail.smtp.socketFactory.port", System.getProperty("email.smtp.socketPortNumber"));
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", System.getProperty("email.smtp.portNumber"));
		props.put("mail.smtp.starttls.enable", "true");

		// This will handle the complete authentication
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(System.getProperty("email.user.emailId"),
						System.getProperty("email.user.password"));
			}
		});

		try {

			// Create object of MimeMessage class
			Message message = new MimeMessage(session);

			// Set the from address
			message.setFrom(new InternetAddress(System.getProperty("email.user.emailAddress")));
			// String mailToId2 = "chandan.navara@royalcyber.com";
			// String mailToId3 = "swathin@royalcyber.com";
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailId));
			// message.addRecipients(Message.RecipientType.CC,
			// InternetAddress.parse(mailToId2));
			message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccMail));
			// Set the recipient
			// address
			String lDate = LocalDate.now().toString();
			String lTime = LocalTime.now().toString();
			// Add the subject link

			Collection<String> values = Hooks.scenarioStatus.values();
			String flag;
			int failCount = 0;
			int passCount = 0;
			int skipCount = 0;
			for (String x : values) {
				if (x.equalsIgnoreCase("Failed")) {
					failCount++;
				} else if (x.equalsIgnoreCase("Passed")) {
					passCount++;
				} else {
					skipCount++;
				}
			}
			if (values.contains("FAILED") || values.contains("SKIPPED")) {
				flag = "Build Failed";
			} else {
				flag = "Build Passed";
			}
			String subject = "RC TEAF Execution Report - " + ":" + " on " + lDate + ": " + lTime + " :" + flag;
			message.setSubject(subject);
			MimeMultipart multipart = new MimeMultipart("related");

			// Create object to add multimedia type content
			BodyPart messageBodyPart1 = new MimeBodyPart();

			StringBuffer ScenarioTable = new StringBuffer();
			Map<String, String> mp = new TreeMap<String, String>();
			mp.putAll(Hooks.scenarioStatus);
			Set<Entry<String, String>> entrySet = mp.entrySet();

			for (Entry<String, String> entry : entrySet) {

				if (entry.getValue().equalsIgnoreCase("passed")) {
					ScenarioTable.append("<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + entry.getKey()
							+ "</TD><TD bgcolor= '#419c4d' >" + entry.getValue() + "</TD> " + "</TR>");
				} else {
					ScenarioTable.append("<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + entry.getKey()
							+ "</TD><TD bgcolor= '#d63a29' >" + entry.getValue() + "</TD> " + "</TR>");
				}

			}
			// Set the body of email
			String htmlText = "<img src=\"cid:image\"><BR><H1>Script Execution Summary | Email Report </H1><BR>"
					+ "<BR><H1>TEST URL : " + System.getProperty("test.appUrl") + "</H1><BR>"
					+ "<TABLE WIDTH='60%' CELLPADDING='4' CELLSPACING='1'>"
					+ "<TR> <TH bgcolor = '#a8a7a7' COLSPAN='4'><H2>Test Execution Summary</H2></TH></TR><TR><TH>Total Test Cases: </TH><TH>Total TCs Passed: </TH><TH>Total TCs Failed: </TH><TH>Total TCs Skipped: </TH>"
					+ "</TR>" + "<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + values.size()
					+ "</TD><TD bgcolor= '#419c4d' >" + passCount + "</TD><TD bgcolor= '#d63a29' >" + failCount
					+ "</TD><TD bgcolor= '#6bbbc7' >" + skipCount + "</TD>" + "</TR>"
					+ "</TABLE><H4>Please find attached a detailed Test Automation execution report using RC TEAF with this email</H4> "
					+ "\n" + "\n" + "<H4>Test Scenarios executed:</H4>"
					+ "<TABLE WIDTH='60%' CELLPADDING='4' CELLSPACING='1'>"
					+ "<TR ALIGN='CENTER' bgcolor ='#3767B6'><TD color: 'white'> Scenario Name </TD><TD bgcolor ='#3767B6' color: 'white'>Result</TD></TR>"
					+ ScenarioTable + "</TABLE>" + "\n" + "\n" +

					"Thanks, " + "\n" + "\n" + "RC QA";
			messageBodyPart1.setContent(htmlText, "text/html");
			// add it
			multipart.addBodyPart(messageBodyPart1);

			// second part (the image)
			messageBodyPart1 = new MimeBodyPart();
			DataSource fds = new FileDataSource(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\teaf.jpeg");

			messageBodyPart1.setDataHandler(new DataHandler(fds));
			messageBodyPart1.setHeader("Content-ID", "<image>");

			// add image to the multipart
			multipart.addBodyPart(messageBodyPart1);

			// Create another object to add another content
			MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			MimeBodyPart messageBodyPart3 = new MimeBodyPart();

			// Mention the file which you want to send
			String fEN = System.getProperty("user.dir") + "/TestExecution_ExtentReports.zip";
			String fUI = System.getProperty("user.dir") + "/TestExecution_UIReports.zip";

			multipart.addBodyPart(messageBodyPart1);

			if (java.nio.file.Files.exists(Paths.get(fEN), LinkOption.NOFOLLOW_LINKS)) {
				DataSource src = new FileDataSource(fEN);
				// set the handler
				messageBodyPart2.setDataHandler(new DataHandler(src));
				// set the file
				messageBodyPart2.setFileName(fEN);
				multipart.addBodyPart(messageBodyPart2);
			}

			if (java.nio.file.Files.exists(Paths.get(fUI), LinkOption.NOFOLLOW_LINKS)) {
				DataSource source1 = new FileDataSource(fUI);

				// set the handler
				messageBodyPart3.setDataHandler(new DataHandler(source1));

				// set the file
				messageBodyPart3.setFileName(fUI);
				multipart.addBodyPart(messageBodyPart3);

			}
			message.setContent(multipart);

			// finally send the email
			Transport.send(message);

			log.info("\n =====Reports Sent through Email from " + System.getProperty("email.user.emailAddress")
					+ "=====");

		} catch (MessagingException e) {
			log.error(e.getMessage());
		} finally {
			Utilities.deleteZipFiles("TestExecution_ExtentReports");
			Utilities.deleteZipFiles("TestExecution_UIReports");
		}
	}

	public static void testStatusToastMessage(String message) throws InterruptedException {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Check for jQuery on the page, add it if need be
			js.executeScript("if (!window.jQuery) {"
					+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
					+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
					+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
			Thread.sleep(1000);
			// Use jQuery to add jquery-growl to the page
			js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");
			// js.executeScript("$.getScript('/Users/NaveenKhunteta/Documents/workspace/Test/src/testcases/jquery.growl.js')");

			// Use jQuery to add jquery-growl styles to the page
			js.executeScript("$('head').append('<link id=\"scenariotoast\" rel=\"stylesheet\" "
					+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
			Thread.sleep(1000);
			// jquery-growl w/ no frills
			js.executeScript("$.growl({ title: 'Scenario', message: '" + message + "' });");

			Thread.sleep(3000);

			js.executeScript("var element = document.getElementById('scenariotoast');"
					+ "element.parentNode.removeChild(element);");
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());

		}

	}

	public static void testStatusFailToastMessage(String message) throws InterruptedException {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Check for jQuery on the page, add it if need be
			js.executeScript("if (!window.jQuery) {"
					+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
					+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
					+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
			Thread.sleep(1000);
			// Use jQuery to add jquery-growl to the page
			js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");
			// js.executeScript("$.getScript('/Users/NaveenKhunteta/Documents/workspace/Test/src/testcases/jquery.growl.js')");

			// Use jQuery to add jquery-growl styles to the page
			js.executeScript("$('head').append('<link id=\"failtoast\" rel=\"stylesheet\" "
					+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
			Thread.sleep(1000);
			// jquery-growl w/ no frills
			js.executeScript("$.growl.error({ title: 'Failed', message: '" + message + "' });");

			Thread.sleep(1000);

			js.executeScript(
					"var element = document.getElementById('failtoast');" + "element.parentNode.removeChild(element);");
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());

		}

	}

	public static void testStatusToastPass(String message) throws InterruptedException {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Check for jQuery on the page, add it if need be
			js.executeScript("if (!window.jQuery) {"
					+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
					+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
					+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
			Thread.sleep(1000);
			// Use jQuery to add jquery-growl to the page
			js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");
			// js.executeScript("$.getScript('/Users/NaveenKhunteta/Documents/workspace/Test/src/testcases/jquery.growl.js')");

			// Use jQuery to add jquery-growl styles to the page
			js.executeScript("$('head').append('<link id=\"passtoast\" rel=\"stylesheet\" "
					+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
			Thread.sleep(1000);
			// jquery-growl w/ no frills
			js.executeScript("$.growl.notice({ title: 'Passed', message: '" + message + "' });");
			Thread.sleep(3000);

			js.executeScript(
					"var element = document.getElementById('passtoast');" + "element.parentNode.removeChild(element);");
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());

		}

	}

	/********************
	 * Email Handling Functions
	 *****************************************/

	private String getTextFromMessage(Message message) throws IOException, MessagingException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws IOException, MessagingException {

		int count = mimeMultipart.getCount();
		if (count == 0)
			throw new MessagingException("Multipart with no body parts not supported.");
		boolean multipartAlt = new ContentType(mimeMultipart.getContentType()).match("multipart/alternative");
		if (multipartAlt)
			// alternatives appear in an order of increasing
			// faithfulness to the original content. Customize as req'd.
			return getTextFromBodyPart(mimeMultipart.getBodyPart(count - 1));
		String result = "";
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			result += getTextFromBodyPart(bodyPart);
		}
		return result;
	}

	private String getTextFromBodyPart(BodyPart bodyPart) throws IOException, MessagingException {

		String result = "";
		if (bodyPart.isMimeType("text/plain")) {
			result = (String) bodyPart.getContent();
		} else if (bodyPart.isMimeType("text/html")) {
			String html = (String) bodyPart.getContent();
			result = org.jsoup.Jsoup.parse(html).text();
		} else if (bodyPart.getContent() instanceof MimeMultipart) {
			result = getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
		}
		return result;
	}

	/*
	 * public static void main(String[] args) throws Exception { // Create client
	 * object and connect it to Nylas using // an account's access token NylasClient
	 * client = new NylasClient(); // Provide the access token for a specific
	 * account NylasAccount account = client.account("ACCESS_TOKEN");
	 * 
	 * // Return the most recent email message Message message = messages.list(new
	 * MessageQuery().limit(1)).get(0);
	 * System.out.printf("Subject: %s | Unread: %s | ID: %s\n",
	 * message.getSubject(), message.getUnread(), message.getId() );
	 * 
	 * // Return the 5 most recent unread threads. ThreadQuery unread = new
	 * ThreadQuery().limit(5).unread(true); List<Thread> threads =
	 * account.threads().list(unread);
	 * 
	 * for (Thread thread : threads) { StringBuilder participants = new
	 * StringBuilder(); for (NameEmail participant : thread.getParticipants()) {
	 * participants.append(participant.getEmail()).append(" "); }
	 * System.out.printf("Subject: %s | Participants: %s\n", thread.getSubject(),
	 * participants ); } // Search for the most recent email from a specific address
	 * Message message = account.messages().search("from:swag@nylas.com").get(0);
	 * System.out.println(message.getSubject()); }
	 */

	public String readRecentMail_byCount_and_Subject(String host, String storeType, String user, String password,
			int mailCount, String expectedSubject, String fromAddr) {
		try {

			// create properties field
			Properties properties = new Properties();

			properties.put("mail.pop3.host", host);
			properties.put("mail.pop3.port", "995");
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);

			// create the POP3 store object and connect with the pop server
			Store store = emailSession.getStore("pop3s");

			store.connect(host, user, password);

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
			System.out.println("messages.length---" + messages.length);

			int n = messages.length;
			int EmailNumber = 0;
			String Subject = "";
			String From = "";
			String emailContent = "";
			boolean mailFound = false;
			for (int i = n - 1; i >= n - mailCount; i--) {
				Message message = messages[i];

				if (message.getSubject().equalsIgnoreCase(expectedSubject)
						&& message.getFrom()[0].toString().contains(fromAddr)) {
					EmailNumber = (i + 1);
					Subject = message.getSubject();
					From = message.getFrom()[0].toString();
					emailContent = message.getContent().toString();
					mailFound = true;
					break;
					// System.out.println("Text: " + message.getContent().toString());
				}
			}

			if (mailFound) {
				System.out.println("Email Number: " + EmailNumber);
				System.out.println("Subject: " + Subject);
				System.out.println("From: " + From);
				System.out.println("emailContent: " + emailContent);
			} else {
				System.out.println("No Mails from " + fromAddr + " with Subject: " + expectedSubject + " in the last "
						+ mailCount + " mails");
			}

			// close the store and folder objects
			emailFolder.close(false);
			store.close();
			return emailContent;
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/********************
	 * Email Handling Functions
	 *****************************************/

	public static String get_MFRM_VerificationCode(String mailContent) {
		String code = "";
		try {
			code = StringUtils.substringBetween(mailContent, "Verification Code: ", "\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return code;
	}

	public static String get_SF_VerificationCode(String mailBodyContent) {
		String code = "";
		try {
			code = StringUtils.substringBetween(mailBodyContent, "Verification Code: ", "\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return code;
	}

	public static void main(String[] args) {
		Utilities util = new Utilities();
		String host = "outlook.office365.com";// change accordingly
		String mailStoreType = "imaps";
		String username = "muhammad.hasan@royalcyber.com";// change accordingly
		String password = "Spur!@#$%";// change accordingly

		String body = util.readRecentMail_byCount_and_Subject(host, mailStoreType, username, password, 5,
				"[EXTERNAL]Sandbox: Verify your identity in Salesforce", "noreply@salesforce.com");
		System.out.println(util.get_SF_VerificationCode(body));
	}

	public void CheckPDFContent(String filePath, String fileName, String fieldName, String Value) {
		try {
			File pdfFile = new File(filePath + fileName);
			PDDocument pdfDoc = PDDocument.load(pdfFile);
			int pageCount = pdfDoc.getNumberOfPages();
			System.out.println("Total pages in PDF: " + pageCount);
			// assertEquals("Page Count Mismatch", 1,pageCount);
			PDFTextStripper pdfTextStripper = new PDFTextStripper();
			String pdfContent = pdfTextStripper.getText(pdfDoc);
			System.out.println("Page Content: " + pdfContent);
			assertTrue(fieldName + " not present in PDF File", pdfContent.contains(Value));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String Fetch_Last_email_MatchingSubject_FromGmail(String username, String password, String subject)
			throws Exception {
		String mailBodyContent = null;
		try {

			String host = "imap.gmail.com";// change accordingly

			// String username = "hasantestuser@gmail.com";// change accordingly

			// String password = "Hasan123!@#";// change accordingly

			Properties props = new Properties();

			props.setProperty("mail.imap.starttls.enable", "true");

			// set any other needed mail.imap.* properties here

			Session session = Session.getInstance(props);

			Store store = session.getStore("imaps");

			store.connect(host, username, password);

			System.out.println(store.isConnected());

			store.getDefaultFolder();

			System.out.println();

			IMAPFolder folder = (IMAPFolder) store.getFolder("Inbox");

			folder.open(Folder.READ_WRITE);

			Message[] messages = folder.getMessages();

			for (int i = 1; i < 3; i++) {
				Message message = messages[messages.length - i];
				if (message.getSubject().equals(subject)) {
					System.out.println("Subject: " + message.getSubject() + " Message date/time: "
							+ message.getReceivedDate() + " Message number: " + message.getMessageNumber());
					if (message.isMimeType("multipart/*")) {
						Multipart Multipart = (Multipart) message.getContent();
						mailBodyContent = getContentFromMultipartMessage(Multipart);
						System.out.println("i= " + i + " Result: " + mailBodyContent);
						break;
					}
				}
			}
			/*
			 * for(Message message : messages ) { System.out.println(message.getSubject());
			 * if(message.getSubject().equals("Sandbox: Let's reset your password.")) {
			 * System.out.println("Content Type: "+message.getContentType()); Multipart
			 * Multipart = (Multipart) message.getContent(); int count =
			 * Multipart.getCount();
			 * 
			 * for (int i = 0; i < count; i++) { //System.out.println(
			 * "Message body: "+Multipart.getBodyPart(i)); if
			 * (message.isMimeType("multipart/*")) { mailBodyContent =
			 * getContentFromMultipartMessage(Multipart);
			 * System.out.println("i= "+i+" Result: "+mailBodyContent); Document doc =
			 * Jsoup.parse(readURL()); Elements p_tags = doc.select("a"); String attr =
			 * null; for (Element p : p_tags) { // System.out.println("P tag is "+p.text());
			 * //if (p.text().equals("RESET NOW")) {
			 * 
			 * attr = p.attr("href"); System.out.println(attr); } } } }
			 * 
			 * }
			 */
			store.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mailBodyContent;
	}

	private static String getContentFromMultipartMessage(Multipart mimeMultipart)
			throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			IMAPBodyPart bodyPart = (IMAPBodyPart) mimeMultipart.getBodyPart(i);
			// System.out.println(i+" Content Type: "+bodyPart.getContentType());
			if (bodyPart.isMimeType("TEXT/HTML; charset=utf-8")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getContentFromMultipartMessage((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}

	public static void writePart(Part p) throws Exception {
		if (p instanceof Message)
			// Call methos writeEnvelope
			writeEnvelope((Message) p);
		// System.out.println("----------------------------");
		System.out.println("CONTENT-TYPE: " + p.getContentType());
		// check if the content is plain text
		if (p.isMimeType("text/plain")) {
			System.out.println("This is plain text");
			System.out.println("---------------------------");
			System.out.println((String) p.getContent());
		}
		// check if the content has attachment
		else if (p.isMimeType("multipart/*")) {
			System.out.println("This is a Multipart");
			System.out.println("---------------------------");
			Multipart mp = (Multipart) p.getContent();
			int count = mp.getCount();
			for (int i = 0; i < count; i++)
				writePart(mp.getBodyPart(i));
		}
		// check if the content is a nested message
		else if (p.isMimeType("message/rfc822")) {
			System.out.println("This is a Nested Message");
			System.out.println("---------------------------");
			writePart((Part) p.getContent());
		}
		// check if the content is an inline image
//			      else if (p.isMimeType("image/jpeg")) {
//			         System.out.println("--------> image/jpeg");
//			         Object o = p.getContent();
//			         InputStream x = (InputStream) o;
//			         // Construct the required byte array
//			         System.out.println("x.length = " + x.available());
//			         while ((i = (int) ((InputStream) x).available()) > 0) {
//			            int result = (int) (((InputStream) x).read(bArray));
//			            if (result == -1)
//			         int i = 0;
//			         byte[] bArray = new byte[x.available()];
//			            break;
//			         }
//			         FileOutputStream f2 = new FileOutputStream("/tmp/image.jpg");
//			         f2.write(bArray);
//			      } 
		else if (p.getContentType().contains("image/")) {
			System.out.println("content type" + p.getContentType());
			File f = new File("image" + new Date().getTime() + ".jpg");
			DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(f)));
			com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p.getContent();
			byte[] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = test.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
		} else {
			Object o = p.getContent();
			if (o instanceof String) {
				System.out.println("This is a string");
				System.out.println("---------------------------");
//			            System.out.println((String) o);
			}
//			         else if (o instanceof InputStream) {
//			            System.out.println("This is just an input stream");
//			            System.out.println("---------------------------");
//			            InputStream is = (InputStream) o;
//			            is = (InputStream) o;
//			            int c;
//			            while ((c = is.read()) != -1)
//			               System.out.write(c);
//			         } 
			else {
				System.out.println("This is an unknown type");
				System.out.println("---------------------------");
				// System.out.println(o.toString());
			}
		}
	}

	/*
	 * This method would print FROM,TO and SUBJECT of the message
	 */
	public static void writeEnvelope(Message m) throws Exception {
		System.out.println("This is the message envelope");
		System.out.println("---------------------------");
		Address[] a;
		// FROM
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("FROM: " + a[j].toString());
		}
		// TO
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("TO: " + a[j].toString());
		}
		// SUBJECT
		if (m.getSubject() != null)
			System.out.println("SUBJECT: " + m.getSubject());
	}

	public static String Fetch_Last_email_MatchingSubject_FromGmail_To_Reply(String username, String password,
			String subject) throws Exception {
		String mailBodyContent = null;
		String sub = null;
		String writePart = "";

		try {

			String host = "imap.gmail.com";// change accordingly

			// String username = "hasantestuser@gmail.com";// change accordingly

			// String password = "Hasan123!@#";// change accordingly

			Properties props = new Properties();
			props.put("mail.store.protocol", "pop3");
			props.put("mail.pop3.host", host);
			props.put("mail.pop3.port", "995");
			props.setProperty("mail.imap.starttls.enable", "true");

			// set any other needed mail.imap.* properties here

			Session session = Session.getInstance(props);

			Store store = session.getStore("imaps");

			store.connect(host, username, password);

			System.out.println(store.isConnected());

			store.getDefaultFolder();

			System.out.println();

			IMAPFolder folder = (IMAPFolder) store.getFolder("Inbox");

			folder.open(Folder.READ_ONLY);

			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			Message[] messages = folder.getMessages();
			System.out.println("messages.length---" + messages.length);
			for (int i = messages.length; i >= messages.length - 15; i--) {
				Message message = messages[i - 1];
				System.out.println(message.getSubject());

				if (message.getSubject().contains(subject) && sub == null) {
					log.info("Subject Matched " + message.getSubject() + "with " + subject);
					sub = message.getSubject().toString();
					System.out.println("---------------------------------");
					writePart(message);
////			            String line = reader.readLine();
//			            if ("YES".equals(line)) {
//			               message.writeTo(System.out);
//			            } else if ("QUIT".equals(line)) {
					break;
				}
			}
//			         }

//					Message[] messages = folder.getMessages();
//					
//					for(int i=1; i<3;i++) {
//			            Message message = messages[messages.length-i];
//			                if(message.getSubject().equals(subject)) {
//			                System.out.println("Subject: "+message.getSubject()+" Message date/time: "+message.getReceivedDate()+" Message number: "+message.getMessageNumber());
//			                if (message.isMimeType("multipart/*")) {
//			                	Multipart Multipart = (Multipart) message.getContent();
//							    mailBodyContent = getContentFromMultipartMessage(Multipart);
//						        System.out.println("i= "+i+" Result: "+mailBodyContent);
//			                break;
//			                }
//			            }
//					}
			/*
			 * for(Message message : messages ) { System.out.println(message.getSubject());
			 * if(message.getSubject().equals("Sandbox: Let's reset your password.")) {
			 * System.out.println("Content Type: "+message.getContentType()); Multipart
			 * Multipart = (Multipart) message.getContent(); int count =
			 * Multipart.getCount();
			 * 
			 * for (int i = 0; i < count; i++) { //System.out.println(
			 * "Message body: "+Multipart.getBodyPart(i)); if
			 * (message.isMimeType("multipart/*")) { mailBodyContent =
			 * getContentFromMultipartMessage(Multipart);
			 * System.out.println("i= "+i+" Result: "+mailBodyContent); Document doc =
			 * Jsoup.parse(readURL()); Elements p_tags = doc.select("a"); String attr =
			 * null; for (Element p : p_tags) { // System.out.println("P tag is "+p.text());
			 * //if (p.text().equals("RESET NOW")) {
			 * 
			 * attr = p.attr("href"); System.out.println(attr); } } } }
			 * 
			 * }
			 */
			folder.close(false);
			store.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return writePart;
	}

	private static String getContentFromMultipartMessage1(Multipart mimeMultipart)
			throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			IMAPBodyPart bodyPart = (IMAPBodyPart) mimeMultipart.getBodyPart(i);
//			       System.out.println(i+" Content Type: "+bodyPart.getContentType());
			if (bodyPart.isMimeType("TEXT/HTML; charset=utf-8")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getContentFromMultipartMessage((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}

	/********************
	 * Email Replying Functions
	 * 
	 * @return
	 *****************************************/
//			
	public static void sendEmail(String toEmail, String body) {

		// String host = "imap.gmail.com";// change accordingly
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		Properties props = new Properties();
		props.setProperty("mail.imap.starttls.enable", "true");

		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.auth", "true");
		props.put("mail.debug", "true");
		props.put("mail.store.protocol", "pop3");
		props.put("mail.transport.protocol", "smtp");
		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("hasantestuser@gmail.com", "Hasan123!@#");

			}
		});
		try {
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", "hasantestuser@gmail.com", "Hasan123!@#");

			// Create a Folder object and open the folder
			IMAPFolder folder = (IMAPFolder) store.getFolder("Inbox");

			folder.open(Folder.READ_ONLY);

			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			Message[] messages = folder.getMessages();
			System.out.println("messages.length---" + messages.length);
			if (messages.length != 0) {
				for (int i = messages.length; i >= messages.length - 15; i--) {
					Message message = messages[i - 1]; // Get all the information from the message
					String from = InternetAddress.toString(message.getFrom());
					if (from != null) {
						System.out.println("From: " + from);
					}
					String replyTo = InternetAddress.toString(message.getReplyTo());
					if (replyTo != null) {
						System.out.println("Reply-to: " + replyTo);
					}
					String to = InternetAddress.toString(message.getRecipients(Message.RecipientType.TO));
					if (to != null) {
						System.out.println("To: " + to);
					}
					String subject = message.getSubject();
					if (subject != null) {
						System.out.println("Subject: " + subject);
					}
					Date sent = message.getSentDate();
					if (sent != null) {
						System.out.println("Sent: " + sent);
					}
					System.out.println(message.getContent());

					// compose the message to forward
					Message message2 = new MimeMessage(session);
					message2 = (MimeMessage) message.reply(false);
					message2.setSubject("RE: " + message.getSubject());
					message2.setFrom(new InternetAddress(from));
					message2.setReplyTo(message.getReplyTo());
//                      	   message2.addRecipient(Message.RecipientType.TO, new InternetAddress(to));  
					message2.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));

					// Create your new message part
					BodyPart messageBodyPart = new MimeBodyPart();
					messageBodyPart.setText(body);

					// Create a multi-part to combine the parts
					Multipart multipart = new MimeMultipart();
					multipart.addBodyPart(messageBodyPart);

					// Create and fill part for the forwarded content
					messageBodyPart = new MimeBodyPart();
					messageBodyPart.setDataHandler(message.getDataHandler());

					// Add part to multi part
					multipart.addBodyPart(messageBodyPart);

					// Associate multi-part with message
					message2.setContent(multipart);

					// Send message
					Transport.send(message2);

					System.out.println("message replied successfully ....");
					break;
				}

			}

			folder.close(false);
			store.close();

			// store.getDefaultFolder();
//                MimeMessage msg = new MimeMessage(session);
//                //set message headers
//                msg.setFrom(new InternetAddress(fromEmail));
//                msg.setReplyTo(InternetAddress.parse(toEmail, false));
//                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
////                msg.setSubject(subject, "Sandbox: Case");
//                msg.setText(body);
//
//                msg.setSentDate(new Date());
//                System.out.println("Message is ready");
//                Transport.send(msg);  
//                System.out.println("EMail Sent Successfully!!");
//              }
		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String getTestData(String key) {
		objConfig = new Properties();
		try {
			objConfig.load(
					new FileInputStream(System.getProperty("user.dir") + "/src/test/java/com/TEAF/TestFiles/Config/"
							+ System.getProperty("test_env_URL") + "_configuration.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objConfig.getProperty(key);

	}

	public static void sendEmailToRecipient(String toEmail, String body, String emailSubject) {

		// String host = "imap.gmail.com";// change accordingly
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		Properties props = new Properties();
		props.setProperty("mail.imap.starttls.enable", "true");

		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.auth", "true");
		props.put("mail.debug", "true");
		props.put("mail.store.protocol", "pop3");
		props.put("mail.transport.protocol", "smtp");
		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("hasantestuser@gmail.com", "Hasan123!@#");

			}
		});
		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress("dowjonestest0@gmail.com"));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));

			// Set Subject: header field
			message.setSubject(emailSubject);

			// Now set the actual message
			message.setText(body);

			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}
}

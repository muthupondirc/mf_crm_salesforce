package com.TEAF.framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import org.openqa.selenium.By;

import cucumber.runtime.CucumberException;

import org.apache.log4j.Logger;
import org.apache.poi.EmptyFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @ScriptName : PageObjectAccess
 * @Description : This class contains Page Object mapping logic to support .csv
 *              page object repository. (Can be modified to additionally support
 *              Excel / Access DB / DB object repository storage)
 * @Author : Swathin Ratheendren
 * @Creation Date : September 2016 @Modified Date:
 */
public class GetPageObjectRead {
	static String PageObjectFilesPath = "/src/test/java/com/TEAF/TestFiles/PageObjects";
	static ClassLoader cl;
	static Class<?> cls;
	public static BufferedReader OR;
	static By by;
	static String[] LocatorType;
	static String[] urlParameter;
	static String url;
	static String ClassPath;
	static String elementName;
	static String locator;
	public static String App;

	static Logger log = Logger.getLogger(GetPageObjectRead.class);

	public static String OR_GetURL(String app) {
		try {
			App = app;
			if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("csv")) {
				GetPageObjectRead.ReadCSV(app);
				urlParameter = HashMapContainer.getPO(app.toLowerCase()).split(",");
				url = urlParameter[1];
				// System.out.println("URL: " + url);
				log.info("URL: " + url);
			} else if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("class")) {
				GetPageObjectRead.ReadPOClass(app);
				url = HashMapContainer.getPO("url");
				// System.out.println("URL: " + url);
				log.info("URL: " + url);

			} else if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("xlsx")) {
				log.info("App: " + app);
				if (GetPageObjectRead.ReadExcel(app)) {
					// System.out.println("Locators File read completed successfully!");
					log.info("Locators File read completed successfully!");

					urlParameter = HashMapContainer.getPO(app.toLowerCase()).split(",");
					try {
						url = System.getProperty("test.appUrl", urlParameter[1]);
					} catch (NullPointerException e) {
						url = urlParameter[1];
					}
					// System.out.println("URL: " + url);
					log.info("GetPageObjectRead Class - URL: " + url);

				} else {
					log.info("AUT Folder or Locator Files not available! ");
					// System.out.println("AUT Folder or Locator Files not available! ");
					log.info("New App Locator Folder Created: " + System.getProperty("user.dir")
							+ "/src/test/java/com/TEAF/TestFiles/PageObjects/" + app);

//					System.out.println("New App Locator Folder Created: " + System.getProperty("user.dir")
//							+ "/src/test/java/com/TEAF/TestFiles/PageObjects/" + app);
					url = System.getProperty("test.appUrl");
					// System.out.println("URL: " + url);
					log.info("URL: " + url);

				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
			log.error(e);
		}
		System.out.println("---------------------------------------------------------URLLLL: "+url);
		return url.toString().trim();
	}

	public static By OR_GetElement(String element) {
		try {
			String Locator = HashMapContainer.getPO(element);
			if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("xlsx")) {
				LocatorType = Locator.split(",", 2);
			} else if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("class")) {
				log.debug("Element: " + element);
				log.debug("HM Value of Element: " + HashMapContainer.getPO(element));
				log.debug("Locator: " + Locator);
				// System.out.println("Element: "+element);
				// System.out.println("HM Value of Element: "+ HashMapContainer.getPO(element));
				// System.out.println("Locator: "+Locator);
				LocatorType = Locator.split("=", 2);

			}
		} catch (Exception e) {
			log.error(e);
			// e.printStackTrace();
			log.info("Verify entries of PageObject data in your \"" + System.getProperty("test.pageObjectMode")
					+ "\" file!: " + element);
//			System.out.println("Verify entries of PageObject data in your \""
//					+ System.getProperty("test.pageObjectMode") + "\" file!: " + element);
			throw new CucumberException(e);
		}
		return WrapperFunctions.setLocator(LocatorType[0].toString().trim(), LocatorType[1].toString().trim());
	}

	public void add(String Element) {
		// System.out.println("Element "+Element);
		String[] ElementRef = Element.split("::");
		// System.out.println("ElemenetRef0 "+ElementRef[0]);
		// System.out.println("ElemenetRef1 "+ElementRef[1]);
		HashMapContainer.addPO(ElementRef[0].toLowerCase(), ElementRef[1]);
	}

	public static void ReadCSV(String fileName) {
		try {
			String sCSVPath = "";
			// Based on device Type
			sCSVPath = System.getProperty("user.dir") + PageObjectFilesPath + "/" + fileName + ".csv";

			File ORFile = new File(sCSVPath);
			FileInputStream fis = new FileInputStream(ORFile);
			// DataInputStream dis = new DataInputStream(fis);
			OR = new BufferedReader(new InputStreamReader(fis, Charset.forName("windows-1252")));

			OR.readLine();

			while (OR.ready()) {
				String ORLines = OR.readLine();
				String[] LocatorName = ORLines.split(",", 2);
				HashMapContainer.addPO(LocatorName[0].toString().trim().toLowerCase(),
						LocatorName[1].toString().trim());
			}
			System.out.println("Page Objects load completed!");
			log.info("Page Objects load completed!");
		} catch (IOException e) {
			//e.printStackTrace();
			log.error(e);
		} catch (Exception e) {
			//e.printStackTrace();
			log.error(e);

		}
	}

	public static boolean ReadExcel(String fileName) {
		try {
			String xlsxPath = "";
			// Based on device Type
			xlsxPath = System.getProperty("user.dir") + PageObjectFilesPath + "/" + fileName;
			File locatorFileFolder = new File(xlsxPath);
			if (!locatorFileFolder.exists()) {
				locatorFileFolder.mkdir();
				return false;
			}
		//	System.out.println(xlsxPath);
			log.info(xlsxPath);
			File dir = new File(xlsxPath);
			File[] listFiles = dir.listFiles();
			if (listFiles.length == 0) {
				log.info("No Locator files present in folder:" + xlsxPath);
			//	System.out.println("No Locator files present in folder:" + xlsxPath);
				return false;
			} else {
				for (int i = 0; i < listFiles.length; i++) {
					log.info("Page object File(Count and Path): " + (i + 1) + " : " + listFiles[i]);
					//System.out.println("Page object File(Count and Path): " + (i + 1) + " : " + listFiles[i]);
				}
				for (File file : listFiles) {
					if (file.toString().endsWith(".xlsx")) {
						FileInputStream fin = new FileInputStream(file);
						XSSFWorkbook workbook;
						try {
							workbook = new XSSFWorkbook(fin);

							for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
								XSSFSheet sheet = workbook.getSheetAt(i);
								for (int j = 1; j < sheet.getPhysicalNumberOfRows(); j++) {
									Row row = sheet.getRow(j);
									Cell cell = row.getCell(0, MissingCellPolicy.CREATE_NULL_AS_BLANK);
									elementName = cell.getStringCellValue();
									elementName = elementName.toLowerCase();
									String sK = row.getCell(1, MissingCellPolicy.CREATE_NULL_AS_BLANK)
											.getStringCellValue();
									Cell cellV = row.getCell(2, MissingCellPolicy.CREATE_NULL_AS_BLANK);
									String sV = null;
									if (cellV.getCellType().equals(CellType.STRING)) {
										sV = row.getCell(2, MissingCellPolicy.CREATE_NULL_AS_BLANK)
												.getStringCellValue();
									} else if (cellV.getCellType().equals(CellType.NUMERIC)) {
										double d = row.getCell(2, MissingCellPolicy.CREATE_NULL_AS_BLANK)
												.getNumericCellValue();
										long l = (long) d;
										sV = String.valueOf(l);
									}
									log.debug("Locator: "+sK+" "+sV);
									// System.out.println("Locator: "+sK+" "+sV);
									locator = sK + "," + sV;
									if (locator.length() > 2) {
										log.debug("ElementName: "+elementName);
										log.debug("Locator: "+locator);
										// System.out.println("ElementName: "+elementName);
										// System.out.println("Locator: "+locator);
										HashMapContainer.addPO(elementName, locator);
									}
								}
							}
						} catch (EmptyFileException e) {
							// TODO Auto-generated catch block
							//System.out.println("Locator File is Empty");
							log.error(e);
							log.info("Locator File is Empty");
							return false;
						} catch (Exception e) {
							// TODO: handle exception
						//	e.printStackTrace();
							log.error(e);
							
							throw new CucumberException(e);
						}

					}
				}
			}
			//System.out.println("Page Objects load completed!");
			log.info("Page Objects load completed!");
		} catch (Exception e) {
			//e.printStackTrace();
			log.error(e);
		}
		return true;
	}

	public static void ReadPOClass(String className) throws Exception {
		try {

		//	System.out.println(StepBase.testPlatform);
			log.info(StepBase.testPlatform);
			// Based on device Type
			ClassPath = "com.TEAF.TestFiles.PageObjects.";

			/*
			 * File file = new File(ClassPath); // Convert File to a URL URL url =
			 * file.toURI().toURL(); URL[] urls = new URL[]{url};
			 * System.out.println("ClassPath: "+url.toString()); // Create a new class
			 * loader with the directory cl = new URLClassLoader(urls); cls =
			 * cl.loadClass(className); cls.getMethod("loadallPageObjects");
			 */

			// File file = new File("./src/test/java/");
			// URL url = file.toURI().toURL();
			// Class<?> cls = Class.forName(ClassPath+className);
			Class<?> cls = Class.forName(ClassPath + className);
			Object obj = cls.getConstructor().newInstance();
			Class<?> noparams[] = {};
			Method method = cls.getDeclaredMethod("loadallPageObjects", noparams);
			method.invoke(obj);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

}

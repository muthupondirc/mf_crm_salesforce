@S5_ChatCustomQuickTextComponent_Storefront
Feature: Chat(IS)-Create a CustomQuick Text for mattressfirm.com in Storefront
 1. To verify that Chat (IS) - Custom QuickText Component - SP5_TC2
 
Scenario: 1. To verify that Chat (IS) - Custom QuickText Component - SP5_TC2
Given My WebApp 'MF_SalesForce' is open 
And I wait for '60' seconds
#waiting for Agent kim to be Online in OmniChannel
#------------------QA URL------------------
#And I navigate to 'https://storefront:face@dev.mattressfirm.com/' application
#------------------QA URL------------------
And I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' application
And I should see element 'CookiesCloseButton_Hybrid' present on page
And I click by JS 'CookiesCloseButton_Hybrid'
And I wait for visibility of element 'MattressSite_CloseButton'
And I click 'MattressSite_CloseButton'
#And I wait for '15' seconds
#And I refresh the WebPage
#And I wait for '5' seconds
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
#And I click by JS 'Email_Field_Omni'
#And I wait for '20' seconds
#And I clear the text and enter 'Joesmith@test.com' in field 'Email_Field_Omni'
#And I wait for '20' seconds
#And I click by JS 'HomePhone_Field_Omni'
#And I wait for '20' seconds
#And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
#And I wait for '20' seconds
And I should see element 'LiveShopping_Button' present on page
And I click 'LiveShopping_Button'
#And I select option 'Sales' in dropdown 'ChatWith_Dropdown_Omni' by 'value'
And I click by JS 'StartChat_Button_Omni'
And I wait for '5' seconds
And I should see element 'WorrySection_Live' present on page
And I should see element 'AnswerSection_Live' present on page
And I should see element 'WhatSection_Live' present on page
And I should see element 'TwinSize_Live' present on page
And I click by JS 'TwinSize_Live'
And I click by JS 'MattressOnlyOption'
And I should see element 'DesiredSection_Custom' present on page
And I click by JS 'SofterOption_Custom'
And I should see element 'ChatStartedSection_Custom' present on page






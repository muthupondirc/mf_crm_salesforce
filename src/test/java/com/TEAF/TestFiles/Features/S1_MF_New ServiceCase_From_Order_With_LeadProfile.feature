@S1_MF_ServiceCase_Order_Lead
Feature: Creating a new Service Case from Order with Lead Profile
 
 1. Login to Mattress Firm Sales Force Application
 1.1 Creating a new Service Case from Order (Lead or above) - SP1_TC23
 
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Lead User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Lead User 2' present on page at 'ProfileCardPopUp' 
And I click by JS 'UserImageButton'

Scenario: 1.1 Creating a new Service Case from Order (Lead Or above) - SP1_TC23
Given I click 'All_Dropdown_Agent'
And I click by JS 'ContactsFilter_ContactLead'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
#Order number example: SLP-9857045, AX-C005088650, SLP-9887857
And I clear the text and enter 'SLP-9857045' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I wait for '10' seconds
And I should see element 'ContactName_Orderheader' present on page
And I click by JS 'ContactName_Orderheader'
And I wait for '10' seconds
And I should see element 'OrderNumber_Orderheader' present on page
And I click by JS 'OrderNumber_Orderheader'
And I wait for '5' seconds
And I should see element 'NewCase_Button_Order' present on page
And I click 'NewCase_Button_Order'
And I click by JS 'SelectRecordType_Contact'
And I select option 'Service' in dropdown 'SelectRecordType_Contact' by 'value'
And I click by JS 'NextButton_Contact' 
And I should see element 'NewCase_Header_Order' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click 'Phone_Text_Agent'
And I click 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click 'SubRecordType_Dropdown_Agent'
And I click by JS '120DayPrice_Option'
And I click 'TypeOfProduct(s)_Dropdown'
And I click by JS 'CoreProduct_Option'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I should see element 'OriginalOrderNumberX_Order' present on page
And I should see element 'ContactName_Order' present on page
And I click by JS 'ContactName_Order'
And I refresh the WebPage
And I wait for '10' seconds
And I scroll till element 'Cases_Order'
And I wait for '5' seconds
And I should see element 'Cases_Order' present on page
And I should see text '$$CaseNumber_Agent' contained on page at 'NewNumber_Order'
And I should see text 'Service' contained on page at 'CaseRecordType_Order'
And I click by JS 'Close_Agent' 

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page
@MF_SalesFlow_LogACallOnAnOrder
Feature: Log a Call on an Order

1. Login with Standard user Credentials
2. SP1_TC29 Log a Call On an Order 
3. Logout with Standard user

Scenario: Login with Standard user Credentials
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

Scenario: SP1_TC29 Log a call on an Order 
Given I click 'SearchTypeDD'
#And I click by JS 'SearchType_Order'
And I hit tab-key on element 'SearchTypeDD'
#UAT ORDER NUMBER
#And I enter 'AX-S015348301' in field 'GlobalSearchField'
#QA Order Number
And I enter 'AX-S015348301' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'LogaCallBtn' present on page
And I click by JS 'LogaCallBtn'
And I should see element 'MFRM' present on page
And I should see element 'MFRMDD' present on page
And I wait for visibility of element 'MFRMDD'
And I click 'MFRMDD'
And I wait for '3' seconds
And I click 'MFRMDDValue'
#And I select option 'Delivery ' in dropdown 'MFRMDD' by 'value'
And I should see element 'Subject' present on page
And I should see element 'Comments' present on page
And I enter 'Testing 1,2,3' in field 'CommentsTxtField'
And I click by JS 'SaveAndCloseBtn'
And I wait for '5' seconds
And I should see element 'OrderInfoHeader' present on page
And I scroll to 'element' - 'AllActivity'
And I should see element 'ActivityHistory' present on page
And I should see element 'ActivityHistorySubject' present on page
And I should see element 'ActivityDate' present on page
And I wait for '2' seconds
And I click by JS 'View' 
And I wait for '3' seconds
And I switch to iFrame 'IframeContent'
And I wait for '3' seconds
And I should see element 'AllActivityHistory' present on page
And I should see element 'LastModified' present on page
And I should see element 'MFRMCallTypeActivity' present on page
And I should see element 'ActivityComments' present on page
And I switch back to Main Window
And I wait for '5' seconds
And I click by JS 'TaskCloseBtn'
And I wait for '5' seconds
#And I click by JS 'Close'
And I click by JS 'ContactLink'
And I wait for '5' seconds 
And I should see element 'CustomerWarning' present on page
And I scroll to 'element' - 'Billing_Info'
And I wait for '2' seconds
And I should see element 'ContactActivityHistory' present on page
And I should see element 'ActivitySubject' present on page
And I should see element 'ActivityMFRMCallType' present on page
And I click by JS 'ContactViewAll'
And I wait for '3' seconds
And I switch to iFrame 'IframeContent'
And I wait for '3' seconds
And I should see element 'AllActivityHistory' present on page
And I should see element 'LastModified' present on page
And I should see element 'MFRMCallTypeActivity' present on page
And I should see element 'ActivityComments' present on page
And I switch back to Main Window
And I wait for '5' seconds
And I click by JS 'TaskCloseBtn'
And I wait for '5' seconds
And I click by JS 'Close'


@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page


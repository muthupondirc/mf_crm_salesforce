@MF_SalesFlow_Sprint4_Cases
Feature: Sprint 4 cases.

1.SP4_TC11 - Chatbot - Update Chat doFind Logic by Phone Number
2.SP4_TC6 ChatBot- Case Submission Enhancement.
3.SP4_TC5 ChatBot - Address Existing Issue on Bot Case Creation
4.Logout Scenario
@@MF_SalesFlow_Sprint4_Cases
Scenario: SP4_TC11 - Chatbot - Update Chat doFind Logic by Phone Number
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'telesales' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'telesales_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds
And I should see element 'OmniButton' present on page
And I click by JS 'OmniButton'
And I wait for '3' seconds
And I should see element 'OmniStatus' present on page
And I should see element 'OmniStatusDD' present on page
And I click by JS 'OmniStatusDD'
And I click by JS 'AvailableChat'
And I wait for '5' seconds
And I should see element 'ChatStatusUtilityBar' present on page
And I wait for '2' seconds
And I click by JS 'ChatStatusUtilityBar'
#And I click by JS 'MinimizeOmniBtn'
And I wait for '5' seconds
And I Switch the Tab and enter the url 'MFPortalURL'
#For QA
#And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/Home-Show'
And I wait for '20' seconds
And I click by JS 'linkAcceptCookieClose'
And I wait for '5' seconds
#And I click by JS 'WebClosePopup'
And I wait for '5' seconds
#Temparory Solution for Chat Bot
#-------------------

#And I should see element 'BedsandFrame' present on page
#And I mouse over 'BedsandFrame'
#And I wait for '3' seconds
#And I should see element 'BoxSpring' present on page
#And I click by JS 'BoxSpring'
#And I wait for '15' seconds
#---------
#QAFOR
And I should see element 'ChatWithExpertBtn' present on page
And I click by JS 'ChatWithExpertBtn'

#And I should see element 'ChatWithExpertBtnUAT' present on page
#And I click by JS 'ChatWithExpertBtnUAT'
#And I should see element 'ChatHeader' present on page
And I wait for '5' seconds
#FORNEWCHAT UAT

And I should see element 'SFChatFirstNameUAT' present on page
And I click by JS 'SFChatFirstNameFieldUAT'
And I enter 'Jared' in field 'SFChatFirstNameFieldUAT'
And I should see element 'SFChatLastNameUAT' present on page
And I should see element 'SFChatLastNameFieldUAT' present on page
And I click by JS 'SFChatLastNameFieldUAT'
And I enter 'Steiner' in field 'SFChatLastNameFieldUAT'
And I should see element 'SFChatPhoneNumberUAT' present on page
And I should see element 'SFChatPhoneNumberFieldUAT' present on page
And I click by JS 'SFChatPhoneNumberFieldUAT'
And I enter '5165410544' in field 'SFChatPhoneNumberFieldUAT'
And I should see element 'SFChatLiveShoppingBtnUAT' present on page
And I click by JS 'SFChatLiveShoppingBtnUAT'
And I wait for '2' seconds
And I should see element 'SFChatStartChatBtnUAT' present on page
And I click by JS 'SFChatStartChatBtnUAT'
And I wait for '15' seconds
And I should see element 'ChatTextbox' present on page
And I click by JS 'ChatTextbox'
And I wait for '3' seconds
#And I should see element 'SFStoreReferralChatQ1' present on page
#And I should see element 'SFStoreReferralChatQ1Value' present on page
#And I click by JS 'SFStoreReferralChatQ1Value'
#And I wait for '5' seconds
#And I should see element 'SFStoreReferralChatQ2' present on page
#And I should see element 'SFStoreReferralChatQ2Value' present on page
#And I click by JS 'SFStoreReferralChatQ2Value'
#And I wait for '5' seconds
#And I should see element 'SFStoreReferralChatQ3' present on page
#And I should see element 'SFStoreReferralChatQ3Value' present on page
#And I click by JS 'SFStoreReferralChatQ3Value'
#And I wait for '15' seconds
And I wait for visibility of element 'ChatwithUser'
And I should see element 'ChatwithUser' present on page

#FOR OLD CHAT IN QA
#And I should see element 'FirstNameTxtField' present on page
#And I click by JS 'FirstNameTxtField'
#And I enter 'Jared' in field 'FirstNameTxtField'
#And I should see element 'LastName' present on page
#And I should see element 'LastNameTxtField' present on page
#And I click by JS 'LastNameTxtField'
#And I enter 'Steiner' in field 'LastNameTxtField'
#And I should see element 'Email' present on page
#And I should see element 'PhoneTxtField' present on page
#And I click by JS 'PhoneTxtField'
#And I enter '5165410544' in field 'PhoneTxtField'
#And Blur 'PhoneTxtField'
#And I wait for '3' seconds
#And I should see element 'StartChatBtn' present on page
#And I click by JS 'StartChatBtn'
#And I wait for '20' seconds
##And I should see element 'NeedHelpChatOption' present on page
##And I click by JS 'NeedHelpChatOption'
##And I wait for '5' seconds
#And I should see element 'ChatTextbox' present on page
#And I click by JS 'ChatTextbox'
#And I enter 'Agent' in field 'ChatTextbox'
#And I hit enter-key on element 'ChatTextbox'
#And I wait for '10' seconds
#Temporary solution for chat
#--------------------

#And I should see element 'OtherQA' present on page
#And I click by JS 'OtherQA'
#And I wait for '10' seconds
#And I enter 'Agent' in field 'ChatTextbox'
#And I hit enter-key on element 'ChatTextbox'
#And I wait for '10' seconds
#--------------

And I Switch between Tab '0'
And I wait for '5' seconds
And I should see element 'ChatStartedTabHeader' present on page
#And I should see element 'PortalChatHeader' present on page
And I should see element 'VisitorFirstName' present on page
And I should see element 'VisitorLastName' present on page
And I should see element 'VisitorPhone' present on page

And I should see element 'headerCreateCaseFromChat' present on page
And I should see element 'headerContactDetails' present on page
And I should see element 'headerChatTranscriptDetails' present on page
And I should see element 'tabConversation' present on page
And I should see element 'tabPastChats' present on page
And I should see element 'tabDetails' present on page

And I should see element 'PortalEndChat' present on page
And I click by JS 'PortalEndChat'
And I wait for '2' seconds
And I should see element 'PortalEndChatTooltip' present on page
And I click by JS 'PortalEndChatTooltip'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds
#@Ignore
#Scenario: Verify logout of Sales Force application
#Given I click by JS 'UserImageButton'
#And I click link 'Log Out'
#And I should see element 'login_username' present on page
@@MF_SalesFlow_Sprint4_Cases
Scenario: SP4_TC11 - Chatbot - Update Chat doFind Logic By Email Address
#Given My WebApp 'MF_SalesForce' is open
#And I wait for '5' seconds
#When I login to Mattress Firm Sales Force with valid 'Donna Delia' User Credentials 
#And I click by JS 'UserImageButton'
#And I should see text 'Donna Delia' present on page at 'ProfileCardPopUp'
#And I click by JS 'UserImageButton'
#And I wait for '5' seconds
And I should see element 'ChatStatusUtilityBar' present on page
And I click by JS 'ChatStatusUtilityBar'
And I wait for '3' seconds
#And I should see element 'OmniStatus' present on page
And I should see element 'OmniStatusDD' present on page
And I click by JS 'OmniStatusDD'
And I click by JS 'AvailableChat'
And I wait for '5' seconds
#And I click by JS 'MinimizeOmniBtn'
And I Switch between Tab '1'
And I refresh the WebPage
#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
And I wait for '20' seconds
#And I click by JS 'WebClosePopup'
#ForQA
And I should see element 'ChatWithExpertBtn' present on page
And I click by JS 'ChatWithExpertBtn'

#ForUAT
#And I should see element 'ChatWithExpertBtnUAT' present on page
#And I click by JS 'ChatWithExpertBtnUAT'

And I wait for '5' seconds
#FOR NEW CHAT UAT

And I should see element 'SFChatFirstNameUAT' present on page
And I click by JS 'SFChatFirstNameFieldUAT'
And I enter 'Jared' in field 'SFChatFirstNameFieldUAT'
And I should see element 'SFChatLastNameUAT' present on page
And I should see element 'SFChatLastNameFieldUAT' present on page
And I click by JS 'SFChatLastNameFieldUAT'
And I enter 'Steiner' in field 'SFChatLastNameFieldUAT'
And I should see element 'SFChatEmaikAddressUAT' present on page
And I should see element 'SFChatEmaikAddressFieldUAT' present on page
And I click by JS 'SFChatEmaikAddressFieldUAT'
And I enter 'jaredsteiner813@gmail.com' in field 'SFChatEmaikAddressFieldUAT'
And I should see element 'SFChatLiveShoppingBtnUAT' present on page
And I click by JS 'SFChatLiveShoppingBtnUAT'
And I wait for '2' seconds
And I should see element 'SFChatStartChatBtnUAT' present on page
And I click by JS 'SFChatStartChatBtnUAT'
And I wait for '10' seconds

#For Old Chat
#And I should see element 'FirstNameTxtField' present on page
#And I click by JS 'FirstNameTxtField'
#And I enter 'Jared' in field 'FirstNameTxtField'
#And I should see element 'LastName' present on page
#And I should see element 'LastNameTxtField' present on page
#And I click by JS 'LastNameTxtField'
#And I enter 'Steiner' in field 'LastNameTxtField'
#And I should see element 'Email' present on page
#And I should see element 'EmailTxtField' present on page
#And I click by JS 'EmailTxtField'
#And I enter 'jaredsteiner813@gmail.com' in field 'EmailTxtField'
#And Blur 'EmailTxtField'
#And I wait for '3' seconds
#And I should see element 'StartChatBtn' present on page
#And I click by JS 'StartChatBtn'
#And I wait for '20' seconds
#And I should see element 'NeedHelpChatOption' present on page
#And I click by JS 'NeedHelpChatOption'
#And I wait for '5' seconds
And I should see element 'ChatTextbox' present on page
And I click by JS 'ChatTextbox'
And I wait for '3' seconds
And I should see element 'SFStoreReferralChatQ1' present on page
And I should see element 'SFStoreReferralChatQ1Value' present on page
And I click by JS 'SFStoreReferralChatQ1Value'
And I wait for '5' seconds
And I should see element 'SFStoreReferralChatQ2' present on page
And I should see element 'SFStoreReferralChatQ2Value' present on page
And I click by JS 'SFStoreReferralChatQ2Value'
And I wait for '5' seconds
And I should see element 'SFStoreReferralChatQ3' present on page
And I should see element 'SFStoreReferralChatQ3Value' present on page
And I click by JS 'SFStoreReferralChatQ3Value'
And I wait for '15' seconds
And I wait for visibility of element 'ChatwithUser'
And I should see element 'ChatwithUser' present on page
And I wait for '10' seconds
#Temporary solution for chat
#--------------------

#And I should see element 'OtherQA' present on page
#And I click by JS 'OtherQA'
#And I wait for '10' seconds
#And I enter 'Agent' in field 'ChatTextbox'
#And I hit enter-key on element 'ChatTextbox'
#And I wait for '10' seconds
#-------------------

And I Switch between Tab '0'
And I wait for '5' seconds
And I should see element 'ChatStartedTabHeader' present on page
#And I should see element 'PortalChatHeader' present on page
And I should see element 'VisitorFirstName' present on page
And I should see element 'VisitorLastName' present on page
And I should see element 'VisitorEmail' present on page
And I should see element 'PortalEndChat' present on page
And I click by JS 'PortalEndChat'
And I wait for '2' seconds
And I should see element 'PortalEndChatTooltip' present on page
And I click by JS 'PortalEndChatTooltip'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds

@@MF_SalesFlow_Sprint4_Cases
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

Scenario: SP4_TC6 ChatBot- Case Submission Enhancement.
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'NavHome'
And I wait for '3' seconds
And I should see element 'OmniButton' present on page
And I click by JS 'OmniButton'
And I wait for '3' seconds
And I should see element 'OmniStatus' present on page
And I should see element 'OmniStatusDD' present on page
And I click by JS 'OmniStatusDD'
And I click by JS 'AvailableChat'
And I wait for '10' seconds
#And I click by JS 'MinimizeOmniBtn'
#For UAT
And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'

#For QA
#And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/Home-Show'
And I wait for '20' seconds
#Temporary solution for chat
#--------------------

#And I should see element 'BedsandFrame' present on page
#And I mouse over 'BedsandFrame'
#And I wait for '3' seconds
#And I should see element 'BoxSpring' present on page
#And I click by JS 'BoxSpring'
#And I wait for '15' seconds
#--------------
#ForQA
And I should see element 'ChatWithExpertBtn' present on page
And I click by JS 'ChatWithExpertBtn'

#For UAT
#And I should see element 'ChatWithExpertBtnUAT' present on page
#And I click by JS 'ChatWithExpertBtnUAT'
And I wait for '5' seconds
#And I click by JS 'WebClosePopup'
And I wait for '3' seconds
#And I should see element 'ChatHeader' present on page
#FOR NEW CHAT UAT

And I should see element 'SFChatFirstNameUAT' present on page
And I click by JS 'SFChatFirstNameFieldUAT'
And I enter 'Jared' in field 'SFChatFirstNameFieldUAT'
And I should see element 'SFChatLastNameUAT' present on page
And I should see element 'SFChatLastNameFieldUAT' present on page
And I click by JS 'SFChatLastNameFieldUAT'
And I enter 'Steiner' in field 'SFChatLastNameFieldUAT'
And I should see element 'SFChatHelpWithOrderBtnUAT' present on page
And I click by JS 'SFChatHelpWithOrderBtnUAT'
And I wait for '2' seconds
And I should see element 'SFChatStartChatBtnUAT' present on page
And I click by JS 'SFChatStartChatBtnUAT'


#FOR OLD CHAT
#And I should see element 'FirstName' present on page
#And I should see element 'FirstNameTxtField' present on page
#And I click by JS 'FirstNameTxtField'
#And I enter 'Hasan' in field 'FirstNameTxtField'
#And I should see element 'LastName' present on page
#And I should see element 'LastNameTxtField' present on page
#And I click by JS 'LastNameTxtField'
#And I enter 'Khan' in field 'LastNameTxtField'
#And I wait for '2' seconds
#And I should see element 'ChatWithLabel' present on page
#And I should see element 'ChatDD' present on page
#And I click by JS 'ChatDD'
#And I wait for '2' seconds
#And I select option 'Service' in dropdown 'ChatDD' by 'text'
#And I wait for '5' seconds
##And Blur 'ChatDDValue'
#And I wait for '5' seconds
#And I should see element 'StartChatBtn' present on page
#And I click by JS 'StartChatBtn'

And I wait for '20' seconds
And I Switch between Tab '0'
And I should see element 'OmniStatusDD' present on page
And I click by JS 'OmniStatusDD'
And I click by JS 'SFStatusAway'
And I wait for '5' seconds
And I Switch between Tab '1'
And I wait for '3' seconds
And I should see element 'ChatTextbox' present on page
And I click by JS 'ChatTextbox'
And I wait for '2' seconds
And I enter 'Agent' in field 'ChatTextbox'
And I wait for '2' seconds
And I hit enter-key on element 'ChatTextbox'
And I wait for '5' seconds
And I should see element 'YesChatButton' present on page
And I click by JS 'YesChatButton'
And I wait for '5' seconds
And I should see element 'ServiceFormQuestion' present on page
And I should see element 'YesChatButton' present on page
And I click by JS 'YesChatButton'
And I wait for '5' seconds
And I should see element 'ServiceFormQuestion1' present on page
And I should see element 'DonotHaveBtn' present on page
And I click by JS 'DonotHaveBtn'
And I wait for '5' seconds
#Temporary solution for chat
#--------------------

And I should not see element 'QuestionQA' present on page
And I click by JS 'AnswerQA'
And I wait for '5' seconds
#-------------------

And I should see element 'ServiceFormQuestion2' present on page
And I click by JS 'ChatTextbox'
And I enter '5165410544' in field 'ChatTextbox'
And I wait for '2' seconds
And I hit enter-key on element 'ChatTextbox'
And I wait for '5' seconds
And I should see element 'ServiceFormQuestion3' present on page
And I click by JS 'ChatTextbox'
And I enter 'hasantestuser@gmail.com' in field 'ChatTextbox'
And I wait for '2' seconds
And I hit enter-key on element 'ChatTextbox'
And I wait for '5' seconds
And I should see element 'ServiceFormQuestion4' present on page
And I should see element 'ReturnExchange' present on page
And I click by JS 'ReturnExchange'
And I wait for '5' seconds
And I should see element 'ComfortExchange' present on page
And I click by JS 'ComfortExchange'
And I wait for '5' seconds
#Temporary solution for chat
#--------------------

And I should see element 'QAserviceCaseYes' present on page
And I click by JS 'QAserviceCaseYes'
And I wait for '5' seconds
And I click by JS 'ChatTextbox'
And I enter 'Yes' in field 'ChatTextbox'
And I hit enter-key on element 'ChatTextbox'
And I wait for '5' seconds
#--------------------

And I should see element 'ServiceFormCase' present on page
And I get Case Number from Chat 'ServiceFormCase'
And I wait for '3' seconds
And I should see element 'CloseChatBtn' present on page
And I click by JS 'CloseChatBtn'
And I wait for '3' seconds
And I Switch between Tab '0'
And I wait for '10' seconds
#And I click by JS 'MinimizeOmniBtn'
And I wait for '5' seconds
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I wait for '2' seconds
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$ServiceFormCase' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'ChatCase' present on page
And I click by JS 'ChatCase'
And I wait for '5' seconds
And I should see element 'ChatContactName' present on page
And I should see element 'ChatCaseOrigin' present on page
And I wait for '2' seconds
And I scroll till element 'PriorityLabel'
And I wait for '3' seconds
And I scroll till element 'DiscriptionLabel'
And I wait for '2' seconds
And I should see element 'ChatLabel' present on page
And I should see element 'ChatTranscriptID' present on page
And I wait for '3' seconds
And I should see element 'OmniStatusDD' present on page
And I wait for '3' seconds
And I click by JS 'OmniStatusDD'
And I wait for '5' seconds
And I click by JS 'ChatStatusOffline'
And I wait for '2' seconds
And I should see element 'OmniButton' present on page
And I wait for '5' seconds
And I click by JS 'OmniButton'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '3' seconds

##@fail
##Scenario: SP4_TC6 Chatbot - Case Creation Updates
##Given My WebApp 'MF_SalesForce' is open
##And I wait for '5' seconds
##When I login to Mattress Firm Sales Force with valid 'agent_QA' User Credentials 
##And I click by JS 'UserImageButton'
##And I should see text 'Gwenlyn Lee' present on page at 'ProfileCardPopUp'
##And I click by JS 'UserImageButton'
##And I wait for '5' seconds
##And I should see element 'OmniButton' present on page
##And I wait for '3' seconds
##And I click by JS 'OmniButton'
##And I wait for '5' seconds
###And I should see element 'OmniStatus' present on page
##And I should see element 'OmniStatusDD' present on page
##And I click by JS 'OmniStatusDD'
##And I click by JS 'AvailableChat'
##And I wait for '5' seconds
##And I Switch between Tab '1'
##And I refresh the WebPage
##And I wait for '10' seconds
####And I click by JS 'MinimizeOmniBtn'
###And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
###And I wait for '20' seconds
###And I click by JS 'WebClosePopup'
##And I should see element 'ChatWithExpertBtn' present on page
##And I click by JS 'ChatWithExpertBtn'
##And I wait for '3' seconds
###And I should see element 'ChatHeader' present on page
###And I should see element 'FirstName' present on page
##And I should see element 'FirstNameTxtField' present on page
##And I click by JS 'FirstNameTxtField'
##And I enter 'Hasan' in field 'FirstNameTxtField'
##And I should see element 'LastName' present on page
##And I should see element 'LastNameTxtField' present on page
##And I click by JS 'LastNameTxtField'
##And I enter 'Khan' in field 'LastNameTxtField'
##And I wait for '2' seconds
##And I should see element 'ChatWithLabel' present on page
##And I should see element 'ChatDD' present on page
##And I click by JS 'ChatDD'
##And I wait for '2' seconds
##And I select option 'Service' in dropdown 'ChatDD' by 'text'
##And I wait for '5' seconds
###And Blur 'ChatDDValue'
##And I wait for '5' seconds
##And I should see element 'StartChatBtn' present on page
##And I click by JS 'StartChatBtn'
##And I wait for '20' seconds
##And I Switch between Tab '0'
##And I should see element 'OmniStatusDD' present on page
##And I click by JS 'OmniStatusDD'
##And I click by JS 'SFStatusAway'
##And I wait for '5' seconds
##And I Switch between Tab '1'
##And I wait for '3' seconds
##And I should see element 'ChatTextbox' present on page
##And I click by JS 'ChatTextbox'
##And I wait for '2' seconds
##And I enter 'Agent' in field 'ChatTextbox'
##And I wait for '2' seconds
##And I hit enter-key on element 'ChatTextbox'
##And I wait for '5' seconds
##And I should see element 'YesChatButton' present on page
##And I click by JS 'YesChatButton'
##And I wait for '5' seconds
##And I should see element 'ServiceFormQuestion' present on page
##And I should see element 'YesChatButton' present on page
##And I click by JS 'YesChatButton'
##And I wait for '5' seconds
##And I should see element 'ServiceFormQuestion1' present on page
##And I should see element 'DonotHaveBtn' present on page
##And I click by JS 'DonotHaveBtn'
##And I wait for '5' seconds
##And I should see element 'ServiceFormQuestion2' present on page
##And I click by JS 'ChatTextbox'
##And I enter '5165410544' in field 'ChatTextbox'
##And I wait for '2' seconds
##And I hit enter-key on element 'ChatTextbox'
##And I wait for '5' seconds
##And I should see element 'ServiceFormQuestion3' present on page
##And I click by JS 'ChatTextbox'
##And I enter 'hasantestuser@gmail.com' in field 'ChatTextbox'
##And I wait for '2' seconds
##And I hit enter-key on element 'ChatTextbox'
##And I wait for '5' seconds
##And I should see element 'ServiceFormQuestion4' present on page
##And I should see element 'ReturnExchange' present on page
##And I click by JS 'ReturnExchange'
##And I wait for '5' seconds
##And I should see element 'ComfortExchange' present on page
##And I click by JS 'ComfortExchange'
##And I wait for '5' seconds
##And I should see element 'ServiceFormCase' present on page
##And I get Case Number from Chat 'ServiceFormCase'
##And I wait for '3' seconds
##And I should see element 'CloseChatBtn' present on page
##And I click by JS 'CloseChatBtn'
##And I wait for '3' seconds
##And I Switch between Tab '0'
##And I wait for '5' seconds
##And I should see element 'ChatStatusUtilityBarBusy' present on page
##And I click by JS 'ChatStatusUtilityBarBusy'
###And I click by JS 'MinimizeOmniBtn'
##And I wait for '5' seconds
##And I should see element 'ShowNavMenu' present on page
##And I click by JS 'ShowNavMenu'
##And I wait for '3' seconds
##And I click by JS 'NavCases'
##And I wait for '5' seconds
##And I should see element 'ListViewDD' present on page
##And I click by JS 'ListViewDD'
##And I wait for '5' seconds
##And I should see element 'ListDDSearchText' present on page
##And I click by JS 'ListDDSearchText'
##And I enter 'Service - MFRM' in field 'ListDDSearchText'
##And I wait for '3' seconds
##And I click by JS 'ServiceMFRMCare'
##And I wait for '10' seconds
##And I should see element 'ServiceMFRMCareCase' present on page
##And I wait for '5' seconds
##And I click by JS 'ChatStatusUtilityBarBusy'
###And I should see element 'OmniStatusDD' present on page
##And I click by JS 'OmniStatusDD'
##And I wait for '2' seconds
##And I click by JS 'ChatStatusOffline'
##And I wait for '2' seconds
##And I should see element 'OmniButton' present on page
##And I wait for '3' seconds
##And I click by JS 'OmniButton'
##And I wait for '5' seconds
#@Ignore
#Scenario: Verify logout of Sales Force application
#Given I click by JS 'UserImageButton'
#And I click link 'Log Out'
#And I should see element 'login_username' present on page

@address
Scenario: SP4_TC5 ChatBot - Address Existing Issue on Bot Case Creation
#Given My WebApp 'MF_SalesForce' is open
#And I wait for '5' seconds
#When I login to Mattress Firm Sales Force with valid 'agent_QA' User Credentials 
#And I click by JS 'UserImageButton'
#And I should see text 'Gwenlyn Lee' present on page at 'ProfileCardPopUp'
#And I click by JS 'UserImageButton'
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I wait for '5' seconds
And I wait for '5' seconds
And I should see element 'OmniButton' present on page
And I wait for '3' seconds	
And I click by JS 'OmniButton'
And I wait for '5' seconds
And I should see element 'OmniStatusDD' present on page
And I wait for '3' seconds
And I click by JS 'OmniStatusDD'
And I wait for '5' seconds
And I click by JS 'AvailableChat'
And I wait for '5' seconds
#And I click by JS 'MinimizeOmniBtn'
#For UAT
And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#for QA

#And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/Home-Show'
#And I Switch between Tab '1'
And I wait for '15' seconds
#And I click by JS 'WebClosePopup'
#Temporary solution for chat
#--------------------

#And I should see element 'BedsandFrame' present on page
#And I wait for '5' seconds
#And I mouse over 'BedsandFrame'
#And I wait for '3' seconds
#And I should see element 'BoxSpring' present on page
#And I click by JS 'BoxSpring'
#And I wait for '15' seconds
#---------


#ForOLD CHAT FORM

#And I should see element 'ChatWithExpertBtn' present on page
#And I click by JS 'ChatWithExpertBtn'
#And I wait for '3' seconds
#And I wait for '5' seconds
##And I should see element 'ChatHeader' present on page
#And I should see element 'FirstName' present on page
#And I should see element 'FirstNameTxtField' present on page
#And I click by JS 'FirstNameTxtField'
#And I enter 'Hasan' in field 'FirstNameTxtField'
#And I should see element 'LastName' present on page
#And I should see element 'LastNameTxtField' present on page
#And I click by JS 'LastNameTxtField'
#And I enter 'Khan' in field 'LastNameTxtField'
#And I wait for '2' seconds
#And I should see element 'ChatWithLabel' present on page
#And I should see element 'ChatDD' present on page
#And I click by JS 'ChatDD'
#And I wait for '2' seconds
#And I select option 'Service' in dropdown 'ChatDD' by 'text'
#And I wait for '5' seconds
#And I should see element 'StartChatBtn' present on page
#And I click by JS 'StartChatBtn'

#FOR New CHAT UAT

#And I should see element 'ChatWithExpertBtnUAT' present on page
#And I click by JS 'ChatWithExpertBtnUAT'

And I should see element 'ChatWithExpertBtn' present on page
And I click by JS 'ChatWithExpertBtn'
And I wait for '10' seconds
And I should see element 'SFChatFirstNameUAT' present on page
And I click by JS 'SFChatFirstNameFieldUAT'
And I enter 'Jared' in field 'SFChatFirstNameFieldUAT'
And I should see element 'SFChatLastNameUAT' present on page
And I should see element 'SFChatLastNameFieldUAT' present on page
And I click by JS 'SFChatLastNameFieldUAT'
And I enter 'Steiner' in field 'SFChatLastNameFieldUAT'
And I should see element 'SFChatHelpWithOrderBtnUAT' present on page
And I click by JS 'SFChatHelpWithOrderBtnUAT'
And I wait for '2' seconds
And I should see element 'SFChatStartChatBtnUAT' present on page
And I click by JS 'SFChatStartChatBtnUAT'
And I wait for '5' seconds
And I click by JS 'ChatTextbox'
And I enter 'Sales' in field 'ChatTextbox'
And I hit enter-key on element 'ChatTextbox'
And I wait for '5' seconds
And I should see element 'TransferToSaleBtn' present on page
And I click by JS 'TransferToSaleBtn'
And I wait for '5' seconds
And I should see element 'TransferChatMessage' present on page

#And I wait for '10' seconds
#And I Switch between Tab '0'
#And I should see element 'OmniStatusDD' present on page
#And I click by JS 'OmniStatusDD'
#And I click by JS 'SFStatusAway'
#And I wait for '5' seconds
#And I Switch between Tab '1'
#And I wait for '3' seconds
#And I should see element 'ChatTextbox' present on page
#And I click by JS 'ChatTextbox'
#And I wait for '2' seconds
#And I enter 'Agent' in field 'ChatTextbox'
#And I wait for '2' seconds
#And I hit enter-key on element 'ChatTextbox'
#And I wait for '5' seconds
And I should see element 'YesChatButton' present on page
And I click by JS 'YesChatButton'
And I wait for '5' seconds
And I should see element 'ServiceFormQuestion' present on page
And I should see element 'YesChatButton' present on page
And I click by JS 'YesChatButton'
And I wait for '5' seconds
And I should see element 'ServiceFormQuestion1' present on page
And I should see element 'DonotHaveBtn' present on page
And I click by JS 'DonotHaveBtn'
#Temporary solution for chat
#--------------------

And I should not see element 'QuestionQA' present on page
And I click by JS 'AnswerQA'
And I wait for '5' seconds
#-------------------

And I wait for '5' seconds
And I should see element 'ServiceFormQuestion2' present on page
And I click by JS 'ChatTextbox'
And I enter '5165410544' in field 'ChatTextbox'
And I wait for '2' seconds
And I hit enter-key on element 'ChatTextbox'
And I wait for '5' seconds
And I should see element 'ServiceFormQuestion3' present on page
And I click by JS 'ChatTextbox'
And I enter 'hasantestuser@gmail.com' in field 'ChatTextbox'
And I wait for '2' seconds
And I hit enter-key on element 'ChatTextbox'
And I wait for '5' seconds
And I should see element 'ServiceFormQuestion4' present on page
And I should see element 'ReturnExchange' present on page
And I click by JS 'ReturnExchange'
And I wait for '5' seconds
And I should see element 'ComfortExchange' present on page
And I click by JS 'ComfortExchange'
And I wait for '5' seconds
#Temporary solution for chat
#--------------------

And I should see element 'QAserviceCaseYes' present on page
And I click by JS 'QAserviceCaseYes'
And I wait for '5' seconds
And I click by JS 'ChatTextbox'
And I enter 'Yes' in field 'ChatTextbox'
And I hit enter-key on element 'ChatTextbox'
And I wait for '5' seconds
#-----------------

And I should see element 'ServiceFormCase' present on page
And I get Case Number from Chat 'ServiceFormCase'
And I wait for '3' seconds
And I click by JS 'CloseChatBtn'
And I wait for '5' seconds
And I should see element 'ChatWithExpertBtn' present on page
And I wait for '3' seconds
And I Switch between Tab '0'
And I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'NavHome'
And I wait for '3' seconds
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I wait for '2' seconds
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$ServiceFormCase' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'ChatCase' present on page
And I click by JS 'ChatCase'
And I wait for '5' seconds
And I should see element 'ChatContactName' present on page
And I should see element 'ChatCaseOrigin' present on page
And I wait for '2' seconds
And I scroll till element 'PriorityLabel'
And I wait for '3' seconds
And I scroll till element 'DiscriptionLabel'
And I wait for '2' seconds
And I should see element 'ChatLabel' present on page
And I should see element 'ChatTranscriptID' present on page
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds
And I should see element 'ChatStatusUtilityBar' present on page
And I should see element 'OmniStatusDD' present on page
And I wait for '3' seconds
And I click by JS 'OmniStatusDD'
And I wait for '3' seconds
And I click by JS 'ChatStatusOffline'
And I wait for '2' seconds
And I should see element 'OmniButton' present on page
And I wait for '3' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
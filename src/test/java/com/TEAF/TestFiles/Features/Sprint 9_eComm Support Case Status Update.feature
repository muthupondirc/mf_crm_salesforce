@Sprint9_Customer_Service_Secondary_Routing_Priority
Feature: eComm Support Case Status Update with value "Monitor"
1. Login with Ecomm Agent user.
2. SP9_1- eComm Support Case Status Update with value "Monitor"
3. Logout Scenario

Scenario: Login with Ecomm Agent user.
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'ecomagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'ecomagent_displayname' present on page at 'ProfileCardPopUp'
And I wait for '3' seconds

Scenario: SP9_TC22242 - eComm Support Case Status Update with value "Monitor"
Given I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'SFNav_Cases'
And I wait for '5' seconds
And I should see element 'SFCase_New' present on page
And I click by JS 'SFCase_New'
And I wait for '5' seconds
And I should see element 'EcommCase' present on page
And I should see element 'EcommCaseRadioBtn' present on page
And I wait for '3' seconds
And I click by JS 'EcommCaseRadioBtn'
And I wait for '2' seconds
And I should see element 'EcommCaseNext' present on page
And I wait for '2' seconds
And I click by JS 'EcommCaseNext'
And I wait for '5' seconds
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'Ecomm_SubRecordType_Value'
And I wait for '2' seconds
And I should see element 'Ecomm_SubType' present on page
And I should see element 'Ecomm_SubTypeDD' present on page
And I click by JS 'Ecomm_SubTypeDD'
And I click by JS 'Ecomm_SubTypeValue'
And I wait for '5' seconds
And I should see element 'EcommStatusDD' present on page
And I click by JS 'EcommStatusDD'
And I should see element 'EcommStatusDD_MonitorValue' present on page
And I click by JS 'EcommStatusDD_MonitorValue'
And I wait for '5' seconds
And I should see element 'BedBugSave' present on page
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'NavHome'
And I wait for '5' seconds
And I click by JS 'EcommCaseTab'
And I wait for '3' seconds
And I should see CaseOwner name 'ecomagent_displayname' present on page
And I should see element 'EcomCaseOrigin' present on page
And I should see element 'EcomCaseOriginValue' present on page
And I scroll till element 'EcomCaseOriginValue'
And I should see element 'EcommCaseSource' present on page
And I should see element 'EcommCaseSourceValue' present on page
And I should see element 'EcomSubRecordTypeValue' present on page
And I should see element 'EcommCaseRecordType' present on page
And I should see element 'EcommCaseRecordTypeValue' present on page
And I should see element 'EcommStatusLabel' present on page
And I should see element 'EcommStatusMonitor' present on page
And I click by JS 'Close'
And I wait for '3' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
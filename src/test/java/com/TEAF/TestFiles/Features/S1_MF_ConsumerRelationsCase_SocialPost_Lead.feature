@S1_MF_ConsumerRelationsCase_SocialPost_Lead
Feature: Creating a new ConsumerRelations Case for Social Post with Lead Profile
 
 1. Login to Mattress Firm Sales Force Application
 1.1 Creating a new ConsumerRelations Case for Social Post (Lead or above) - SP1_TC74


Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'consumerrelationagent' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Sr Mgr User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'ProfileCardPopUp'
And I scroll till element 'Connect_Text' 
#And I should see text 'Customer Relations Representative' present on page at 'Stenio_Title'
And I click by JS 'Close_Agent' 

Scenario: 1.1 Creating a new Consumer Relations Case from List View (Lead and above) - SP1_TC74
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I should see element 'New_Button_Agent' present on page
And I click 'New_Button_Agent'
Then I should see element 'NewCase_Text_Lead' present on page
And I click 'ConsumerRelations_Button_Creat'
And I click 'Next_Button_Lead'
Then I should see element 'NewCaseConsumerRelations_Text_Creat' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click 'Email_Text_Creat'
And I click 'State_Dropdown_Creat'
And I click 'NY_Text_Creat'
And I click 'Brand_Dropdown_Creat'
And I click 'MattressFirm_Text_Creat'
And I click 'Source_Dropdown_Agent'
And I click 'CustomerCare_Text_Creat'
And I click 'MerchandiseDisposition_Dropdown_Creat'
And I click 'Prime_Text_Creat'
And I click 'CRCaseType_Dropdown_Creat'
And I click 'Agency_Text_Creat'
And I click 'SubCaseType_Dropdown_Creat'
And I click 'BetterBusinessBureau(BBB)_Text_Creat'
And I click 'CurrentState_Dropdown_Creat'
And I click 'AccommodationOffer_Text_Creat'
And I click 'CR_Reason_Dropdown_Creat'
And I click 'Billing/Financing_Text_Creat'
And I click 'CR_ReasonDetails_Dropdown_Creat'
And I click 'CreditCard-Overcharged_Text_Creat'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I wait for '5' seconds
And I scroll till element 'RelatedCase_CreateRelated'
And I wait for '5' seconds
And I scroll till element 'Files_field'
And I wait for '10' seconds
And I should see element 'SocialPost_field' present on page
And I should see element 'SocialPost_Dropdown' present on page
And I click by JS 'SocialPost_Dropdown'
And I should see element 'New_Social' present on page
And I click by JS 'New_Social'
And I should see element 'NameField_Social' present on page
And I clear the text and enter 'CCO Sr Mgr User 2' in field 'NameField_Social'
And I click 'Save_Button_Close'
And I wait for '5' seconds
And I should see element 'EditButton_Social' present on page
And I click by JS 'Close_Agent' 
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '10' seconds
And I should see text '$$CaseNumber_Agent' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I wait for '5' seconds
And I scroll till element 'RelatedCase_CreateRelated'
And I wait for '5' seconds
And I scroll till element 'Files_field'
And I wait for '10' seconds
And I should see element 'SocialPost_field' present on page
And I should see element 'SocialNew_Dropdown' present on page
And I click by JS 'SocialNew_Dropdown'
And I should see element 'SocialEdit_Dropdown' present on page
And I refresh the WebPage
And I wait for '5' seconds
And I click by JS 'Close_Agent' 

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

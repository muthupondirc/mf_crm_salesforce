@S3_ServiceChatDispositioning_Agent
Feature: Service Chat Dis-positioning mandatory upon ending a chat as an agent
 
 1. Login to Mattress Firm Sales Force Application
1.1 To verify that Service Chat Dis-positioning mandatory upon ending a chat as an agent - SP3_TC2
2. To Verify that Adding New Picklist Value to the Service Chat Disposition - SP5_TC4
 
Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 To verify that Service Chat Dis-positioning mandatory upon ending a chat as an agent and validating all Picklist Values -SP3_TC2 and SP5_TC4
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
And I wait for '120' seconds
#And I refresh the WebPage
#And I wait for '15' seconds
And I should see element 'JoeTab' present on page 
And I wait for '5' seconds
And I should see element 'EndChat_Button' present on page
And I click by JS 'EndChat_Button'
And I should see element 'Areyousure_popup' present on page
And I click by JS 'EndChat_Popup'
And I wait for '10' seconds
And I Validate All Dropdown Values
#And I select option '<Dropdownvalues>' in dropdown 'ChatDispDropdown_EndChat' by 'text'
And I should see element 'ChatDisposition_Dropdown' present on page
And I click by JS 'ChatDisposition_Dropdown'
And I wait for '5' seconds
And I select option 'Cancel My Entire Order' in dropdown 'ChatDisposition_Dropdown' by 'value'
And I wait for '5' seconds
And I click by JS 'UpdateButton'
And I wait for '10' seconds
And I should see element 'ChatEndedByAgent_AgentChat' present on page
And I wait for '5' seconds
#
#Examples:
#|Dropdownvalues|
#|Please select a disposition|
#|Abandoned � Pre Chat|
#|120 Day Price Match Guarantee Request|
#|Cancel My Entire Order|
#|Cancel Part Of My Order|
#|Certificate of Insurance (COI)|
#|Change Product(s) on My Existing Order|
#|Incorrect/Missing/Damaged Item(s) Received|
#|None of the Provided Options Meet my Needs|
#|Pre-Warranty Claim Assistance|
#|Request an Invoice|
#|Request Comfort Exchange|
#|Request to Return Product Already Recevied|
#|Reschedule My Delivery|
#|Status of My Delivery|
#|Status of Open Warranty Claim|
#|Status of Pending Refund|
#|Update My Shipping / Billing Information|

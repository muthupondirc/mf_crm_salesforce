@S11_MF_Verify_DependentFields_RetentionAgentProfile
Feature: Retention Agent user - the dependent fields has to be created for store Operations

1. Login to Mattress Firm Sales Force Application
2. Verify all the dependent picklist values for Sub Record Type Store Operational Support_SP11-TC20
3. Logout of Sales Force application

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'HybridAgent_QA' User Credentials 
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'hybrid_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

Scenario: Verify all the dependent picklist values for Sub Record Type Store Operational Support_SP11-TC20
Given I click by JS 'ShowNavMenu'
And I wait for '2' seconds
And I click by JS 'CasesMenu'
And I should see element 'button_New' present on page
And I click 'button_New'
And I wait for '3' seconds
Then I should see element 'radioBtn_CustomerRetention' present on page
And I click by JS 'radioBtn_CustomerRetention'
And I click 'button_Next'
Then I should see element 'text_CustomerRetention' present on page
And I click by JS 'dropdown_CaseOrigin'
And I select value 'Chat' from the dropdown
And I click by JS 'dropdown_Source'
And I select value 'Event' from the dropdown
And I click by JS 'dropdown_Sub Record Type'
And I select value 'Store Operations Support' from the dropdown
And I should see element 'option_Store Operations Support' present on page
And I click by JS 'dropdown_Status'
And I select value 'Closed' from the dropdown
And I click by JS 'dropdown_SubType'
And I select value 'Credit Card Dispute' from the dropdown
And I click by JS 'button_Save'
Then I should see text 'A case Reason and Resolution are required to close a Retention case.' present on page at 'lbl_ErrorMsg1'
Then I should see text 'A Type of Product is required to close the case.' present on page at 'lbl_ErrorMsg2'
And I click by JS 'dropdown_TypeOfProducts'
And I select value 'Direct to Consumer' from the dropdown
And I click by JS 'dropdown_SubType'
And I select value 'Credit Card Dispute' from the dropdown
And I click by JS 'dropdown_Reason'
And I select value 'Credit Not Processed' from the dropdown
And I click by JS 'dropdown_CurrentState'
And I select value 'Pending Reply External' from the dropdown
And I click 'dropdown_Resolutions'
And I select value 'Resolved' from the dropdown
And I click by JS 'button_Save'
And I should see element 'text_Customer Retention' present on page
And I click by JS 'Close'
And I wait for '3' seconds

Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

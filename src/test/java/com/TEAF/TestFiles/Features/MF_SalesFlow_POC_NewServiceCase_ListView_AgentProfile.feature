@TestScn
Feature: New Service Case from List View with Agent Profile 

1.Login with Standard user Credentials
2.SP1_TC2 Adding a Contact to a case
3.SP1_TC3 & SP1_TC5 Link Order to a case with order number to search
4.SP1_TC4 & SP1_TC6 Clear Order Number in Case 
5.SP1_TC7 Link Order to a case with Customer Name to search 
6.SP1_TC8 Clear Order Number in Case 
7.SP1_TC9 Link Order to a case with Customer Name to search 
8.SP1_TC10 Clear Order Number in Case
9.SP1_TC11 Closing a Service Case
10.SP1_TC12 Verify as an agent Logging a Call on a Case 
11.SP1_TC13 Changing Case Owner 
12.SP1_TC14 Enter a Note in Case Comments - Service Case

Scenario: Login with Standard user Credentials
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '2' seconds

Scenario: SP1_TC2 Adding a Contact to a case 
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'SFNav_Cases'
And I wait for '5' seconds
And I should see element 'SFCase_New' present on page
And I click by JS 'SFCase_New'
And I wait for '3' seconds
And I should see element 'NewCaseSelectionBtn' present on page
And I should see element 'NewCaseSelectionLabel' present on page
And I click by JS 'NewCaseSelectionBtn'
And I should see element 'NewCaseSelectionNxt' present on page
And I click by JS 'NewCaseSelectionNxt'
And I wait for '5' seconds
And I should see element 'New_Case_Header' present on page
And I should see element 'SFServiceCaseContactName' present on page
And I wait for '2' seconds
And I click by JS 'SFServiceCaseContactName'
And I enter 'jared' in field 'SFServiceCaseContactName'
And I wait for '3' seconds
And I hit down Arrow key on element 'SFServiceCaseContactName'
And I wait for '3' seconds
And I hit enter-key on element 'SFServiceCaseContactName'
And I wait for '2' seconds
And Blur 'SFServiceCaseContactName'
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'TypeOfProduct' present on page
And I click by JS 'TypeOfProductDD'
And I click by JS 'TypeOfProductValue'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'SubRecordType_NeedOption_Value'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'CaseNo' present on page
And I get text from 'CaseNumberValue' and store
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I should see element 'HomeMenu' present on page
And I click by JS 'HomeMenu'
And I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I wait for '3' seconds
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'CaseHeader' present on page
And I click by JS 'CaseNumber'
And I wait for '10' seconds
And I click by JS 'EscalateEdit'
And I should see element 'Contact_Label' present on page
And I click by JS 'Remove_Contact'
And I should see element 'Search_Txt' present on page
And I enter 'Jared Steiner' in field 'Search_Txt'
And I wait for '3' seconds
And I hit down Arrow key on element 'Search_Txt'
And I wait for '3' seconds
And I hit enter-key on element 'Search_Txt'
And I click by JS 'ServiceCaseSaveBtn'
And I wait for '5' seconds
And I should see element 'HighlightPanel' present on page
And I should see element 'CustomerInfo' present on page
And I should see element 'ContactFullName' present on page
#ForUAT

And I should see element 'ContactCustCodeUAT' present on page
And I should see element 'BillingInfo' present on page
And I should see element 'ContactBillingName' present on page
And I should see element 'ContactBillingAddr' present on page
And I should see element 'ContactBillingAddress' present on page
And I should see element 'ContactBillingEmail' present on page
And I wait for '2' seconds

#ForQA
#And I should see element 'ContactCustCode' present on page
#And I should see element 'BillingInfo' present on page
#And I should see element 'ContactBillingName' present on page
#And I should see element 'ContactBillingAddrQA' present on page
#And I should see element 'ContactBillingAddressQA' present on page
#And I should see element 'ContactBillingEmailQA' present on page
#And I wait for '2' seconds



Scenario: SP1_TC3 & SP1_TC5 Link Order to a case with order number to search 
Given I click by JS 'SearchTypeDD'
And I wait for '3' seconds
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'CaseHeader' present on page
And I click by JS 'CaseNumber'
And I wait for '5' seconds
And I should see element 'LinkOrderToCase' present on page
And I wait for '3' seconds
And I should see element 'LinkSearch' present on page
And I enter 'AX-S015348301' in field 'LinkSearch'
And I hit enter-key on element 'LinkSearch'
And I wait for '5' seconds
And I click by JS 'SearchButton'
And I wait for '5' seconds
And I click 'SelectReturnedOrder'
And I wait for '3' seconds
And I click by JS 'SelectButton' 
And I wait for '5' seconds
And I mouse over 'CasePanel'
And I scroll to 'element' - 'CaseNumHeader'
And I should see element 'OriginalOrderNumber' present on page
And I should see element 'HighlightPanel' present on page
And I wait for '2' seconds
And I should see element 'ShippingInfo' present on page
And I should see element 'ShipToName' present on page
And I should see element 'ShippingCell' present on page
And I should see element 'ShippingAddr' present on page
And I should see element 'ShippingAddress' present on page
And I should see element 'ShippingEmail' present on page
And I wait for '2' seconds


Scenario: SP1_TC4 & SP1_TC6 Clear Order Number in Case 
And I wait for '3' seconds
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'CaseHeader' present on page
And I click by JS 'CaseNumber'
And I wait for '5' seconds
And I scroll to 'element' - 'OriginalOrderNumber'
And I wait for '2' seconds
And I should see element 'LinkOrderToCase' present on page
And I wait for '3' seconds
And I click by JS 'ClearOrderNumber'
And I should not see element 'OriginalOrderNumberValue' present on page
And I refresh the WebPage
And I should see element 'ShippingInfo' present on page
And I should not see element 'ShipToName' present on page
And I should not see element 'ShippingCell' present on page
And I should not see element 'ShippingAddr' present on page
And I should not see element 'ShippingAddress' present on page

Scenario: SP1_TC7 Link Order to a case with Customer Name to search 
And I wait for '3' seconds
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'CaseHeader' present on page
And I click by JS 'CaseNumber'
And I wait for '5' seconds
And I should see element 'LinkOrderToCase' present on page
And I wait for '3' seconds
And I should see element 'LinkSearch' present on page
And I enter 'Monique Davis' in field 'LinkSearch'
And I hit enter-key on element 'LinkSearch'
And I wait for '5' seconds
And I click by JS 'SearchButton'
And I wait for '5' seconds
And I click by JS 'SelectOrderQA'
And I wait for '3' seconds
And I click by JS 'SelectButton' 
And I wait for '5' seconds
And I mouse over 'CasePanel'
And I scroll to 'element' - 'CaseNumHeader'
#And I wait for '2' seconds
And I should see element 'OriginalOrderNumber' present on page
And I should see element 'HighlightPanel' present on page
And I wait for '2' seconds
#ForUAT

#And I should see element 'ShippingInfo' present on page
#And I should see element 'ShipToName' present on page
#And I should see element 'ShippingCell' present on page
#And I should see element 'ShippingAddr' present on page
#And I should see element 'ShippingAddress' present on page
##And I should see element 'ShippingEmail' present on page
#And I wait for '2' seconds
#ForQA

And I should see element 'ShippingInfo' present on page
And I should see element 'ShipToName' present on page
And I should see element 'ShippingCellQA' present on page
And I should see element 'ShippingAddrQA' present on page
And I should see element 'ShippingAddressQA' present on page
#And I should see element 'ShippingEmail' present on page
And I wait for '2' seconds

Scenario: SP1_TC8 Clear Order Number in Case
And I wait for '3' seconds
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'CaseHeader' present on page
And I click by JS 'CaseNumber'
And I wait for '5' seconds
And I scroll to 'element' - 'OriginalOrderNumber'
And I wait for '2' seconds
And I should see element 'LinkOrderToCase' present on page
And I wait for '3' seconds
And I click by JS 'ClearOrderNumber'
And I should not see element 'OriginalOrderNumberValue' present on page
And I refresh the WebPage
And I should see element 'ShippingInfo' present on page
And I should not see element 'ShipToName' present on page
And I should not see element 'ShippingCell' present on page
And I should not see element 'ShippingAddr' present on page
And I should not see element 'ShippingAddress' present on page


Scenario: SP1_TC9 Link Order to a case with Customer Name to search 

And I wait for '3' seconds
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'CaseHeader' present on page
And I click by JS 'CaseNumber'
And I wait for '5' seconds
And I should see element 'LinkOrderToCase' present on page
And I wait for '3' seconds
And I should see element 'LinkSearch' present on page
And I enter '347-365-9483' in field 'LinkSearch'
And I hit enter-key on element 'LinkSearch'
And I wait for '5' seconds
And I click by JS 'SearchButton'
And I wait for '5' seconds
And I click 'SelectReturnedOrder'
And I wait for '3' seconds
And I click by JS 'SelectButton' 
And I wait for '5' seconds
And I mouse over 'CasePanel'
And I scroll to 'element' - 'OriginalOrderNumber'
And I should see element 'OriginalOrderNumber' present on page
And I should see element 'HighlightPanel' present on page
And I wait for '2' seconds
And I should see element 'ShippingInfo' present on page
And I should see element 'ShipToName' present on page
And I should see element 'ShippingCell' present on page
And I should see element 'ShippingAddr' present on page
And I should see element 'ShippingAddress' present on page
And I wait for '2' seconds


Scenario: SP1_TC10 Clear Order Number in Case 
And I wait for '3' seconds
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'CaseHeader' present on page
And I click by JS 'CaseNumber'
And I wait for '5' seconds
And I scroll to 'element' - 'OriginalOrderNumber'
And I wait for '2' seconds
And I should see element 'LinkOrderToCase' present on page
And I wait for '3' seconds
And I click by JS 'ClearOrderNumber'
And I should not see element 'OriginalOrderNumberValue' present on page
And I refresh the WebPage
And I should see element 'ShippingInfo' present on page
And I should not see element 'ShipToName' present on page
And I should not see element 'ShippingCell' present on page
And I should not see element 'ShippingAddr' present on page
And I should not see element 'ShippingAddress' present on page
And I click by JS 'Close'

Scenario: SP1_TC11 Closing a Service Case 
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
Then I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I click by JS 'CaseSuggestion'
And I wait for '5' seconds
And I click by JS 'EscalateEdit'
#And I wait for visibility of element 'EditCase_Header'
#And I should see element 'EditCase_Header' present on page
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Status' present on page
And I click by JS 'Status'
And I click by JS 'StatusDD'
And I click by JS 'StatusValueClosed'
And I click by JS 'EditServiceCaseSaveBtn'
And I wait for '3' seconds
And I click by JS 'Close'

Scenario: SP1_TC12 Verify as an agent Logging a Call on a Case 
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
Then I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I click by JS 'CaseSuggestion'
And I wait for '5' seconds
And I should see element 'LogACall' present on page
And I click by JS 'Add_Btn'
And I wait for '3' seconds
And I should see element 'MFRM_Label' present on page
And I click by JS 'MFRM_DD'
And I click by JS 'MRFMDDValue'
And I should see element 'LogCall_Subject' present on page
And I clear field 'LogCall_Subject_Txt'
And I enter 'Delivery Update' in field 'LogCall_Subject_Txt'
And I should see element 'LogCall_Comments' present on page
And I enter 'Testing 1,2,3 Date: 10/8/2020' in field 'Comment_Txt_Field'
And Blur 'Comment_Txt_Field'
And I wait for '2' seconds
And I scroll to 'element' - 'LogCall_Comments'
And I click by JS 'LogCall_Save'
And I wait for '5' seconds
And I should see element 'Activity_History' present on page
And I scroll to 'element' - 'Activity_View_All'
And I click by JS 'Activity_View_All'
And I wait for '5' seconds
#And I should see element 'All_Activity_History' present on page
#And I should see element 'Updated_Activity' present on page
And I click by JS 'Sub_Tab_Task'
And I wait for '3' seconds
And I scroll to 'element' - 'Cont_Name_Label'
And I click by JS 'Cont_Name'
And I wait for '3' seconds
And I should see element 'HighlightPanel' present on page
And I scroll to 'element' - 'Activity_History'
And I wait for '3' seconds
And I click by JS 'Close'

Scenario: SP1_TC13 Changing Case Owner 
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
Then I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I click by JS 'CaseSuggestion'
And I wait for '5' seconds
And I click by JS 'NextToEdit_DD'
And I click by JS 'Change_Owner'
And I wait for '5' seconds
And I should see element 'Search_ContactName' present on page
And I wait for '5' seconds
And I click by JS 'Search_ContactName'
And I clear the text and enter 'tillery ward' in field 'Search_ContactName'
And I wait for '5' seconds
And I wait for visibility of element 'Contact_Sugesstion'
And I click by JS 'Contact_Sugesstion'
#And I should see element 'tillery ward' present on page
And I click by JS 'SubmitBtn'
And I wait for '3' seconds
And I should see element 'Case_Owner_Label' present on page
And I refresh the WebPage
And I wait for '3' seconds
And I click by JS 'Close'

Scenario: SP1_TC14 Enter a Note in Case Comments - Service Case 

And I click by JS 'SearchTypeDD'
And I wait for visibility of element 'SearchType_Cases'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I clear field 'GlobalSearchField'
And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I click by JS 'CaseSuggestion'
And I wait for '5' seconds
And I should see element 'HighlightPanel' present on page
And I should see element 'ShippingInfo' present on page
And I wait for '2' seconds
And I scroll till element 'ShippingInfo'
And I wait for '5' seconds
And I scroll till element 'HighlightPanelEmails'
And I wait for '5' seconds
And I scroll till element 'HighlightPanelCaseHistory'
And I wait for '5' seconds
And I scroll till element 'HighlightPanelFile_Header'
And I wait for '5' seconds
And I scroll till element 'HighlightPanelFile_Header'
And I wait for '5' seconds
And I should see element 'Case_Comment' present on page
And I click by JS 'Case_Comment_DropDown'
And I wait for '2' seconds
And I should see element 'Case_Comment_ToolTip' present on page
And I wait for '2' seconds
And I click by JS 'Case_Comment_ToolTip'
And I wait for '3' seconds
And I should see element 'New_Case_Comment_Header' present on page
And I should see element 'New_Case_Comment_Body' present on page
And I click by JS 'New_Case_Comment_Body'
And I enter 'ABCDE' in field 'New_Case_Comment_Body'
And I hit enter-key on element 'New_Case_Comment_Body'
And I hit tab-key on element 'New_Case_Comment_Body'
And I wait for '2' seconds
And I should see element 'Public_Label' present on page
And I click by JS 'Public_Checkbox'
And I click by JS 'Case_Comp_SaveBtn'
And I wait for '3' seconds
And I should see element 'Case_Comp_Errormsg' present on page
And I click by JS 'Close'


@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

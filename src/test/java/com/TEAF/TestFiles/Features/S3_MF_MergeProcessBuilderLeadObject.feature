@S3_MergeProcessBuilderLeadObject
Feature: Merge Process Builder Lead Object
 
 1. Login to Mattress Firm Sales Force Application
1.1. Lead Management Field Updates - Rule No1:If Lead Email opt out is true and LeadSource is (Progessive OR Syncrohny OR ChatBot)-SP3_TC1
2. Rule No 2:If Order is not Null and Lead Source is (Progessive OR Syncrohny OR ChatBot)-SP3_TC1.1

Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'IS Mgr User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'IS Mgr User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1. Verify Functionality of Phone Store Referral for Unknown Contacts -SP3_TC1
And I should see element 'StoreReferral_Utilitybar' present on page
And I click by JS 'StoreReferral_Utilitybar'
And I should see element 'ZipCodeField_Store' present on page
And I clear the text and enter '11801' in field 'ZipCodeField_Store'
And I should see element 'DistanceMiles_Dropdown_Store' present on page
And I click by JS 'DistanceMiles_Dropdown_Store'
And I click by JS '10MilesRadius_Option'
And I should see element 'SearchButton_Store' present on page
And I click by JS 'SearchButton_Store'
And I should see element 'StoreItem1List' present on page
And I click by JS 'StoreItem1List'
And I should see element 'NextButton_Store' present on page
And I click by JS 'NextButton_Store'
And I should see element 'FirstNameField_Store' present on page
And I clear the text and enter 'Sindhu' in field 'FirstNameField_Store'
And I should see element 'LastNameField_Store' present on page
And I clear the text and enter 'rangappa' in field 'LastNameField_Store'
And I should see element 'EmailField_Store' present on page
And I clear the text and enter 'sindhu.r@royalcyber.com' in field 'EmailField_Store'
And I get text from 'EmailField_Store' and store
And I should see element 'PhoneField_Store' present on page
And I clear the text and enter '8971984986' in field 'PhoneField_Store'
And I should see element 'StoreField_Store' present on page
And I click by JS 'NextButton_Store'
And I should see element 'NewCreatedLeadText_Store' present on page
And I should see element 'FinishButton_Store' present on page
And I click by JS 'FinishButton_Store'
And I click by JS 'Minimize_store'
And I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'LeadsMenu_Store' present on page
And I click by JS 'LeadsMenu_Store'
And I should see element 'NewContactField_Store_Unknown' present on page
And I get text from 'NewContactField_Store_Unknown' and store
And I click by JS 'NewContactField_Store_Unknown'

Scenario: 1.2. Lead Management Field Updates - Rule No1:If Lead Email opt out is true and LeadSource is (Progessive OR Syncrohny OR ChatBot)-SP3_TC1
And I should see element 'ChangeRecordTypeButton_Merge' present on page
And I click by JS 'ChangeRecordTypeButton_Merge'
And I should see element 'ProgessiveRadioButton_Merge' present on page
And I click by JS 'ProgessiveRadioButton_Merge'
And I should see element 'NextButton_Merge' present on page
And I click by JS 'NextButton_Merge'
And I wait for '5' seconds
And I scroll till element 'EmailoptoutCheckbox_Merge'
And I wait for '5' seconds
#And I should see element 'EmailOpt' present on page
And I click by JS 'EmailOptCheckbox'
#And I should see element 'EmailoptoutCheckbox_Merge' present on page
#And I click by JS 'EmailoptoutCheckbox_Merge'
And I scroll till element 'LeadSource_Dropdown'
And I should see element 'LeadSource_Dropdown' present on page
And I click by JS 'LeadSource_Dropdown'
And I wait for '5' seconds
And I click by JS 'ProgressiveOption_Merge'
And I should see element 'Company_Field' present on page
And I clear the text and enter 'Progressive' in field 'Company_Field'
And I wait for '5' seconds
And Blur 'Company_Field'
And I wait for '5' seconds
And I should see element 'SaveButton_Merge' present on page
And I click by JS 'SaveButton_Merge'
And I wait for '5' seconds
And I scroll till element 'EmailoptoutCheckboxField'
And I should see element 'EmailoptoutCheckboxField' present on page
And I verify checkbox 'EmailoptoutCheckboxField' is 'checked'
And I should see text 'Do Not Contact' present on page at 'PreferredContactMethodField'
And I should see text 'Closed No Sale' present on page at 'LeadStatusField'
And I should see text 'DNC/Opt Out' present on page at 'ClosedReasonField'
And I click by JS 'Close_Agent' 

Scenario: 1. Verify Functionality of Phone Store Referral for Unknown Contacts -SP3_TC1
And I should see element 'StoreReferral_Utilitybar' present on page
And I click by JS 'StoreReferral_Utilitybar'
And I should see element 'ZipCodeField_Store' present on page
And I clear the text and enter '11801' in field 'ZipCodeField_Store'
And I should see element 'DistanceMiles_Dropdown_Store' present on page
And I click by JS 'DistanceMiles_Dropdown_Store'
And I click by JS '10MilesRadius_Option'
And I should see element 'SearchButton_Store' present on page
And I click by JS 'SearchButton_Store'
And I should see element 'StoreItem1List' present on page
And I click by JS 'StoreItem1List'
And I should see element 'NextButton_Store' present on page
And I click by JS 'NextButton_Store'
And I should see element 'FirstNameField_Store' present on page
And I clear the text and enter 'Sindhu' in field 'FirstNameField_Store'
And I should see element 'LastNameField_Store' present on page
And I clear the text and enter 'rangappa' in field 'LastNameField_Store'
And I should see element 'EmailField_Store' present on page
And I clear the text and enter 'sindhu.r@royalcyber.com' in field 'EmailField_Store'
And I get text from 'EmailField_Store' and store
And I should see element 'PhoneField_Store' present on page
And I clear the text and enter '8971984986' in field 'PhoneField_Store'
And I should see element 'StoreField_Store' present on page
And I click by JS 'NextButton_Store'
And I should see element 'NewCreatedLeadText_Store' present on page
And I should see element 'FinishButton_Store' present on page
And I click by JS 'FinishButton_Store'
And I click by JS 'Minimize_store'
And I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'LeadsMenu_Store' present on page
And I click by JS 'LeadsMenu_Store'
And I should see element 'NewContactField_Store_Unknown' present on page
And I get text from 'NewContactField_Store_Unknown' and store
And I click by JS 'NewContactField_Store_Unknown'

Scenario: 2. Rule No 2:If Order is not Null and Lead Source is (Progessive OR Syncrohny OR ChatBot)-SP3_TC1
And I should see element 'ChangeRecordTypeButton_Merge' present on page
And I click by JS 'ChangeRecordTypeButton_Merge'
And I should see element 'ProgessiveRadioButton_Merge' present on page
And I click by JS 'ProgessiveRadioButton_Merge'
And I should see element 'NextButton_Merge' present on page
And I click by JS 'NextButton_Merge'
And I wait for '5' seconds
And I scroll till element 'EmailoptoutCheckbox_Merge'
And I wait for '5' seconds
And I should see element 'EmailOpt' present on page
And I click by JS 'EmailOptCheckbox'
And I should see element 'OrderNumberField_Merge' present on page
And I clear the text and enter '02762882' in field 'OrderNumberField_Merge'
And I wait for '5' seconds
And Blur 'OrderNumberField_Merge'
And I wait for '5' seconds
And I should see element 'OrderAmountField_Merge' present on page
And I clear the text and enter '100' in field 'OrderAmountField_Merge'
And I wait for '5' seconds
And Blur 'OrderAmountField_Merge'
And I wait for '5' seconds
And I scroll till element 'LeadSource_Dropdown'
And I should see element 'LeadSource_Dropdown' present on page
And I click by JS 'LeadSource_Dropdown'
And I wait for '5' seconds
And I click by JS 'ProgressiveOption_Merge'
And I should see element 'Company_Field' present on page
And I clear the text and enter 'Progressive' in field 'Company_Field'
And I wait for '5' seconds
And Blur 'Company_Field'
And I wait for '5' seconds
And I should see element 'SaveButton_Merge' present on page
And I click by JS 'SaveButton_Merge'
And I wait for '5' seconds
And I scroll till element 'EmailoptoutCheckboxField'
And I wait for '5' seconds
And I should see element 'EmailoptoutCheckboxField' present on page
And I verify checkbox 'EmailoptoutCheckboxField' is 'checked'
And I should see text 'Converted Sale' present on page at 'LeadStatusField'
And I should see text 'Converted Sale' present on page at 'ClosedReasonField'
And I click by JS 'Close_Agent' 



















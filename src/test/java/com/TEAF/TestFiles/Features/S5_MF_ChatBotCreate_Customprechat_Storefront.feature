@S5_ChatBotCreate_Customprechat_Storefront
Feature: ChatBot-Create a custom pre-chat for mattressfirm.com in Storefront
 1. To verify that First Name and Last Name Required,if not provided Alert will appear - SP5_TC1
 2. To verify that For email if the email field have value it should check whether its valid email or not - SP5_TC1.1  
 3. To verify that If Clicking on Live shopping then it shoud drive to sales queue  - SP5_TC1.2 
 4. To verify that If clicking on help with existing order it should drive to service queue - SP5_TC1.3

Scenario: 1. To verify that First Name and Last Name Required,if not provided Alert will appear - SP5_TC1
Given My WebApp 'MF_SalesForce' is open 
#And I wait for '60' seconds
#waiting for Agent kim to be Online in OmniChannel
#------------------UAT URL------------------
#And I navigate to 'https://storefront:face@dev.mattressfirm.com/' application
#------------------QA URL------------------
And I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' application
And I should see element 'CookiesCloseButton_Hybrid' present on page
And I click by JS 'CookiesCloseButton_Hybrid'
And I wait for visibility of element 'MattressSite_CloseButton'
And I click 'MattressSite_CloseButton'
#And I wait for '15' seconds
#And I refresh the WebPage
#And I wait for '5' seconds
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click 'HelpWithExistingOrder_Option'
And I click by JS 'StartChat_Button_Omni'
And I wait for '5' seconds
And I should see text 'Please fill Last Name' contained on page at 'NameError_Chat1'
#And I should see text 'Please fill Last Name' contained on page at 'LastNameError_Chat'
#-----------close and open chat again-------
And I click by JS 'CloseButton_Help'
Then I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
#------------------------
And I clear field 'FirstName_Field_Omni'
And I wait for '3' seconds
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click 'HelpWithExistingOrder_Option'
And I click by JS 'StartChat_Button_Omni'
And I wait for '5' seconds
And I should see text 'Please fill First Name' contained on page at 'NameError_Chat2'
And I wait for '10' seconds

Scenario: 2. To verify that For email if the email field have value it should check whether its valid email or not - SP5_TC1.1 
#-----------close and open chat again-------
And I click by JS 'CloseButton_Help'
Then I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
#------------------------
And I should see element 'Chat_widget_Omni' present on page
And I wait for '10' seconds
And I click 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I wait for '3' seconds
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'Joesmith.test.com' in field 'Email_Field_Omni'
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click 'HelpWithExistingOrder_Option'
And I click by JS 'StartChat_Button_Omni'
And I wait for '5' seconds
#And I should see text 'Invalid Email Address' contained on page at 'EmailError_Chat'
And I should see text 'Please fill valid email address' contained on page at 'InvalidEmailError_Chat'
And I wait for '3' seconds

Scenario: 3. To verify that If Clicking on Live shopping then it shoud drive to sales queue  - SP5_TC1.2 
#-----------close and open chat again-------
And I click by JS 'CloseButton_Help'
Then I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
#------------------------
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'Joesmith@test.com' in field 'Email_Field_Omni'
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I should see element 'LiveShopping_Button' present on page
And I click 'LiveShopping_Button'
#And I select option 'Sales' in dropdown 'ChatWith_Dropdown_Omni' by 'value'
And I click by JS 'StartChat_Button_Omni'
And I wait for '5' seconds
And I should see element 'WorrySection_Live' present on page
And I should see element 'AnswerSection_Live' present on page
And I should see element 'WhatSection_Live' present on page
And I should see element 'TwinSize_Live' present on page
And I click by JS 'TwinSize_Live'
And I click by JS 'MattressOnlyOption'
And I should see element 'DesiredSection_Custom' present on page
And I click by JS 'SofterOption_Custom'
#And I should see element 'ChatStartedSection_Custom' present on page
And I click by JS 'CloseButton_Help'
And I click by JS 'ConfirmEndChat_Help'
And I click by JS 'CloseChat_Help'
And I wait for '3' seconds
Then I should see element 'ChatwithanExpert_Button_Omni' present on page

Scenario:  4. To verify that If clicking on help with existing order it should drive to service queue - SP5_TC1.3
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'Joesmith@test.com' in field 'Email_Field_Omni'
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click 'HelpWithExistingOrder_Option'
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'
And I click by JS 'StartChat_Button_Omni'
And I wait for '5' seconds
#And I should see element 'HelloSection_Help' present on page
#And I should see element 'PleaseSelectSection_Help' present on page
#And I should see text 'Hello! I'm Zzzack, your Mattress Firm chat robot.' contained on page at 'HelloSection_Help'
#And I should see text 'Please select from these items below.' contained on page at 'PleaseSelectSection_Help'
And I should see element 'MenuItems_Help' present on page
#And I should see element 'DeliveryButton_Help' present on page
#And I click by JS 'DeliveryButton_Help'
#And I should see element 'Message_Help' present on page
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
#delay in qa
And I wait for '10' seconds
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I wait for visibility of element 'Textarea_Field_Omni'
#And I send 'Hello' message in omni channel
#And I wait for '10' seconds



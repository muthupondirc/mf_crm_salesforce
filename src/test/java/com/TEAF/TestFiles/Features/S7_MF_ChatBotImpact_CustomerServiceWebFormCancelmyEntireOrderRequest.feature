@S7_ChatBotImpact
Feature: ChatBot Impact- Customer Service Web Form - Cancel my Entire Order Request

1. ChatBot Impact- Customer Service Web Form - Cancel my Entire Order Request - SP7_TC2
2. Customer Service Web Form - Cancel my Entire Order Request - SP7_TC3

Scenario: 1.Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

#Scenario: 1.1 Omni Channel - make Service Agent as Available Chat
#Given I should see element 'OmniChannel_Button_Omni' present on page
#And I click 'OmniChannel_Button_Omni'
#And I click by JS 'Status_Dropdown_Omni'
#And I click by JS 'AvailableChat_Button_Omni'
#And I should see element 'AvailableChat_Text_Omni' present on page
#And I click by JS 'Minimize_Button_Omni'


Scenario: ChatBot Impact- Customer Service Web Form - Cancel my Entire Order Request- SP7_TC
#-----------UAT URL--------------------------------------------------------------------------------
#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#-----------QA URL--------------------------------------------------------------------------------- 

And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' 
And I should see element 'CookiesCloseButton_Hybrid' present on page
And I click by JS 'CookiesCloseButton_Hybrid'
And I wait for visibility of element 'MattressSite_CloseButton'
And I click 'MattressSite_CloseButton'
#And I wait for '15' seconds
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click by JS 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'joesmith@test.com' in field 'Email_Field_Omni'
And I get text from 'Email_Field_Omni' and store
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I get text from 'HomePhone_Field_Omni' and store
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click by JS 'HelpWithExistingOrder_Option'

#--------------next line is for old chat bot step-----------------------------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'

And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds

#--------------next 2 lines are old chat bot steps---------------------------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
#-----------------------console-----------------------------

And I Switch between Tab '0'
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AwayButton_Omni'
And I should see element 'AwayText_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
#------------------------------------------------------------------------

And I Switch between Tab '1'
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'NoAgentsText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I should see element 'ChatStartedText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I click by JS 'DoNotHaveButton_Chat'
And I click by JS 'PhoneSalesAgentButton_Chat'
And I click by JS 'YesButton_Chat'
And I click by JS 'YesButton_Chat'
And I should see element 'CancellationButton_Cancel' present on page
And I click by JS 'CancellationButton_Cancel'
And I should see element 'CancelPartofMyOrderButton_Cancel' present on page
And I should see element 'CancelMyEntireOrderButton_Cancel' present on page
And I click by JS 'CancelMyEntireOrderButton_Cancel'
And I should see element 'DidNotDeliverasPromisedButton_Cancel' present on page
And I should see element 'FoundLowerPriceButton_Cancel' present on page
And I should see element 'NeedProductSoonerButton_Cancel' present on page
And I should see element 'NoLongerNeededButton_Cancel' present on page
And I should see element 'PreferNottoSay/JustWantstoCancelButton_Cancel' present on page
And I click by JS 'DidNotDeliverasPromisedButton_Cancel'
And I should see element 'CaseCreatedMessage_Chat' present on page
And I get Case Number from Chat 'CaseCreatedMessage_Chat' and store
And I Switch between Tab '0'

#Scenario: 1.3 Verify logout of Sales Force application
#Given I click 'UserImageButton'
#And I wait for '5' seconds
#And I click link 'Log Out'
#And I wait for '5' seconds
#And I should see element 'login_username' present on page
#
#Scenario: 1.4 Login to Mattress Firm Sales Force Application
#Given My WebApp 'MF_SalesForce' is open 
#And I wait for '3' seconds
#When I login to Mattress Firm Sales Force with valid 'Jarely Cruz_QA' User Credentials 
#And I wait for '5' seconds
#And I click by JS 'UserImageButton'
#And I wait for '5' seconds
#And I should see text 'Jarely Cruz' present on page at 'ProfileCardPopUp'
#And I click by JS 'UserImageButton'

Scenario: Validating in SF Console
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseCreatedMessage_Chat' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I should see text 'Service - MFRM Care' contained on page at 'CaseOwnerField_Chat'
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumberField_CR'
And I should see element 'ContactNameField_Hybrid' present on page
And I should see text 'Chatbot' contained on page at 'CaseOriginField_Hybrid'
And I scroll till element 'SourceField_Hybrid'
And I wait for '5' seconds
And I should see text 'phone' contained on page at 'SourceField_Hybrid'
And I should see text 'Cancel My Entire Order' contained on page at 'SubRecordTypeField_Hybrid'
And I should see text 'Did Not Deliver as Promised' contained on page at 'SubTypeField_Hybrid'
And I scroll till element 'WebEmailField_Hybrid'
And I wait for '5' seconds
And I should see text 'joesmith@test.com' contained on page at 'WebEmailField_Hybrid'
And I should see text '5168618800' contained on page at 'WebPhoneField_Hybrid'
And I click by JS 'Close_Agent'
And I click by JS 'Close_Agent'

Scenario: 2. Customer Service Web Form - Cancel my Entire Order Request - SP7_TC
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseCreatedMessage_Chat' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I should see element 'EditButton_HybridCase' present on page
And I click by JS 'EditButton_HybridCase'
And I click 'Status_Dropdown_Close'
And I click by JS 'Closed_Text_Close'
And I click 'Save_Button_Close'
And I wait for '3' seconds
And I should see text 'When Cancel My Entire Order is selected, the Reason field is required to close the case' contained on page at 'ReasonErrorMessage_Cancel'
And I should see text 'A Type of Product is required to close the case.' contained on page at 'ProductErrorMessage_Cancel'
And I click by JS 'TypeOfProduct(s)_Dropdown'
And I click by JS 'CoreProduct_Option'
And I click by JS 'ReasonDropdown_Cancel'
And I click by JS 'CanceledOption_Cancel'
And I click 'Save_Button_Close'
And I wait for '3' seconds
And I scroll till element 'SourceField_Hybrid'
And I wait for '5' seconds
And I should see text 'Canceled' contained on page at 'ReasonFieldValue_Cancel'
And I should see text 'Closed' contained on page at 'Status_Agent'
And I should see text 'Core Product' contained on page at 'TypeofProduct(s)FieldValue_Cancel'
And I click by JS 'Close_Agent'
And I click by JS 'Close_Agent'

Scenario: 2.1 Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page





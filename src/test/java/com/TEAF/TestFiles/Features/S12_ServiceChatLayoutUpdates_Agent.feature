@S12_ServiceChatLayoutUpdates_Agent
Feature: Verify the existing layout updated to an enhanced view as Service chat agent
 
1. Login to Mattress Firm Sales Force Application
1.1 To verify that Chat - Visual indication to agent that a new chat (without input) dropped in - S12_TC08_SFLC-27204
2. To verify that Chat - Show alert to agent when a chat is ended - S12_TC08_SFLC-27204
 

Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: To verify that Chat - Visual indication to agent that a new chat (without input) dropped in - S12_TC08_SFLC-27204
And I click by JS 'dropdown_NavigationMenu'
And I select value 'Omni Supervisor' from the dropdown
And I click by JS 'bottomTab_OmniChannel'
And I click by JS 'dropdown_OmniChannel'
And I click by JS 'option_AvailableChat_Omni'
And I should see element 'Available - Chat' present on page
And I click by JS 'button_Minimize_Omni'
And I wait for '120' seconds
#And I refresh the WebPage
#And I wait for '15' seconds
And I should see element 'tab_Joe' present on page
And I wait for '10' seconds
And I Validate Green Color Field
And I click by JS 'tab_Joe'
And I should see element 'field_Conversation' present on page
And I should see element 'txt_ChatStarted' present on page
And I should see element 'textarea_Field_Console' present on page
And I click 'textarea_Field_Console'
And I clear the text and enter 'Hello Joe' in field 'textarea_Field_Console'
And I hit enter-key on element 'textarea_Field_Console'
Then I should see element 'field_ChatTranscriptDetails' present on page
Then I should see element 'field_PastChats' present on page
Then I should see element 'field_Details' present on page
Then I should see element 'field_ChatTranscriptName' present on page
Then I should see element 'field_VisitorEmail' present on page
Then I should see element 'field_VisitorFirstName' present on page
Then I should see element 'field_VisitorLastName' present on page
Then I should see element 'field_VisitorHomePhone' present on page
Then I should see element 'field_VisitorIPAddress' present on page
And I should see element 'txtValue_ChatTranscriptName' present on page
And I should see element 'txtValue_VisitorEmailChatTranscriptName' present on page
And I should see element 'txtValue_VisitorFirstName' present on page
And I should see element 'txtValue_VisitorLastName' present on page
And I should see element 'txtValue_VisitorHomePhone' present on page
And I should see element 'txtValue_VisitorIPAddress' present on page
Then I should see element 'field_CreateCasefromChatTranscript' present on page
Then I should see element 'field_ContactDetails' present on page
Then I should see element 'field_Cases_rightPanel' present on page
Then I should see element 'field_knowledge' present on page
Then I should see element 'searchField_knowledge' present on page

Scenario: To verify that Chat (IS) - Show alert to agent when a chat is ended - S12_TC08_SFLC-27204
And I should see element 'EndChat_Button' present on page
And I click by JS 'EndChat_Button'
And I should see element 'Areyousure_popup' present on page
And I click by JS 'EndChat_Popup'
And I wait for '10' seconds
And I should see element 'ChatDisposition_Dropdown' present on page
And I click by JS 'ChatDisposition_Dropdown'
And I wait for '5' seconds
And I select option 'Cancel My Entire Order' in dropdown 'ChatDisposition_Dropdown' by 'value'
And I wait for '5' seconds
And I click by JS 'UpdateButton'
And I wait for '10' seconds
And I should see element 'ChatEndedByAgent_AgentChat' present on page
And I Validate Red Color Field
And I wait for '5' seconds
@S4_NewLogaCall_for_EcommSupport
Feature: New Log a Call type for Ecomm Support Team
 
 1. Login to Mattress Firm Sales Force Application
1.1 Verify Functionality of eComm Support will be added to the Log a Call call type and used on Cases -SP4_TC1
2. Verify Functionality of eComm Support will be added to the Log a Call call type and used on Contacts -SP4_TC1.2
3. Verify Functionality of eComm Support will be added to the Log a Call call type and used on Order -SP4_TC1.3
4. Verify Functionality of eComm Support will be added to the Log a Call call type and used on Utility bar -SP4_TC1.4


Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Ecomm user 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Ecomm user 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1. Verify Functionality of eComm Support will be added to the Log a Call call type and used on Cases -SP4_TC1
And I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'Cases_Ecomm' present on page
And I click by JS 'Cases_Ecomm'
And I should see element 'NewButton_Ecomm' present on page
And I click by JS 'NewButton_Ecomm'
And I should see element 'EcommSupport_RadioButton' present on page
And I click by JS 'EcommSupport_RadioButton'
And I should see element 'NextButton_Ecomm' present on page
And I click by JS 'NextButton_Ecomm'
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click by JS 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click by JS 'SubRecordTypeDropdown_Ecomm'
And I click by JS 'AccountingOption_Ecomm'
And I click by JS 'SubTypeDropdown_Ecomm'
And I click 'GeneralInquiryOption_Ecomm'
And I should see element 'SaveButton_Ecomm' present on page
And I click by JS 'SaveButton_Ecomm'
And I wait for '5' seconds
And I should see element 'Actions_Tab' present on page
And I should see element 'LogaCall_Button' present on page
And I should see element 'Add_Button_Log' present on page
And I click by JS 'Add_Button_Log'
And I should see element 'MFRMCallType_Dropdown' present on page
And I click by JS 'MFRMCallType_Dropdown'
And I click by JS 'ecommSupportOption_Ecom'
And I should see element 'Subject_Field' present on page
#And I clear the text and enter 'Call' in field 'Subject_Field'
And I should see element 'Comments_Field' present on page
And I clear the text and enter 'testing value' in field 'Comments_Field'
And I wait for '5' seconds
And Blur 'Comments_Field'
And I wait for '5' seconds
And I click by JS 'Save_Button_Log'
And I scroll till element 'ActiveHistory_Log'
And I wait for '5' seconds
And I should see element 'ActiveHistory_Log' present on page
And I should see element 'SubjectField_Case' present on page
And I should see text 'Call' present on page at 'SubjectField_Case'
And I should see text 'eComm Support' present on page at 'MFRMCallTypeField_Case'
And I should see element 'LastModifiedDateOption_Activity' present on page
And I get text from 'LastModifiedDateOption_Activity' and store
And I should see text 'CCO Ecomm user 2' present on page at 'AssignedToField_Case'
And I click by JS 'ViewAll_Link'
And I wait for '5' seconds
And I refresh the WebPage
And I wait for '15' seconds
And I switch to iFrame 'Frame_Log'
And I wait for '5' seconds
And I should see element 'AllActiveHistory_Header' present on page
And I should see element 'LastModifiedDate_Log' present on page
And I should see element 'Comments_Field_Text' present on page
And I should see text 'testing value' present on page at 'Comments_Field-Activity'
And I should see text 'eComm Support' present on page at 'MFRMcallType_Case'
And I should see text 'CCO Ecomm user 2' present on page at 'Name_Case'
And I switch to default content
And I click by JS 'Close_Agent' 

Scenario: 2. Verify Functionality of eComm Support will be added to the Log a Call call type and used on Contacts -SP4_TC1.2
And I click 'All_Dropdown_Agent'
And I click by JS 'ContactsFilter_ContactLead'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I clear the text and enter 'Jared Steiner' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I should see element 'ContactNameField_Contact' present on page
And I click 'ContactNameField_Contact'
And I wait for '10' seconds
And I should see element 'Actions_Tab' present on page
And I should see element 'LogaCall_Button' present on page
And I should see element 'Add_Button_Log' present on page
And I click by JS 'Add_Button_Log'
And I should see element 'MFRMCallType_Dropdown' present on page
And I click by JS 'MFRMCallType_Dropdown'
And I click by JS 'ecommSupportOption_Ecom'
And I should see element 'Subject_Field' present on page
#And I clear the text and enter 'Call' in field 'Subject_Field'
And I should see element 'Comments_Field' present on page
And I clear the text and enter 'testing contacts' in field 'Comments_Field'
And I wait for '5' seconds
And Blur 'Comments_Field'
And I wait for '5' seconds
And I click by JS 'Save_Button_Log'
And I scroll till element 'ActiveHistory_Contact'
And I wait for '5' seconds
And I should see element 'ActiveHistory_Log' present on page
And I should see element 'Subject_Contact' present on page
And I should see text 'Call' present on page at 'Subject_Contact'
And I should see text 'eComm Support' present on page at 'MFRMCallType_Contact'
And I should see element 'LastModifiedDate_Contact' present on page
And I get text from 'LastModifiedDateOption_Activity' and store
And I should see text 'CCO Ecomm user 2' present on page at 'AssignedTo_Contact'
And I click by JS 'ViewAllLink_Contact'
And I refresh the WebPage
And I wait for '15' seconds
And I switch to iFrame 'Frame_Log'
And I wait for '5' seconds
And I should see element 'AllActiveHistory_Header' present on page
And I should see element 'LastModifiedDate_Log' present on page
And I should see element 'Comments_Field_Text' present on page
And I should see text 'testing contacts' present on page at 'Comments_Field-Activity'
And I should see text 'eComm Support' present on page at 'MFRMcallType_Case'
And I should see text 'CCO Ecomm user 2' present on page at 'Name_Case'
And I switch to default content
And I click by JS 'Close_Agent' 


Scenario: 3. Verify Functionality of eComm Support will be added to the Log a Call call type and used on Order -SP4_TC1.3
Given I click 'All_Dropdown_Agent'
And I click by JS 'ContactsFilter_ContactLead'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I clear the text and enter 'Jared Steiner' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I should see element 'ContactNameField_Contact' present on page
And I click 'ContactNameField_Contact'
And I wait for '10' seconds
And I should see element 'OrderNumber_Order' present on page
And I click by JS 'OrderNumber_Order'
And I should see element 'LogACallButton_Order' present on page
And I click by JS 'LogACallButton_Order'
And I should see element 'MFRMCallTypeDropdown_Order' present on page
And I click by JS 'MFRMCallTypeDropdown_Order'
And I select option 'eComm Support' in dropdown 'MFRMCallTypeDropdown_Order' by 'value'
And I should see element 'SubjectDropdown_Order' present on page
And I select option 'Call' in dropdown 'SubjectDropdown_Order' by 'value'
And I should see element 'CommentsField_Order' present on page
And I clear the text and enter 'testing order' in field 'CommentsField_Order'
And I wait for '5' seconds
And Blur 'CommentsField_Order'
And I wait for '5' seconds
And I click by JS 'SaveAndCloseButton_Order'
And I scroll till element 'ActiveHistoryLog_Order'
And I wait for '5' seconds
And I should see element 'ActiveHistoryLog_Order' present on page
And I should see element 'SubjectField_Order' present on page
And I should see text 'Call' present on page at 'SubjectField_Order'
And I should see text 'eComm Support' present on page at 'MFRMCallTypeField_Order'
And I should see element 'LastModifiedDateOptionActivity_Order' present on page
And I get text from 'LastModifiedDateOptionActivity_Order' and store
And I should see text 'CCO Ecomm user 2' present on page at 'AssignedToField_Order'
And I click by JS 'ViewAllLink_Order'
And I refresh the WebPage
And I wait for '15' seconds
And I switch to iFrame 'Frame_Log'
And I wait for '5' seconds
And I should see element 'AllActiveHistory_Header' present on page
And I should see element 'LastModifiedDate_Log' present on page
And I should see element 'Comments_Field_Text' present on page
And I should see text 'testing order' present on page at 'Comments_Field-Activity'
And I should see text 'eComm Support' present on page at 'MFRMcallType_Case'
And I should see text 'CCO Ecomm user 2' present on page at 'Name_Case'
And I switch to default content
And I click by JS 'Close_Agent' 

Scenario: 4.Verify Functionality of eComm Support will be added to the Log a Call call type and used on Utility bar -SP4_TC1.4
And I should see element 'LogACall_Call' present on page
And I click by JS 'LogACall_Call'
And I should see element 'MFRMCallTypeDropdown_Call' present on page
And I click by JS 'MFRMCallTypeDropdown_Call'
And I select option 'MFRMCallType.eComm Support' in dropdown 'MFRMCallTypeDropdown_Call' by 'value'
And I should see element 'SubjectField_Call' present on page
And I clear the text and enter 'testing Call' in field 'CommentsField_Call'
And I should see element 'NextButton_Call' present on page
And I click by JS 'NextButton_Call'
And I click by JS 'MinimizeButton_Call'
And I click 'All_Dropdown_Agent'
And I click by JS 'TasksOption_Call'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I clear the text and enter 'testing Call' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I should see element 'FirstSubjectField_Call' present on page
And I click by JS 'FirstSubjectField_Call'
And I scroll till element 'MFRMCallTypeValue_Call'
And I wait for '5' seconds
And I should see text 'eComm Support' present on page at 'MFRMCallTypeValue_Call'
And I should see text 'Call' present on page at 'SubjectValue_Call'
#And I should see text 'testing Call' present on page at 'CommentsValue_Call'
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent' 

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

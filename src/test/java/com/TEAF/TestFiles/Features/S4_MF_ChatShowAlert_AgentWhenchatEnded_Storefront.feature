@S4_ChatShowAlertAgent_Storefront
Feature: Show alert to agent when a chat is ended in Portal-Chat(IS)

1. To verify that Chat (IS) - Show alert to agent when a chat is ended - SP4_TC2
 
Scenario: 1. To verify that Service Chat Dis-positioning mandatory upon ending a chat as an agent - SP4_TC2
Given My WebApp 'MF_SalesForce' is open 
And I wait for '60' seconds
#waiting for Agent kim to be Online in OmniChannel
#------------------UAT URL------------------
#And I navigate to 'https://storefront:face@dev.mattressfirm.com/' application
#------------------QA URL------------------
And I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' application
And I should see element 'CookiesCloseButton_Hybrid' present on page
And I click by JS 'CookiesCloseButton_Hybrid'
And I wait for visibility of element 'MattressSite_CloseButton'
And I click 'MattressSite_CloseButton'
#And I wait for '15' seconds
#And I refresh the WebPage
#And I wait for '5' seconds
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I click 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'Joesmith@test.com' in field 'Email_Field_Omni'
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click 'HelpWithExistingOrder_Option'
#---------------OLD CHAT BOT-------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'
And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds
#---------------OLD CHAT BOT-------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
#delay in qa
And I wait for '10' seconds
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'




@S3_ChatterPriortoWarrantycase
Feature: Guest Able to Use Chatter Prior to Warranty case Submitted
 
 1. Login to Mattress Firm Sales Force Application
1.1 To verify that Guest Able to Use Chatter Prior to Warranty case Submitted - SP3_TC6
 
Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
And I change the screen dimension to '375' - '812'
#UAT URL
#And I navigate to 'https://storefront:face@dev.mattressfirm.com/' application
#-------------QA URL --------------
And I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' application
And I wait for '10' seconds
And I wait for visibility of element 'MattressSite_CloseButton'
And I click 'MattressSite_CloseButton'
And I should see element 'MobileToggle' present on page
And I click by JS 'MobileToggle'
And I scroll till element 'Login_Chatter_UAT'
And I wait for '10' seconds
And I should see element 'Login_Chatter_UAT' present on page
And I click by JS 'Login_Chatter_UAT'
And I wait for '10' seconds
And I should see element 'EmailField_Case' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_Case'
And I should see element 'PasswordField_Case' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_Case'
And Blur 'PasswordField_Case'
And I wait for '3' seconds
And I should see element 'LoginButton_Case' present on page
And I click by JS 'LoginButton_Case'
And I wait for '5' seconds
And I should see element 'MobileToggle' present on page
And I click by JS 'MobileToggle'
And I should see element 'ProfileName_Chatter' present on page
And I click by JS 'ProfileName_Chatter'
And I should see element 'ContinueButton_Chatter' present on page
And I click by JS 'ContinueButton_Chatter'
And I should see element 'MyPortalheader_Chatter' present on page
And I should see element 'UnderReviewLink_Chatter' present on page
And I click by JS 'UnderReviewLink_Chatter'
And I Close Window Tab
And I switch back to Main Window
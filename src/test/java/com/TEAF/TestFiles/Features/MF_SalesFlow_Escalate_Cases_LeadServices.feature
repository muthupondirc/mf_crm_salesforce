@MF_SalesFlow_Escalate_Cases_LeadServices
Feature: As an Agent Escalate Cases to Lead Services			

1. Login with Standard user Credentials
2. SP1_TC62 Escalate to Leads 
3. SP1_TC63 Payment Required 
4. Verify logout of Sales Force application
@Login
Scenario: Login Scenario
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'

Scenario: SP1_TC62 Escalate to Leads 
And I click by JS 'ShowNavMenu'
And I wait for '2' seconds
And I click by JS 'Home'
And I wait for '5' seconds
And I click by JS 'SearchTypeDD'
And I click by JS 'OrderHeaderGlobalSearch'
And I enter '03094905' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '10' seconds
And I click by JS 'SearchTypeDD'
And I click by JS 'AllGlobalSearch'
And I hit tab-key on element 'SearchTypeDD'
And I clear field 'GlobalSearchField'
And I enter '5165410544' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '10' seconds
And I should see element 'Contact_Header' present on page
And I wait for '10' seconds
And I scroll till element 'Contact_Header'
And I wait for '10' seconds
#And I wait for visibility of element 'EmailMessages'
#And I scroll to 'element' - 'EmailMessages'
And I wait for visibility of element 'Order_Header'
And I scroll till element 'Order_Header'
#And I scroll to 'element' - 'EmailMessages'
And I wait for '10' seconds
And I should see element 'Order_Header' present on page
And I click by JS 'Select_Order_Number'
And I wait for '5' seconds
And I click by JS 'New_Case'
And I wait for '3' seconds
And I should see element 'NewCaseRecordTypeLabel' present on page
And I should see element 'NewCaseRecordTypeDD' present on page
And I click by JS 'NewCaseRecordTypeDD'
And I wait for '2' seconds
And I select option 'Service' in dropdown 'NewCaseRecordTypeDD' by 'text'
And I wait for '2' seconds
And I should see element 'NewCaseRecordTypeNext' present on page
And I click by JS 'NewCaseRecordTypeNext'
And I wait for '5' seconds
And I should see element 'New_Case_Header' present on page
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'TypeOfProduct' present on page
And I click by JS 'TypeOfProductDD'
And I click by JS 'TypeOfProductValue'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'SubRecordType_NeedOption_Value'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'CaseNo' present on page
And I should see element 'EscalateCaseNumberValue' present on page
And I should see element 'SubTabOriginalOrderValue' present on page
And I wait for '2' seconds
And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_Escalate'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I wait for visibility of element 'CaseOwnerLeadServices'
And I should see element 'CaseOwner' present on page
And I scroll to 'element' - 'CaseOwnerLeadServices'
And I should see element 'CaseOwnerLeadServices' present on page
And I should see element 'EscalateCheckLabel' present on page
And I should see element 'EscalateCheckbox' present on page
And I click by JS 'Close'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '3' seconds

Scenario: SP1_TC63 Payment Required
And I click by JS 'SearchTypeDD'
And I click by JS 'OrderHeaderGlobalSearch'
And I enter '5165410544' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '10' seconds
And I click by JS 'SearchTypeDD'
And I click by JS 'AllGlobalSearch'
And I clear field 'GlobalSearchField'
And I enter '5165410544' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'Contact_Header' present on page
And I wait for '10' seconds
And I scroll till element 'Contact_Header'
And I wait for '10' seconds
#And I wait for visibility of element 'EmailMessages'
#And I scroll to 'element' - 'EmailMessages'
And I wait for visibility of element 'Order_Header'
And I scroll till element 'Order_Header'
#And I scroll to 'element' - 'EmailMessages'
And I wait for '10' seconds
And I should see element 'Order_Header' present on page
And I click 'Select_Order_Number'
And I wait for '5' seconds
And I click 'New_Case'
And I wait for '3' seconds
And I should see element 'NewCaseRecordTypeLabel' present on page
And I should see element 'NewCaseRecordTypeDD' present on page
And I click by JS 'NewCaseRecordTypeDD'
And I wait for '2' seconds
And I select option 'Service' in dropdown 'NewCaseRecordTypeDD' by 'text'
And I wait for '2' seconds
And I should see element 'NewCaseRecordTypeNext' present on page
And I click by JS 'NewCaseRecordTypeNext'
And I wait for '5' seconds
And I should see element 'New_Case_Header' present on page
And I should see element 'Case_Origin_Label' present on page
And I click 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click 'Source_DD'
And I click 'Source_DD_Value'
And I should see element 'TypeOfProduct' present on page
And I click by JS 'TypeOfProductDD'
And I click by JS 'TypeOfProductValue'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'SubRecordType_NeedOption_Value'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'CaseNo' present on page
And I should see element 'EscalateCaseNumberValue' present on page
And I should see element 'SubTabOriginalOrderValue' present on page
And I wait for '2' seconds
And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_Payment'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I wait for visibility of element 'CaseOwnerLeadServices'
#And I wait for visibility of element 'StoredCaseNumber'
#And I get text from 'StoredCaseNumber' and store
And I should see element 'CaseOwner' present on page
And I should see element 'CaseOwnerLeadServices' present on page
And I should see element 'PaymentCheckLabel' present on page
And I should see element 'PaymentCheckbox' present on page
And I click by JS 'Close'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '3' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
@MF_Escalation_process_for_Retention
Feature: SFLC - 25163_As a Retention Agent, I want to have the ability to escalate the cases in Salesforce.
1.Login with Hybrid User
2.SP10_1 Create a Retention case in salesforce
3.SP10_2 Verify escalate retention case in saleforce
4.Verify logout of Sales Force application

Scenario: Creating a Login Issue Case from Mattress Firm Web Site
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'hybrid' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'hybrid_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: Create a Retention case in salesforce
And I click by JS 'ShowNavMenu'
And I click by JS 'NavCases'
And I click by JS 'lnkCases'
And I should see element 'CreateNewCase' present on page
And I click by JS 'CreateNewCase'
And I should see element 'NewRetentionCaseSelectionBtn' present on page
And I should see element 'NewRetentionCaseSelectionLabel' present on page
And I click by JS 'NewRetentionCaseSelectionBtn'
And I should see element 'NewCaseSelectionNxt' present on page
And I click by JS 'NewCaseSelectionNxt'
And I should see element 'New_RetentionCase_Header' present on page
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'SubRecordType_CancelPartOrder'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'CaseNo' present on page
And I should see element 'CaseNumberValue' present on page
And I should see CaseOwner name 'hybrid_displayname' present on page
And I get text from 'CaseNumberValue' and store
And I click by JS 'Close'
And I wait for '5' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

Scenario: Verify escalate retention case in saleforce
Given I login to Mattress Firm Sales Force with valid 'servicelead' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'servicelead_displayname' present on page at 'ProfileCardPopUp'
And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '3' seconds
And I should see element 'lbl_NumberOfTimesEscalated' present on page
And I should see text '0' present on page at 'txtValue_NumberOfTimesEscalated'
And I should see element 'lbl_EscalatedAgent' present on page
And I should see element 'lbl_EscalatedDate' present on page

And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I click by JS 'Status_CaseEscalation'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I scroll till element 'EscalateOwner' 
And I should see element 'CheckBoxRetentionCaseEscalated' present on page
And I should see text 'Case Escalation' present on page at 'StatusValue'
And I should see text 'Customer Retention' present on page at 'CaseRecordTypeInValue'
And I should see text '1' present on page at 'txtValue_NumberOfTimesEscalated'

And I click by JS 'EscalateEdit'
And I should see element 'TypeOfProduct' present on page
And I click by JS 'TypeOfProductDD'
And I click by JS 'TypeOfProductValue'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I should see element 'lbl_NumberOfTimesEscalated' present on page
And I should see text '1' present on page at 'txtValue_NumberOfTimesEscalated'
And I click by JS 'EscalateEdit'
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I click by JS 'Status_PendingReply'
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'ShowOptionButtonToAccept' present on page
And I click by JS 'ShowOptionButtonToAccept'
And I click by JS 'AcceptButton'
And I wait for '2' seconds
And I click by JS 'EscalateEdit'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_CaseEscalation'
And I click by JS 'BedBugSave'
And I should see element 'lbl_NumberOfTimesEscalated' present on page
And I should see text '2' present on page at 'txtValue_NumberOfTimesEscalated'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

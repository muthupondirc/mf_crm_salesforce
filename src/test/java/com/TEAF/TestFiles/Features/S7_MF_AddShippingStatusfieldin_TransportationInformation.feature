@S7_Shippingstatus
Feature: Add Shipping status field in transportation information - Transportation Request 3

1. Add Shipping status field in transportation information - Transportation Request 3 - SP7_TC1


Scenario: 1.Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'Logistics Mgr User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'Logistics Mgr User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: Add Shipping status field in transportation information - Transportation Request 3 - SP7_TC
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I click by JS 'SelectListViewDropdown_CR'
And I should see element 'SearchField_CR' present on page
And I clear the text and enter 'All Nationwide Cases' in field 'SearchField_CR'
And I hit enter-key on element 'SearchField_CR'
And I wait for '5' seconds
And I should see element 'NewEscalatedCaseNumber_CR' present on page
And I click by JS 'NewEscalatedCaseNumber_CR'
And I should see element 'EditButton_Shipping' present on page
And I click by JS 'EditButton_Shipping'
And I scroll till element 'ShippingDropdown'
And I click by JS 'ShippingDropdown'
And I click by JS 'ProcessingOption'
And I click 'Save_Button_Close'
And I wait for '5' seconds
And I scroll till element 'ShippingStatusField'
And I should see text 'Processing' present on page at 'ShippingStatusField'
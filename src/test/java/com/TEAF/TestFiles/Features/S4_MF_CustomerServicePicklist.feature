@S4_CustomerServicePicklist
Feature: Customer Service-Add additional picklist values to Ecomm Support Cases
 
1. Login to Mattress Firm Sales Force Application
1.1 To verify that Customer Service-Add additional picklist values to Ecomm Support Cases - SP4_TC3

Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Ecomm user 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Ecomm user 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 To verify that Customer Service-Add additional picklist values to Ecomm Support Cases - SP4_TC3
And I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'Cases_Ecomm' present on page
And I click by JS 'Cases_Ecomm'
And I should see element 'NewButton_Ecomm' present on page
And I click by JS 'NewButton_Ecomm'
And I should see element 'EcommSupport_RadioButton' present on page
And I click by JS 'EcommSupport_RadioButton'
And I should see element 'NextButton_Ecomm' present on page
And I click by JS 'NextButton_Ecomm'
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click by JS 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click by JS 'SubRecordTypeDropdown_Ecomm'
And I click by JS 'ExternalOption_Service'
And I click by JS 'SubTypeDropdown_Ecomm'
And I click 'ProductReselectionOption_Service'
And I should see element 'SaveButton_Ecomm' present on page
And I click by JS 'SaveButton_Ecomm'
And I wait for '5' seconds
And I scroll till element 'SubRecordTypeField_Service'
And I should see text 'External' present on page at 'SubRecordTypeField_Service'
And I should see text 'Product Reselection' present on page at 'SubTypeField_Service'
And I click by JS 'Close_Agent' 

Scenario: 2. To verify that Customer Service-Add additional picklist values to Ecomm Support Cases - SP4_TC3.1
And I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'Cases_Ecomm' present on page
And I click by JS 'Cases_Ecomm'
And I should see element 'NewButton_Ecomm' present on page
And I click by JS 'NewButton_Ecomm'
And I should see element 'EcommSupport_RadioButton' present on page
And I click by JS 'EcommSupport_RadioButton'
And I should see element 'NextButton_Ecomm' present on page
And I click by JS 'NextButton_Ecomm'
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click by JS 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click by JS 'SubRecordTypeDropdown_Ecomm'
And I click by JS 'StoreOperationsOption_Service'
And I click by JS 'SubTypeDropdown_Ecomm'
And I click 'OrderIssueOption_Service'
And I should see element 'SaveButton_Ecomm' present on page
And I click by JS 'SaveButton_Ecomm'
And I wait for '5' seconds
And I scroll till element 'SubRecordTypeField_Service'
And I should see text 'Store Operations' present on page at 'SubRecordTypeField_Service'
And I should see text 'Order Issue' present on page at 'SubTypeField_Service'
And I click by JS 'Close_Agent' 


Scenario: 3. To verify that Customer Service-Add additional picklist values to Ecomm Support Cases - SP4_TC3.2
And I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'Cases_Ecomm' present on page
And I click by JS 'Cases_Ecomm'
And I should see element 'NewButton_Ecomm' present on page
And I click by JS 'NewButton_Ecomm'
And I should see element 'EcommSupport_RadioButton' present on page
And I click by JS 'EcommSupport_RadioButton'
And I should see element 'NextButton_Ecomm' present on page
And I click by JS 'NextButton_Ecomm'
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click by JS 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click by JS 'SubRecordTypeDropdown_Ecomm'
And I click by JS 'WarehouseOption_Service'
And I click by JS 'SubTypeDropdown_Ecomm'
And I click 'DeliverySlotNeededOption_Service'
And I should see element 'SaveButton_Ecomm' present on page
And I click by JS 'SaveButton_Ecomm'
And I wait for '5' seconds
And I scroll till element 'SubRecordTypeField_Service'
And I should see text 'Warehouse' present on page at 'SubRecordTypeField_Service'
And I should see text 'Delivery Slot Needed' present on page at 'SubTypeField_Service'
And I click by JS 'Close_Agent' 


Scenario: 4. To verify that Customer Service-Add additional picklist values to Ecomm Support Cases - SP4_TC3.3
And I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'Cases_Ecomm' present on page
And I click by JS 'Cases_Ecomm'
And I should see element 'NewButton_Ecomm' present on page
And I click by JS 'NewButton_Ecomm'
And I should see element 'EcommSupport_RadioButton' present on page
And I click by JS 'EcommSupport_RadioButton'
And I should see element 'NextButton_Ecomm' present on page
And I click by JS 'NextButton_Ecomm'
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click by JS 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click by JS 'SubRecordTypeDropdown_Ecomm'
And I click by JS 'InsideSalesSupportOption_Service'
And I click by JS 'SubTypeDropdown_Ecomm'
And I click 'ProductReselectionOption_Service'
And I should see element 'SaveButton_Ecomm' present on page
And I click by JS 'SaveButton_Ecomm'
And I wait for '5' seconds
And I scroll till element 'SubRecordTypeField_Service'
And I should see text 'Inside Sales Support' present on page at 'SubRecordTypeField_Service'
And I should see text 'Product Reselection' present on page at 'SubTypeField_Service'
And I click by JS 'Close_Agent' 

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page
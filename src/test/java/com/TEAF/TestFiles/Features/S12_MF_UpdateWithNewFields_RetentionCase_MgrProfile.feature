@S12_MF_UpdateWithNewFields_RetentionCase_MgrProfile
Feature: Retention case update with new fields - layout updates

1. Login to Mattress Firm Sales Force Application
2. Retension case - update on all the New fields - Manager Profile _ SP12_TC04-SFLC - 27552
3. Retension case - verifying a field in Agent Profile _ SP12_TC04-SFLC - 27552
4. Logout of Sales Force application

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'HybridMgr_QA' User Credentials 
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'hybridmgr_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

Scenario: Retension case - update on all the New fields_SP12_TC04-SFLC - 27552
And I should see element 'button_New' present on page
And I click 'button_New'
And I wait for '3' seconds
Then I should see element 'NewCase' present on page
Then I should see element 'CustomerRetention' present on page
And I click by JS 'radiobtn_CustomerRetention'
And I click 'button_Next'
Then I should see element 'text_CustomerRetention' present on page
And I click by JS 'dropdown_CaseOrigin'
And I select value 'Chat' from the dropdown
Then I should see element 'field_CR_DisputeAmount' present on page
And I click 'txtbox_CR_DisputeAmount'
And I clear the text and enter '55' in field 'txtbox_CR_DisputeAmount'
Then I should see element 'field_CR_DisputeDeadline' present on page
And I click 'txtbox_CR_DisputeDeadline'
And I click by JS 'date'
Then I should see element 'field_CR_HiveNo' present on page
And I click 'txtbox_CR_HiveNo'
And I clear the text and enter '123' in field 'txtbox_CR_HiveNo'
Then I should see element 'field_CR_PodiumConversationURL' present on page
And I click 'txtbox_CR_PodiumConversationURL'
And I clear the text and enter 'Test 123' in field 'txtbox_CR_PodiumConversationURL'
Then I should see element 'field_CR_AgentCoachingOpportunity' present on page
And I click 'checkbox_CR_AgentCoachingOpportunity'
Then I should see element 'field_CR_PaymentRequired' present on page
And I click 'checkbox_CR_PaymentRequired'
Then I should see element 'field_AttemptContact' present on page
And I click by JS 'dropdown_AttemptContact'
And I select value 'First' from the dropdown
And I click by JS 'dropdown_Source'
#And I select value 'Phone' from the dropdown
#And I click by JS 'option_Chat'
And I click by JS 'dropdown_Sub Record Type'
And I select value 'Miscellaneous' from the dropdown
And I click by JS 'button_Save'

Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'HybridAgent_QA' User Credentials 
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'hybrid_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

Scenario: Retension case - verifying a field in Agent Profile _ SP12_TC04-SFLC - 27552
#03115598 #03114996
And I should see element 'txtBox_SearchField' present on page
And I click by JS 'txtBox_SearchField'
And I clear the text and enter '03115598' in field 'txtBox_SearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'txtBox_SearchField'
And I wait for '2' seconds
And I hit down Arrow key on element 'txtBox_SearchField'
And I wait for '2' seconds
And I hit enter-key on element 'txtBox_SearchField'
And I wait for '5' seconds
And I click by JS 'button_Edit'
Then I should see element 'txt_Edit' present on page
#Then I should not see element 'field_CR_AgentCoachingOpportunity' present on page

Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page


@S12_MF_EscalationQueue_AllTrasportationProcess_Logistics

Feature: One Escalation Queue for all Transportation Process

1. Login to Mattress Firm Sales Force Application
2. Escalation Queue for Transportation Claim-S12_TC13_SFLC-28143
3. Escalation Queue for Transportation Invoice-S12_TC13_SFLC-28143
4. Escalation Queue for Nation-wide-S12_TC13_SFLC-28143
5. Logout of Sales Force application

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'Logistics Mgr User 2_QA' User Credentials 
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'transportationmgr_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

Scenario: Escalation Queue for Transportation Claim-S12_TC13_SFLC-28143
And I should see element 'button_New' present on page
And I click 'button_New'
And I wait for '3' seconds
Then I should see element 'radiobtn_TransportationClaim' present on page
And I click by JS 'radiobtn_TransportationClaim'
And I click 'button_Next'
Then I should see element 'txt_NewCase:TransportationClaim' present on page
And I click by JS 'dropdown_CaseOrigin'
And I select value 'Chat' from the dropdown
And I should see element 'field_MFRMReferenceNumber' present on page
And I click 'textbox_MFRMReferenceNumber'
And I clear the text and enter 'Text 123' in field 'textbox_MFRMReferenceNumber'
Then I should see element 'field_EscalateCase' present on page
And I click 'checkbox_EscalateCase'
Then I should see element 'field_ReasonforEscalation' present on page
And I click by JS 'dropdown_ReasonforEscalation'
And I select value 'Executive Approval Required' from the dropdown
And I click by JS 'button_Save'
And I wait for '3' seconds
And I should see element 'field_CaseOwner' present on page
Then I should see element 'text_Transportation Escalation Queue' present on page
And I should see element 'field_EscalateCase' present on page
Then I verify checkbox 'checkbox3_EscalateCase' is 'checked'
Then I should see element 'field_ReasonforEscalation' present on page
Then I should see element 'value_ReasonforEscalation' present on page
And I close all the opened tabs 'iconClose'

Scenario: Escalation Queue for Transportation Invoice-S12_TC13_SFLC-28143
And I should see element 'button_New' present on page
And I click 'button_New'
And I wait for '3' seconds
Then I should see element 'radiobtn_TransportationInvoice' present on page
And I click by JS 'radiobtn_TransportationInvoice'
And I click 'button_Next'
Then I should see element 'txt_NewCase:TransportationInvoice' present on page
And I click by JS 'dropdown_CaseOrigin'
And I select value 'Chat' from the dropdown
And I should see element 'field_MFRMReferenceNumber' present on page
And I click 'textbox_MFRMReferenceNumber'
And I clear the text and enter 'Text 123' in field 'textbox_MFRMReferenceNumber'
Then I should see element 'field_EscalateCase' present on page
And I click 'checkbox_EscalateCase'
Then I should see element 'field_ReasonforEscalation' present on page
And I click by JS 'dropdown_ReasonforEscalation'
And I select value 'Executive Approval Required' from the dropdown
And I click by JS 'button_Save'
And I wait for '3' seconds
And I should see element 'field_CaseOwner' present on page
Then I should see element 'text_Transportation Escalation Queue' present on page
And I should see element 'field_EscalateCase' present on page
Then I verify checkbox 'checkbox3_EscalateCase' is 'checked'
Then I should see element 'field_ReasonforEscalation' present on page
Then I should see element 'value_ReasonforEscalation' present on page
And I close all the opened tabs 'iconClose'

Scenario: Escalation Queue for Nation-wide-S12_TC13_SFLC-28143
And I should see element 'txtBox_SearchField' present on page
And I click by JS 'txtBox_SearchField'
And I clear the text and enter '03110191' in field 'txtBox_SearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'txtBox_SearchField'
And I wait for '2' seconds
And I hit down Arrow key on element 'txtBox_SearchField'
And I wait for '2' seconds
And I hit enter-key on element 'txtBox_SearchField'
And I wait for '5' seconds
And I click by JS 'button_Edit'
Then I should see element 'field2_ReasonforEscalation' present on page
And I click by JS 'dropdown_ReasonforEscalation'
And I select value 'Executive Approval Required' from the dropdown
Then I should see element 'field2_EscalateCase' present on page
And I click 'checkbox2_EscalateCase'
And I click by JS 'button_Save_Nationwide'
Then I should see element 'lbl_Error_Nationwide' present on page
And I click by JS 'button_Cancel'
#And I click by JS 'button_Save_Nationwide'
And I wait for '3' seconds
And I should see element 'field_CaseOwner' present on page
Then I should see element 'text_Transportation Escalation Queue' present on page
Then I should see element 'field_ReasonforEscalation' present on page
Then I should see element 'value_ReasonforEscalation' present on page
And I should see element 'field3_EscalateCase' present on page
Then I verify checkbox 'checkbox3_EscalateCase' is 'notchecked'

Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page



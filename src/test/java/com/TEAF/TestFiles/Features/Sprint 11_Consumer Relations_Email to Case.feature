@MF_Emailtocase 
Feature: Email To Case - Consumer Relations 

1.SFLC-17639 - Consumer Relations - Email to Case
2.Validate the Nationwide record type cases
3.Verify Consumer Relations - Email to Case for Vendor and Driver manage emails
4.Verify Logistics - Case Process  for Transportation Invoices.

@email 
Scenario Outline: SFLC-17639 - Consumer Relations - Email to Case 
	Given I send email from gmail inbox with subject '<email>' '<subject>' '<content>' 
	And My WebApp 'MF_SalesForce' is open 
	And I wait for '5' seconds 
	When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
	And I close all the opened tabs 'iconClose' 
	And I click by JS 'UserImageButton' 
	And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp' 
	And I click by JS 'ShowNavMenu' 
	And I click by JS 'CasesMenu' 
	And I wait for visibility of element 'SelectListViewDropdown' 
	And I click by JS 'SelectListViewDropdown' 
	And I wait for visibility of element 'SearchListViewInputBox' 
	And I enter '<subject>' in field 'SearchListViewInputBox' 
	And I wait for visibility of element 'SelectSearchItem' 
	And I click by JS 'SelectSearchItem' 
	And I wait for '5' seconds 
	And I select the last case from the list 'SelectFirstCase' 
	And I wait for '5' seconds 
	And I wait for visibility of element 'CaseOwnerValue' 
	And I should see text '<subject>' present on page at 'CaseOwnerValue' 
	And I should see text 'Email' present on page at 'CaseOriginValue' 
	And I should see text 'New' present on page at 'StatusValue' 
	And I should see text '<casetype>' present on page at 'CRCaseTypeValue' 
	And I should see text '<subject>' present on page at 'SubjectValue' 
	And I should see text '<content>' present on page at 'DescriptionValue' 
	And I should see text 'dowjonestest0@gmail.com' present on page at 'WebEmailValue' 
	And I click link 'Log Out' 
	
	Examples: 
	
		|email							|subject		|content	 	|casetype				  |
		|Email_Agency					|CR - Medium	|testcontent1	|Agency				      |
		|Email_ExecutiveCorrespondence	|CR - High		|testcontent2	|Executive Correspondence |
		|Email_FieldSupport				|CR - Medium	|testcontent3	|Field Support			  |
		|Email_Legal					|CR - High		|testcontent4	|Legal					  |
		|Email_Media					|CR - High		|testcontent5	|Media					  |
		|Email_Norton					|CR - Low		|testcontent6	|Norton				      |
		|Email_PreDeliveryInfestation	|CR - Medium	|testcontent7	|Pre Delivery Infestation |
		|Email_SFOChargeback			|CR - Medium	|testcontent8	|SFO Chargeback		      |
		
		
@Nationwiderecords 
Scenario Outline: Validate the Nationwide record type cases 
	Given I send email from gmail inbox with subject '<email>' '<subject>' '<content>' 
	And My WebApp 'MF_SalesForce' is open 
	And I wait for '5' seconds 
	When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
	And I close all the opened tabs 'iconClose' 
	And I click by JS 'UserImageButton' 
	And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp' 
	And I click by JS 'ShowNavMenu' 
	And I wait for '2' seconds 
	And I click by JS 'CasesMenu' 
	And I wait for visibility of element 'SelectListViewDropdown' 
	And I click by JS 'SelectListViewDropdown' 
	And I wait for visibility of element 'SearchListViewInputBox' 
	And I enter '<searchtext>' in field 'SearchListViewInputBox' 
	And I wait for '2' seconds 
	And I wait for visibility of element 'SelectSearchItem' 
	And I click by JS 'SelectSearchItem' 
	And I wait for '5' seconds 
	#And I click by JS 'CaseNumberColumn'
	#And I wait for '5' seconds 
	And I Check all 10 cases have the values '<caseowner>' '<caseorigin>' '<caserecordtype>' '<resolutiondetails>' 
	And I click by JS 'UserImageButton' 
	And I click link 'Log Out' 
	And I should see element 'login_username' present on page 
	
	
	Examples: 
	
		|searchtext		|caseorigin		|caseowner			|caserecordtype			|resolutiondetails	 	|
		|Nationwide Cutoff |Mulesoft	|Nationwide Cutoff 	|Nationwide	|Your request has not been processed Reason: We are not authorized to process tickets that are not profitable or under $1,000 as nationwide. Attention: Please either offer guest a DTC option or reach out to your management for an alternative solution and update guest accordingly.|
		

Scenario Outline: Verify Consumer Relations - Email to Case for Vendor and Driver manage emails
	Given I send email from gmail inbox with subject '<email>' '<subject>' '<content>' 
	And My WebApp 'MF_SalesForce' is open 
	And I wait for '5' seconds 
	When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
	And I close all the opened tabs 'iconClose' 
	And I click by JS 'UserImageButton' 
	And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp' 
	And I click by JS 'ShowNavMenu' 
	And I wait for '2' seconds 
	And I click by JS 'CasesMenu' 
	And I wait for visibility of element 'SelectListViewDropdown' 
	And I click by JS 'SelectListViewDropdown' 
	And I wait for visibility of element 'SearchListViewInputBox' 
	And I enter '<subject>' in field 'SearchListViewInputBox' 
	And I wait for '2' seconds 
	And I wait for visibility of element 'SelectSearchItem' 
	And I click by JS 'SelectSearchItem' 
	And I wait for '5' seconds 
	#And I click by JS 'CaseNumberColumn'
	#And I wait for '5' seconds 
	And I select the last case from the list 'SelectFirstCase' 
	And I wait for '5' seconds 
	And I wait for visibility of element 'CaseOwnerValue' 
	And I should see text '<subject>' present on page at 'CaseOwnerValue' 
	And I should see text 'Email' present on page at 'CaseOriginValue' 
	And I should see text 'New' present on page at 'StatusValue' 
	And I should see text '<casetype>' present on page at 'CRCaseTypeValue' 
	And I should see text '<content>' present on page at 'DescriptionValue' 
	And I should see text 'hasantestuser@gmail.com' present on page at 'WebEmailValue' 
	And I click by JS 'EscalateEdit' 
	And I scroll to 'element' - 'Case_Classification' 
	And I wait for '2' seconds 
	And I should see element 'BedBugStatus' present on page 
	And I should see element 'BedBugStatusDD' present on page 
	And I should see element 'checkBoxSecondNotice' present on page 
	And I click by JS 'BedBugStatusDD' 
	And I select value 'Closed' from the dropdown 
	And I click by JS 'ddState' 
	And I select value 'AK' from the dropdown 
	And I click by JS 'ddBrand' 
	And I select value 'Mattress Firm' from the dropdown 
	And I click by JS 'ddSource' 
	And I select value 'Chat' from the dropdown 
	And I click by JS 'ddMerchandiseDisposition' 
	And I select value 'Prime' from the dropdown 
	And I click by JS 'ddCurrentState' 
	And I select value 'Customer Reviewing Offer' from the dropdown 
	And I click by JS 'ddCR_Reason' 
	And I select value 'E-Commerce' from the dropdown 
	And I click by JS 'ddCR_Reason Details' 
	And I select value 'Direct Delivery' from the dropdown 
	And I wait for '2' seconds 
	And I click by JS 'BedBugSave' 
	And I wait for '5' seconds 
	And I click by JS 'EcommSendExternalEmail' 
	And I should see element 'EcommToEmailField' present on page 
	And I click by JS 'EcommToEmailField' 
	And I should see element 'EcommSendEmailButton' present on page 
	And I click by JS 'EcommSendEmailButton' 
	And I reply email from gmail inbox 'Email_VendorRequest' 
	And I wait for '10' seconds 
	And I click by JS 'EcommTabHeaderDropdown' 
	And I should see element 'EcommRefreshTab' present on page 
	And I click by JS 'EcommRefreshTab' 
	And I should see element 'EcommNewEmail' present on page 
	And I should see element 'EcommNewEmailCheckBox' present on page 
	And I click by JS 'Close' 
	And I click link 'Log Out' 
	
	
	Examples: 
	
		|email							|subject		|content	 	|casetype				|
		|Email_VendorRequest			|CR - Medium	|testcontent1	|Vendor Request			|
		|Email_DriverDamage				|CR - Medium	|testcontent1	|Driver Damage			|
		
	
Scenario: Verify Logistics - Case Process  for Transportation Invoices.

	Given My WebApp 'MF_SalesForce' is open 
	And I wait for '5' seconds 
	When I login to Mattress Firm Sales Force with valid 'transportation' User Credentials 
	And I close all the opened tabs 'iconClose' 
	And I click by JS 'UserImageButton' 
	And I should see profile name 'transportation_displayname' present on page at 'ProfileCardPopUp' 
	And I click by JS 'ShowNavMenu' 
	And I wait for '2' seconds 
	And I click by JS 'CasesMenu'
	And I should see element 'CreateNewCase' present on page
	And I click by JS 'CreateNewCase'
	And I click by JS 'radioBtnTransportationInvoice'
	And I click by JS 'NewCaseSelectionNxt'
	And I should see element 'headerTransportationInvoices' present on page
	And I click by JS 'ddCaseOrigin'
	And I select value 'Chat' from the dropdown
	And I click by JS 'BedBugSave'
	And I wait for '5' seconds
	And I should see CaseOwner name 'transportation_displayname' present on page
	And I should see text 'Chat' present on page at 'CaseOriginValue'
	And I should see text 'Transportation Invoices' present on page at 'CaseRecordTypeInValue'
	And I click by JS 'Close'
	And I send email from gmail inbox with subject '<email>' '<subject>' '<content>'
	And I wait for visibility of element 'SelectListViewDropdown' 
	And I click by JS 'SelectListViewDropdown' 
	And I wait for visibility of element 'SearchListViewInputBox' 
	And I enter 'Transportation Invoices' in field 'SearchListViewInputBox' 
	And I wait for '2' seconds 
	And I wait for visibility of element 'SelectSearchItem' 
	And I click by JS 'SelectSearchItem' 
	And I wait for '5' seconds 
	#And I click by JS 'CaseNumberColumn'
	#And I wait for '5' seconds 
	And I select the last case from the list 'SelectFirstCase' 
	And I wait for '5' seconds 
	And I wait for visibility of element 'CaseOwnerValue' 
	And I should see text 'Transportation Invoices' present on page at 'CaseOwnerValue'
	And I should see text 'Medium' present on page at 'valuePriority'
	And I should see text 'Transportation Invoices' present on page at 'CaseRecordTypeInValue'
	And I should see element 'EcommNewEmailCheckBox' present on page
	
	
	
	|email							|subject		|content	 	|
	|Email_transportation			|CR - Medium	|testcontent1	|
	
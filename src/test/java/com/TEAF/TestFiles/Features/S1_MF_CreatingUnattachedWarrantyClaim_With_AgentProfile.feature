@S1_MF_Unattached_Warranty_Claim_Agent
Feature: Creating Unattached Warranty Claim with Agent Profile
 
1. Login to Mattress Firm Sales Force Application
1.1 Creating Unattached Warranty Claim - SP1_TC44
2. Creating Unattached Warranty Claim (Continued) - SP1_TC45
3. Creating Unattached Warranty Claim (Continued) - SP1_TC46
4. Making a Determination on Unattached Warranty Claim - SP1_TC47
5. Verifying Approval and Denial Letters on Unattached Warranty Claim - SP1_TC48
6. Logging a Call on an Unattached Warranty Claim - SP1_TC49

Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
#below steps are Using Auto IT 
#And I wait for '10' seconds
#And I should click PDF Download button on page
#And I validate PDF file - filename: 'WarrantyCasePDF.pdf' & path: 'C:\\Users\\Sindhu\\Downloads' content - Field Name 'Name' & Field Value: 'Michael Smith'
#And I validate PDF file - filename: 'WarrantyCasePDF.pdf' & path: 'C:\\Users\\Sindhu\\Downloads' content - Field Name 'Address' & Field Value: '1000 South Oyster Bay Rd'
#And I validate PDF file - filename: 'WarrantyCasePDF.pdf' & path: 'C:\\Users\\Sindhu\\Downloads' content - Field Name 'Customer Code' & Field Value: 'C012345678'
#And I validate PDF file - filename: 'WarrantyCasePDF.pdf' & path: 'C:\\Users\\Sindhu\\Downloads' content - Field Name 'Case Number' & Field Value: '02911293'
#And I validate PDF file - filename: 'WarrantyCasePDF.pdf' & path: 'C:\\Users\\Sindhu\\Downloads' content - Field Name ' Original Purchase Date' & Field Value: 'March 01, 2021'
#And I validate PDF file - filename: 'WarrantyCasePDF.pdf' & path: 'C:\\Users\\Sindhu\\Downloads' content - Field Name 'Total In-Store Credit' & Field Value: '$250.00'
#And I validate PDF file - filename: 'WarrantyCasePDF.pdf' & path: 'C:\\Users\\Sindhu\\Downloads' content - Field Name 'Returning Item(s)' & Field Value: 'Sealy Queen Box Spring'


Scenario: 1.1 Creating Unattached Warranty Claim - SP1_TC44
Given I click by JS 'UnattachedWarrantyClaim_Button'
And I click by JS 'FileUnattachedWarrantyClaim_Button'
And I click by JS 'Minimize_Button'
And I switch to iFrame 'Frame'
And I should see element 'CustomerInformation_header' present on page
And I should see element 'FirstName_Field_Warranty' present on page
And I clear the text and enter 'Michael' in field 'FirstName_Field_Warranty'
And I should see element 'LastName_Field' present on page
And I clear the text and enter 'Smith' in field 'LastName_Field'
And I should see element 'Street_Field_Claim' present on page
And I clear the text and enter '1000 South Oyster Bay Rd' in field 'Street_Field_Claim'
And I should see element 'City_Field_Claim' present on page
And I clear the text and enter 'Hicksville' in field 'City_Field_Claim'
And I select option 'NY' in dropdown 'State_Dropdown_Claim' by 'value'
And I should see element 'PostalCode_Field_Claim' present on page
And I clear the text and enter '10001' in field 'PostalCode_Field_Claim'
And I should see element 'CustomerPrimaryPhone_Field' present on page
And I clear the text and enter '5168617625' in field 'CustomerPrimaryPhone_Field'
And I should see element 'CustomerSecondaryPhone_Field' present on page
And I clear the text and enter '5168618800' in field 'CustomerSecondaryPhone_Field'
And I should see element 'Email_Field' present on page
And I clear the text and enter 'Michael.Smith@aol.com' in field 'Email_Field'
And I click 'Next_Button1'
And I should see element 'Order_Field' present on page
And I clear the text and enter 'S012345678' in field 'Order_Field'
And I should see element 'WrittenDate_Field' present on page
And I click 'WrittenDate_Field'
And I wait for '3' seconds
And I click 'January_Date'
And I click '1_Date'
And I should see element 'BusinessChannel_Dropdown' present on page
And I select option 'MFRM' in dropdown 'BusinessChannel_Dropdown' by 'value'
And I should see element 'ProductionDescription_Field' present on page
And I clear the text and enter 'Sealy Queen Mattress' in field 'ProductionDescription_Field'
And I should see element 'PricePaid_Field' present on page
And I clear the text and enter '1000' in field 'PricePaid_Field'
And I should see element 'ProductType_Dropdown' present on page
And I select option 'Mattress/Box Spring' in dropdown 'ProductType_Dropdown' by 'value'
And I should see element 'ProductSize_Dropdown' present on page
And I select option 'Queen' in dropdown 'ProductSize_Dropdown' by 'value'
And I should see element 'Comments_Text' present on page
And I clear the text and enter 'Mattress' in field 'Comments_Text'
And I click 'option+'
And I should see element 'ProductionDescription_Field2' present on page
And I clear the text and enter 'Sealy Queen Box Spring' in field 'ProductionDescription_Field2'
And I should see element 'PricePaid_Field2' present on page
And I clear the text and enter '250' in field 'PricePaid_Field2'
And I should see element 'ProductType_Dropdown2' present on page
And I select option 'Mattress/Box Spring' in dropdown 'ProductType_Dropdown2' by 'value'
And I should see element 'ProductSize_Dropdown2' present on page
And I select option 'Queen' in dropdown 'ProductSize_Dropdown2' by 'value'
And I should see element 'Comments_Text2' present on page
And I clear the text and enter 'Box' in field 'Comments_Text2'
And I click 'Next_Button2'


Scenario: 2. Creating Unattached Warranty Claim (Continued) - SP1_TC45
Given I should see element 'SealyQueenMattress_Dropdown' present on page
And I select option 'MAT - BODY INDENTATIONS' in dropdown 'SealyQueenMattress_Dropdown' by 'value'
And I should see element 'SealyQueenBoxspring_Dropdown' present on page
And I select option 'BOX - COMPRESSION OF MODULES' in dropdown 'SealyQueenBoxspring_Dropdown' by 'value'
And I click 'Next_Button3'
And I wait for '5' seconds
And I should see element 'Question#1_Text' present on page
And I click 'Yes_Button'
And I should see element 'Question#2_Text' present on page
And I click 'Metal_Button'
And I wait for '8' seconds
And I should see element 'Question#2a_Text' present on page
And I select option '1' in dropdown 'Option_1' by 'value'
And I should see element 'Question#3_Text' present on page
And I select option '5' in dropdown 'Option_5' by 'value'
And I should see element 'Question#4_Text' present on page
And I click 'Option_No'
And I click 'Next_Button4'
And I wait for '5' seconds
And I should see element 'VendorInspection_Text' present on page
And I click 'VendorInspection_Button'
And I click 'Complete_Button'
And I wait for '5' seconds
And I should see element 'MattressMarshals_Header' present on page
And I should see element 'PaymentLink' present on page
And I click 'PaymentLink'
And I switch to window with title 'USAePay Login'
And I should see element 'USAePAY_Header' present on page
And I wait for '3' seconds
And I Close Window Tab
And I switch to iFrame 'Frame'
And I should see element 'CaseNumber_Warranty' present on page
And I click 'CaseNumber_Warranty'
#And I refresh the WebPage
#And I wait for '10' seconds
And I switch to default content
And I should see element 'CaseWarrantyClaim_Header' present on page


Scenario: 3. Creating Unattached Warranty Claim (Continued) - SP1_TC46
Given I should see element 'Edit_Button_Warranty' present on page
And I click 'Edit_Button_Warranty'
And I scroll till element 'InspectionVendor_Text'
And I wait for '5' seconds
And I should see text 'Mattress Marshals' present on page at 'InspectionVendor_Text'
And I click 'PaymentStatus_Dropdown'
And I click 'Paid_Option'
And I should see element 'PaymentAuth.Code_Field' present on page
And I clear the text and enter 'XXXXXXX' in field 'PaymentAuth.Code_Field'
And I click by JS 'CurrentState_Dropdown_Warranty'
And I click by JS 'VendorInspectionRequested_Option'
And I wait for '2' seconds
And I click by JS 'Save_Button_Warranty'
And I wait for '4' seconds
And I should see element 'CaseOwner_Field_Warranty' present on page
And I should see text 'Service - Pending Inspection' present on page at 'CaseOwner_Field_Warranty'
And I scroll till element 'CaseOwner_Field_Warranty'
And I wait for '5' seconds
And I should see element 'CaseNumber_Agent' present on page
And I get text from 'CaseNumber_Agent' and store
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent' 

Scenario: 4. Making a Determination on Unattached Warranty Claim - SP1_TC47
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '10' seconds
And I should see text '$$CaseNumber_Agent' contained on page at 'CaseNumber_Dropdown_Agent'
And I click by JS 'CaseNumber_Dropdown_Agent'
And I wait for '10' seconds
And I scroll till element 'RelatedCaseSection_Det'
And I wait for '5' seconds
And I scroll till element 'CaseProducts_Section'
And I wait for '10' seconds
And I should see element 'SealyQueenBoxspring_Text_Determination' present on page
And I should see element 'SealyQueenBoxspring_Dropdown_Determination' present on page
And I click by JS 'SealyQueenBoxspring_Dropdown_Determination'
And I click by JS 'Edit_Warranty1'
And I should see element 'Edit_Warranty_Header' present on page
And I scroll till element 'ProductInformation_Tab'
And I wait for '5' seconds
And I should see element 'ProductManual_Field' present on page
#And I should see text 'Sealy Queen Box Spring' contained on page at 'ProductManual_Field'
And I should see element 'ProductComments_Field' present on page
#And I should see text 'Product Comments: Box' contained on page at 'ProductComments_Field'
And I should see element 'ResultCode_Dropdown' present on page
And I click by JS 'ResultCode_Dropdown'
And I click by JS 'YYB-NUTSBox_Option'
And I click 'SaveButton_Determination'
And I wait for '5' seconds
And I should see element 'SealyQueenMattress_Text_Determination' present on page
And I should see element 'SealyQueenMattress_Dropdown_Determination' present on page
And I click by JS 'SealyQueenMattress_Dropdown_Determination'
And I click by JS 'Edit_Warranty2'
And I should see element 'Edit_Warranty_Header' present on page
And I scroll till element 'ProductInformation_Tab'
And I wait for '3' seconds
And I should see element 'ProductManual_Field' present on page
#And I should see text 'Sealy Queen Mattress' contained on page at 'ProductManual_Field'
And I should see element 'ProductComments_Field' present on page
#And I should see text 'Product Comments: Box' contained on page at 'ProductComments_Field'
And I should see element 'ResultCode_Dropdown' present on page
And I click by JS 'ResultCode_Dropdown'
And I click by JS 'XXX-UTS_Option'
And I click by JS 'SaveButton_Determination'
And I click by JS 'Edit_Button_Warranty'
And I scroll till element 'CustomerCode_Field'
And I wait for '5' seconds
And I should see element 'CustomerCode_Field' present on page
And I clear the text and enter 'C012345678' in field 'CustomerCode_Field'
And I click by JS 'CurrentState_Dropdown_Warranty'
And I click by JS 'Approved:Defective_Option'
And I click by JS 'Resolutions_Warranty_Dropdown'
And I click by JS 'CreditApproved_Warranty_Option'
And I wait for '2' seconds
And I click by JS 'Status_Warranty_Dropdown'
And I click by JS 'Closed_Warranty_Option'
And I click by JS 'Save_Button_Warranty'
And I get text from 'CaseNumber_Agent' and store
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent' 


Scenario: 5. Verifying Approval and Denial Letters on Unattached Warranty Claim - SP1_TC48
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '10' seconds
And I should see text '$$CaseNumber_Agent' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I refresh the WebPage
And I wait for '10' seconds
And I click by JS 'WarrantyApprovalLetter_Button' 
And I wait for '25' seconds
And I should see element 'PDFContent_Approval' present on page
And I click by JS 'CloseTab_Approval'
And I click by JS 'WarrantyDenialLetter_Button' 
And I wait for '25' seconds
And I should see element 'PDFContent_Approval' present on page
#And I refresh the WebPage
#And I wait for '10' seconds
#And I navigate to PDF url
#And I wait for '4' seconds
#And I download PDP file for MF
#And I wait for '4' seconds
#And I wait for '10' seconds
#And I click 'Download_Button_Denial'
#And I wait for '4' seconds
#And I validate PDF file - filename: 'MFRMDenialPDF.pdf' & path: 'D://' content - Field Name 'Name' & Field Value: 'Michael Smith'
#And I validate PDF file - filename: 'MFRMDenialPDF.pdf' & path: 'D://' content - Field Name 'Address' & Field Value: '1000 South Oyster Bay Rd'
#And I validate PDF file - filename: 'MFRMDenialPDF.pdf' & path: 'D://' content - Field Name 'Customer Code' & Field Value: 'C012345678'
#And I validate PDF file - filename: 'MFRMDenialPDF.pdf' & path: 'D://' content - Field Name 'Case Number' & Field Value: '02631714'
#And I validate PDF file - filename: 'MFRMDenialPDF.pdf' & path: 'D://' content - Field Name ' Original Purchase Date' & Field Value: 'January 01, 2019'
And I click by JS 'Close_Agent' 
#And I click by JS 'Close_Agent' 


Scenario: 6. Logging a Call on an Unattached Warranty Claim - SP1_TC49
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '10' seconds
And I should see text '$$CaseNumber_Agent' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I should see element 'Actions_Tab' present on page
And I should see element 'LogaCall_Button' present on page
And I should see element 'Add_Button_Log' present on page
And I click by JS 'Add_Button_Log'
And I should see element 'MFRMCallType_Dropdown' present on page
And I click by JS 'MFRMCallType_Dropdown'
And I click by JS 'Warranty_Option'
And I should see element 'Subject_Field' present on page
#And I clear the text and enter 'Determination' in field 'Subject_Field' by JS
And I clear the text and enter 'Determination' in field 'Subject_Field'
And I should see element 'Comments_Field' present on page
And I clear the text and enter 'testing salesforce' in field 'Comments_Field'
And I wait for '5' seconds
And Blur 'Comments_Field'
And I wait for '5' seconds
And I click by JS 'Save_Button_Log'
And I scroll till element 'ActiveHistory_Log'
And I wait for '5' seconds
And I should see element 'ActiveHistory_Log' present on page
And I should see element 'SubjectOption_Activity' present on page
And I should see text 'CallDetermination' present on page at 'SubjectOption_Activity'
And I should see text 'Warranty' present on page at 'MFRMCallTypeOption_Activity'
And I should see element 'LastModifiedDateOption_Activity' present on page
And I get text from 'LastModifiedDateOption_Activity' and store
And I should see text 'CCO Agent User 2' present on page at 'AssignedToOption_Activity'
And I click by JS 'ViewAll_Link'
And I refresh the WebPage
And I wait for '15' seconds
And I switch to iFrame 'Frame_Log'
And I wait for '5' seconds
And I should see element 'AllActiveHistory_Header' present on page
#And I should see element 'LastModifiedDate_Log' present on page
#And I should see element 'Comments_Field_Text' present on page
And I should see text 'testing salesforce' present on page at 'Comments_Field-Activity'
And I should see text 'Warranty' present on page at 'MFRMCallType_Log'
And I should see text 'CCO Agent User 2' present on page at 'Name_Log'
And I switch to default content
And I should see element 'Task_Link' present on page
And I click by JS 'Close_Agent' 


Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page


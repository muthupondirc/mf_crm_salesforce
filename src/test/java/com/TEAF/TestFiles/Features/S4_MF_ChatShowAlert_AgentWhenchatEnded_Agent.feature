@S4_ChatShowAlert_Agent
Feature: Show alert to agent when a chat is ended in SF Console-Chat(IS)
 
1. Login to Mattress Firm Sales Force Application
1.1 To verify that Chat (IS) - Visual indication to agent that a new chat (without input) dropped in - SP4_TC2
2. To verify that Chat (IS) - Show alert to agent when a chat is ended - SP4_TC2.2
 

Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 To verify that Chat (IS) - Visual indication to agent that a new chat (without input) dropped in - SP4_TC2
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
And I wait for '120' seconds
#And I refresh the WebPage
#And I wait for '15' seconds
And I should see element 'JoeTab' present on page 
And I wait for '10' seconds
And I Validate Green Color Field


Scenario: To verify that Chat (IS) - Show alert to agent when a chat is ended - SP4_TC2.2
And I should see element 'EndChat_Button' present on page
And I click by JS 'EndChat_Button'
And I should see element 'Areyousure_popup' present on page
And I click by JS 'EndChat_Popup'
And I wait for '10' seconds
And I should see element 'ChatDisposition_Dropdown' present on page
And I click by JS 'ChatDisposition_Dropdown'
And I wait for '5' seconds
And I select option 'Cancel My Entire Order' in dropdown 'ChatDisposition_Dropdown' by 'value'
And I wait for '5' seconds
And I click by JS 'UpdateButton'
And I wait for '10' seconds
And I should see element 'ChatEndedByAgent_AgentChat' present on page
And I Validate Red Color Field
And I wait for '5' seconds
@MF_MyAccount
Feature: verify that Customer Portal- My Account - Add Track My Delivery button
 
1. To verify that Customer Portal- My Account - Add Track My Delivery button - SP5_TC6

Scenario: 1. To verify that Customer Portal- My Account - Add Track My Delivery button - SP5_TC6
Given My WebApp 'MF_SalesForce' is open
#-----------UAT URL-------- 
#And I navigate to 'https://storefront:face@dev.mattressfirm.com/' application
#------------------QA URL------------------
And I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' application
And I should see element 'CookiesCloseButton_Hybrid' present on page
And I click by JS 'CookiesCloseButton_Hybrid'
And I wait for visibility of element 'MattressSite_CloseButton'
And I click by JS 'MattressSite_CloseButton'
And I should see element 'MyAccountField' present on page
And I mouse over 'MyAccountField'
And I wait for '5' seconds
And I click by JS 'LogInField_MyAccount'
And I wait for '5' seconds
And I should see element 'CustomerPortal_Header' present on page
And I should see element 'EmailField_Case' present on page
And I clear the text and enter 'JaredSteiner813+501@gmail.com' in field 'EmailField_Case'
And I should see element 'PasswordField_Case' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_Case'
And Blur 'PasswordField_Case'
And I wait for '3' seconds
And I should see element 'LoginButton_Case' present on page
And I click by JS 'LoginButton_Case'
And I wait for '10' seconds
And I mouse over 'MyAccountField'
And I wait for '5' seconds
And I click by JS 'UserProfileName'
And I wait for '5' seconds
And I click by JS 'ViewMyOrdersbutton'
And I wait for '3' seconds
And I should see element 'OrderNumber_MyAccountQA' present on page
And I get text from 'OrderNumber_MyAccountQA' and store
And I click by JS 'TrackOrderButtonQA'
And I wait for '3' seconds
And I switch to window with title 'Mattress Firm Delivery Tracker | Best Mattress Prices-Top Brands-Same Day Delivery'
And I wait for '5' seconds
And I should see element 'TrackMyOrder' present on page
And I should see element 'OrderNumberHeader_MyAccount' present on page
#And I should see element 'Shipmentheader_MyAccount' present on page
#And I should see element 'ShipmentActivity_MyAccount' present on page
And I should see element 'ProductItems1' present on page
#And I should see element 'ProductItems2' present on page
#And I should see element 'Status_MyAccount' present on page
#And I should see element 'TrackingNumber_MyAccountQA' present on page
And I Close Window Tab





@Sprint6Case_CustomerService_Abilityfor_ServiceAgentto_CreateCase_off_Chat
Feature: Sprint 6 Cases Service Agent Have Ability To Create Case Off Chat
1. Login as Service Agent
2. SP6_TC1 - Service Agent Have Ability To Create Case During Chat
3. SP6_TC2 - Service Agent Have Ability To Create Case Off Chat
4. Logout Scenario


Scenario: Service Agent Have Ability To Create Case During Chat

Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I should see element 'OmniButton' present on page
And I click by JS 'OmniButton'
And I wait for '3' seconds
And I should see element 'OmniStatus' present on page
And I should see element 'OmniStatusDD' present on page
And I click by JS 'OmniStatusDD'
And I click by JS 'AvailableChat'
And I wait for '5' seconds
And I should see element 'ChatStatusUtilityBar' present on page
And I wait for '2' seconds
And I click by JS 'ChatStatusUtilityBar'
And I wait for '5' seconds
#ForQA

#And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/Home-Show'
#ForUAT
And I Switch the Tab and enter the url 'MFPortalURL'

And I wait for '20' seconds
#And I click by JS 'AcceptCookiesBtn'
And I wait for '5' seconds
#And I click by JS 'WebClosePopup'
And I wait for '5' seconds
#QAFOR
And I should see element 'ChatWithExpertBtn' present on page
And I click by JS 'ChatWithExpertBtn'

#And I should see element 'ChatWithExpertBtnUAT' present on page
#And I click by JS 'ChatWithExpertBtnUAT'
#And I should see element 'ChatHeader' present on page
And I wait for '5' seconds
#FORNEWCHAT UAT

And I should see element 'SFChatFirstNameUAT' present on page
And I click by JS 'SFChatFirstNameFieldUAT'
And I enter 'Jared' in field 'SFChatFirstNameFieldUAT'
And I should see element 'SFChatLastNameUAT' present on page
And I should see element 'SFChatLastNameFieldUAT' present on page
And I click by JS 'SFChatLastNameFieldUAT'
And I enter 'Steiner' in field 'SFChatLastNameFieldUAT'
And I should see element 'SFChatPhoneNumberUAT' present on page
And I should see element 'SFChatPhoneNumberFieldUAT' present on page
And I click by JS 'SFChatPhoneNumberFieldUAT'
And I enter '5165410544' in field 'SFChatPhoneNumberFieldUAT'
And I should see element 'SFChatHelpWithOrderBtnUAT' present on page
And I click by JS 'SFChatHelpWithOrderBtnUAT'
And I wait for '2' seconds
And I should see element 'SFChatStartChatBtnUAT' present on page
And I click by JS 'SFChatStartChatBtnUAT'
And I wait for '10' seconds
And I should see element 'ChatTextbox' present on page
And I click by JS 'ChatTextbox'
And I wait for '3' seconds
And I enter 'Agent' in field 'ChatTextbox'
And I wait for '5' seconds
And I hit enter-key on element 'ChatTextbox'
And I wait for '5' seconds
And I should see element 'ChatTransferedHeader' present on page
And I wait for '5' seconds
And I Switch between Tab '0'
And I wait for '5' seconds
And I should see element 'ChatStartedTabHeader' present on page
And I wait for '3' seconds
And I should see element 'CreateCaseFromChatTranscript' present on page
And I should see element 'CreateCaseNextButton' present on page
And I click by JS 'CreateCaseNextButton'
And I wait for '3' seconds
And I should see element 'CreateCaseFirstNameField' present on page
And I should see element 'CreateCaseLastNameField' present on page
And I should see element 'CreateCaseEmail' present on page
And I click by JS 'CreateCaseEmail'
And I enter 'jaredstiener817@gmail.com' in field 'CreateCaseEmail'
And I wait for '3' seconds
And I should see element 'CreateCasePhoneField' present on page
And I should see element 'CreateCaseNextButton' present on page
And I click by JS 'CreateCaseNextButton'
And I wait for '5' seconds
And I should see element 'CreateCaseSubRecordType' present on page
And I click by JS 'CreateCaseSubRecordTypeDD'
And I select option '120 Day Price Match Guarantee Request' in dropdown 'CreateCaseSubRecordTypeDD' by 'text'
And I wait for '2' seconds
And I should see element 'PurchaseSource' present on page
And I click by JS 'PurchaseSourceDD'
And I select option 'Chat' in dropdown 'PurchaseSourceDD' by 'text'
And I wait for '2' seconds
And I should see element 'CreateCaseTypeOfProducts' present on page
And I click by JS 'CreateCaseTypeOfProductsDD'
And I select option 'Core Product' in dropdown 'CreateCaseTypeOfProductsDD' by 'text'
And I wait for '3' seconds
And I should see element 'CreateCaseNextButton' present on page
And I click by JS 'CreateCaseNextButton'
And I wait for '5' seconds
And I should see element 'CaseCreationMessage' present on page
And I get Case Number from Chat Transcript 'CaseCreationMessage'
And I should see element 'PortalEndChat' present on page
And I click by JS 'PortalEndChat'
And I wait for '3' seconds
And I should see element 'PortalEndChatTooltip' present on page
And I click by JS 'PortalEndChatTooltip'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'NavHome'
And I wait for '3' seconds
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I wait for '2' seconds
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$CaseCreationMessage' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '10' seconds
And I should see element 'ChatCreateCaseCaseOwner' present on page
#And I should see element 'ChatCreateCaseCaseOwnerValue' present on page
And I should see element 'ChatCreateCaseCaseOrigin' present on page
And I should see element 'ChatCreateCaseCaseOriginValue' present on page
And I scroll till element 'ChatCreateCaseCaseOriginValue'
And I should see element 'ChatCreateCaseSourceValue' present on page
And I should see element 'ChatCreateCaseSubRecordTypeValue' present on page
And I should see element 'ChatCreateCaseTypeOfProduct' present on page
And I should see element 'ChatCreateCaseTypeOfProductVaue' present on page
And I should see element 'ChatCreateCaseStatus' present on page
And I should see element 'ChatCreateCaseStatusValue' present on page
And I should see element 'ChatCreateCaseRecordType' present on page
And I should see element 'ChatCreateCaseRecordTypeValue' present on page
And I wait for '2' seconds
And I scroll till element 'ChatCreateCaseRecordTypeValue'
And I should see element 'ChatTranscriptID' present on page
And I should see element 'ChatTranscriptIDValue' present on page
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds
And I refresh the WebPage

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

@Sprint6Case_CustomerService_Abilityfor_ServiceAgentto_CreateCase_off_Chat1
Scenario: Creating a New Case from Chat Transcript.

Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I wait for '3' seconds
Given I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'NavChatTranscript'
And I wait for '5' seconds
And I should see element 'ChatTranscriptCaseNumber' present on page
And I click by JS 'ChatTranscriptCaseNumber'
And I wait for '10' seconds
And I should see element 'CreateCaseFromChatTranscript' present on page
And I should see element 'CreateCaseNextButton' present on page
And I click by JS 'CreateCaseNextButton'
And I wait for '3' seconds
And I should see element 'CreateCaseFirstNameField' present on page
And I should see element 'CreateCaseLastNameField' present on page
And I should see element 'CreateCaseEmail' present on page
And I click by JS 'CreateCaseEmail'
And I enter 'jaredstiener815@gmail.com' in field 'CreateCaseEmail'
And I wait for '3' seconds
And I should see element 'CreateCasePhoneField' present on page
And I should see element 'CreateCaseNextButton' present on page
And I click by JS 'CreateCaseNextButton'
And I wait for '5' seconds
And I should see element 'CreateCaseSubRecordType' present on page
And I click by JS 'CreateCaseSubRecordTypeDD'
And I select option '120 Day Price Match Guarantee Request' in dropdown 'CreateCaseSubRecordTypeDD' by 'text'
And I wait for '2' seconds
And I should see element 'PurchaseSource' present on page
And I click by JS 'PurchaseSourceDD'
And I select option 'Chat' in dropdown 'PurchaseSourceDD' by 'text'
And I wait for '2' seconds
And I should see element 'CreateCaseTypeOfProducts' present on page
And I click by JS 'CreateCaseTypeOfProductsDD'
And I select option 'Core Product' in dropdown 'CreateCaseTypeOfProductsDD' by 'text'
And I wait for '3' seconds
And I should see element 'CreateCaseNextButton' present on page
And I click by JS 'CreateCaseNextButton'
And I wait for '5' seconds
And I should see element 'CaseCreationMessage' present on page
And I get Case Number from Chat Transcript 'CaseCreationMessage'
And I wait for '5' seconds
And I click by JS 'Close'
And I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'NavHome'
And I wait for '3' seconds
Given I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I wait for '2' seconds
And I hit tab-key on element 'SearchTypeDD'
And I enter '$$CaseCreationMessage' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '10' seconds
And I should see element 'ChatCreateCaseCaseOwner' present on page
And I should see element 'ChatCreateCaseCaseOrigin' present on page
And I should see element 'ChatCreateCaseCaseOriginValue' present on page
And I scroll till element 'ChatCreateCaseCaseOriginValue'
And I should see element 'ChatCreateCaseSourceValue' present on page
And I should see element 'ChatCreateCaseSubRecordTypeValue' present on page
And I should see element 'ChatCreateCaseTypeOfProduct' present on page
And I should see element 'ChatCreateCaseTypeOfProductVaue' present on page
And I should see element 'ChatCreateCaseStatus' present on page
And I should see element 'ChatCreateCaseStatusValue' present on page
And I should see element 'ChatCreateCaseRecordType' present on page
And I should see element 'ChatCreateCaseRecordTypeValue' present on page
And I wait for '2' seconds
And I scroll till element 'ChatCreateCaseRecordTypeValue'
And I should see element 'ChatTranscriptID' present on page
And I should see element 'ChatTranscriptIDValue' present on page
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
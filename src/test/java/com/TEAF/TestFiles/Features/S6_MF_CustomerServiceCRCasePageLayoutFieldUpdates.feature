@S6_CustomerServiceCRCasePageLayout
Feature: Customer Service - CR Case Page Layout and Field Updates
 
 1. Login to Mattress Firm Sales Force Application
1.1 Customer Service - CR Case Page Layout and Field Update - SP6_TC4

Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO CR User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO CR User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 Customer Service - CR Case Page Layout and Field Update in Consumer Relations-SP6_TC4
Given I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I should see element 'New_Button_Agent' present on page
And I click 'New_Button_Agent'
Then I should see element 'NewCase_Text_Lead' present on page
And I click 'ConsumerRelations_Button_Creat'
And I click 'Next_Button_Lead'
Then I should see element 'NewCaseConsumerRelations_Text_Creat' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click 'Email_Text_Creat'
And I click 'State_Dropdown_Creat'
And I click 'NY_Text_Creat'
And I click 'Brand_Dropdown_Creat'
And I click 'MattressFirm_Text_Creat'
And I click 'Source_Dropdown_Agent'
And I click 'CustomerCare_Text_Creat'
And I click 'MerchandiseDisposition_Dropdown_Creat'
And I click 'Prime_Text_Creat'
And I click 'CRCaseType_Dropdown_Creat'
And I click 'Agency_Text_Creat'
And I click 'SubCaseType_Dropdown_Creat'
And I click 'BetterBusinessBureau(BBB)_Text_Creat'
And I click 'CurrentState_Dropdown_Creat'
And I click 'AccommodationOffer_Text_Creat'
And I click 'CR_Reason_Dropdown_Creat'
And I click 'Billing/Financing_Text_Creat'
And I click 'CR_ReasonDetails_Dropdown_Creat'
And I click 'CreditCard-Overcharged_Text_Creat'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I click by JS 'Close_Agent' 
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I click 'CaseNumber_Dropdown_Agent'
And I click 'Edit_Button_Close' Tab
And I should see element 'Edit_Header' present on page
And I click by JS 'StatusDropdown_CR'
And I click by JS 'DoubleEscalation_Option_CR'
And I click 'Save_Button_Close'
And I wait for '5' seconds
And I should see text 'CR Escalations' contained on page at 'CaseOwnerField_CR'
And I get text from 'CaseOwnerField_CR' and store
And I scroll till element 'EscalateCaseCheckbox_CR'
And I wait for '5' seconds
And I verify checkbox 'EscalateCaseCheckbox_CR' is 'checked'
And I should see text '1' contained on page at 'NumberOfTimesEscalated_CR'
And I get text from 'NumberOfTimesEscalated_CR' and store
And I should see text 'CCO CR User 2' contained on page at 'EscalatedAgentField_CR'
And I get text from 'EscalatedAgentField_CR' and store
And I scroll till element 'DescriptionInformation_Field_Bug'
And I wait for '5' seconds
And I should see text 'Consumer Relations' present on page at 'CaseRecordType_Field_Bug'
And I get text from 'CaseRecordType_Field_Bug' and store
And I click by JS 'Close_Agent'
And I click by JS 'Close_Agent'

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Sr Mgr User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Sr Mgr User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.2 Customer Service CR Case Page Layout and Field Update in Consumer Relations Manager-SP6_TC4
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I click by JS 'SelectListViewDropdown_CR'
And I should see element 'SearchField_CR' present on page
And I clear the text and enter 'CR Escalations' in field 'SearchField_CR'
And I hit enter-key on element 'SearchField_CR'
And I should see element 'NewEscalatedCaseNumber_CR' present on page
And I should see text '$$CaseNumber_Agent' contained on page at 'NewEscalatedCaseNumber_CR'
And I should see text 'New' contained on page at 'StatusField_CR'
And I should see text '$$CaseOwnerField_CR' contained on page at 'OwnerNameField_CR'
And I click by JS 'NewEscalatedCaseNumber_CR'
And I wait for '5' seconds
And I should see text '$$CaseOwnerField_CR' contained on page at 'CaseOwnerField_CR'
And I should see text '$$CaseNumber_Agent' contained on page at 'CaseNumberField_CR'
And I scroll till element 'EscalateCaseCheckbox_CR'
And I wait for '5' seconds
And I verify checkbox 'EscalateCaseCheckbox_CR' is 'checked'
And I should see text '$$NumberOfTimesEscalated_CR' contained on page at 'NumberOfTimesEscalated_CR'
And I should see text '$$EscalatedAgentField_CR' contained on page at 'EscalatedAgentField_CR'
And I scroll till element 'DescriptionInformation_Field_Bug'
And I wait for '5' seconds
And I should see text '$$CaseRecordType_Field_Bug' contained on page at 'CaseRecordType_Field_Bug'
And I click by JS 'Close_Agent'

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page




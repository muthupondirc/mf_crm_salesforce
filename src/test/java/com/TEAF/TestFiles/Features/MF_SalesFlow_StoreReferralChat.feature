@MF_SalesFlow_StoreReferralChat
Feature: Store Referral using Chat

1. Login with SalesRep user Credentials
2. SP2_TC7 & SP2_TC8 Merge Process Builer attached and Unattached log a call 
3. SP2_TC5 Store referral using Chat
4. Logout with Standard user


Scenario: Login with Standard user Credentials
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I wait for '3' seconds

Scenario: SP2_TC Merge Process Builer Link log Unattached call Task to Service Case
Given I should see element 'Log_A_Callbtn' present on page
And I click 'Log_A_Callbtn'
And I wait for '2' seconds
And I should see element 'Log_Call_header' present on page
And I should see element 'Call_Type_label' present on page
And I click 'Call_Type_DD'
And I click 'LogTypeWarranty'
#And I should see element 'Subject_Txt' present on page
#And I clear the text and enter 'Bottom Panel' in field 'Subject_Txt'
And I should see element 'Comment_Txt_Field' present on page
And I click by JS 'Comment_Txt_Field'
And I wait for '3' seconds
And I enter 'Testing salesforce 12' in field 'Comment_Txt_Field'
And Blur 'Comment_Txt_Field'
And I wait for '2' seconds
And I click 'Next_btn'
#And I click by JS 'Minimize_btn' 
And I wait for '20' seconds
And I click by JS 'Log_A_Callbtn'
And I wait for '10' seconds
And I click by JS 'SearchTypeDD'
And I click by JS 'SearchTypeTasks' 
And I hit tab-key on element 'SearchTypeDD'
And I enter 'Testing salesforce' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'Tasks' present on page
And I wait for '3' seconds
And I click by JS 'TaskSubjectCall'
And I wait for '5' seconds
And I should see element 'RelatedToCase' present on page
And I should see element 'DummyCaseNumber' present on page
And I scroll to 'element' - 'ScrollDNIS'
And I should see element 'CaseCallType' present on page
And I should see element 'CaseCallTypeValue' present on page
And I should see element 'CaseSubjectType' present on page
And I should see element 'CaseSubjectTypeValue' present on page
And I should see element 'CaseCommentType' present on page
And I should see element 'CaseCommentTypeValue' present on page
And I wait for '2' seconds
And I click by JS 'DummyRelateToCase'
And I wait for '5' seconds
And I should see element 'DummyCaseActivity' present on page
And I click by JS 'DummyCaseCall'
And I wait for '5' seconds
And I should see element 'RelatedToCase' present on page
And I should see element 'DummyCaseNumber' present on page
And I scroll till element 'ScrollDNIS'
And I should see element 'CaseCallType' present on page
And I should see element 'CaseCallTypeValue' present on page
And I should see element 'CaseSubjectType' present on page
And I should see element 'CaseSubjectTypeValue' present on page
And I should see element 'CaseCommentType' present on page
And I should see element 'CaseCommentTypeValue' present on page
And I click by JS 'Close'
And I wait for '5' seconds
And I click by JS 'Close'
And I wait for '5' seconds

#Scenario: Login with Standard user Credentials
#Given My WebApp 'MF_SalesForce' is open 
#And I wait for '5' seconds
#When I login to Mattress Firm Sales Force with valid 'agent_QA' User Credentials 
#And I click by JS 'UserImageButton'
#And I should see text 'CCO Agent User 1' present on page at 'ProfileCardPopUp'
#And I click by JS 'UserImageButton'
#And I wait for '3' seconds


#Scenario: SP2_TC Merge Process Builer Link log Unattached call Task to Service Case
#Given I should see element 'Log_A_Callbtn' present on page
#And I click 'Log_A_Callbtn'
#And I wait for '2' seconds
#And I should see element 'Log_Call_header' present on page
#And I should see element 'Call_Type_label' present on page
#And I click 'Call_Type_DD'
#And I click 'LogTypeWarranty'
##And I should see element 'Subject_Txt' present on page
##And I clear the text and enter 'Bottom Panel' in field 'Subject_Txt'
#And I should see element 'Comment_Txt_Field' present on page
#And I click by JS 'Comment_Txt_Field'
#And I wait for '3' seconds
#And I enter 'Testing salesforce 12' in field 'Comment_Txt_Field'
#And Blur 'Comment_Txt_Field'
#And I wait for '2' seconds
#And I click 'Next_btn'
##And I click by JS 'Minimize_btn' 
#And I wait for '20' seconds
#And I click by JS 'Log_A_Callbtn'
#And I wait for '10' seconds
#And I click by JS 'SearchTypeDD'
#And I click by JS 'SearchTypeTasks' 
#And I hit tab-key on element 'SearchTypeDD'
#And I enter 'Testing salesforce' in field 'GlobalSearchField'
#And I wait for '3' seconds
#And I hit enter-key on element 'GlobalSearchField'
#And I wait for '5' seconds
#And I should see element 'Tasks' present on page
#And I wait for '3' seconds
#And I click by JS 'TaskSubjectCall'
#And I wait for '5' seconds
#And I should see element 'RelatedToCase' present on page
#And I should see element 'DummyCaseNumber' present on page
#And I scroll to 'element' - 'ScrollDNIS'
#And I should see element 'CaseCallType' present on page
#And I should see element 'CaseCallTypeValue' present on page
#And I should see element 'CaseSubjectType' present on page
#And I should see element 'CaseSubjectTypeValue' present on page
#And I should see element 'CaseCommentType' present on page
#And I should see element 'CaseCommentTypeValue' present on page
#And I wait for '2' seconds
#And I click by JS 'DummyRelateToCase'
#And I wait for '5' seconds
#And I should see element 'DummyCaseActivity' present on page
#And I click by JS 'DummyCaseCall'
#And I wait for '5' seconds
#And I should see element 'RelatedToCase' present on page
#And I should see element 'DummyCaseNumber' present on page
#And I scroll till element 'ScrollDNIS'
#And I should see element 'CaseCallType' present on page
#And I should see element 'CaseCallTypeValue' present on page
#And I should see element 'CaseSubjectType' present on page
#And I should see element 'CaseSubjectTypeValue' present on page
#And I should see element 'CaseCommentType' present on page
#And I should see element 'CaseCommentTypeValue' present on page
#And I click by JS 'Close'
#And I wait for '5' seconds
#And I click by JS 'Close'
#And I wait for '5' seconds
#
#Scenario: SP2_TC Merge Process Builder Link log a call Attached Task to Service Case
#And I click by JS 'ShowNavMenu'
#And I wait for '2' seconds
#And I click by JS 'NavCases'
#And I wait for '5' seconds
#And I should see element 'SFCase_New' present on page
#And I click by JS 'SFCase_New'
#And I wait for '3' seconds
#And I should see element 'NewCaseSelectionBtn' present on page
#And I should see element 'NewCaseSelectionLabel' present on page
#And I click by JS 'NewCaseSelectionBtn'
#And I should see element 'NewCaseSelectionNxt' present on page
#And I click by JS 'NewCaseSelectionNxt'
#And I wait for '5' seconds
#And I should see element 'New_Case_Header' present on page
#And I should see element 'SFServiceCaseContactName' present on page
#And I wait for '2' seconds
#And I click by JS 'SFServiceCaseContactName'
#And I enter 'jared' in field 'SFServiceCaseContactName'
#And I wait for '3' seconds
#And I hit down Arrow key on element 'SFServiceCaseContactName'
#And I wait for '3' seconds
#And I hit enter-key on element 'SFServiceCaseContactName'
#And I wait for '2' seconds
#And Blur 'SFServiceCaseContactName'
#And I should see element 'Case_Origin_Label' present on page
#And I click by JS 'Case_Origin_DD'
#And I click by JS 'Case_Origin_DD_Phone'
#And I wait for '2' seconds
#And I scroll to 'element' - 'Case_Classification'
#And I should see element 'Source_Label' present on page
#And I click by JS 'Source_DD'
#And I click by JS 'Source_DD_Value'
#And I should see element 'TypeOfProduct' present on page
#And I click by JS 'TypeOfProductDD'
#And I click by JS 'TypeOfProductValue'
#And I should see element 'SubRecordTypeDD' present on page
#And I click by JS 'SubRecordTypeDD'
#And I click by JS 'SubRecordType_NeedOption_Value'
#And I wait for '5' seconds
#And I click by JS 'BedBugSave'
#And I wait for '5' seconds
#And I should see element 'CaseNo' present on page
#And I get text from 'CaseNumberValue' and store
#And I wait for '3' seconds
#And I click by JS 'Close'
#And I wait for '5' seconds
#And I click by JS 'SearchTypeDD'
#And I click by JS 'SearchType_Cases'
#And I wait for '3' seconds
#And I hit tab-key on element 'SearchTypeDD'
#And I enter '$$CaseNumberValue' in field 'GlobalSearchField'
#And I wait for '3' seconds
#And I hit down Arrow key on element 'GlobalSearchField'
#And I wait for '3' seconds
#And I hit enter-key on element 'GlobalSearchField'
##And I should see element 'AttachedCallCaseNumber' present on page
##And I should see element 'AttachedCallCaseNumberStatus' present on page
##And I click by JS 'AttachedCallCaseNumber'
#And I wait for '5' seconds
#And I click by JS 'AttachedContactAdd'
#And I should see element 'AttachedContactCallType' present on page
#And I click by JS 'AttachedContactCallType'
#And I click by JS 'ContactCallTypeDD'
#And I click by JS 'ContactCallTypeValue'
#And I hit tab-key on element 'ContactCallTypeValue'
#And I wait for '2' seconds
##And I hit tab-key on element 'ContactCallSubject'
##And I wait for '2' seconds
#And I should see element 'AttachedCaseComment' present on page
#And I wait for '3' seconds
#And I click by JS 'AttachedComments'
#And I wait for '5' seconds
##And I enter 'Salesforce1' in field 'ContactComments'
#And I enter 'Salesforce12345' in field 'AttachedComments'
#And I wait for '5' seconds
#And Blur 'AttachedComments'
#And I scroll to 'element' - 'AttachedComments'
#And I wait for '2' seconds
#And I click by JS 'ContactLogCallSave'
#And I wait for '5' seconds
#And I should see element 'DummyCaseActivity' present on page
#And I click by JS 'DummyCaseCall'
#And I wait for '5' seconds
#And I should see element 'RelatedToCase' present on page
#And I should see element 'DummyCaseNumber' present on page
#And I should see element 'AttachedCallContactName' present on page
#And I should see element 'AttachedCallContactNameValue' present on page
#And I scroll to 'element' - 'ScrollDNIS'
#And I should see element 'CaseCallType' present on page
#And I should see element 'ContactSubjectType' present on page
#And I should see element 'ContactCommentsVal' present on page
#And I wait for '5' seconds
#And I click by JS 'AttachedCallContactNameValue'
#And I wait for '5' seconds
#And I scroll till element 'ActivityContact'
#And I should see element 'ActivityContact' present on page
#And I should see element 'ActivityContactCall' present on page
#And I click by JS 'ActivityContactCall'
#And I wait for '5' seconds
#And I should see element 'CaseCallType' present on page
#And I should see element 'ContactSubjectType' present on page
#And I should see element 'ContactCommentsVal' present on page
#And I wait for '5' seconds
#And I click by JS 'Close'
#And I wait for '2' seconds
#

#Scenario: Verify logout of Sales Force application
#Given I click by JS 'UserImageButton'
#And I click link 'Log Out'
#And I should see element 'login_username' present on page


Scenario: SP2_TC5 Login with Standard user Credentials Store Referral Case
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'telesales' User Credentials 
And I click by JS 'UserImageButton'
And I should see profile name 'telesales_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds
And I should see element 'OmniButton' present on page
And I click by JS 'OmniButton'
And I wait for '3' seconds
And I should see element 'OmniStatus' present on page
And I should see element 'OmniStatusDD' present on page
And I click by JS 'OmniStatusDD'
And I click by JS 'AvailableChat'
And I wait for '5' seconds
And I should see element 'ChatStatusUtilityBar' present on page
And I wait for '2' seconds
And I click by JS 'ChatStatusUtilityBar'
#And I click by JS 'MinimizeOmniBtn'
And I wait for '5' seconds
#For UAT

#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com'

#For QA
And I Switch the Tab and enter the url 'MFPortalURL'
And I wait for '20' seconds
#And I should see element 'AcceptCookiesBtn' present on page
#And I click by JS 'AcceptCookiesBtn'
#And I wait for '5' seconds
#And I click by JS 'WebClosePopup'
And I wait for '10' seconds
##Temporary Solution for ChatBot
##------------- 
#
#And I should see element 'BedsandFrame' present on page
#And I mouse over 'BedsandFrame'
#And I wait for '3' seconds
#And I should see element 'BoxSpring' present on page
#And I click by JS 'BoxSpring'
#And I wait for '15' seconds
#-------------
#For QA

And I should see element 'ChatWithExpertBtn' present on page
And I click by JS 'ChatWithExpertBtn'
#For UAT

#And I should see element 'ChatWithExpertBtnUAT' present on page
#And I click by JS 'ChatWithExpertBtnUAT'
And I wait for '5' seconds
#And I should see element 'ChatHeader' present on page
#And I should see element 'FirstName' present on page
And I wait for '2' seconds
#ForOLD CHAT
#And I should see element 'FirstNameTxtField' present on page
#And I click by JS 'FirstNameTxtField'
#And I enter 'Hasan' in field 'FirstNameTxtField'
#And I should see element 'LastName' present on page
#And I should see element 'LastNameTxtField' present on page
#And I click by JS 'LastNameTxtField'
#And I enter 'Khan' in field 'LastNameTxtField'
#And I should see element 'Email' present on page
#And I should see element 'EmailTxtField' present on page
#And I click by JS 'EmailTxtField'
#And I enter 'hasantestuser@gmail.com' in field 'EmailTxtField'
#And Blur 'EmailTxtField'
#FOR NEW CHAT UAT

And I should see element 'SFChatFirstNameUAT' present on page
And I click by JS 'SFChatFirstNameFieldUAT'
And I enter 'Hasan' in field 'SFChatFirstNameFieldUAT'
And I should see element 'SFChatLastNameUAT' present on page
And I should see element 'SFChatLastNameFieldUAT' present on page
And I click by JS 'SFChatLastNameFieldUAT'
And I enter 'Khan' in field 'SFChatLastNameFieldUAT'
And I should see element 'SFChatEmaikAddressUAT' present on page
And I should see element 'SFChatEmaikAddressFieldUAT' present on page
And I click by JS 'SFChatEmaikAddressFieldUAT'
And I enter 'hasantestuser@gmail.com' in field 'SFChatEmaikAddressFieldUAT'
And I should see element 'SFChatLiveShoppingBtnUAT' present on page
And I click by JS 'SFChatLiveShoppingBtnUAT'
And I wait for '2' seconds
And I should see element 'SFChatStartChatBtnUAT' present on page
And I click by JS 'SFChatStartChatBtnUAT'
And I wait for '20' seconds
And I should see element 'ChatTextbox' present on page
And I click by JS 'ChatTextbox'
And I wait for '3' seconds
And I should see element 'SFStoreReferralChatQ1' present on page
And I should see element 'SFStoreReferralChatQ1Value' present on page
And I click by JS 'SFStoreReferralChatQ1Value'
And I wait for '10' seconds
And I should see element 'SFStoreReferralChatQ2' present on page
And I should see element 'SFStoreReferralChatQ2Value' present on page
And I click by JS 'SFStoreReferralChatQ2Value'
And I wait for '5' seconds
And I should see element 'SFStoreReferralChatQ3' present on page
And I should see element 'SFStoreReferralChatQ3Value' present on page
And I click by JS 'SFStoreReferralChatQ3Value'
And I wait for '15' seconds

#Temporary Solution for chat bot
#---------------

#And I should see element 'OtherQA' present on page
#And I click by JS 'OtherQA'
#And I wait for '10' seconds
#And I enter 'Agent' in field 'ChatTextbox'
#And I hit enter-key on element 'ChatTextbox'
#And I wait for '10' seconds
##------------------

And I Switch between Tab '0'
And I wait for '5' seconds
And I wait for visibility of element 'ChatStartedHeader'
And I should see element 'ChatStartedHeader' present on page
#And I should see element 'PortalChatHeader' present on page
And I should see element 'SFFirstNameValue' present on page
And I should see element 'SFLastNameValue' present on page
And I should see element 'SFEmailValue' present on page
And I wait for '3' seconds
And I should see element 'SFStoreReferral' present on page
And I should see element 'SFZipCode' present on page
And I should see element 'SFZipCodeSearchField' present on page
And I click by JS 'SFZipCodeSearchField'
And I wait for '2' seconds
And I enter '11801' in field 'SFZipCodeSearchField'
And I wait for '2' seconds
And Blur 'SFZipCodeSearchField'
And I should see element 'SFDistanceMiles' present on page
And I should see element 'SFDistanceMilesDD' present on page
And I click by JS 'SFDistanceMilesDD'
And I wait for '3' seconds
And I click by JS 'SDDistaceMilesValue'
And I wait for '2' seconds
And Blur 'SFDistanceMilesDD'
And I should see element 'SFStoreReferralSearch' present on page
And I click by JS 'SFStoreReferralSearch'
And I wait for '5' seconds
And I click by JS 'SFStoreSelectionCheckBox'
And I wait for '3' seconds
And I should see element 'SFStoreReferralNext' present on page
And I click by JS 'SFStoreReferralNext'
And I wait for '3' seconds
And I should see element 'SFStorereferralFirstName' present on page
And I should see element 'SFStorereferralLastName' present on page
And I should see element 'SFStorerefrralEmail' present on page
And I should see element 'SFTestingStore' present on page
And I wait for '3' seconds
And I should see element 'SFStoreReferralNext' present on page
And I click by JS 'SFStoreReferralNext'
And I wait for '3' seconds
And I should see element 'LeadConfirmation' present on page
#And I should see element 'RecordID' present on page
#And I should see element 'FinishBtn' present on page
And I should see element 'FinishBtn' present on page
And I click by JS 'FinishBtn'
And I wait for '3' seconds
And I should see element 'PortalEndChat' present on page
And I click by JS 'PortalEndChat'
And I wait for '2' seconds
And I should see element 'PortalEndChatTooltip' present on page
And I click by JS 'PortalEndChatTooltip'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds

Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

Scenario: SP2_TC6 Verify Lead Created Using Store Referral Chat
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'telesales' User Credentials 
And I click by JS 'UserImageButton'
And I should see profile name 'telesales_displayname' present on page at 'ProfileCardPopUp'
And I wait for '3' seconds
And I click by JS 'ShowNavMenu'
And I wait for '2' seconds
And I click by JS 'SFLeads'
And I wait for '5' seconds
And I click by JS 'NavCasesDD'
And I wait for '2' seconds
And I should see element 'NavSearchBox' present on page
And I click by JS 'NavSearchBox'
#And I enter 'Store Referral Leads' in field 'NavSearchBox'
And I enter 'Today' in field 'NavSearchBox'
And I wait for '3' seconds
And I hit down Arrow key on element 'NavSearchBox'
And I wait for '2' seconds
And I hit enter-key on element 'NavSearchBox'
#And I click by JS 'SFStoreReferralLeads'
And I wait for '5' seconds
And I should see element 'SFLeadName' present on page
And I click by JS 'SFLeadName'
And I wait for '5' seconds
And I should see element 'Details' present on page
And I click by JS 'Details'
And I wait for '3' seconds
And I should see element 'SFLeadNameValue' present on page
And I should see element 'SFLeadEmail' present on page
And I scroll till element 'SFAdditionalInfo'
And I wait for '3' seconds
And I should see element 'LeadSourceValue' present on page
#And I should see element '' present on page
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '3' seconds
@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
@S6_CaseManagementCapabilities
Feature: IS - Case Management Capabilities for IS Hybrid Team in SF Console

1. IS - Case Management Capabilities for IS Hybrid Team in SF Console- SP6_TC2
1.1 Accept case via omni - SP6_TC2.1
1.2 Associate case to a customerís contact - SP6_TC2.2
1.3 Associate case to an order - SP6_TC2.3
1.4 Log a call - SP6_TC2.4
1.5 Send internal and external emails within the case - SP6_TC2.5
1.6 Close a case - SP6_TC2.6
1.7 Able to transfer Chat through telesales rep user Dona Dillea Hybrid agent received Chat - SP6_TC2.7 

Scenario: 1.Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 Omni Channel - make Service Agent as Available Chat
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'


Scenario: 1.2 IS - Case Management Capabilities for IS Hybrid Team in SF Console- SP6_TC2
#-----------UAT URL--------------------------------------------------------------------------------
#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#-----------QA URL--------------------------------------------------------------------------------- 

And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' 
And I should see element 'CookiesCloseButton_Hybrid' present on page
And I click by JS 'CookiesCloseButton_Hybrid'
And I wait for visibility of element 'MattressSite_CloseButton'
And I click 'MattressSite_CloseButton'
#And I wait for '15' seconds
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click by JS 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'joesmith@test.com' in field 'Email_Field_Omni'
And I get text from 'Email_Field_Omni' and store
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I get text from 'HomePhone_Field_Omni' and store
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click by JS 'HelpWithExistingOrder_Option'

#--------------next line is for old chat bot step-----------------------------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'

And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds

#--------------next 2 lines are old chat bot steps---------------------------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
#-----------------------console-----------------------------

And I Switch between Tab '0'
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AwayButton_Omni'
And I should see element 'AwayText_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
#------------------------------------------------------------------------

And I Switch between Tab '1'
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'NoAgentsText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I should see element 'ChatStartedText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I click by JS 'DoNotHaveButton_Chat'
And I click by JS 'PhoneSalesAgentButton_Chat'
And I click by JS 'YesButton_Chat'
And I click by JS 'YesButton_Chat'
And I should see element 'DeliveryButton_Chat' present on page
And I click by JS 'DeliveryButton_Chat'
And I should see element 'RescheduleMyDeliverButton_Chat' present on page
And I click by JS 'RescheduleMyDeliverButton_Chat'
And I should see element 'Provideatesection_Chat' present on page
And I clear the text and enter 'phone' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'CaseCreatedMessage_Chat' present on page
And I get Case Number from Chat 'CaseCreatedMessage_Chat' and store
And I Switch between Tab '0'

Scenario: 1.3 Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

Scenario: 1.4 Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'Retention Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'Retention Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.5 Accept case via omni - SP6_TC2.1
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I click by JS 'SelectListViewDropdown_CR'
And I should see element 'SearchField_CR' present on page
And I clear the text and enter 'Service - MFRM Care' in field 'SearchField_CR'
And I hit enter-key on element 'SearchField_CR'
And I wait for '5' seconds
And I should see element 'NewEscalatedCaseNumber_CR' present on page
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'NewEscalatedCaseNumber_CR'
And I click by JS 'NewEscalatedCaseNumber_CR'
And I should see text 'Service - MFRM Care' contained on page at 'CaseOwnerField_Chat'
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumberField_CR'
And I should see element 'ContactNameField_Hybrid' present on page
And I should see text 'Chatbot' contained on page at 'CaseOriginField_Hybrid'
And I scroll till element 'SourceField_Hybrid'
And I wait for '5' seconds
And I should see text 'phone' contained on page at 'SourceField_Hybrid'
And I should see text 'Reschedule My Delivery' contained on page at 'SubRecordTypeField_Hybrid'
And I scroll till element 'WebEmailField_Hybrid'
And I wait for '5' seconds
And I should see text 'joesmith@test.com' contained on page at 'WebEmailField_Hybrid'
And I should see text '5168618800' contained on page at 'WebPhoneField_Hybrid'
#And I click by JS 'Close_Agent'


Scenario: 1.9 Send internal and external emails within the case - SP6_TC2.5
And I should see element 'SendInternalEmailButton_Hybrid' present on page
And I click by JS 'SendInternalEmailButton_Hybrid'
And I should see element 'ToField_Hybrid' present on page
And I click by JS 'ToField_Hybrid'
And I wait for '5' seconds
#And I clear the text and enter 'sindhu.r@royalcyber.com' in field 'ToField_Hybrid' by JS
And I enter 'sindhu.r@royalcyber.com' in the feild 'ToField_Hybrid' using actions
And I click by JS 'ToField_Hybrid'
And I should see element 'ToAddressFieldValue_Hybrid' present on page
And I should see element 'SubjectField_Hybrid' present on page
And I clear the text and enter 'internal' in field 'SubjectField_Hybrid'
And I scroll till element 'SendButton_Hybrid'
And I click by JS 'SendButton_Hybrid'
And I wait for '5' seconds
And I scroll till element 'HighlightPanel'
And I wait for '10' seconds
And I scroll till element 'ShippingInfoSection_Hybrid'
And I wait for '10' seconds
And I should see element 'EmailSection_Hybrid' present on page
And I should see element 'EmailSubjectField_Hubrid' present on page
And I should see text 'sindhu.r@royalcyber.com' present on page at 'FromAddressValue_Hybrid'
And I should see text 'sindhu.r@royalcyber.com' present on page at 'ToAddressValue_Hybrid'
And I should see element 'MessageDateValue_Hybrid' present on page
And I wait for '5' seconds
And I should see element 'SendExternalEmailButton_Hybrid' present on page
And I click by JS 'SendExternalEmailButton_Hybrid'
And I should see element 'ToField_Hybrid' present on page
#And I click by JS 'ToField_Hybrid'
#And I enter 'sindhu.r@royalcyber.com' in the feild 'ToField_Hybrid' using actions
#And I click by JS 'ToField_Hybrid'
And I should see element 'ToAddressFieldValue_Hybrid' present on page
And I should see element 'SubjectField_Hybrid' present on page
And I scroll till element 'SendButton_Hybrid'
And I click by JS 'SendButton_Hybrid'
And I wait for '5' seconds
And I scroll till element 'EmailSection_Hybrid'
And I wait for '10' seconds
And I should see element 'EmailSection_Hybrid' present on page
And I should see element 'ExternalEmailSubjectField_Hubrid' present on page
And I should see text 'sindhu.r@royalcyber.com' present on page at 'FromAddressValue_Hybrid'
And I should see text 'sindhu.r@royalcyber.com' present on page at 'ToAddressValue_Hybrid'
And I should see element 'MessageDateValue_Hybrid' present on page


Scenario: 1.6 Associate case to a customerís contact - SP6_TC2.2
And I scroll till element 'EditButton_HybridCase'
And I wait for '10' seconds
And I should see element 'EditButton_HybridCase' present on page
And I click by JS 'EditButton_HybridCase'
And I wait for '5' seconds
And I should see element 'DeleteButton_HybridCase' present on page
And I click by JS 'DeleteButton_HybridCase'
And I clear the text and enter 'Jared Steiner' in field 'SearchContactsBar_HybridCase'
And I click by JS 'JaredSteinerContactOption_Hybrid'
And I should see element 'JaredSteinerContactValue_Hybrid' present on page
And Blur 'JaredSteinerContactValue_Hybrid'
And I click by JS 'SaveButton_HybridCase'
And I wait for '5' seconds
And I should see text 'Jared Steiner' contained on page at 'ContactNameValue_HybridCase'
And I get text from 'ContactNameValue_HybridCase' and store

Scenario: 1.7 Associate case to an order - SP6_TC2.3
And I click by JS 'ContactNameValue_HybridCase'
And I should see element 'OrderNumber_Orderheader' present on page
And I get text from 'OrderNumber_Orderheader' and store
And I click by JS 'OrderNumber_Orderheader'
And I wait for '5' seconds
And I should see text '$$OrderNumber_Orderheader' contained on page at 'OrderNumberValue_Details'
And I should see text '$$ContactNameValue_HybridCase' contained on page at 'CustomerNameValue_Details'

Scenario: 1.8 Log a call - SP6_TC2.4
And I should see element 'LogACallButton_Order' present on page
And I click by JS 'LogACallButton_Order'
And I should see element 'MFRMCallTypeDropdown_Order' present on page
And I select option 'Retention' in dropdown 'MFRMCallTypeDropdown_Order' by 'value'
And I should see element 'SubjectFieldValue_Hybrid' present on page
And I should see element 'CommentsField_Order' present on page
And I clear the text and enter 'testing contacts' in field 'CommentsField_Order'
And I wait for '5' seconds
And Blur 'CommentsField_Order'
And I wait for '5' seconds
And I click by JS 'SaveAndCloseButton_Order'
And I scroll till element 'ActiveHistory_Hybrid'
And I wait for '5' seconds
And I should see element 'ActiveHistory_Hybrid' present on page
And I should see element 'Subject_Hybrid' present on page
And I should see text 'Call' present on page at 'Subject_Hybrid'
And I should see text 'Retention' present on page at 'MFRMCallType_Hybrid'
And I should see element 'LastModifiedDate_Hybrid' present on page
And I get text from 'LastModifiedDate_Hybrid' and store
And I should see text 'Retention Agent User 2' present on page at 'AssignedTo_Hybrid'
And I click by JS 'ViewAllLink_Hybrid'
And I wait for '5' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I should see element 'Task_Link' present on page
And I switch to iFrame 'Frame_Log'
And I should see element 'AllActiveHistory_Header' present on page
And I should see text 'Call' present on page at 'Subjectvalue_Hybrid'
And I should see element 'LastModifiedDateValue_Hybrid' present on page
#And I should see text '$$LastModifiedDate_Hybrid' present on page at 'LastModifiedDateValue_Hybrid'
And I should see text 'testing contacts' present on page at 'CommentsFieldActivity_Hybrid'
And I should see text 'Retention' present on page at 'MFRMcallTypeFieldActivity_Hybrid'
And I should see text 'Retention Agent User 2' present on page at 'NameCase_Hybrid'
And I switch to default content
And I click by JS 'CloseTaskButton_Hybrid' 
And I click by JS 'CloseCaseAXButton_Hybrid' 
And I click by JS 'CloseTedButton_Hybrid' 


Scenario: 2.0 Close a case - SP6_TC2.6
And I should see element 'EditButton_HybridCase' present on page
And I click by JS 'EditButton_HybridCase'
And I click 'Status_Dropdown_Close'
And I click by JS 'Closed_Text_Close'
And I click 'TypeOfProduct(s)_Dropdown'
And I click by JS 'CoreProduct_Option'
And I click 'Save_Button_Close'
And I wait for '5' seconds
And I click by JS 'Close_Agent' 


Scenario: 2.1 Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

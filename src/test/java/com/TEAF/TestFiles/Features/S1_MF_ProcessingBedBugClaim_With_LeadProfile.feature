@S1_ProcessingBedBugClaim_Lead
Feature: Processing Bed Bug Claim with Lead Profile

 1. Login to Mattress Firm Sales Force Application
 1.2 Converting a Bed Bug Claim to Consumer Relations Record Type (Lead and Above) - SP1_TC54
 
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Sr Mgr User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Sr Mgr User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'


Scenario: 1.2 Converting a Bed Bug Claim to Consumer Relations Record Type (Lead and Above) - SP1_TC54
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
#create number from testcase 84(before Processing Bed Bug Claim with Lead Profile) 
And I clear the text and enter '03091986' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see element 'CaseNumber_Dropdown_Agent' present on page
And I click by JS 'CaseNumber_Dropdown_Agent'
And I wait for '10' seconds
And I should see element 'Edit_Dropdown_Bug' present on page
And I click by JS 'Edit_Dropdown_Bug'
And I wait for '5' seconds
And I click by JS 'Accept_Option_Bug'
And I should see text 'CCO Sr Mgr User 2' present on page at 'CaseOwner_Field_Bug'
And I click by JS 'Edit_Dropdown_Bug'
And I wait for '5' seconds
And I click by JS 'ChangeRecordType_Option_Bug'
And I click by JS 'ConsumerRelations_Button_Bug'
And I click by JS 'Next_Bug'
And I wait for '5' seconds
And I should see element 'Edit_Header_Bug' present on page
And I click by JS 'CaseOrigin_Dropdown_Bug'
And I click by JS 'Phone_Option_Bug'
And I click by JS 'State_Dropdown_Bug'
And I click by JS 'NY_Option_Bug'
And I click by JS 'StatusDropdown_Bug'
And I click by JS 'NewOption_Bug'
And I click by JS 'Brand_Dropdown_Bug'
And I click by JS 'MattressFirm_Option_Bug'
And I click by JS 'Source_Dropdown_Bug'
And I click by JS 'CustomerCare_Option_Bug'
And I click by JS 'MerchandiseDisposition_Dropdown_Bug'
And I click by JS 'Prime_Option_Bug'
And I click by JS 'CRCaseType_Dropdown_Bug'
And I click by JS 'PreDeliveryInfestation_Option_Bug'
And I click by JS 'SubCaseType_Dropdown_Bug'
And I click by JS 'InfestationCustomerPickup_Option_Bug'
And I click by JS 'CR_Current State_Dropdown_Bug'
And I click by JS 'PendingAccountReview_Option_Bug'
And I click by JS 'CR_Reason_Dropdown_Bug'
And I click by JS 'Infestation_Reason_Bug'
And I click by JS 'CR_ReasonDetails_Dropdown_Bug'
And I click by JS 'Other_Option_Bug'
And I click by JS 'Save_Button_Bug'
And I wait for '5' seconds
And I scroll till element 'DescriptionInformation_Field_Bug'
And I wait for '5' seconds
And I should see text 'Consumer Relations' present on page at 'CaseRecordType_Field_Bug'
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent' 


Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

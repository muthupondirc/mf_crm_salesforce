@S9_MF_EnableDraftEmail
Feature: Enable Draft Emails to Support In-Store CRM Project
 
 1. Service Agent to Save draft emails in CRM. 
 2. IS Agent to Save draft emails in CRM. 
 3. Retention Agent to Save draft emails in CRM. 

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1. Service Agent to Save draft emails in CRM.
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I click by JS 'SelectListViewDropdown_CR'
And I should see element 'SearchField_CR' present on page
And I wait for '3' seconds
And I clear the text and enter 'Service - MFRM Care' in field 'SearchField_CR'
And I wait for '3' seconds
And I click by JS 'ServiceMFRMCareOption'
And I should see element 'NewEscalatedCaseNumber_CR' present on page
And I click by JS 'NewEscalatedCaseNumber_CR'
And I should see element 'Actions_Tab' present on page
And I click by JS 'Actions_Tab'
And I should see element 'SendInternalEmailButton_Hybrid' present on page
And I click by JS 'SendInternalEmailButton_Hybrid'
And I should see element 'ToField_Hybrid' present on page
And I click by JS 'ToField_Hybrid'
#And I wait for '5' seconds
And I enter 'divyaalakshmi.p@royalcyber.com' in field 'ToField_Hybrid'
And I wait for '5' seconds
And I hit down Arrow key on element 'ToField_Hybrid'
#And I click by JS 'ToField_Hybrid'
#And I should see element 'ToAddressFieldValue_Hybrid' present on page
And I should see element 'SubjectField_Hybrid' present on page
And I clear the text and enter 'CaseEmail' in field 'SubjectField_Hybrid'
And I scroll till element 'SubjectField_Hybrid'
And I wait for '5' seconds
And I switch to iFrame 'Emailframe1'
And I switch to iFrame 'Emailframe2'
And I should see element 'EmailBodyField' present on page
And I click by JS 'EmailBodyField'
And I clear the text and enter 'ServiceCaseEmail' in field 'EmailBodyField'
And I switch to default content
And I should see element 'SavedText' present on page
And I scroll till element 'HighlightPanel'
And I wait for '10' seconds
And I scroll till element 'ShippingInfoSection_Hybrid'
And I wait for '10' seconds
And I should see element 'EmailSection_Hybrid' present on page
And I should see text 'CaseEmail' contained on page at 'CaseEmailSubject_Field'
And I should see text 'divyaalakshmi.p@royalcyber.com' present on page at 'FromAddressValue_Hybrid'
And I should see text 'divyaalakshmi.p@royalcyber.com' present on page at 'ToAddressValue_Hybrid'
And I should see element 'MessageDateValue_Hybrid' present on page
And I wait for '5' seconds
And I click by JS 'Close_Agent' 

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page


Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'telesales' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'telesales_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'


Scenario: 2. IS Agent to Save draft emails in CRM.
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I click by JS 'SelectListViewDropdown_CR'
And I should see element 'SearchField_CR' present on page
And I wait for '3' seconds
And I clear the text and enter 'Service - MFRM Care' in field 'SearchField_CR'
And I wait for '3' seconds
And I should see element 'ServiceMFRMCareOption' present on page
And I click by JS 'ServiceMFRMCareOption'
And I should see element 'NewEscalatedCaseNumber_CR' present on page
And I click by JS 'NewEscalatedCaseNumber_CR'
And I should see element 'Actions_Tab' present on page
And I click by JS 'Actions_Tab'
And I should see element 'SendExternalEmailButton_Hybrid' present on page
And I click by JS 'SendExternalEmailButton_Hybrid'
#And I should see element 'SendInternalEmailButton_Hybrid' present on page
#And I click by JS 'SendInternalEmailButton_Hybrid'
And I should see element 'ToField_Hybrid' present on page
And I click by JS 'ToField_Hybrid'
And I clear the text and enter 'divyaalakshmi.p@royalcyber.com' in field 'ToField_Hybrid'
#And I enter 'divyaalakshmi.p@royalcyber.com' in field 'ToField_Hybrid'
And I wait for '5' seconds
And I hit down Arrow key on element 'ToField_Hybrid'
#And I click by JS 'ToField_Hybrid'
#And I should see element 'ToAddressFieldValue_Hybrid' present on page
And I should see element 'SubjectField_Hybrid' present on page
And I clear the text and enter 'CaseEmail' in field 'SubjectField_Hybrid'
And I scroll till element 'SubjectField_Hybrid'
And I wait for '5' seconds
And I switch to iFrame 'Emailframe1'
And I switch to iFrame 'Emailframe2'
And I should see element 'EmailBodyField' present on page
And I click by JS 'EmailBodyField'
And I clear the text and enter 'ISCaseEmail' in field 'EmailBodyField'
And I switch to default content
And I wait for '10' seconds
And I should see element 'SavedText' present on page
And I scroll till element 'HighlightPanel'
And I wait for '10' seconds
And I scroll till element 'ShippingInfoSection_Hybrid'
And I wait for '10' seconds
And I should see element 'EmailSection_Hybrid' present on page
And I should see text 'CaseEmail' contained on page at 'CaseEmailSubject_Field'
And I should see text 'divyaalakshmi.p@royalcyber.com' present on page at 'FromAddressValue_Hybrid'
And I should see text 'divyaalakshmi.p@royalcyber.com' present on page at 'ToAddressValue_Hybrid'
And I should see element 'MessageDateValue_Hybrid' present on page
And I wait for '5' seconds
And I click by JS 'Close_Agent' 

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page



Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'hybrid' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'hybrid_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'


Scenario: 3. Retention Agent to Save draft emails in CRM. 
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I click by JS 'SelectListViewDropdown_CR'
And I should see element 'SearchField_CR' present on page
And I wait for '3' seconds
And I clear the text and enter 'Service - MFRM Care' in field 'SearchField_CR'
And I wait for '3' seconds
And I should see element 'ServiceMFRMCareOption' present on page
And I click by JS 'ServiceMFRMCareOption'
And I should see element 'NewEscalatedCaseNumber_CR' present on page
And I click by JS 'NewEscalatedCaseNumber_CR'
And I should see element 'Actions_Tab' present on page
And I click by JS 'Actions_Tab'
And I should see element 'SendInternalEmailButton_Hybrid' present on page
And I click by JS 'SendInternalEmailButton_Hybrid'
And I should see element 'ToField_Hybrid' present on page
And I click by JS 'ToField_Hybrid'
And I clear the text and enter 'divyaalakshmi.p@royalcyber.com' in field 'ToField_Hybrid'
#And I enter 'sindhu.r@royalcyber.com' in field 'ToField_Hybrid'
And I wait for '5' seconds
And I hit down Arrow key on element 'ToField_Hybrid'
#And I click by JS 'ToField_Hybrid'
#And I should see element 'ToAddressFieldValue_Hybrid' present on page
And I should see element 'SubjectField_Hybrid' present on page
And I clear the text and enter 'CaseEmail' in field 'SubjectField_Hybrid'
And I scroll till element 'SubjectField_Hybrid'
And I wait for '5' seconds
And I switch to iFrame 'Emailframe1'
And I switch to iFrame 'Emailframe2'
And I should see element 'EmailBodyField' present on page
And I click by JS 'EmailBodyField'
And I clear the text and enter 'RetentionCaseEmail' in field 'EmailBodyField'
And I switch to default content
And I should see element 'SavedText' present on page
And I scroll till element 'HighlightPanel'
And I wait for '10' seconds
And I scroll till element 'ShippingInfoSection_Hybrid'
And I wait for '10' seconds
And I should see element 'EmailSection_Hybrid' present on page
And I should see text 'CaseEmail' contained on page at 'CaseEmailSubject_Field'
And I should see text 'divyaalakshmi.p@royalcyber.com' present on page at 'FromAddressValue_Hybrid'
And I should see text 'divyaalakshmi.p@royalcyber.com' present on page at 'ToAddressValue_Hybrid'
And I should see element 'MessageDateValue_Hybrid' present on page
And I wait for '5' seconds
And I click by JS 'Close_Agent' 

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

















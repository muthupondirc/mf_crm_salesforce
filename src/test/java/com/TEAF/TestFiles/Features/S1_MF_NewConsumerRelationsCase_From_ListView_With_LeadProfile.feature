@MF_ConsumerRelationsCase_Lead
Feature: Creating a new Consumer Relations Case from List View with Lead Profile
 
 1. Login to Mattress Firm Sales Force Application
 1.2 Creating a new Consumer Relations Case from List View (Lead and above) -SP1_TC16
 2. Closing a Consumer Relations Case (Lead and above) - SP1_TC17
 3. Enter a Note in Case Comments - Consumer Relations Case - SP1_TC18
 
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Sr Mgr User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Sr Mgr User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.2 Creating a new Consumer Relations Case from List View (Lead and above) -SP1_TC16
Given I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I should see element 'New_Button_Agent' present on page
And I click 'New_Button_Agent'
Then I should see element 'NewCase_Text_Lead' present on page
And I click 'ConsumerRelations_Button_Creat'
And I click 'Next_Button_Lead'
Then I should see element 'NewCaseConsumerRelations_Text_Creat' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click 'Email_Text_Creat'
And I click 'State_Dropdown_Creat'
And I click 'NY_Text_Creat'
And I click 'Brand_Dropdown_Creat'
And I click 'MattressFirm_Text_Creat'
And I click 'Source_Dropdown_Agent'
And I click 'CustomerCare_Text_Creat'
And I click 'MerchandiseDisposition_Dropdown_Creat'
And I click 'Prime_Text_Creat'
And I click 'CRCaseType_Dropdown_Creat'
And I click 'Agency_Text_Creat'
And I click 'SubCaseType_Dropdown_Creat'
And I click 'BetterBusinessBureau(BBB)_Text_Creat'
And I click 'CurrentState_Dropdown_Creat'
And I click 'AccommodationOffer_Text_Creat'
And I click 'CR_Reason_Dropdown_Creat'
And I click 'Billing/Financing_Text_Creat'
And I click 'CR_ReasonDetails_Dropdown_Creat'
And I click 'CreditCard-Overcharged_Text_Creat'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I click by JS 'Close_Agent' 
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseNumber_Agent' contained on page at 'CaseNumber_Dropdown_Agent'
And I should see text 'New' contained on page at 'Status_Agent'
And I should see text 'Consumer Relations' contained on page at 'CaseRecordType_Creat'
And I should see text 'CCO Sr Mgr User 2' contained on page at 'OwnerName_Lead'
And I click by JS 'Close_Agent' 

Scenario: 2. Closing a Consumer Relations Case (Lead and above)-SP1_TC17
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I click 'CaseNumber_Dropdown_Agent'
And I click 'Edit_Button_Close' Tab
And I should see element 'Edit_Header' present on page
And I click 'CR_Resolution_Dropdown_Close'
And I click 'NoFurtherActionNeeded_Text_Close'
And I click 'Status_Dropdown_Close'
And I click by JS 'Closed_Text_Close'
And I click 'Save_Button_Close'
And I wait for '5' seconds
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent' 


Scenario:  3. Enter a Note in Case Comments - Consumer Relations Case -SP1_TC18
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I click 'CaseNumber_Dropdown_Agent'
And I wait for '5' seconds
And I scroll till element 'RelatedCase_CreateRelated'
And I wait for '15' seconds
And I scroll till element 'CaseHistory_Note'
And I wait for '10' seconds
And I scroll till element 'Files_field'
And I wait for '10' seconds
And I should see element 'CaseComments_Text_Note' present on page
And I click by JS 'CaseComments_Dropdown_Note'
And I click 'New_Text_Note'
And I should see element 'Body_Textarea_Note' present on page
And I click 'Body_Textarea_Note'
And I enter 'ConsumerCase' in field 'Body_Textarea_Note'
And I click 'Public_Checkbox_Note'
And I click 'Save_Note_button_Note'
And I wait for '5' seconds
And I should see text 'You do not have permissions to create new Case Comments. Please log this interaction using the "Log a Call" action.' present on page at 'Error_Message_Note'
And I click by JS 'Close_Agent'
And I click by JS 'Close_Agent'

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page


















@New_WarrantyCase_From_ListView_AgentProfile
Feature: New Attached Warranty Case with Agent Profile 

1. Login with Standard user Credentials
2. SP1_TC37 Creating Attached Warranty Claim from Order
3. SP1_TC38 Creating Attached Warranty Claim from Order (continued) 
4. SP1_TC39 Making a Determination on Attached Warranty Claim
5. SP1_TC40 Verifying Approval and Denial Letters on Attached Warranty Claim 
6. SP1_TC41 Logging a Call on an Attached Warranty Claim 
7. SP1_TC42 Creating a New Blank Related Warranty Case Using the Related Case Component 
8. SP1_TC43 Warranty Case- Enter a note in comments 
9. Logout Scenario

@test
Scenario: Login with Standard user Credentials
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '2' seconds

Scenario: SP1_TC37 Creating Attached Warranty Claim from Order 
And I wait for '3' seconds
Given I click 'SearchTypeDD'
And I click by JS 'SearchType_Order'
And I hit tab-key on element 'SearchTypeDD'
#QA Order Number
And I enter 'SLP-105174834-1130' in field 'GlobalSearchField'
#UAT Order Number
#And I enter 'AX-S015348301' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I hit enter-key on element 'GlobalSearchField'
And I wait for visibility of element 'File_Warranty_ClaimBtn'
#And I should see element 'Order_Header' present on page
#And I click by JS 'Order_Number'
#And I wait for '5' seconds
And I should see element 'File_Warranty_ClaimBtn' present on page
And I click by JS 'File_Warranty_ClaimBtn'
And I wait for '3' seconds
And I switch to iFrame 'IframeWarrantyTab'
And I should see element 'DefaultAddress' present on page
And I wait for '5' seconds
And I click 'DefaultAddress_NextBtn'
And I wait for '3' seconds
And I click by JS 'HR2000_Checkbox'
#And I click by JS 'SY_TITAN_Checkbox'
And I click by JS 'Warranty_Quantity_Txtbox'
And I enter '1' in field 'Warranty_Quantity_Txtbox'
#And I click by JS 'Warranty_Txt_Entry'
#And I enter '1' in field 'Warranty_Txt_Entry'
And I click by JS 'NextButton'
And I wait for '3' seconds
#And I should see element 'ProductName_AX-V000069623' present on page
And I click by JS 'Product_AX-V000069623_DD'
And I click 'Product_AX-V000069623_DD_Value'
#And I should see element 'ProductName_AX-V000055988 ' present on page
#And I click by JS 'Product_AX-V000055988_DD'
#And I click 'Product_AX-V000055988_DD_Value'
And I click by JS 'NextButton'
And I wait for '3' seconds
And I should see element 'Question1_Label' present on page
And I click by JS 'Question1_RadioBtn'
And I should see element 'Question2_Label' present on page
And I click by JS 'Question2_RadioBtn'
And I should see element 'Question2a_Label' present on page
And I click by JS 'Question2_DD'
And I select option '1' in dropdown 'Question2_DD' by 'text'
And I should see element 'Question3_Label' present on page
And I click by JS 'Question3_DD'
And I select option '5' in dropdown 'Question3_DD' by 'text'
#And I click by JS 'Question3_DD_Value'
And I should see element 'Question4_Label' present on page
And I click by JS 'Question4_RadioBtn'
And I click by JS 'NextButton'
And I wait for '3' seconds
And I should see element 'VendorInspection' present on page
And I click by JS 'VendorInspection_Checkbox'
And I click by JS 'CompleteBtn'
And I wait for '3' seconds
And I should see element 'CaseNumber_Btn' present on page
And I should see element 'Payment_Link' present on page
And I click by JS 'Payment_Link'
And I click by JS 'CaseNumber_Btn'
And I switch back to Main Window
And I wait for '5' seconds
And I should see element 'WarrantyClaim_Header' present on page


Scenario: SP1_TC38 Creating Attached Warranty Claim from Order (continued) 
And I wait for '5' seconds
And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I should see element 'EditCaseTitle' present on page
#And I should see element 'CaseNumber_Header' present on page
And I scroll till element 'Source'
And I should see element 'Inspection_Vendor_Label' present on page
And I should see element 'Inspection_Vendor_Value' present on page
And I should see element 'Payment_Status_Label' present on page
And I click by JS 'Payment_DD'
And I click by JS 'PaymentDD_Value'
And I scroll till element 'Reason_Label'
And I should see element 'Payment_Auth_Label' present on page
And I click by JS 'Payment_Auth_Field'
And I enter 'xxxxxxx' in field 'Payment_Auth_Field'
And I should see element 'Current_State_Label' present on page
And I click by JS 'CurrentState_DD'
And I click by JS 'Current_State_Vendor'
And I click by JS 'WarrantyEditSaveBtn'
And I wait for '5' seconds
And I should see element 'Case_Owner_Label' present on page
And I should see element 'CaseNumberValue' present on page
And I get text from 'CaseNumberValue' and store
And I click by JS 'CloseTab'
And I wait for '2' seconds
And I click by JS 'Close'

@test
Scenario: SP1_TC39 Making a Determination on Attached Warranty Claim TC-64
And I wait for '3' seconds
Given I click 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I clear field 'GlobalSearchField'
And I enter from stored variable '$$CaseNumberValue' into feild 'GlobalSearchField'  
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '2' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '10' seconds
And I scroll till element 'ShippingInfo'
And I wait for '5' seconds
And I scroll till element 'RelatedCase'
And I wait for '4' seconds
And I should see element 'CaseProduct' present on page
And I should see element 'ProductNumber' present on page
And I click by JS 'ProductNumber_DD'
And I click by JS 'ProductNumber_ToolTip'
And I wait for '4' seconds
And I should see element 'ProductType_Label' present on page
And I click by JS 'ProductType_Field'
And I click by JS 'ProductType_Value'
And I should see element 'Size_Label' present on page
And I click by JS 'Size_DD'
And I click by JS 'Size_Value'
And I scroll till element 'Product_Info'
And I should see element 'ProductComments_Label' present on page
And I should see element 'ProductComments_Field' present on page
And I should see element 'ResultCode_Label' present on page
And I click by JS 'ResultCode_DD'
And I click by JS 'ResultCode_Value'
And I click by JS 'Save'
#And I wait for '3' seconds
#And I click by JS 'CloseNewCaseTab'
And I wait for '4' seconds
#And I click by JS 'Switch_CaseTab'
#And I wait for '2' seconds
#And I should see element 'Prod_Number' present on page 
#And I click by JS 'Prod_Number_DD'
#And I click by JS 'Prod_Number_Tooltip'
#And I should see element 'Size_Label' present on page
#And I click by JS 'Size_DD'
#And I click by JS 'Size_Value'
#And I scroll to 'element' - 'Product_Info'
#And I should see element 'ProductComments_Label' present on page
#And I should see element 'ProductComments_Field' present on page
##And I should see element 'ResultCode_Label' present on page
#And I click by JS 'ResultCode_DD'
#And I click by JS 'ResultCode'
#And I click by JS 'Save'
And I wait for '3' seconds
And I click by JS 'EscalateEdit'
And I wait for '4' seconds
And I scroll till element 'Reason_Label'
And I should see element 'CurrentState_Label' present on page
And I click by JS 'CurrentState_DD'
And I click by JS 'CurrentState_DD_Value'
And I should see element 'Resolution' present on page
And I click by JS 'Resolutions_DD'
And I click by JS 'Reso_DD_Value'
And I click by JS 'WarrantyStatus_DD'
And I click by JS 'WarrantyStatus_DD_Value'
And I click by JS 'WarrantyEditSaveBtn'
And I wait for '5' seconds
And I click by JS 'Close'
And I wait for '3' seconds


Scenario: SP1_TC40 Verifying Approval and Denial Letters on Attached Warranty Claim 

And I wait for '3' seconds
Given I click 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I clear field 'GlobalSearchField'
And I enter from stored variable '$$CaseNumberValue' into feild 'GlobalSearchField'
#And I enter '02905023' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'WarrantyApprovalLetter' present on page
And I click by JS 'WarrantyApprovalLetter'
And I wait for '5' seconds
#And I should see element 'PDFContent' present on page
#And I mouse over 'PDFPageContent'
#And I click by JS 'DownloadPDFBtn'
#And I validate PDF file - filename: '(.*)' & path: '(.*)' content - Field Name '(.*)' & Field Value: '(.*)'
And I click by JS 'WarrantyTabCloseBtn'
And I wait for '3' seconds
And I should see element 'WarrantyDenialLetter' present on page
And I click by JS 'WarrantyDenialLetter'
And I wait for '5' seconds
#And I should see element 'PDFContent' present on page
#And I mouse over 'PDFPageContent'
#And I click by JS 'DownloadPDFBtn'
#And I validate PDF file - filename: '(.*)' & path: '(.*)' content - Field Name '(.*)' & Field Value: '(.*)'
And I click by JS 'WarrantyTabCloseBtn'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '2' seconds



Scenario: SP1_TC41 Logging a Call on an Attached Warranty Claim 
And I wait for '3' seconds
Given I click 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I enter from stored variable '$$CaseNumberValue' into feild 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'Warranty_LogCall' present on page
And I click by JS 'Warrant_Add'
And I should see element 'Warranty_MFRM_Label' present on page
And I click by JS 'Warranty_MFRM_DD'
And I click by JS 'Warranty_MFRM_DD_Value'
And I should see element 'Warranty_Subject' present on page
And I click by JS 'Warranty_Subject_Txt'
And I clear field 'Warranty_Subject_Txt'
And I enter 'Determination' in field 'Warranty_Subject_Txt'
And I should see element 'Warranty_comments_label' present on page
And I click by JS 'Warranty_Comments_Txt_Field'
And I wait for '2' seconds
And I enter 'Testing 1,2,3' in field 'Warranty_Comments_Txt_Field'
And Blur 'Warranty_Comments_Txt_Field'
And I wait for '2' seconds
And I click by JS 'Warranty_LogCall_Save'
And I wait for '3' seconds
And I scroll to 'element' - 'Contact_BillingInfo'
And I should see element 'Warranty_ActivityHistory' present on page
And I should see element 'Warranty_ActivityHistory_Subject' present on page
And I click by JS 'Warranty_ViewAll'
And I wait for '3' seconds
And I click by JS 'Switch_SubTab'
And I wait for '3' seconds
And I should see element 'Warranty_Contact' present on page
And I click by JS 'Warranty_Contact'
And I wait for '3' seconds
And I scroll till element 'ActivityHistory_Contact'
And I wait for '3' seconds
And I click by JS 'TaskTab'
And I wait for '3' seconds
And I switch to iFrame 'IframeWarrantyTab'
And I wait for '2' seconds
And I should see element 'LastModifiedDate' present on page
And I switch back to Main Window
And I wait for '2' seconds
And I click by JS 'GlobalSearchField'
And I clear field 'GlobalSearchField'
And I click by JS 'Close'

Scenario: SP1_TC42 Creating a New Blank Related Warranty Case Using the Related Case Component 
Given I click 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I clear field 'GlobalSearchField'
And I enter from stored variable '$$CaseNumberValue' into feild 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'HighlightPanel' present on page
And I wait for '5' seconds
And I scroll till element 'ShippingInfo'
And I wait for '5' seconds
And I scroll till element 'Emails'
And I wait for '5' seconds
And I should see element 'RelatedCases' present on page
And I wait for '3' seconds
And I click by JS 'RelatedCasesDD'
And I wait for '2' seconds
And I should see element 'RelatedCasesTooltip' present on page
And I click by JS 'Close'

Scenario: SP1_TC43 Warranty Case- Enter a note in comments 
And I click 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I enter from stored variable '$$CaseNumberValue' into feild 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'HighlightPanel' present on page
And I should see element 'ShippingInfo' present on page
And I wait for '2' seconds
And I scroll till element 'ShippingInfo'
And I wait for '5' seconds
And I scroll till element 'HighlightPanelEmails'
And I wait for '5' seconds
And I scroll till element 'HighlightPanelCaseHistory'
And I wait for '5' seconds
And I scroll till element 'HighlightPanelFile_Header'
And I wait for '5' seconds
And I scroll till element 'HighlightPanelFile_Header'
And I wait for '5' seconds
And I should see element 'Case_Comment' present on page
And I click by JS 'Case_Comment_DropDown'
And I click by JS 'Case_Comment_ToolTip'
And I wait for '5' seconds
And I should see element 'New_Case_Comment_Header' present on page
And I should see element 'New_Case_Comment_Body' present on page
And I click by JS 'New_Case_Comment_Body'
And I enter 'ABCDE' in field 'New_Case_Comment_Body'
And I hit enter-key on element 'New_Case_Comment_Body'
And I hit tab-key on element 'New_Case_Comment_Body'
And I wait for '2' seconds
And I should see element 'Public_Label' present on page
And I click by JS 'Public_Checkbox'
And I wait for '5' seconds
And I click by JS 'Case_Comp_SaveBtn'
And I wait for '3' seconds
And I should see element 'Case_Comp_Errormsg' present on page
And I wait for '3' seconds
And I click by JS 'Close'


@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
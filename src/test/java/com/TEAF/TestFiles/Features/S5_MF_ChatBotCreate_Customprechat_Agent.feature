@S5_ChatBotCreate_Customprechat_Agent
Feature: ChatBot-Create a custom pre-chat for mattressfirm.com
 
 1. Login to Mattress Firm Sales Force Application
1.1 To verify that ChatBot - Creating a custom pre-chat for mattressfirm.com - SP5_TC1

Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 To verify that ChatBot - Creating a custom pre-chat for mattressfirm.com - SP5_TC1
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
And I wait for '340' seconds
#And I refresh the WebPage
#And I wait for '15' seconds
#And I should see element 'JoeTab_Alanna' present on page 
#And I wait for '10' seconds


@S6_HybridAgentRecievingChat
Feature: Hybrid Agent Recieving Chat from TelesalesRep

1. Hybrid Agent Recieving Chat from TelesalesRep - SP6_TC2.8

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'Retention Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'Retention Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 Omni Channel - Hybrid Agent as Available Chat
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'

Scenario: 1. Hybrid Agent Recieving Chat from TelesalesRep - SP6_TC
Given I wait for '140' seconds
#waiting for telesales rep to get connected in Chat
And I should see element 'JoeTab' present on page
And I click by JS 'JoeTab'
And I wait for '10' seconds
And I should see element 'Sent_Section' present on page
And I should see element - 'Sent_Section' contains text 'IS Agent sent a transfer request to Retention Agent'
And I should see element 'Accepted_Section' present on page
And I should see element - 'Accepted_Section' contains text 'Retention Agent accepted the chat'
And I wait for '5' seconds
And I should see element 'TabCloseButton_Hybrid' present on page
And I click by JS 'TabCloseButton_Hybrid'
And I click by JS 'EndConversationButton_Hybrid'









#Scenario: 1.3 Verify logout of Sales Force application
#Given I click 'UserImageButton'
#And I wait for '5' seconds
#And I click link 'Log Out'
#And I wait for '5' seconds
#And I should see element 'login_username' present on page
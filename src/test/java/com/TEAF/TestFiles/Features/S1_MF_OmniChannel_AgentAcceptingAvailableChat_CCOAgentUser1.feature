@MF_OmniChannel_AgentAcceptingAvailableChat_Agent1Agent
Feature: Omni Channel - Agent Accepting an Available Chat with Agent1 Profile
 
 1. Login to Mattress Firm Sales Force Application
1.1 Omni Channel - Transfer Chat to Another Agent - SP1_TC56
Note: Agent profile should be Alanna Troy
2. Omni Channel - Transfer Chat to a Chat Button - SP1_TC57
 
Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 Omni Channel - Transfer Chat to Another Agent - SP1_TC56
Given I wait for '160' seconds
#waiting for Agent ashley to get connected in Chat
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
And I wait for '20' seconds
And I should see element 'JoeTab' present on page
And I click 'JoeTab'
And I should see element 'Sent_Section' present on page
#And I should see element - 'Sent_Section' contains text 'Ashley sent a transfer request to Kim'
And I should see element 'Accepted_Section' present on page
#And I should see element - 'Accepted_Section' contains text 'Kim accepted the chat'
And I should see element 'Hello_Section' present on page
And I should see element - 'Hello_Section' contains text 'Hello'
And I scroll till element 'Textarea_Kim'
And I wait for '5' seconds
And I should see element 'Textarea_Kim' present on page
And I wait for '5' seconds
And I clear the text and enter 'hello' in field 'Textarea_Kim'
And I hit enter-key on element 'Textarea_Kim'
And I wait for '20' seconds

Scenario: 2. Omni Channel - Transfer Chat to a Chat Button - SP1_TC57
Given I wait for visibility of element 'Transfer_Icon'
And I click by JS 'Transfer_Icon'
And I wait for '5' seconds
And I should see element 'TransferChat_Header' present on page 
And I click by JS 'UserDropdown'
And I wait for '5' seconds
And I click by JS 'ChatButton_Option_Ashley'
And I click 'SelectanItem_Field_Omni'
And I click by JS 'MFIServiceBtn_Option'
And I clear the text and enter 'Hello' in field 'Textarea_Field_Transfer'
And I wait for '5' seconds
And I click 'Transfer_Button_Omni'
And I wait for '5' seconds
#And I should see empty value in the Input feild 'JoeTab'
#And I wait for '10' seconds
















@MF_StoreReferralsEmail_WebLightningComponent
Feature: Verify Functionality of Store Referrals Email WebLightning Component

1. Login to Mattress Firm Sales Force Application
1.2 Verify Functionality of Phone Store Referral for Unknown Contacts -SP2_TC1
2. Verify Agent should be able to View Lead details -SP2_TC2
3. Verify Functionality of Phone Store Referral for Existing Contacts -SP2_TC3
4. Verify Agent should be able to View Lead details -SP2_TC4

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'IS Mgr User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'IS Mgr User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1. Verify Functionality of Phone Store Referral for Unknown Contacts -SP2_TC1
And I should see element 'StoreReferral_Utilitybar' present on page
And I click by JS 'StoreReferral_Utilitybar'
And I should see element 'ZipCodeField_Store' present on page
And I clear the text and enter '11801' in field 'ZipCodeField_Store'
And I should see element 'DistanceMiles_Dropdown_Store' present on page
And I click by JS 'DistanceMiles_Dropdown_Store'
And I click by JS '10MilesRadius_Option'
And I should see element 'SearchButton_Store' present on page
And I click by JS 'SearchButton_Store'
And I should see element 'StoreItem1List' present on page
And I click by JS 'StoreItem1List'
And I should see element 'NextButton_Store' present on page
And I click by JS 'NextButton_Store'
And I should see element 'FirstNameField_Store' present on page
And I clear the text and enter 'Sindhu' in field 'FirstNameField_Store'
And I should see element 'LastNameField_Store' present on page
And I clear the text and enter 'rangappa' in field 'LastNameField_Store'
And I should see element 'EmailField_Store' present on page
And I clear the text and enter 'sindhu.r@royalcyber.com' in field 'EmailField_Store'
And I get text from 'EmailField_Store' and store
And I should see element 'PhoneField_Store' present on page
And I clear the text and enter '8971984986' in field 'PhoneField_Store'
And I should see element 'StoreField_Store' present on page
And I click by JS 'NextButton_Store'
And I should see element 'NewCreatedLeadText_Store' present on page
And I should see element 'FinishButton_Store' present on page
And I click by JS 'FinishButton_Store'
And I click by JS 'Minimize_store'


Scenario: 2. Verify Agent should be able to View Lead details -SP2_TC2
And I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'LeadsMenu_Store' present on page
And I click by JS 'LeadsMenu_Store'
And I wait for '5' seconds
And I should see element 'NewContactField_Store_Unknown' present on page
And I get text from 'NewContactField_Store_Unknown' and store
And I click by JS 'NewContactField_Store_Unknown'
And I should see text '$$NewContactField_Store_Unknown' present on page at 'LeadName_Store'
And I should see text 'sindhu.r@royalcyber.com' contained on page at 'EmailFieldName_Store'
And I should see element 'DetailsSection_Store' present on page
And I click by JS 'DetailsSection_Store'
And I should see text 'IS Mgr User 2' present on page at 'LeadOwnerName_DetailsStore'
And I should see text '$$NewContactField_Store_Unknown' present on page at 'CustomerName_DetailsStore'
And I should see text 'Store Referral' present on page at 'CompanyName_DetailsStore'
And I should see text 'New' present on page at 'LeadStatus_DetailsStore'
And I should see text 'sindhu.r@royalcyber.com' contained on page at 'EmailField_DetailsStore_Unknown'
And I should see text 'Phone Agent' present on page at 'LeadSource_DetailsStore'
And I click by JS 'Close_Agent' 

Scenario: 3. Verify Functionality of Phone Store Referral for Existing Contacts -SP2_TC3
Given I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'ContactsMenu_Store' present on page
And I click by JS 'ContactsMenu_Store'
And I should see element 'NewContactField_Store_Existing' present on page
And I get text from 'NewContactField_Store_Existing' and store
And I click by JS 'NewContactField_Store_Existing'
And I click by JS 'DetailsSection_Store'
And I get text from 'CustomerName_DetailsStore' and store
And I get text from 'EmailField_DetailsStore_Existing' and store
And I should see element 'StoreReferralButton_ExistingCont' present on page
And I click by JS 'StoreReferralButton_ExistingCont'
And I wait for '20' seconds
And I switch to iFrame 'iframe_Store'
And I should see element 'ZipCodeField_Store' present on page
And I clear the text and enter '11801' in field 'ZipCodeField_Store'
And I should see element 'DistanceMiles_Dropdown_Store' present on page
And I click by JS 'DistanceMiles_Dropdown_Store'
And I click by JS '10MilesRadius_Option'
And I should see element 'SearchButton_Store' present on page
And I click by JS 'SearchButton_Store'
And I should see element 'StoreItem1List' present on page
And I click by JS 'StoreItem1List'
And I should see element 'NextButton_Store' present on page
And I click by JS 'NextButton_Store'
#And I should see text 'sindhu' present on page at 'FirstNameField_Store'
#And I should see text 'r' present on page at 'LastNameField_Store'
And I should see text '$$EmailField_DetailsStore_Existing' contained on page at 'EmailField_Store'
And I should see element 'FirstNameField_Store' present on page
And I should see element 'LastNameField_Store' present on page
And I should see element 'PhoneField_Store' present on page
And I should see element 'StoreField_Store' present on page
And I click by JS 'NextButton_Store'
And I should see element 'NewCreatedLeadText_Store' present on page
And I should see element 'FinishButton_Store' present on page
And I click by JS 'FinishButton_Store'
And I switch to default content
And I click by JS 'Close_Agent' 

Scenario: 4. Verify Agent should be able to View Lead details -SP2_TC4
And I should see element 'ShowNavigationButton' present on page
And I click by JS 'ShowNavigationButton'
And I should see element 'LeadsMenu_Store' present on page
And I click by JS 'LeadsMenu_Store'
And I wait for '5' seconds
And I should see element 'NewContactField_Store_Existing' present on page
And I get text from 'NewContactField_Store_Existing' and store
And I click by JS 'NewContactField_Store_Existing'
And I should see text '$$NewContactField_Store_Existing' present on page at 'LeadName_Store'
And I should see text 'IS Mgr User 2' present on page at 'LeadOwnerName_Store'
And I should see text '$$EmailField_DetailsStore_Existing' contained on page at 'EmailFieldName_Store'
And I should see element 'DetailsSection_Store' present on page
And I click by JS 'DetailsSection_Store'
And I should see text 'IS Mgr User 2' present on page at 'LeadOwnerName_DetailsStore'
And I should see text '$$NewContactField_Store_Existing' present on page at 'CustomerName_DetailsStore'
And I should see text 'Store Referral' present on page at 'CompanyName_DetailsStore'
And I should see text 'New' present on page at 'LeadStatus_DetailsStore'
And I should see text '$$EmailField_DetailsStore_Existing' contained on page at 'EmailField_DetailsStore_Existing'
And I should see text 'Phone Agent' present on page at 'LeadSource_DetailsStore'
And I click by JS 'Close_Agent'

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

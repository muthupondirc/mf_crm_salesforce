@S1_MF_ServiceCase_CustomeContact_Lead
Feature: Creating a new Service Case from CustomeContact with Lead Profile

 1. Login to Mattress Firm Sales Force Application
 1.2 Creating a new Service Case from Customer Contact (Lead and above) - SP1_TC32
 
 
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Lead User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Lead User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario:  1. Creating a new Service Case from Customer Contact (Lead and above) - SP1_TC32
Given I click 'All_Dropdown_Agent'
And I click by JS 'ContactsFilter_ContactLead'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I clear the text and enter 'Jared Steiner' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I should see element 'ContactNameLink_ContactLead' present on page
And I click 'ContactNameLink_ContactLead'
And I should see element 'NewCase_ContactLead' present on page
And I click by JS 'NewCase_ContactLead'
#next 3 steps for uat crm
And I click by JS 'SelectRecordType_Contact'
And I select option 'Service' in dropdown 'SelectRecordType_Contact' by 'value'
And I click by JS 'NextButton_Contact' 
Then I should see element 'NewCaseService_Text_Lead' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click 'Phone_Text_Agent'
And I click 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click 'SubRecordType_Dropdown_Agent'
And I click by JS '120DayPrice_Option'
And I click 'TypeOfProduct(s)_Dropdown'
And I click by JS 'CoreProduct_Option'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I scroll till element 'CaseOwner_ContactLead'
And I wait for '4' seconds
And I should see element 'CaseNumber_Order' present on page
And I should see element 'ContactName_ConsumerOrder' present on page
And I should see element - 'ContactName_ConsumerOrder' contains text 'Jared Steiner'
And I click by JS 'ContactName_ConsumerOrder'
And I refresh the WebPage
And I wait for '10' seconds
And I scroll till element 'Cases_ContactLead'
And I wait for '10' seconds
And I should see element 'Cases_ContactLead' present on page
And I should see text '$$CaseNumber_Agent' contained on page at 'NewNumber_Order'
And I should see text 'Service' contained on page at 'CaseRecordType_Order'
And I click by JS 'Close_Agent' 


Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page
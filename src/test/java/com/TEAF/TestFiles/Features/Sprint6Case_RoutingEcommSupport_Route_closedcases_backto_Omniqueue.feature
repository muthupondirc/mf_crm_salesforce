@Sprint6Case_RoutingEcommSupport_Route_closedcases_backto_Omniqueue
Feature: Routing an EComm support closed cases back to Omni queue
1. Login with Ecomm Agent user.
2. SP6_TC3 - Routing an EComm support closed cases back to Omni queue
3. Logout Scenario
@l
Scenario: Login with Ecomm Agent user.
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'ecomagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'ecomagent_displayname' present on page at 'ProfileCardPopUp'
And I wait for '3' seconds
@l
Scenario: SP6_TC3 - Routing an EComm support closed cases back to Omni queue
Given I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'SFNav_Cases'
And I wait for '5' seconds
And I should see element 'SFCase_New' present on page
And I click by JS 'SFCase_New'
And I wait for '5' seconds
And I should see element 'EcommCase' present on page
And I should see element 'EcommCaseRadioBtn' present on page
And I wait for '3' seconds
And I click by JS 'EcommCaseRadioBtn'
And I wait for '2' seconds
And I should see element 'EcommCaseNext' present on page
And I wait for '2' seconds
And I click by JS 'EcommCaseNext'
And I wait for '5' seconds
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'Ecomm_SubRecordType_Value'
And I wait for '2' seconds
And I should see element 'Ecomm_SubType' present on page
And I should see element 'Ecomm_SubTypeDD' present on page
And I click by JS 'Ecomm_SubTypeDD'
And I click by JS 'Ecomm_SubTypeValue'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'NavHome'
And I wait for '5' seconds
And I click by JS 'EcommCaseTab'
And I wait for '3' seconds
And I should see element 'Ecomm_SupportHeader' present on page
And I should see CaseOwner name 'ecomagent_displayname' present on page
And I should see element 'EcommCaseNumberValue' present on page
And I get text from 'EcommCaseNumberValue' and store
And I should see element 'EcommEditCaseBtn' present on page
And I wait for '2' seconds
And I click by JS 'EcommEditCaseBtn'
And I wait for '5' seconds
And I scroll till element 'EcommScrollCaseOriginEdit'
And I wait for '3' seconds
And I should see element 'EcommStatus' present on page
And I should see element 'EcommStatusDD' present on page
And I click by JS 'EcommStatusDD'
And I should see element 'EcommStatusDD_Value' present on page
And I click by JS 'EcommStatusDD_Value'
And I wait for '5' seconds
And I should see element 'BedBugSave' present on page
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see CaseOwner name 'ecomagent_displayname' present on page
And I should see element 'EcomCaseOrigin' present on page
And I should see element 'EcomCaseOriginValue' present on page
And I scroll till element 'EcomCaseOriginValue'
And I should see element 'EcommCaseSource' present on page
And I should see element 'EcommCaseSourceValue' present on page
And I should see element 'EcommSubRecordType' present on page
And I should see element 'EcomSubRecordTypeValue' present on page
And I should see element 'EcommCaseRecordType' present on page
And I should see element 'EcommCaseRecordTypeValue' present on page
And I should see element 'EcommStatusLabel' present on page
And I should see element 'EcommStatusClosed' present on page
And I should see element 'EcommSendExternalEmail' present on page
And I click by JS 'EcommSendExternalEmail'
And I wait for '3' seconds
And I should see element 'EcommToEmailField' present on page
And I click by JS 'EcommToEmailField'
And I enter 'hasantestuser@gmail.com' in field 'EcommToEmailField'
And I wait for '5' seconds
#And I hit down Arrow key on element 'EcommToEmailField'
And I wait for '3' seconds
And I hit enter-key on element 'EcommToEmailField'
And I wait for '5' seconds
And I switch to iFrame 'EcommIframeemailBody'
And I wait for '5' seconds
And I should see element 'EcommToEmailBody' present on page
And I click by JS 'EcommToEmailBody'
And Blur 'EcommToEmailBody'
And I wait for '5' seconds
And I click by JS 'EcommToEmailBody'
And I enter 'Hi ' in field 'EcommToEmailBody'
And I wait for '3' seconds
And Blur 'EcommToEmailBody'
And I switch back to Main Window
And I wait for '2' seconds
And I should see element 'EcommSendEmailButton' present on page
And I click by JS 'EcommSendEmailButton'
And I wait for '5' seconds
And I scroll till element 'ShippingInfo'
And I wait for '5' seconds
And I should see element 'EcommHighlightedPannelEmail' present on page
And I wait for '5' seconds
And I reply email from gmail inbox
And I wait for '10' seconds
And I click by JS 'EcommTabHeaderDropdown'
And I wait for '2' seconds
And I should see element 'EcommRefreshTab' present on page
And I click by JS 'EcommRefreshTab'
And I wait for '5' seconds
And I should see element 'EcommNewEmail' present on page
And I should see element 'EcommNewEmailCheckBox' present on page
#And I should see element 'EcommCaseOwnerValue_EmailCheck' present on page
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds
And I should see element 'OmniButton' present on page
And I click by JS 'OmniButton'
And I wait for '3' seconds
And I should see element 'OmniStatusDD' present on page
And I click by JS 'OmniStatusDD'
And I wait for '2' seconds
And I should see element 'AvailableCaseBtn' present on page
And I click by JS 'AvailableCaseBtn'
And I wait for '5' seconds
And I should see element 'EcommRoutingPendingReplyStatus' present on page
And I should see element 'EcommNewEmailCheckBox' present on page
And I should see CaseOwner name 'ecomagent_displayname' present on page
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '3' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page


@S10_MF_StoreHoursNewFields
Feature: Store Hours - New Fields added in Page Layout for Account record type as Store
 
 1. Verify Store Hours fields added in Page Layout for Account record type as Store
 
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

Scenario: 1. Verify Store Hours fields added in Page Layout for Account record type as Store
Given I click 'All_Dropdown_Agent'
And I should see element 'AccountsOption' present on page
And I click by JS 'AccountsOption'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
#Account number example: UAT -- 126009, 126010, 508008, QA -- 037030
And I clear the text and enter '037030' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '10' seconds
And I should see element 'CaseNumber_Dropdown_Agent' present on page
And I should see text 'Store' present on page at 'AccountRecordTypeValue'
And I click 'CaseNumber_Dropdown_Agent'
And I wait for '5' seconds
And I scroll till element 'SundayStoreHourfield'
And I should see element 'SundayStoreHourfield' present on page
And I should see element 'SaturdayStoreHourfield' present on page
And I should see element 'MondayStoreHourfield' present on page
And I should see element 'TuesdayStoreHourfield' present on page
And I should see element 'WednesdayStoreHourfield' present on page
And I should see element 'ThursdayStoreHourfield' present on page
And I should see element 'FridayStoreHourfield' present on page
#And I scroll till element 'FridayStoreHourfield'
#And I wait for '5' seconds
And I should see text 'Store' present on page at 'PurchaseChannelField'
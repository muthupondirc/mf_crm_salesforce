@MF_Sprint7_DifferentScreen_ResolutionsCase_ForChat
Feature: Chat scenarios on different resolutions
Scenario: As An Agent i Want access Chat on Multiple Browser Resolutions
Given My WebApp 'MF_SalesForce' is open
And I navigate to application url 'MFPortalURL' 
And I wait for visibility of element 'MattressFirmLogo'
And I should see element 'MattressFirmLogo' present on page
And I wait for '5' seconds
#QAFOR

And I should see element 'ChatWithExpertBtn' present on page
And I click by JS 'ChatWithExpertBtn'
#ForUAT

#And I should see element 'ChatWithExpertBtnUAT' present on page
#And I click by JS 'ChatWithExpertBtnUAT'
And I wait for '10' seconds
And I should see element 'SFChatFirstNameUAT' present on page
And I click by JS 'SFChatFirstNameFieldUAT'
And I wait for '5' seconds
#And I wait for visibility of element 'WebClosePopup'
#And I click by JS 'WebClosePopup'
And I enter 'Hasan' in field 'SFChatFirstNameFieldUAT'
And I should see element 'SFChatLastNameUAT' present on page
And I should see element 'SFChatLastNameFieldUAT' present on page
And I click by JS 'SFChatLastNameFieldUAT'
And I enter 'Khan' in field 'SFChatLastNameFieldUAT'
And I should see element 'SFChatEmaikAddressUAT' present on page
And I should see element 'SFChatEmaikAddressFieldUAT' present on page
And I click by JS 'SFChatEmaikAddressFieldUAT'
And I enter 'hasantestuser@gmail.com' in field 'SFChatEmaikAddressFieldUAT'
And I should see element 'SFChatLiveShoppingBtnUAT' present on page
And I click by JS 'SFChatLiveShoppingBtnUAT'
And I wait for '2' seconds
And I should see element 'SFChatStartChatBtnUAT' present on page
And I click by JS 'SFChatStartChatBtnUAT'
And I wait for '20' seconds
And I should see element 'ChatTextbox' present on page
And I click by JS 'ChatTextbox'
And I wait for '3' seconds


@MF_Web_Login_Cases
Feature: As a Customer i want to create an account and create login issue from Mattress Firm web site.
1. SP1_TC70 Create login issue case from MFRM website
2. Login to Salesforce portal with agent account
3.SP1_TC71 Validate case in salesforce portal which was created through website.
4.Logout Scenario.
5.SP1_TC68 As a customer i Reset my password for Mattress Firm website
6.SP1_TC69 As a Customer Create an Account through MFRM WebSite

Scenario: SP1_TC70  Creating a Login Issue Case from Mattress Firm Web Site
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
#UAT URL
And I navigate to application url 'MFPortalURL'
#QA URL
#And I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/Home-Show' application
And I wait for visibility of element 'MyAccount'
And I should see element 'MyAccount' present on page
And I click by JS 'MyAccount'
And I wait for '5' seconds
And I should see element 'Logging_Issue' present on page
And I click by JS 'Logging_Issue'
And I wait for '5' seconds
And I should see element 'IssueLabel' present on page
And I click by JS 'Issue_DD'
And I select option 'Unable to reset portal account password' in dropdown 'Issue_DD' by 'text'
#And I click by JS 'Issue_DD'
#And I click by JS 'Issue_DD_Value'
And I click by JS 'FirstNameField'
And I enter 'Muhammad' in field 'FirstNameField'
And I click by JS 'LastNameField'
And I enter 'Hasan' in field 'LastNameField'
And I click by JS 'PhoneField'
And I enter '6122235566' in field 'PhoneField'
And I click by JS 'EmailField'
And I enter 'hasantestuser@gmail.com' in field 'EmailField'
And I click by JS 'DescriptionField'
And I enter 'Test Account' in field 'DescriptionField'
And I click by JS 'SubmitWebBtn'
And I wait for '10' seconds

Scenario: SP1_TC72 Validate login issue case in Salesforce portal
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I wait for '3' seconds
And I click by JS 'PortalCaseDD'
And I wait for '2' seconds
And I click by JS 'PotalCaseDDValue'
And I wait for '5' seconds
And I click by JS 'CasesSelection'
And I wait for '5' seconds
And I click by JS 'CasesSelectionTxt'
And I enter 'Portal' in field 'CasesSelectionTxt'
And I wait for '2' seconds
And I hit down Arrow key on element 'CasesSelectionTxt'
And I wait for '2' seconds
And I hit enter-key on element 'CasesSelectionTxt'
And I wait for '10' seconds
And I should see element 'PortalIssues' present on page
#And I scroll to 'element' - 'Count'
And I wait for '5' seconds
And I get latest warranty case
#And I click by JS 'lastCaseNumber'
And I wait for '5' seconds
And I should see element 'CaseHead' present on page
And I should see element 'WebStatus' present on page
And I should see element 'WebStatusValue' present on page
And I should see element 'SubjectValue' present on page
And I wait for '3' seconds
And I scroll till element 'WebDescriptionInfo'
And I wait for '3' seconds
And I should see element 'WebDescription' present on page
And I scroll till element 'WebImpactInfo'
And I wait for '3' seconds
And I should see element 'WebCaseOrigin' present on page
And I should see element 'WebStatus' present on page
And I should see element 'WebEmail' present on page
And I should see element 'WebContactName' present on page
And I should see element 'WebFirstName' present on page
And I should see element 'WebLastName' present on page
And I should see element 'WebPhone' present on page
And I should see element 'WebCreatedByValue' present on page
And I click by JS 'Close'
And I wait for '5' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

@welcome
Scenario: SP1_TC68 As a Customer Create an Account through MFRM WebSite
#ForQA

#Given I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/Home-Show' application
#UATURL
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
And I navigate to application url 'MFPortalURL'

And I wait for visibility of element 'MyAccount'
And I should see element 'MyAccount' present on page
And I click by JS 'MyAccount'
And I wait for '10' seconds
And I click by JS 'CreateAnAccount'
And I wait for '5' seconds
And I should see element 'CreateAccountHeading' present on page
And I click by JS 'CreateFirstName'
And I enter 'Micky' in field 'CreateFirstName'
And I click by JS 'CreateLastName'
And I enter 'Roy' in field 'CreateLastName'
And I click by JS 'CreateAccountEmail'
And I generate random email id and enter in field 'CreateAccountEmail'
#And I enter 'hasantestuser+999@gmail.com' in field 'CreateAccountEmail'
And I click by JS 'CreatePassword'
And I enter 'Hasan123!@' in field 'CreatePassword'
And I wait for '3' seconds
And I click by JS 'ConfirmPassword'
And I enter 'Hasan123!@' in field 'ConfirmPassword'
And I wait for '3' seconds
And I hit tab-key on element 'ConfirmPassword'
And I wait for '3' seconds
And I click by JS 'CreateAccountButton'
And I wait for '25' seconds
And I wait for visibility of element 'IdentificationHeader'
#And I should see element 'IdentificationHeader' present on page
And I extract the Identification Code from gmail inbox 'OTP'
And I wait for '10' seconds
#And I hit tab-key on element 'IdentityVerificationCode'
And I wait for '5' seconds
And I click by JS 'CompleteSignUpButton'
And I wait for '10' seconds
And I verify welcome mail from gmail inbox 'Thanks for creating an account!'
And I wait for '5' seconds
And I click by JS 'btn_Allow'
And I mouse over 'AccountIcon'
And I wait for '2' seconds
And I click by JS 'Signout'
And I wait for '5' seconds


@resetpassword
Scenario: SP1_TC68 As a customer i Reset my password for Mattress Firm website 
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
#Given I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/Home-Show' application
#UATURL
And I navigate to application url 'MFPortalURL'

And I wait for visibility of element 'MyAccount'
And I should see element 'MyAccount' present on page
And I click by JS 'MyAccount'
And I wait for '5' seconds
And I should see element 'ForgotPassword' present on page
And I click by JS 'ForgotPassword'
And I wait for '5' seconds
And I should see element 'PasswordReset' present on page
And I click by JS 'PasswordResetEmail'
#And I enter 'hasantestuser@gmail.com' in field 'PasswordResetEmail'
And I enter from system variable 'randomemail' into feild 'PasswordResetEmail'
And I wait for '2' seconds
And I hit tab-key on element 'PasswordResetEmail'
And I wait for '2' seconds
And I should see element 'ResetContinueButton' present on page
And I click by JS 'ResetContinueButton'
And I wait for '5' seconds
And I should see element 'CheckEmailHeading' present on page
And I wait for '10' seconds
And I extract password reset link from gmail inbox & store to 'EmailLink'
And I navigate to '$$EmailLink' application
And I wait for '10' seconds
And I should see element 'ChangePasswordHeading' present on page
And I click by JS 'ResetNewPassword'
And I enter 'Hasan123!@#' in field 'ResetNewPassword'
And I click by JS 'ConfirmResetPassword'
And I enter 'Hasan123!@#' in field 'ConfirmResetPassword'
And I wait for '2' seconds
And I should see element 'ChangePasswordBtn' present on page
And I click by JS 'ChangePasswordBtn'
And I wait for '5' seconds
And I should see element 'HomeScreen' present on page
And I wait for '5' seconds
And I mouse over 'AccountIcon'
And I wait for '2' seconds
And I click by JS 'Signout'
And I wait for '10' seconds
And I click by JS 'SignInBack'
And I wait for '10' seconds
And I click by JS 'LoginEmail'
And I enter from stored variable '$$randomemail' into feild 'LoginEmail'
#And I enter 'hasantestuser@gmail.com' in field 'LoginEmail'
And I click by JS 'LoginPassword'
And I enter 'Hasan123!@#' in field 'LoginPassword'
And I hit tab-key on element 'LoginPassword'
And I click by JS 'MFRMLoginButton'
And I wait for '10' seconds
And I should see element 'HomeScreen' present on page
And I wait for '5' seconds
And I mouse over 'AccountIcon'
And I wait for '2' seconds
And I click by JS 'Signout'
And I wait for '5' seconds


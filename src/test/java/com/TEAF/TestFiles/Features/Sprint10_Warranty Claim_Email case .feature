@MF_Sprint10_Warranty_Claim_Email_case
Feature: Sprint10_SFLC - 16835_Warranty_Claim_Email_case
1.Login to MF Salesforce as Service Agent
2.SP10_TC1 Verify "New Chatter" checkbox is checked for Warranty claim case once user shared the post
3.SP10_TC2 Verify "New Email" checkbox is checked for Warranty claim case once user replies to external email
4.Verify logout of Sales Force application


Background: Login to MF Salesforce as Service 
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: Verify "New Chatter" checkbox is checked for Warranty claim case once user shared the post
Given I Switch the Tab and enter the url 'MFWarrantyExclusions'
And I wait for '10' seconds
And I click by JS 'btnContinue'
And I should see element 'EmailField_CaseMf' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_CaseMf'
And I should see element 'PasswordField_CaseMf' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_CaseMf'
And I wait for '5' seconds
And I should see element 'LoginButton_CaseMf' present on page
And I click by JS 'LoginButton_CaseMf'
And I wait for '5' seconds
And I should see element 'Step1Select_HeaderMf' present on page
And I scroll till element 'Step1Select_NextButtonMf'
And I should see element 'Step1Select_NextButtonMf' present on page
And I click by JS 'Step1Select_NextButtonMf'
And I wait for '5' seconds
And I click by JS 'ContinuePopup'
And I should see element 'Step2TellUs_HeaderMf' present on page
And I should see element 'PartA_HeaderMf' present on page
And I should see element 'Brand_Dropdown_CaseMf' present on page
And I select option 'Sealy' in dropdown 'Brand_Dropdown_CaseMf' by 'value'
And I should see element 'ProductType_Dropdown_CaseMf' present on page
And I select option 'Mattress and Box Spring' in dropdown 'ProductType_Dropdown_CaseMf' by 'value'
And I should see element 'ProductSize_Dropdown_CaseMf' present on page
And I select option 'Queen' in dropdown 'ProductSize_Dropdown_CaseMf' by 'value'
And I should see element 'DefectReason_Dropdown_CaseMf' present on page
And I select option 'My mattress is sagging' in dropdown 'DefectReason_Dropdown_CaseMf' by 'value'
And I should see element 'FoundationType_Dropdown_CaseMf' present on page
And I select option 'Metal' in dropdown 'FoundationType_Dropdown_CaseMf' by 'value'
And I should see element 'legs_Dropdown_CaseMf' present on page
And I select option '5' in dropdown 'legs_Dropdown_CaseMf' by 'value'
And I should see element 'slats_Dropdown_CaseMf' present on page
And I select option '1' in dropdown 'slats_Dropdown_CaseMf' by 'value'
And I should see element 'Myproduct_Checkbox_CaseMf' present on page
And I click by JS 'Myproduct_Checkbox_CaseMf'
And I should see element 'Customerinformation_TextareaMf' present on page
And I clear the text and enter 'Mattress is sagging' in field 'Customerinformation_TextareaMf'
And I wait for '5' seconds
And I should see element 'uploadreceiptFilesMf' present on page
And I upload reciept files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'BestPhoneNumber_FieldMf' present on page
And I clear the text and enter '516655784' in field 'BestPhoneNumber_FieldMf'
And I should see element 'Any_CheckboxMf' present on page
And I click by JS 'Any_CheckboxMf'
And I should see element 'Street_FieldMf' present on page
And I clear the text and enter '28 WoodBine DRS' in field 'Street_FieldMf'
And I should see element 'City_FieldMf' present on page
And I clear the text and enter 'Hicksville' in field 'City_FieldMf'
And I should see element 'State_FieldMf' present on page
And I clear the text and enter 'New York' in field 'State_FieldMf'
And I should see element 'Zip_FieldMf' present on page
And I clear the text and enter '11801' in field 'Zip_FieldMf'
And I should see element 'Next_Button_PartBMf' present on page
And I click 'Next_Button_PartBMf'
And I wait for '5' seconds
And I should see element 'WarrantyeClaim_HeaderMf' present on page
And I should see element 'WarrantyeClaim_ContinueButtonMf' present on page
And I click by JS 'WarrantyeClaim_ContinueButtonMf'
And I should see element 'Step3_HeaderMf' present on page
And I should see element 'MainAffectedArea_1_HeaderMf' present on page
And I upload Mainarea files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'CloseUp_2_UploadFileMf' present on page
And I upload CloseUp Of Main Affected Area files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'TopOfTheMattress_7_UploadFileMf' present on page
And I upload Photo Of Law Tag Attached files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'PhotooftheSupport_11_UploadFileMf' present on page
And I upload Frame Support files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds 
And I click by JS 'SubmitButton_Step3Mf'
And I wait for '5' seconds
And I should see element 'TrackStatus_ButtonMf' present on page
And I click by JS 'TrackStatus_ButtonMf'
And I should see element 'txtCaseNumber' present on page
And I get text from 'txtCaseNumber' and store
And I Switch between Tab '0'
And I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I should see element 'HomeMenu' present on page
And I click by JS 'HomeMenu'
And I wait for '5' seconds
And I click by JS 'SearchTypeDD'
And I wait for '3' seconds
And I click by JS 'SearchType_Cases'
And I wait for '3' seconds
And I hit tab-key on element 'SearchTypeDD'
And I enter from stored variable '$$txtCaseNumber' into feild 'GlobalSearchField'
#And I clear the text and enter '03109106' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '2' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see CaseOwner label 'caseowner_customercommunity' present on page
And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_Escalate'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I scroll till element 'EscalateOwner'
And I wait for '3' seconds
And I should see element 'EscalateToLeadCheckBox' present on page
And I should see CaseOwner label 'caseowner_serviceleadservices' present on page
And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_Closed'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see CaseOwner label 'caseowner_serviceleadservices' present on page
And I should see element 'ShowOptionButtonToAccept' present on page
And I click by JS 'ShowOptionButtonToAccept'
And I wait for '5' seconds
And I click by JS 'AcceptButton'
And I wait for '5' seconds
And I should see CaseOwner name 'serviceagent_displayname' present on page
And I Switch between Tab '1'
And I click by JS latest warranty case
And I wait for '5' seconds
And I Switch between Tab '2'
And I wait for '5' seconds
And I should see element 'ShareUpdate' present on page
And I click by JS 'ShareUpdate'
And I enter 'New Post' in field 'ShareTextField'
And I should see element 'Share' present on page
And I click by JS 'Share'
And I wait for '5' seconds
And I Switch between Tab '0'
And I click by JS 'RefreshDropdown'
And I should see element 'RefreshTab' present on page
And I click by JS 'RefreshTab'
And I should see CaseOwner name 'serviceagent_displayname' present on page
And I should see element 'EcommRoutingPendingReplyStatus' present on page
And I should see element 'NewChatter' present on page

Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page


Scenario: Verify "New Email" checkbox is checked for Warranty claim case once user replies to external email
Given I Switch the Tab and enter the url 'MFWarrantyExclusions'
And I wait for '10' seconds
And I click by JS 'btnContinue'
And I should see element 'EmailField_CaseMf' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_CaseMf'
And I should see element 'PasswordField_CaseMf' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_CaseMf'
And I wait for '5' seconds
And I should see element 'LoginButton_CaseMf' present on page
And I click by JS 'LoginButton_CaseMf'
And I wait for '5' seconds
And I should see element 'Step1Select_HeaderMf' present on page
And I scroll till element 'Step1Select_NextButtonMf'
And I should see element 'Step1Select_NextButtonMf' present on page
And I click by JS 'Step1Select_NextButtonMf'
And I wait for '5' seconds
And I click by JS 'ContinuePopup'
And I should see element 'Step2TellUs_HeaderMf' present on page
And I should see element 'PartA_HeaderMf' present on page
And I should see element 'Brand_Dropdown_CaseMf' present on page
And I select option 'Sealy' in dropdown 'Brand_Dropdown_CaseMf' by 'value'
And I should see element 'ProductType_Dropdown_CaseMf' present on page
And I select option 'Mattress and Box Spring' in dropdown 'ProductType_Dropdown_CaseMf' by 'value'
And I should see element 'ProductSize_Dropdown_CaseMf' present on page
And I select option 'Queen' in dropdown 'ProductSize_Dropdown_CaseMf' by 'value'
And I should see element 'DefectReason_Dropdown_CaseMf' present on page
And I select option 'My mattress is sagging' in dropdown 'DefectReason_Dropdown_CaseMf' by 'value'
And I should see element 'FoundationType_Dropdown_CaseMf' present on page
And I select option 'Metal' in dropdown 'FoundationType_Dropdown_CaseMf' by 'value'
And I should see element 'legs_Dropdown_CaseMf' present on page
And I select option '5' in dropdown 'legs_Dropdown_CaseMf' by 'value'
And I should see element 'slats_Dropdown_CaseMf' present on page
And I select option '1' in dropdown 'slats_Dropdown_CaseMf' by 'value'
And I should see element 'Myproduct_Checkbox_CaseMf' present on page
And I click by JS 'Myproduct_Checkbox_CaseMf'
And I should see element 'Customerinformation_TextareaMf' present on page
And I clear the text and enter 'Mattress is sagging' in field 'Customerinformation_TextareaMf'
And I wait for '5' seconds
And I should see element 'uploadreceiptFilesMf' present on page
And I upload reciept files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'BestPhoneNumber_FieldMf' present on page
And I clear the text and enter '516655784' in field 'BestPhoneNumber_FieldMf'
And I should see element 'Any_CheckboxMf' present on page
And I click by JS 'Any_CheckboxMf'
And I should see element 'Street_FieldMf' present on page
And I clear the text and enter '28 WoodBine DRS' in field 'Street_FieldMf'
And I should see element 'City_FieldMf' present on page
And I clear the text and enter 'Hicksville' in field 'City_FieldMf'
And I should see element 'State_FieldMf' present on page
And I clear the text and enter 'New York' in field 'State_FieldMf'
And I should see element 'Zip_FieldMf' present on page
And I clear the text and enter '11801' in field 'Zip_FieldMf'
And I should see element 'Next_Button_PartBMf' present on page
And I click 'Next_Button_PartBMf'
And I wait for '5' seconds
And I should see element 'WarrantyeClaim_HeaderMf' present on page
And I should see element 'WarrantyeClaim_ContinueButtonMf' present on page
And I click by JS 'WarrantyeClaim_ContinueButtonMf'
And I should see element 'Step3_HeaderMf' present on page
And I should see element 'MainAffectedArea_1_HeaderMf' present on page
And I upload Mainarea files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'CloseUp_2_UploadFileMf' present on page
And I upload CloseUp Of Main Affected Area files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'TopOfTheMattress_7_UploadFileMf' present on page
And I upload Photo Of Law Tag Attached files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'PhotooftheSupport_11_UploadFileMf' present on page
And I upload Frame Support files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds 
And I click by JS 'SubmitButton_Step3Mf'
And I wait for '5' seconds
And I should see element 'TrackStatus_ButtonMf' present on page
And I click by JS 'TrackStatus_ButtonMf'
And I should see element 'txtCaseNumber' present on page
And I get text from 'txtCaseNumber' and store
And I Switch between Tab '0'
Given I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I should see element 'HomeMenu' present on page
And I click by JS 'HomeMenu'
And I wait for '5' seconds
And I click by JS 'SearchTypeDD'
And I wait for '3' seconds
And I click by JS 'SearchType_Cases'
And I wait for '3' seconds
And I hit tab-key on element 'SearchTypeDD'
And I enter from stored variable '$$txtCaseNumber' into feild 'GlobalSearchField'
#And I clear the text and enter '03109106' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '2' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see CaseOwner label 'caseowner_customercommunity' present on page
And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_Escalate'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I scroll till element 'EscalateOwner'
And I wait for '3' seconds
And I should see element 'EscalateToLeadCheckBox' present on page
And I should see CaseOwner label 'caseowner_serviceleadservices' present on page
And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_Closed'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see CaseOwner label 'caseowner_serviceleadservices' present on page
And I should see element 'ShowOptionButtonToAccept' present on page
And I click by JS 'ShowOptionButtonToAccept'
And I wait for '5' seconds
And I click by JS 'AcceptButton'
And I wait for '5' seconds
And I should see CaseOwner name 'serviceagent_displayname' present on page

And I click by JS 'EcommSendExternalEmail'
And I wait for '3' seconds
And I should see element 'EcommToEmailField' present on page
And I click by JS 'EcommToEmailField'
And I enter 'hasantestuser@gmail.com' in field 'EcommToEmailField'
And I wait for '5' seconds
#And I hit down Arrow key on element 'EcommToEmailField'
And I wait for '3' seconds
And I hit enter-key on element 'EcommToEmailField'
And I wait for '5' seconds
And I switch to iFrame 'EcommIframeemailBody'
And I wait for '5' seconds
And I should see element 'EcommToEmailBody' present on page
And I click by JS 'EcommToEmailBody'
And Blur 'EcommToEmailBody'
And I wait for '5' seconds
And I click by JS 'EcommToEmailBody'
And I enter 'Hi ' in field 'EcommToEmailBody'
And I wait for '3' seconds
And Blur 'EcommToEmailBody'
And I switch back to Main Window
And I wait for '2' seconds
And I should see element 'EcommSendEmailButton' present on page
And I click by JS 'EcommSendEmailButton'
And I wait for '5' seconds
And I reply email from gmail inbox
And I wait for '10' seconds
And I click by JS 'RefreshDropdown'
And I should see element 'RefreshTab' present on page
And I click by JS 'RefreshTab'
And I should see element 'EcommNewEmailCheckBox' present on page
And I should see text 'Pending Reply' present on page at 'StatusValue'

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page







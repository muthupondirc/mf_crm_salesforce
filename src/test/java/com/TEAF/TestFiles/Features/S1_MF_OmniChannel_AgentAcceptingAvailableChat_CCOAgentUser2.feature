@MF_OmniChannel_AgentAcceptingAvailableChat_Agent2
Feature: Omni Channel - Agent Accepting an Available Chat with Agent2 Profile
 
 1. Login to Mattress Firm Sales Force Application
 1.1 Omni Channel - Agent Accepting an Available Chat - SP1_TC56
    Note: Agent profile should be Nateka Joseph
 2 Omni Channel - Transfer Chat to Another Agent(Nateka) - SP1_TC57
   Note: Agent Nateka's flow for Agent Alanna (TC-92 )
 3. Omni Channel - Transfer Chat to a Chat Button - SP1_TC58
 4. Omni Channel - Send Quick Text - SP1_TC59
 5. Omni Channel - End a Chat - SP1_TC60
 6. Omni Channel - Select Service Chat Disposition - SP1_TC61
 
Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent1' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent1_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 Omni Channel - Agent Accepting an Available Chat - SP1_TC56
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
And I wait for '100' seconds
And I refresh the WebPage
And I wait for '15' seconds
#And I should see element 'JoeNewTabOmni' present on page 
#And I wait for '5' seconds
And I should see element 'DetailsTab_Chat' present on page
And I click by JS 'DetailsTab_Chat'
And I scroll till element 'VisitorsDetails_Tab'
And I wait for '10' seconds
And I should see element 'VisitorsFirstName_Field_Omni' present on page
And I should see text 'Joe' contained on page at 'VisitorsFirstName_Field_Omni'
And I should see text 'Smith' contained on page at 'VisitorsLastName_Field_Omni'
And I should see text 'Joesmith@test.com' contained on page at 'VisitorsEmail_Field_Omni'
And I should see text '5168618800' contained on page at 'VisitorsHomePhone_Field_Omni'
And I scroll to top of the page
And I wait for '5' seconds
And I should see element 'Conversation_Tab' present on page
And I click by JS 'Conversation_Tab'
And I wait for '5' seconds

Scenario:  2 Omni Channel - Transfer Chat to Another Agent(Nateka Ashley) - SP1_TC57
And I wait for '5' seconds
And I scroll till element 'Transfer_Icon'
And I wait for '10' seconds
And I should see element 'Transfer_Icon' present on page 
And I click by JS 'Transfer_Icon'
And I wait for '10' seconds
And I should see element 'TransferChat_Header' present on page 
And I should see element 'SelectanItem_Field_Omni' present on page 
And I click 'SelectanItem_Field_Omni'
And I should see element 'Agent1Option_Omni' present on page 
And I click 'Agent1Option_Omni'
And I should see element 'Textarea_Field_Transfer' present on page 
And I clear the text and enter 'Hello' in field 'Textarea_Field_Transfer'
And I should see element 'Transfer_Button_Omni' present on page 
And I click 'Transfer_Button_Omni'
#And I should see element 'JoeNewTabOmni' present on page 
#And I should see empty value in the Input feild 'JoeNewTabOmni'
And I wait for '100' seconds


Scenario: 3. Omni Channel - Transfer Chat to a Chat Button - SP1_TC58
And I wait for '25' seconds
And I should see element 'JoeTab' present on page
And I click 'JoeTab'
And I wait for '5' seconds
#And I should see element 'AshleyAccepted_Section' present on page 
#And I wait for '5' seconds
#And I should see element 'KimSent_Section' present on page
#And I wait for '5' seconds
And I should see element 'HelloNateka_Section' present on page
And I should see element - 'HelloNateka_Section' contains text 'Hello'
And I wait for '5' seconds
And I scroll till element 'Textarea_Kim'
And I wait for '5' seconds
And I should see element 'Textarea_Kim' present on page
And I wait for '5' seconds
And I clear the text and enter 'hello' in field 'Textarea_Kim'
And I hit enter-key on element 'Textarea_Kim'
And I wait for '50' seconds

Scenario: 4. Omni Channel - Send Quick Text - SP1_TC59
Given I should see element 'QuickText_Icon' present on page
And I click by JS 'QuickText_Icon'
#And I should see element 'SearchField_Quick' present on page
And I should see element 'SearchQuickText_Field' present on page
And I clear the text and enter 'Exchange' in field 'SearchQuickText_Field'
And I wait for '5' seconds
And I click by JS 'FirstOption_Quick'
#And I should see element 'SearchQuickText_Field' present on page
#And I scroll till element 'ExchangeFirstOption'
#And I wait for '3' seconds
#And I should see element 'ExchangeFirstOption' present on page
#And I mouse over 'ExchangeFirstOption'
#And I should see element 'QuickText_Chat' present on page
#And I get text from 'QuickText_Chat' and store
#And I click by JS 'ExchangeFirstOption'
#And I should see text '$$QuickText_Chat' contained on page at 'Textarea_Kim'
And I click by JS 'Textarea_Kim'
And I hit enter-key on element 'Textarea_Kim'
And I wait for '80' seconds
#waiting for Chatbot flow to complete

Scenario: 5. Omni Channel - End a Chat - SP1_TC60
And I should see element 'EndChat_Button' present on page
And I click by JS 'EndChat_Button'
And I should see element 'Areyousure_popup' present on page
And I click by JS 'EndChat_Popup'
And I wait for '10' seconds
And I should see element 'ChatDisposition_Dropdown' present on page
And I click by JS 'ChatDisposition_Dropdown'
And I wait for '5' seconds
And I select option 'Cancel My Entire Order' in dropdown 'ChatDisposition_Dropdown' by 'value'
And I wait for '5' seconds
And I click by JS 'UpdateButton'
And I wait for '10' seconds
And I should see element 'ChatEndedByAgent_AgentChat' present on page
#waiting for Chatbot flow to complete

#Scenario: 6. Omni Channel - Select Service Chat Disposition - SP1_TC61
#And I scroll to top of the page
#And I wait for '5' seconds
#And I should see element 'DetailsTab_Chat' present on page
#And I click by JS 'DetailsTab_Chat'
#And I should see element 'Disposition&QualifyingQuestions_Dropdown' present on page
#And I should see element 'ServiceChatDispotion_Section' present on page
##And I should see text 'Please select a disposition' present on page at 'ServiceChatDispotion_Option'
#And I wait for '5' seconds
#And I should see element 'ServiceChatDisposition_Dropdown' present on page
#And I select option 'DispositionValues.Status of My Delivery' in dropdown 'ServiceChatDisposition_Dropdown' by 'value'
#And I should see element 'NextButton_Chat' present on page
#And I click by JS 'NextButton_Chat'
#And I wait for '5' seconds
#And I should see text 'Status of My Delivery' present on page at 'ServiceChatDispotion_Option'
#

@S1_MF_ServiceCase_Lead
Feature: Creating a new Service Case from List View with Lead Profile
 
 1. Login to Mattress Firm Sales Force Application
 1.1 Creating a new Service Case from List View (Lead and above) - SP1_TC15
 
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Lead User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Lead User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 Creating a new Service Case from List View (Lead and above) - SP1_TC15
Given I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I should see element 'New_Button_Agent' present on page
And I click 'New_Button_Agent'
#next 2 steps in uat crm transportation
And I click by JS 'ServiceDropdown_Agent'
And I click by JS 'NextButtonAgent'
Then I should see element 'NewCaseService_Text_Lead' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click 'Phone_Text_Agent'
And I click 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click 'SubRecordType_Dropdown_Agent'
And I click by JS '120DayPrice_Option'
And I click 'TypeOfProduct(s)_Dropdown'
And I click by JS 'CoreProduct_Option'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I click by JS 'Close_Agent' 
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseNumber_Agent' contained on page at 'CaseNumber_Dropdown_Agent'
And I should see text 'New' contained on page at 'Status_Agent'
And I should see text 'Service' contained on page at 'CaseRecordType_Agent'
And I should see text '120 Day Price Match Guarantee Request' contained on page at 'SubRecordType_Agent'
#And I should see text 'CCO Lead User 2' contained on page at 'OwnerLautrice Name_Agent'
And I click by JS 'Close_Agent' 


Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page


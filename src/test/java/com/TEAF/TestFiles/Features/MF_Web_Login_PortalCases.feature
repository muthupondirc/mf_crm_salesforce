
Feature: Portal Cases
1.Verify user is able to track the order by ordernumber
2.Verify user is able to update the account details
3.Verify user is able to update Store Info From My accounts
5.SP1_TC68 As a customer i Reset my password for Mattress Firm website
6.SP1_TC69 As a Customer Create an Account through MFRM WebSite

Scenario: Verify user is able to track the order by ordernumber
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
Given I Switch the Tab and enter the url 'MFPortalURL'
And I wait for '10' seconds
And I wait for visibility of element 'MyAccount'
And I should see element 'MyAccount' present on page
And I click by JS 'MyAccount'
And I wait for '5' seconds
And I should see element 'EmailField_CaseMf' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_CaseMf'
And I should see element 'PasswordField_CaseMf' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_CaseMf'
And I wait for '5' seconds
And I should see element 'LoginButton_CaseMf' present on page
And I click by JS 'LoginButton_CaseMf'
And I wait for '5' seconds
And I click by JS 'lnkLoginUser'
And I click by JS 'btnViewMyOrders'
And I should see element 'lblOrderNumber' present on page
And I should see element 'lblName' present on page
And I should see element 'lblTotal' present on page
And I should see element 'lblOrderDate' present on page
And I should see element 'lblOrderStatus' present on page
And I should see element 'btnTrackOrder' present on page
And I should see element 'valueOrderNumber' present on page
And I get text from 'valueOrderNumber' and store
And I navigate to 'http://dev.mattressfirm.com/track' application
#And I click by JS 'btnTrackOrder'
And I should see element 'headerTrackMyDelivery' present on page
And I enter '$$valueOrderNumber' in field 'inputOrderNumber'
And I should see element 'inputContact' present on page
And I enter 'Steiner' in field 'inputContact'
And I click by JS 'btnTrackNow'
And I should see element 'headerOrderNotFound' present on page
And I should see element 'txtNoOrdersMatch' present on page
And I should see element 'txtReenterNumber' present on page
And I should see element 'btnBackToSearchPage' present on page

@MF_Web_Login_PortalCases
Scenario: Verify user is able to view the order
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
Given I Switch the Tab and enter the url 'MFPortalURL'
And I wait for '10' seconds
And I wait for visibility of element 'MyAccount'
And I should see element 'MyAccount' present on page
And I click by JS 'MyAccount'
And I wait for '5' seconds
And I should see element 'EmailField_CaseMf' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_CaseMf'
And I should see element 'PasswordField_CaseMf' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_CaseMf'
And I wait for '5' seconds
And I should see element 'LoginButton_CaseMf' present on page
And I click by JS 'LoginButton_CaseMf'
And I wait for '5' seconds
And I click by JS 'lnkLoginUser'
And I click by JS 'btnViewMyOrders'
And I click by JS 'btnViewOrder'
And I wait for '3' seconds
And I should see the url 'OrderDetail_URL'

Scenario: Verify user is able to update the account details

Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
Given I Switch the Tab and enter the url 'MFPortalURL'
And I wait for '10' seconds
And I wait for visibility of element 'MyAccount'
And I should see element 'MyAccount' present on page
And I click by JS 'MyAccount'
And I wait for '5' seconds
And I should see element 'EmailField_CaseMf' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_CaseMf'
And I should see element 'PasswordField_CaseMf' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_CaseMf'
And I wait for '5' seconds
And I should see element 'LoginButton_CaseMf' present on page
And I click by JS 'LoginButton_CaseMf'
And I wait for '5' seconds
And I click by JS 'lnkLoginUser'
And I click by JS 'btnAccount'
And I clear field 'FirstNameField'
And I enter '$XSfname' in field 'FirstNameField'
And I get text from 'FirstNameField' and store
And I enter '$XSlname' in field 'LastNameField'
And I get text from 'LastNameField' and store
And I enter '$XN9' in field 'input_Phone'
And I get text from 'input_Phone' and store
And I click by JS 'btn_Submit'
And I click by JS 'btnAccount'
And I should see text '$$FirstNameField' contained on page at 'FirstNameField'
And I should see text '$$LastNameField' contained on page at 'LastNameField'
And I should see text '$$input_Phone' contained on page at 'input_Phone'


Scenario: Verify user is able to update Store Info From My accounts

Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
Given I Switch the Tab and enter the url 'MFPortalURL'
And I wait for '10' seconds
And I wait for visibility of element 'MyAccount'
And I should see element 'MyAccount' present on page
And I click by JS 'MyAccount'
And I wait for '5' seconds
And I should see element 'EmailField_CaseMf' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_CaseMf'
And I should see element 'PasswordField_CaseMf' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_CaseMf'
And I wait for '5' seconds
And I should see element 'LoginButton_CaseMf' present on page
And I click by JS 'LoginButton_CaseMf'
And I wait for '5' seconds
And I click by JS 'lnkLoginUser'
And I click by JS 'btnAskUs'
And I should see text 'NEED HELP WITH AN ORDER?' contained on page at 'headerNeedHelp'
#And I should see text 'No sweat. Just visit your nearest store (there are, like, so many)' contained on page at 'txt_ServiceText'
And I should see element 'txt_Exchanges' present on page
And I should see element 'txt_Refunds' present on page
And I should see element 'txt_Changes' present on page
And I should see element 'lbl_StorePhone' present on page
And I should see element 'lbl_StoreAddress' present on page
And I should see element 'lbl_StoreHours' present on page
And I should see element 'lnk_ChangeMyPreferredStore' present on page
And I click by JS 'lnk_ChangeMyPreferredStore'
And I should see element 'headerFindAStore' present on page
And I clear the text and enter 'ecommerce' in field 'inputFindAStore'
And I should see element 'lnkStore' present on page
And I get text from 'lnkStore' and store
And I click by JS 'lnkStore'
And I get text from 'lnkStore' and store
And I should see text '$$lnkStore' contained on page at 'headerNeedHelp'
And I should see element 'storeAddress'

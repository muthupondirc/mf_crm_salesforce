@MF_SalesForce_POCScript_DT
Feature: Login to Mattress Firm Sales Force Application & Add new Ticket to an existing contact

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
When I login to Mattress Firm Sales Force with valid 'Portal_UAT' User Credentials
And I close all the opened tabs 'iconClose'
And I click 'UserImageButton'
And I should see text 'Swathin Ratheendren' present on page at 'ProfileCardPopUp'
And I click 'UserImageButton'

Scenario Outline: Search for Contact in the Global Search & Validate contact's personal information in Highlights section
Given I click 'SearchTypeDD'
And I click 'SearchType_Contact'
And I hit tab-key on element 'SearchTypeDD'
And I enter '<ContactName>' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'ContactHeader' present on page
And I should see text '<ContactName>' contained on page at '<ContactHeaderElem>'
And I wait for '3' seconds
Then I should see element 'HighlightPanel' present on page
And I should see text 'Name: <ContactName>' contained on page at 'ContactFullName'
And I should see text 'Billing Name: <ContactName>' contained on page at 'ContactBillingName'
And I verify if Contact is Portal User '<isPortalUser>'
And I should see element 'ContactCustCode' present on page
And I should see text '<ContactPhone>' contained on page at 'ContactHomePhone'
And I should see element 'ContactBillingAddr' present on page
And I should see text '<EmailAddress>' contained on page at 'ContactBillingEmail'
And I close '<ContactName>' contact tab

Examples:
|ContactName  |ContactPhone        |ContactHeaderElem |EmailAddress							    |isPortalUser|
|Mike Moran   |Home: 6318829050    |ContactNameHeader1|Billing Email: michael.moran08@gmail.com |Yes		 |
|Yash Ganorkar|Home: (312) 383-8806|ContactNameHeader2|Billing Email: seangeorge1234@gmail.com  |No          |

@Ignore
Scenario: Try to Create a new case for a given Contact and verify mandatory field error message
Given I click 'SearchTypeDD'
And I click 'SearchType_Contact'
And I hit tab-key on element 'SearchTypeDD'
And I enter 'Mike Moran' in field 'GlobalSearchField'
And I wait for '4' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I hit enter-key on element 'GlobalSearchField'
And I wait for '3' seconds
And I should see text 'Mike Moran' contained on page at 'ContactNameHeader1'
Then I click by JS 'NewCaseBtn'
And I wait for '2' seconds
And I should see element 'NewCaseHeader' present on page
And I should see text 'Kayla Simon' present on page at 'NewCaseOwnerName'
And I should see element 'CaseContactName1' present on page
And I click 'CaseSaveBtn'
And I should see element 'CaseSaveErrorMsg' present on page
And I close 'Mike Moran' contact tab

@Ignore
Scenario: Create a new case for a given Contact and verify Success message
Given I click 'SearchTypeDD'
And I click 'SearchType_Contact'
And I hit tab-key on element 'SearchTypeDD'
And I enter 'Yash Ganorkar' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I hit enter-key on element 'GlobalSearchField'
And I wait for '3' seconds
And I should see text 'Yash Ganorkar' contained on page at 'ContactNameHeader2'
Given I click by JS 'NewCaseBtn'
And I wait for '2' seconds
And I should see element 'NewCaseHeader' present on page
And I should see text 'Kayla Simon' present on page at 'NewCaseOwnerName'
And I should see element 'CaseContactName2' present on page
And I click 'SourceDropDown'
And I click 'Source_Corporate'
And I scroll to 'coordinates' - '0,1000'
And I click 'SubrecordTypeDropDown'
And I click 'SubRecordType_Cancellation'
And I click 'SubTypeDropDown'
And I click 'SubType_CartOrders'
And I click 'ReasonDropDown'
And I click 'Reason_DamagedProduct'
And I click 'CurrentStateDropDown'
And I click 'CurrentState_ReferredToSales'
And I click 'CaseSaveBtn'
And I get text from 'SuccessToastMessage' and store
And I close 'Yash Ganorkar' contact tab

@Ignore
Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

@Ignore
Scenario: Verify error message on invalid credentials
Given I enter '127504271@mfrm.com.uat' in field 'login_username'
And I enter 'test123' in field 'login_password'
And I click 'login_submitBtn'
Then I should see text 'Please check your username and password. If you still can't log in, contact your Salesforce administrator.' present on page at 'login_error'

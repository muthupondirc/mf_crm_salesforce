@S4_SFConsoleUserDetail
Feature: Verify Functionality of SF Console User Detail - Personal Information Section
 
 1. Login to Mattress Firm Sales Force Application
1.1 Verify Functionality of SF Console User Detail - Personal Information Section -SP4_TC4

Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Ecomm user 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Ecomm user 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1. Verify Functionality of SF Console User Detail - Personal Information Section -SP4_TC4
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see element 'SettingsLink_Console' present on page
And I click by JS 'SettingsLink_Console'
And I should see element 'PersonalInformationHeader_Console' present on page
And I switch to iFrame 'Frame_Console'
And I should see element 'FirstName_Console' present on page
And I clear the text and enter 'CCO Ecomm1' in field 'FirstName_Console'
And I should see element 'SaveButton_Console' present on page
And I click by JS 'SaveButton_Console'
And I wait for '5' seconds
And I should see element 'SuccessfullMessage_Console' present on page
And I should see text 'Your settings have been successfully saved.' present on page at 'SuccessfullMessage_Console'
And I should see element 'FirstName_Console' present on page
And I clear the text and enter 'CCO Ecomm' in field 'FirstName_Console'
And I should see element 'SaveButton_Console' present on page
And I click by JS 'SaveButton_Console'
And I wait for '5' seconds
And I should see element 'SuccessfullMessage_Console' present on page
And I should see element 'LastName_Console' present on page
And I clear the text and enter 'user 21' in field 'LastName_Console'
And I should see element 'SaveButton_Console' present on page
And I click by JS 'SaveButton_Console'
And I wait for '5' seconds
And I should see element 'ErrorMessage_Console' present on page
And I switch to default content
And I click by JS 'Close_Agent' 

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page
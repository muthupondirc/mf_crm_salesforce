@MF_Updatecancelmyentireorder 
Feature: Update cancel my entire order flow on Chat

1. Create Cases with Sub Type Found Lower Price from Chat bot with Yes Option.
2. Create Cases with Sub Type Found Lower Price from Chat bot with No Option.
3. Create Cases with Subtype Need Product Sooner OR Did Not Deliver as Promised from Chat bot with Yes Option.
4. Create Cases with Subtype Need Product Sooner OR Did Not Deliver as Promised from Chat bot No Option.
5. Create Cases with Subtype No Longer Need OR Prefer Not to Say/Just Wants to Cancel from Chat bot.

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'Retention Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'Retention Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

#Scenario: Omni Channel - make Service Agent as Available Chat
#Given I should see element 'OmniChannel_Button_Omni' present on page
#And I click 'OmniChannel_Button_Omni'
#And I click by JS 'Status_Dropdown_Omni'
#And I click by JS 'AvailableChat_Button_Omni'
#And I should see element 'AvailableChat_Text_Omni' present on page
#And I click by JS 'Minimize_Button_Omni'

Scenario: 1. Create Cases with Sub Type Found Lower Price from Chat bot with Yes Option.
#-----------UAT URL--------------------------------------------------------------------------------
#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#-----------QA URL--------------------------------------------------------------------------------- 
And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' 
And I should see element 'CookiesCloseButton_Hybrid' present on page
And I click by JS 'CookiesCloseButton_Hybrid'
And I wait for visibility of element 'MattressSite_CloseButton'
And I click 'MattressSite_CloseButton'
#And I wait for '15' seconds
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click by JS 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'joesmith@test.com' in field 'Email_Field_Omni'
And I get text from 'Email_Field_Omni' and store
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I get text from 'HomePhone_Field_Omni' and store
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click by JS 'HelpWithExistingOrder_Option'

#--------------next line is for old chat bot step-----------------------------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'

And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds

#--------------next 2 lines are old chat bot steps---------------------------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
#-----------------------console-----------------------------

And I Switch between Tab '0'
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AwayButton_Omni'
And I should see element 'AwayText_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
#------------------------------------------------------------------------

And I Switch between Tab '1'
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'NoAgentsText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I should see element 'ChatStartedText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I click by JS 'DoNotHaveButton_Chat'
And I click by JS 'PhoneSalesAgentButton_Chat'
And I click by JS 'YesButton_Chat'
And I click by JS 'YesButton_Chat'
And I should see element 'CancellationButton_Cancel' present on page
And I click by JS 'CancellationButton_Cancel'
And I should see element 'CancelPartofMyOrderButton_Cancel' present on page
And I should see element 'CancelMyEntireOrderButton_Cancel' present on page
And I click by JS 'CancelMyEntireOrderButton_Cancel'
And I should see element 'DidNotDeliverasPromisedButton_Cancel' present on page
And I should see element 'FoundLowerPriceButton_Cancel' present on page
And I should see element 'NeedProductSoonerButton_Cancel' present on page
And I should see element 'NoLongerNeededButton_Cancel' present on page
And I should see element 'PreferNottoSay/JustWantstoCancelButton_Cancel' present on page
And I click by JS 'FoundLowerPriceButton_Cancel'
And I should see element 'ConfirmationTransitionmessage_Found' present on page
And I should see element 'YesButton_Chat' present on page
And I click by JS 'YesButton_Chat'
And I wait for '5' seconds
And I should see element 'CaseCreatedMessage_Chat' present on page
And I get Case Number from Chat 'CaseCreatedMessage_Chat' and store
And I Close Window Tab
And I Switch between Tab '0'
And I wait for '5' seconds


Scenario: Validating in SF Console
And I refresh the WebPage
And I wait for '15' seconds
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseCreatedMessage_Chat' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I should see text 'Customer Retention' contained on page at 'CaseOwnerField_Cancel'
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumberField_CR'
And I scroll till element 'DescriptionValue'
And I wait for '5' seconds
And I should see text '120 Night Low Price Guarantee' contained on page at 'DescriptionValue'
And I click by JS 'Close_Agent'
And I click by JS 'Close_Agent'


Scenario: 2. Create Cases with Sub Type Found Lower Price from Chat bot with No Option.
#-----------UAT URL--------------------------------------------------------------------------------
#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#-----------QA URL--------------------------------------------------------------------------------- 
And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' 
#And I should see element 'CookiesCloseButton_Hybrid' present on page
#And I click by JS 'CookiesCloseButton_Hybrid'
#And I wait for visibility of element 'MattressSite_CloseButton'
#And I click 'MattressSite_CloseButton'
#And I wait for '15' seconds
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click by JS 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'joesmith@test.com' in field 'Email_Field_Omni'
And I get text from 'Email_Field_Omni' and store
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I get text from 'HomePhone_Field_Omni' and store
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click by JS 'HelpWithExistingOrder_Option'

#--------------next line is for old chat bot step-----------------------------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'

And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds

#--------------next 2 lines are old chat bot steps---------------------------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
#-----------------------console-----------------------------

And I Switch between Tab '0'
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AwayButton_Omni'
And I should see element 'AwayText_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
#------------------------------------------------------------------------

And I Switch between Tab '1'
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'NoAgentsText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I should see element 'ChatStartedText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I click by JS 'DoNotHaveButton_Chat'
And I click by JS 'PhoneSalesAgentButton_Chat'
And I click by JS 'YesButton_Chat'
And I click by JS 'YesButton_Chat'
And I should see element 'CancellationButton_Cancel' present on page
And I click by JS 'CancellationButton_Cancel'
And I should see element 'CancelPartofMyOrderButton_Cancel' present on page
And I should see element 'CancelMyEntireOrderButton_Cancel' present on page
And I click by JS 'CancelMyEntireOrderButton_Cancel'
And I should see element 'DidNotDeliverasPromisedButton_Cancel' present on page
And I should see element 'FoundLowerPriceButton_Cancel' present on page
And I should see element 'NeedProductSoonerButton_Cancel' present on page
And I should see element 'NoLongerNeededButton_Cancel' present on page
And I should see element 'PreferNottoSay/JustWantstoCancelButton_Cancel' present on page
And I click by JS 'FoundLowerPriceButton_Cancel'
And I should see element 'ConfirmationTransitionmessage_Found' present on page
And I should see element 'NoOption' present on page
And I click by JS 'NoOption'
And I wait for '5' seconds
And I should see element 'NoConfirmationTransitionmessage' present on page
And I should see element 'CaseCreatedMessage_Chat' present on page
And I get Case Number from Chat 'CaseCreatedMessage_Chat' and store
And I Close Window Tab
And I Switch between Tab '0'
And I wait for '5' seconds


Scenario: Validating in SF Console
And I refresh the WebPage
And I wait for '15' seconds
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseCreatedMessage_Chat' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I should see text 'Customer Retention' contained on page at 'CaseOwnerField_Cancel'
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumberField_CR'
And I scroll till element 'DescriptionValue'
And I wait for '5' seconds
And I should see text 'Proceed with cancellation' contained on page at 'DescriptionValue'
And I click by JS 'Close_Agent'
And I click by JS 'Close_Agent'



Scenario: 3. Create Cases with Subtype Need Product Sooner OR Did Not Deliver as Promised from Chat bot with Yes Option.
#-----------UAT URL--------------------------------------------------------------------------------
#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#-----------QA URL--------------------------------------------------------------------------------- 
And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' 
#And I should see element 'CookiesCloseButton_Hybrid' present on page
#And I click by JS 'CookiesCloseButton_Hybrid'
#And I wait for visibility of element 'MattressSite_CloseButton'
#And I click 'MattressSite_CloseButton'
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click by JS 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'joesmith@test.com' in field 'Email_Field_Omni'
And I get text from 'Email_Field_Omni' and store
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I get text from 'HomePhone_Field_Omni' and store
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click by JS 'HelpWithExistingOrder_Option'

#--------------next line is for old chat bot step-----------------------------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'

And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds

#--------------next 2 lines are old chat bot steps---------------------------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
#-----------------------console-----------------------------

And I Switch between Tab '0'
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AwayButton_Omni'
And I should see element 'AwayText_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
#------------------------------------------------------------------------

And I Switch between Tab '1'
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'NoAgentsText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I should see element 'ChatStartedText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I click by JS 'DoNotHaveButton_Chat'
And I click by JS 'PhoneSalesAgentButton_Chat'
And I click by JS 'YesButton_Chat'
And I click by JS 'YesButton_Chat'
And I should see element 'CancellationButton_Cancel' present on page
And I click by JS 'CancellationButton_Cancel'
And I should see element 'CancelPartofMyOrderButton_Cancel' present on page
And I should see element 'CancelMyEntireOrderButton_Cancel' present on page
And I click by JS 'CancelMyEntireOrderButton_Cancel'
And I should see element 'DidNotDeliverasPromisedButton_Cancel' present on page
And I should see element 'FoundLowerPriceButton_Cancel' present on page
And I should see element 'NeedProductSoonerButton_Cancel' present on page
And I should see element 'NoLongerNeededButton_Cancel' present on page
And I should see element 'PreferNottoSay/JustWantstoCancelButton_Cancel' present on page
And I click by JS 'NeedProductSoonerButton_Cancel'
And I should see element 'ConfirmationTransitionmessage_NeedProduct' present on page
And I should see element 'YesButton_Chat' present on page
And I click by JS 'YesButton_Chat'
And I wait for '5' seconds
And I should see element 'CaseCreatedMessage_Chat' present on page
And I get Case Number from Chat 'CaseCreatedMessage_Chat' and store
And I Close Window Tab
And I Switch between Tab '0'
And I wait for '5' seconds


Scenario: Validating in SF Console
And I refresh the WebPage
And I wait for '15' seconds
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseCreatedMessage_Chat' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I should see text 'Customer Retention' contained on page at 'CaseOwnerField_Cancel'
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumberField_CR'
And I scroll till element 'DescriptionValue'
And I wait for '5' seconds
And I should see text 'Request to be contacted by a Sleep Expert' contained on page at 'DescriptionValue'
And I click by JS 'Close_Agent'
And I click by JS 'Close_Agent'



Scenario: 4. Create Cases with Subtype Need Product Sooner OR Did Not Deliver as Promised from Chat bot No Option.
#-----------UAT URL--------------------------------------------------------------------------------
#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#-----------QA URL--------------------------------------------------------------------------------- 
And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' 
#And I should see element 'CookiesCloseButton_Hybrid' present on page
#And I click by JS 'CookiesCloseButton_Hybrid'
#And I wait for visibility of element 'MattressSite_CloseButton'
#And I click 'MattressSite_CloseButton'
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click by JS 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'joesmith@test.com' in field 'Email_Field_Omni'
And I get text from 'Email_Field_Omni' and store
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I get text from 'HomePhone_Field_Omni' and store
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click by JS 'HelpWithExistingOrder_Option'

#--------------next line is for old chat bot step-----------------------------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'

And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds

#--------------next 2 lines are old chat bot steps---------------------------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
#-----------------------console-----------------------------

And I Switch between Tab '0'
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AwayButton_Omni'
And I should see element 'AwayText_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
#------------------------------------------------------------------------

And I Switch between Tab '1'
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'NoAgentsText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I should see element 'ChatStartedText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I click by JS 'DoNotHaveButton_Chat'
And I click by JS 'PhoneSalesAgentButton_Chat'
And I click by JS 'YesButton_Chat'
And I click by JS 'YesButton_Chat'
And I should see element 'CancellationButton_Cancel' present on page
And I click by JS 'CancellationButton_Cancel'
And I should see element 'CancelPartofMyOrderButton_Cancel' present on page
And I should see element 'CancelMyEntireOrderButton_Cancel' present on page
And I click by JS 'CancelMyEntireOrderButton_Cancel'
And I should see element 'DidNotDeliverasPromisedButton_Cancel' present on page
And I should see element 'FoundLowerPriceButton_Cancel' present on page
And I should see element 'NeedProductSoonerButton_Cancel' present on page
And I should see element 'NoLongerNeededButton_Cancel' present on page
And I should see element 'PreferNottoSay/JustWantstoCancelButton_Cancel' present on page
And I click by JS 'NeedProductSoonerButton_Cancel'
And I should see element 'ConfirmationTransitionmessage_NeedProduct' present on page
And I should see element 'NoOption' present on page
And I click by JS 'NoOption'
And I wait for '5' seconds
And I should see element 'NoConfirmationTransitionmessage' present on page
And I should see element 'CaseCreatedMessage_Chat' present on page
And I get Case Number from Chat 'CaseCreatedMessage_Chat' and store
And I Close Window Tab
And I Switch between Tab '0'
And I wait for '5' seconds


Scenario: Validating in SF Console
And I refresh the WebPage
And I wait for '15' seconds
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseCreatedMessage_Chat' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I should see text 'Customer Retention' contained on page at 'CaseOwnerField_Cancel'
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumberField_CR'
And I scroll till element 'DescriptionValue'
And I wait for '5' seconds
And I should see text 'Proceed with cancellation' contained on page at 'DescriptionValue'
And I click by JS 'Close_Agent'
And I click by JS 'Close_Agent'


Scenario: 5. Create Cases with Subtype No Longer Need OR Prefer Not to Say/Just Wants to Cancel from Chat bot.
#-----------UAT URL--------------------------------------------------------------------------------
#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#-----------QA URL--------------------------------------------------------------------------------- 
And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' 
#And I should see element 'CookiesCloseButton_Hybrid' present on page
#And I click by JS 'CookiesCloseButton_Hybrid'
#And I wait for visibility of element 'MattressSite_CloseButton'
#And I click 'MattressSite_CloseButton'
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click by JS 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'joesmith@test.com' in field 'Email_Field_Omni'
And I get text from 'Email_Field_Omni' and store
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I get text from 'HomePhone_Field_Omni' and store
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click by JS 'HelpWithExistingOrder_Option'

#--------------next line is for old chat bot step-----------------------------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'

And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds

#--------------next 2 lines are old chat bot steps---------------------------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
#-----------------------console-----------------------------

And I Switch between Tab '0'
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AwayButton_Omni'
And I should see element 'AwayText_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
#------------------------------------------------------------------------

And I Switch between Tab '1'
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'NoAgentsText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I should see element 'ChatStartedText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I click by JS 'DoNotHaveButton_Chat'
And I click by JS 'PhoneSalesAgentButton_Chat'
And I click by JS 'YesButton_Chat'
And I click by JS 'YesButton_Chat'
And I should see element 'CancellationButton_Cancel' present on page
And I click by JS 'CancellationButton_Cancel'
And I should see element 'CancelPartofMyOrderButton_Cancel' present on page
And I should see element 'CancelMyEntireOrderButton_Cancel' present on page
And I click by JS 'CancelMyEntireOrderButton_Cancel'
And I should see element 'DidNotDeliverasPromisedButton_Cancel' present on page
And I should see element 'FoundLowerPriceButton_Cancel' present on page
And I should see element 'NeedProductSoonerButton_Cancel' present on page
And I should see element 'NoLongerNeededButton_Cancel' present on page
And I should see element 'PreferNottoSay/JustWantstoCancelButton_Cancel' present on page
And I click by JS 'NeedProductSoonerButton_Cancel'
And I should see element 'ConfirmationTransitionmessage_NeedProduct' present on page
And I should see element 'YesButton_Chat' present on page
And I click by JS 'YesButton_Chat'
And I wait for '5' seconds
And I should see element 'CaseCreatedMessage_Chat' present on page
And I get Case Number from Chat 'CaseCreatedMessage_Chat' and store
And I Close Window Tab
And I Switch between Tab '0'
And I wait for '5' seconds


Scenario: Validating in SF Console
And I refresh the WebPage
And I wait for '15' seconds
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseCreatedMessage_Chat' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I should see text 'Customer Retention' contained on page at 'CaseOwnerField_Cancel'
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumberField_CR'
And I scroll till element 'DescriptionValue'
And I wait for '5' seconds
#And I should see text 'Proceed with cancellation' contained on page at 'DescriptionValue'
And I should see text 'Request to be contacted by a Sleep Expert' contained on page at 'DescriptionValue'
And I click by JS 'Close_Agent'
And I click by JS 'Close_Agent'

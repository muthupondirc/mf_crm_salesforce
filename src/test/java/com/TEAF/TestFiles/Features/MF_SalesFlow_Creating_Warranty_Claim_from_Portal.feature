@MF_SalesFlow_Creating_Warranty_Claim_from_Portal
Feature: Creating a Warranty Claim From Portal

1.Login with Standard user Credentials
2.SP1_TC50 Processing eClaim from Portal (Order Attached to Claim)
3.SP1_TC51 & SP1_TC52 Creating a New Related Warranty Claim 
4.Verify logout of Sales Force application

Scenario: Login with Standard user Credentials
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

#Scenario: SP1_TC50 Processing eClaim from Portal (Order Attached to Claim)
#
#And I click by JS 'ShowNavigationDD'
#And I wait for '3' seconds
#And I click by JS 'SFNav_Cases'
#And I wait for '5' seconds 
#And I click by JS 'WarrantyCaseCustomerCommunity'
#And I should see element 'CommunityCaseSearchBar' present on page
#And I enter 'Customer Community - Warranty Claims' in field 'CommunityCaseSearchBar'
#And I wait for '2' seconds
#And I hit down Arrow key on element 'CommunityCaseSearchBar'
#And I hit enter-key on element 'CommunityCaseSearchBar'
#And I wait for '5' seconds
#And I should see element 'LastModifiedColumn' present on page
#And I click by JS 'LastModifiedColumn'
#And I wait for '5' seconds
#And I check the sort status to be 'Descending'
#And I should see element 'CustomerCummunityCases' present on page
#And I click by JS 'CustomerCummunityCases'
#And I wait for '5' seconds
#And I click by JS 'ShowNavigationDD'
#And I wait for '3' seconds
#And I click by JS 'HomeMenu'
#And I wait for '3' seconds
#And I click by JS 'TabCase'
#And I should see element 'ShowOptionButtonToAccept' present on page
#And I click by JS 'ShowOptionButtonToAccept'
#And I wait for '5' seconds
#And I click by JS 'AcceptButton'
#And I wait for '3' seconds
#And I wait for '2' seconds
#And I should see element 'ProcessWarrantyClaimPortal' present on page
#And I click by JS 'ProcessWarrantyClaimPortal'
#And I wait for '5' seconds
#And I switch to iFrame 'IframeWarrantyTab'
##And I wait for visibility of element 'ChoosePrimaryOrder'
##And I should see element 'ChoosePrimaryOrder' present on page
##And I should see element 'CommunityOrder' present on page
##And I click by JS 'ChoosePrimaryOrderValue'
##And I click by JS 'CommunityNextBtn'
#And I wait for '5' seconds
#And I should see element 'DefaultAddress' present on page
#And I should see element 'CommunityNxtBtn' present on page
#And I click by JS 'CommunityNxtBtn'
#And I wait for visibility of element 'WarrantyClaimtext'
#And I should see element 'WarrantyClaimtext' present on page
#And I should see element 'CommunityProductCheckBox' present on page
##And I click by JS 'CommunityProductCheckBox'
#And I should see element 'CommunityOrderQuantity' present on page
#And I enter '1' in field 'CommunityOrderQuantity'
#And I click by JS 'CommunityNxtBtn'
#And I wait for visibility of element 'DefectReasonHeader'
#And I should see element 'DefectReasonHeader' present on page
##For QA
#And I should see element 'CommunityProductNameQA' present on page
##For UAT
##And I should see element 'CommunityProductNameUAT' present on page
#
#And I should see element 'ProductNameDD' present on page
#And I wait for '3' seconds
#And I click by JS 'ProductNameDD'
#And I wait for '2' seconds
#And I select option 'MAT - BODY INDENTATIONS' in dropdown 'ProductNameDD' by 'text'
##And I click by JS 'ProductNameValue'
#And I wait for '2' seconds
#And I click by JS 'CommunityNxtBtn'
#And I wait for visibility of element 'Question1_Label'
#And I should see element 'Question1_Label' present on page
#And I click by JS 'Question1_RadioBtn'
#And I should see element 'CommunityQuestion2' present on page
#And I click by JS 'CommunityQuestion2_Value'
#And I wait for '3' seconds
##And I should see element 'Question3_Label' present on page
##And I click by JS 'Question3_DD'
##And I select option '5' in dropdown 'Question3_DD' by 'text'
##And I click by JS 'Question3_DD_Value'
#And I should see element 'CommunityQuestion4' present on page
#And I click by JS 'CommunityQuestion4_Value'
#And I click by JS 'CommunityNxtBtn'
#And I wait for '3' seconds
#And I should see element 'OnlineClaim' present on page
##And I click by JS 'VendorInspection_Checkbox'
#And I click by JS 'CommunityCompleteBtn'
#And I wait for '3' seconds
#And I should see element 'CommunityCaseNumber' present on page
#And I get text from 'CommunityCaseNumber' and store
#And I click by JS 'CommunityCaseNumber'
#And I switch back to Main Window
#And I wait for '5' seconds
#And I click by JS 'CommunityCaseSwitch'
#And I refresh the WebPage
#And I wait for '10' seconds
#And I scroll till element 'CommunityShipping'
#And I wait for '3' seconds
#And I should see element 'CommunityCaseProduct' present on page
#And I should see element 'CommunityProduct' present on page
##For QA
#And I should see element 'CommunityAddedProduct' present on page
##For UAT
##And I should see element 'CommunityAddedProductUAT' present on page
#And I should see element 'CommunityProductDD' present on page
#And I click by JS 'CommunityProductDD'
#And I wait for '2' seconds
#And I click by JS 'CommunityToolTip'
#And I wait for '3' seconds
#And I should see element 'CommunityProductTypeDD' present on page
#And I wait for '2' seconds
#And I click by JS 'CommunityProductTypeDD'
#And I wait for '2' seconds
#And I should see element 'CommunityProductTypeValue' present on page
#And I wait for '2' seconds
#And I click by JS 'CommunityProductTypeValue'
#And I wait for '2' seconds
#And I scroll till element 'CommunityProductTypeDD'
#And I wait for '5' seconds
#And I should see element 'CommunityProductSize' present on page
#And I click by JS 'CommunityProductSize'
#And I wait for '2' seconds
#And I click by JS 'CommunitySizeValue'
#And I wait for '2' seconds
#And I scroll till element 'CommunityProductSize'
#And I wait for '2' seconds
#And I should see element 'CommunityResultCode' present on page
#And I click by JS 'CommunityResultCode'
#And I wait for '2' seconds
#And I click by JS 'CommunityResultCodeValue'
#And I wait for '2' seconds
#And I click by JS 'CommunitySave'
#And I wait for '3' seconds
#And I click by JS 'CommunityEdit'
#And I wait for '3' seconds
#And I scroll till element 'CommunityClassification'
#And I wait for '2' seconds
#And I scroll till element 'CommunityCurrentState'
#And I click by JS 'CommunityCurrentState'
#And I wait for '2' seconds
#And I click by JS 'CommunityCurrentStateValue'
#And I wait for '2' seconds
#And I click by JS 'CommunityResolution'
#And I wait for '2' seconds
#And I click by JS 'CommunityResolutionValue'
#And I wait for '2' seconds
#And I click by JS 'CommunityStatus'
#And I wait for '2' seconds
#And I click by JS 'CommunityStatusValue'
#And I wait for '2' seconds
#And I click by JS 'CommunityEditSave'
#And I wait for '3' seconds
#And I should see element 'CommunityParentCase' present on page
#And I scroll till element 'CommunityParentCase'
#And I wait for '3' seconds
#And I scroll till element 'CommunityCaseClassification'
#And I wait for '3' seconds
#And I scroll till element 'CommunityWarrantyEclaim'
#And I wait for '3' seconds
#And I should see element 'WarrantyStep' present on page
#And I wait for '2' seconds
#And I click by JS 'Close'
#And I wait for '3' seconds



Scenario: SP1_TC51 & SP1_TC52 Creating a New Related Warranty Claim 
And I click 'SearchTypeDD'
And I click by JS 'SearchType_Cases'
And I hit tab-key on element 'SearchTypeDD'
And I enter '03090914' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '3' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '10' seconds
#And I click by JS 'btnMore'
#And I wait for '2' seconds
And I should see element 'NewRelated_Case' present on page
And I click by JS 'NewRelated_Case'
And I wait for '2' seconds
And I should see element 'WarningMessage' present on page
And I click by JS 'NxtButton'
And I wait for '5' seconds
And I refresh the WebPage
And I should see element 'HighlightPanel' present on page
And I scroll till element 'ShippingEmail'
And I wait for visibility of element 'RelatedCasesSection'
And I should see element 'RelatedCasesSection' present on page
And I wait for '3' seconds
And I should see element 'ParentCase' present on page
And I wait for '3' seconds
And I should see element 'CurrentCase' present on page
And I wait for '3' seconds
And I click by JS 'CurrentCase'
And I wait for visibility of element 'ParentCaseLabel'
And I should see element 'ParentCaseLabel' present on page
And I should see element 'ParentCaseValue' present on page
And I scroll to 'element' - 'SubTabCaseClassification'
And I wait for '2' seconds
And I should see element 'SubTabSource' present on page
And I should see text 'External' present on page at 'SubTab_SourceValue'
And I should see element 'SubTab_Subrecordtype' present on page
And I should see text 'Warranty Process' present on page at 'SubTab_SubrecordtypeValue'
And I should see element 'SubTab_SubType' present on page
And I should see element 'SubTab_Reason' present on page
And I should see element 'SubTab_CurrentState' present on page
And I should see element 'SubTab_Status' present on page
And I should see text 'New' present on page at 'subTab_StatusValue'
And I scroll to 'element' - 'ResolutionsLabel'
And I wait for '4' seconds
And I should see element 'CaseRecordType' present on page
And I should see element 'CaseRecordTypeValue' present on page
And I scroll to 'element' - 'ProcessWarrantyClaim'
And I click by JS 'ProcessWarrantyClaim'
And I wait for '5' seconds
And I switch to iFrame 'Warrantyiframe'
And I wait for visibility of element 'ChoosePrimaryOrder'
And I should see element 'ChoosePrimaryOrder' present on page
#FORQA

And I should see element 'ChooseOrder' present on page
#FOR UAT

#And I should see element 'ChooseOrderUAT' present on page
And I click by JS 'ChoosePrimaryOrderValue'
And I should see element 'NxtBtn' present on page
And I click by JS 'NxtBtn'
And I wait for visibility of element 'DefaultAddress'
And I should see element 'DefaultAddress' present on page
And I click by JS 'NextButton'
And I wait for visibility of element 'WarrantyClaimtext'
And I should see element 'WarrantyClaimtext' present on page
And I should see element 'WarrantyOrderValueQA' present on page
And I click by JS 'WarrantyCheckBox'
And I should see element 'WarrantyQuantity' present on page
And I enter '1' in field 'WarrantyQuantity'
And I click by JS 'NextButton'
And I wait for visibility of element 'DefectReasonHeader'
And I should see element 'DefectReasonHeader' present on page
#FORQA
#And I should see element 'ProductName' present on page
#FOR UAT

And I should see element 'ProductNameUAT' present on page
And I should see element 'ProductNameDD' present on page
And I wait for '3' seconds
And I click by JS 'ProductNameDD'
And I wait for '2' seconds
And I select option 'MAT - BODY INDENTATIONS' in dropdown 'ProductNameDD' by 'text'
And I wait for '2' seconds
And I click by JS 'NextButton'
And I wait for visibility of element 'Question1_Label'
And I should see element 'Question1_Label' present on page
And I click by JS 'Question1_RadioBtn'
And I should see element 'Question2_Label' present on page
And I click by JS 'Question2_RadioBtn'
And I should see element 'Question2a_Label' present on page
And I click by JS 'Question2_DD'
And I select option '1' in dropdown 'Question2_DD' by 'text'
And I should see element 'Question3_Label' present on page
And I click by JS 'Question3_DD'
And I select option '5' in dropdown 'Question3_DD' by 'text'
And I should see element 'Question4_Label' present on page
And I click by JS 'Question4_RadioBtn'
And I click by JS 'NextButton'
And I wait for '3' seconds
And I should see element 'VendorInspection' present on page
And I click by JS 'VendorInspection_Checkbox'
And I click by JS 'CompleteBtn'
And I wait for '3' seconds
And I should see element 'CaseNumber_Btn' present on page
And I should see element 'Payment_Link' present on page
And I click by JS 'Payment_Link'
And I click by JS 'CaseNumber_Btn'
And I switch back to Main Window
And I wait for '5' seconds
And I click by JS 'CaseSubTabBtn'
#And I refresh the WebPage
#And I wait for visibility of element 'SubWarrantyHeader'
#And I should see element 'SubWarrantyHeader' present on page
And I should see element 'SubTabEditCase' present on page
And I wait for '3' seconds
And I click by JS 'SubTabEditCase'
And I wait for '5' seconds
#And I should see element 'CaseEditTitle' present on page
And I scroll to 'element' - 'Classification'
And I should see element 'Inspection_Vendor_Label' present on page
#And I should see element 'InspectionValue' present on page
And I should see element 'Payment_Status_Label' present on page
And I click by JS 'Payment_DD'
And I click by JS 'PaymentDD_Value'
And I wait for '2' seconds
And I scroll to 'element' - 'Reason_Label'
And I wait for '2' seconds
And I should see element 'PaymentAuth' present on page
And I click by JS 'PaymentAuthField'
And I enter 'xxxxxxx' in field 'PaymentAuthField'
And I wait for '2' seconds
And I should see element 'Current_State_Label' present on page
And I wait for '2' seconds
And I click by JS 'CurrentStateField'
And I wait for '2' seconds
And I click by JS 'CurrentStateFieldValue'
And I wait for '2' seconds
And I click by JS 'CommEditSaveBtn'
And I wait for '5' seconds
And I click by JS 'CaseSubTabBtn'
And I wait for '3' seconds
And I should see element 'CaseOwnerFieldLabel' present on page
And I should see element 'CaseOwnerFieldLabelValue' present on page
And I wait for '2' seconds
And I click by JS 'Close'
And I wait for '3' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
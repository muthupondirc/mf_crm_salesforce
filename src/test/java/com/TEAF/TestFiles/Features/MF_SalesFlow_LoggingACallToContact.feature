@MF_SalesFlow_LoggingACallToContact
Feature: Log a Call to Contact

1. Login with Standard user Credentials
2. SP1_TC36 Logging a call to Contact
3. Logout with Standard user

Scenario: Login with Standard user Credentials
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

Scenario: SP1_TC36 Log a Call to Contact
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I click by JS 'SFNav_Cases'
And I wait for '5' seconds
And I should see element 'SFCase_New' present on page
And I click by JS 'SFCase_New'
And I wait for '3' seconds
And I should see element 'NewCaseSelectionBtn' present on page
And I should see element 'NewCaseSelectionLabel' present on page
And I click by JS 'NewCaseSelectionBtn'
And I should see element 'NewCaseSelectionNxt' present on page
And I click by JS 'NewCaseSelectionNxt'
And I wait for '5' seconds
And I should see element 'New_Case_Header' present on page
And I should see element 'SFServiceCaseContactName' present on page
And I wait for '2' seconds
And I click by JS 'SFServiceCaseContactName'
And I enter 'jared' in field 'SFServiceCaseContactName'
And I wait for '3' seconds
And I hit down Arrow key on element 'SFServiceCaseContactName'
And I wait for '3' seconds
And I hit enter-key on element 'SFServiceCaseContactName'
And I wait for '2' seconds
And Blur 'SFServiceCaseContactName'
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'TypeOfProduct' present on page
And I click by JS 'TypeOfProductDD'
And I click by JS 'TypeOfProductValue'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'SubRecordType_NeedOption_Value'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'CaseNo' present on page
And I get text from 'CaseNo' and store
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds
Given I click 'SearchTypeDD'
And I click by JS 'SearchType_Contact'
And I hit tab-key on element 'SearchTypeDD'
And I enter 'Jared' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '2' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
#And I should see element 'ContactsHeader' present on page
#And I click by JS 'NameBtn'
And I wait for visibility of element 'Actions'
And I should see element 'Actions' present on page
And I should see element 'LogCall' present on page
And I click by JS 'Add'
And I wait for visibility of element 'MFRMContact'
And I scroll to 'element' - 'Actions'
And I should see element 'MFRMContact' present on page
And I should see element 'MFRMDDContact' present on page
And I click by JS 'MFRMDDContact'
And I click by JS 'MFRMDDContactValue'
And I should see element 'SubjectContact' present on page
And I click by JS 'SubjectTxt'
And I clear the text and enter 'Verify Information' in field 'SubjectTxt'
And I should see element 'ContactComments' present on page
And I click by JS 'ContactCommentsTxt'
And I wait for '3' seconds
And I enter 'Testing 1,2,3' in field 'ContactCommentsTxt'
And Blur 'ContactCommentsTxt'
And I wait for '2' seconds
And I click by JS 'SaveContactCallBtn'
And I wait for '3' seconds
And I should see element 'ContactCustomerWarning' present on page
And I scroll to 'element' - 'ContactBilling_Info'
And I wait for '2' seconds
And I should see element 'ContActivityHistory' present on page
And I should see element 'ContActivitySubject' present on page
And I should see element 'ContActivityMFRMCallType' present on page
And I click by JS 'ContActivityView'
And I wait for '5' seconds
And I switch to iFrame 'ContactIframe'
And I should see element 'LastModified' present on page
And I should see element 'MFRMCallTypeActivity' present on page
And I should see element 'ActivityComments' present on page
And I switch back to Main Window
And I wait for '2' seconds
And I click by JS 'TaskCloseBtn'
And I wait for '3' seconds
And I click by JS 'Close'
#And I wait for '3' seconds
#And I click by JS 'Close'
@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
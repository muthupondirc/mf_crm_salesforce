@MF_ChatlayoutForSalesChat
Feature: Sprint10_SFLC - 16855_Chat (IS) - Layout Changes for Active Vs. Completed Chat

1.SP10_TC1 - Verify Chat layout for Sales chat
2.Logout Scenario

Scenario: Verify Chat layout for Sales chat
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'telesales' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'telesales_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds
And I should see element 'OmniButton' present on page
And I click by JS 'OmniButton'
And I wait for '3' seconds
And I should see element 'OmniStatus' present on page
And I should see element 'OmniStatusDD' present on page
And I click by JS 'OmniStatusDD'
And I click by JS 'AvailableChat'
And I wait for '5' seconds
And I should see element 'ChatStatusUtilityBar' present on page
And I wait for '2' seconds
And I click by JS 'ChatStatusUtilityBar'
#And I click by JS 'MinimizeOmniBtn'
And I wait for '5' seconds
And I Switch the Tab and enter the url 'MFPortalURL'
And I click by JS 'linkAcceptCookieClose'
And I wait for '5' seconds
#And I click by JS 'WebClosePopup'
#And I wait for '5' seconds
And I should see element 'ChatWithExpertBtn' present on page
And I click by JS 'ChatWithExpertBtn'
And I wait for '5' seconds
And I should see element 'SFChatFirstNameUAT' present on page
And I click by JS 'SFChatFirstNameFieldUAT'
And I enter 'Jared' in field 'SFChatFirstNameFieldUAT'
And I should see element 'SFChatLastNameUAT' present on page
And I should see element 'SFChatLastNameFieldUAT' present on page
And I click by JS 'SFChatLastNameFieldUAT'
And I enter 'Steiner' in field 'SFChatLastNameFieldUAT'
And I should see element 'SFChatPhoneNumberUAT' present on page
And I should see element 'SFChatPhoneNumberFieldUAT' present on page
And I click by JS 'SFChatPhoneNumberFieldUAT'
And I enter '5165410544' in field 'SFChatPhoneNumberFieldUAT'
And I should see element 'SFChatLiveShoppingBtnUAT' present on page
And I click by JS 'SFChatLiveShoppingBtnUAT'
And I wait for '2' seconds
And I should see element 'SFChatStartChatBtnUAT' present on page
And I click by JS 'SFChatStartChatBtnUAT'
And I wait for '15' seconds
And I should see element 'ChatTextbox' present on page
And I click by JS 'ChatTextbox'
And I wait for '3' seconds
#And I wait for visibility of element 'ChatwithUser'
#And I should see element 'ChatwithUser' present on page

And I Switch between Tab '0'
And I wait for '5' seconds
And I should see element 'ChatStartedTabHeader' present on page
#And I should see element 'PortalChatHeader' present on page
And I should see element 'VisitorFirstName' present on page
And I should see element 'VisitorLastName' present on page
And I should see element 'VisitorPhone' present on page
And I should see element 'PortalEndChat' present on page
And I click by JS 'PortalEndChat'
And I wait for '2' seconds
And I should see element 'PortalEndChatTooltip' present on page
And I click by JS 'PortalEndChatTooltip'
And I wait for '3' seconds
And I click by JS 'Close'
And I wait for '5' seconds
And I Switch between Tab '1'
And I Switch between Tab '0'
And I click by JS 'ShowNavigationDD'
And I wait for '3' seconds
And I click by JS 'ChatTranscriptMenu'
And I wait for '3' seconds
And I click by JS 'lnkChatTranscriptNumber'

And I should see element 'headerDetails' present on page
And I should see element 'headerStorereferral' present on page
And I should see element 'headerChatDisposition' present on page

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
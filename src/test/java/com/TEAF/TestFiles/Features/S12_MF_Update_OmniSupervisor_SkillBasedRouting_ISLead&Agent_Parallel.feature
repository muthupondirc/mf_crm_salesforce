@S12_MF_Update_OmniSupervison_SkillBasedRouting_ISLead&Agent_Parallel
Feature: IS - Update Omni-Supervisor for skill based routing

1. Login to Mattress Firm Sales Force Application
2. Update Omni-Supervisor for skill based routing in IS Manager User-S12_TC01_SFLC-42542
3. Verify Omni-Supervisor for skill based routing in IS Agent User-S12_TC01_SFLC-42542
4. Logout of Sales Force application

@manager1
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'telesales' User Credentials 
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'telesales_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

@manager1
Scenario: Update Omni-Supervisor for skill based routing in IS Manager User-S12_TC01_SFLC-42542
And I click by JS 'dropdown_NavigationMenu'
And I select value 'Omni Supervisor' from the dropdown
And I click by JS 'bottomTab_OmniChannel'
And I click by JS 'dropdown_OmniChannel'
And I click by JS 'option_AvailableChat_Omni'
#And I select value 'Available - Chat' from the dropdown
And I should see element 'Available - Chat' present on page
And I click by JS 'button_Minimize_Omni'
Then I should see element 'Agent Summary' present on page
And I click 'checkbox_Agent'
And I click by JS 'button_ChangeSkills'
Then I should see element 'text_ChangeSkills' present on page
And I should see element 'field_IS-P3' present on page
And I should see element 'field_IS-P2' present on page
And I should see element 'field_IS-P1' present on page
Then I verify checkbox 'checkbox_IS-P3' is 'checked'
Then I verify checkbox 'checkbox_IS-P2' is 'checked'
Then I verify checkbox 'checkbox_IS-P1' is 'checked'
And I click by JS 'button_ChangeSkills_Save'

@agent
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'telesalesrep' User Credentials 
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'telesalesrep_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

@agent
Scenario: Verify Omni-Supervisor for skill based routing in IS Agent User-S12_TC01_SFLC-42542
And I click by JS 'dropdown_NavigationMenu'
And I select value 'Omni Supervisor' from the dropdown
And I click by JS 'bottomTab_OmniChannel'
And I click by JS 'dropdown_OmniChannel'
And I click by JS 'option_AvailableChat_Omni'
#And I select value 'Available - Chat' from the dropdown
And I should see element 'Available - Chat' present on page
And I click by JS 'button_Minimize_Omni'
Then I should see element 'Agent Summary' present on page
#And I should see element 'link_AgentUser' present on page
Then I should see element 'status_SkillSet' present on page
@manager1 @agent
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

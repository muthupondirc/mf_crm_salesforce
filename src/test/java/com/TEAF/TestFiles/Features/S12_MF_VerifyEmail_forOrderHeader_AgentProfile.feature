@S12_MF_VerifyEmail_forOrderHeader_AgentProfile
Feature: Store Email for Order Header

1. Login to Mattress Firm Sales Force Application
2. Service Agent should have the have the Store Email under Information-S12_TC13_SFLC-27550
3. Logout of Sales Force application

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'Lead_QA' User Credentials 
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'servicelead_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

Scenario: Service Agent should have the have the Store Email under Information-S12_TC13_SFLC-27550
Given I click by JS 'dropDown_AllGlobalSearch'
And I click by JS 'option_OrderHeader'
And I should see element 'txtBox_SearchField' present on page
And I click by JS 'txtBox_SearchField'
And I clear the text and enter 'SLP-105174834-1130' in field 'txtBox_SearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'txtBox_SearchField'
And I wait for '2' seconds
And I hit enter-key on element 'txtBox_SearchField'
And I wait for '5' seconds
And I should see element 'txt_OrderNumber' present on page
And I wait for '3' seconds
Then I should see element 'txt_Email' present on page

Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

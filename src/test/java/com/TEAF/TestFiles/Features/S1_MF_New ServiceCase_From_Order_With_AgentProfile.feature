@S1_MF_ServiceCase_Order_Agent
Feature: Creating a new Service Case from Order with Agent Profile
 
 1. Login to Mattress Firm Sales Force Application
 1.2 Creating a new Service Case from an Order (Agent) - SP1_TC19
 2. Closing a Service Case - SP1_TC20
 3. Creating a New Related Service Case - SP1_TC21
 4. Creating a New Blank Related Service Case Using the Related Case Component - SP1_TC22
  
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.2 Creating a new Service Case from an Order (Agent) - SP1_TC19
Given I click 'All_Dropdown_Agent'
And I click by JS 'ContactsFilter_ContactLead'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
#Order number example: SLP-9857045, AX-C005088650, SLP-9887857
And I clear the text and enter 'SLP-9857045' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I wait for '10' seconds
And I should see element 'ContactNameLink_ContactLead' present on page
And I click 'ContactNameLink_ContactLead'
#And I should see element 'ContactName_Orderheader' present on page
#And I click by JS 'ContactName_Orderheader'
And I wait for '5' seconds
And I should see element 'OrderNumber_Orderheader' present on page
And I click by JS 'OrderNumber_Orderheader'
And I wait for '5' seconds
And I should see element 'NewCase_Button_Order' present on page
And I click 'NewCase_Button_Order'
And I click by JS 'SelectRecordType_Contact'
And I select option 'Service' in dropdown 'SelectRecordType_Contact' by 'value'
And I click by JS 'NextButton_Contact' 
Then I should see element 'NewCase_Text_Agent' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click 'Phone_Text_Agent'
And I click 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click 'SubRecordType_Dropdown_Agent'
And I click by JS '120DayPrice_Option'
And I click 'TypeOfProduct(s)_Dropdown'
And I click by JS 'CoreProduct_Option'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I should see element 'OriginalOrderNumberX_Order' present on page
And I should see element 'ContactName_Order' present on page
And I click by JS 'ContactName_Order'
And I refresh the WebPage
And I wait for '10' seconds
And I click 'Previos_Tab_Agent'
And I scroll till element 'Cases_Order'
And I wait for '5' seconds
And I should see element 'Cases_Order' present on page
And I should see text '$$CaseNumber_Agent' contained on page at 'NewNumber_Order'
And I should see text 'Service' contained on page at 'CaseRecordType_Order'
And I click by JS 'Close_Agent' 


Scenario: 2. Closing a Service Case - SP1_TC20
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '10' seconds
And I click 'CaseNumber_Dropdown_Agent'
And I click 'Edit_Button_Close' Tab
And I should see element 'Edit_Header' present on page
And I click 'Status_Dropdown_Close'
And I click 'Closed_Text_Close'
And I click 'Save_Button_Close'
And I wait for '5' seconds
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent' 


Scenario:  3. Creating a New Related Service Case - SP1_TC21
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I should see element 'New_Button_Agent' present on page
And I click 'New_Button_Agent'
And I click by JS 'ServiceDropdown_Agent'
And I click by JS 'NextButtonAgent'
Then I should see element 'NewCase_Text_Agent' present on page
And I click by JS 'ContactNameField_CreateRelated'
And I wait for '5' seconds
And I click by JS 'ContactNameValue_CreateRelated'
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click by JS 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click 'SubRecordType_Dropdown_Agent'
And I click by JS '120DayPrice_Option'
And I click 'TypeOfProduct(s)_Dropdown'
And I click by JS 'CoreProduct_Option'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I click by JS 'Close_Agent' 
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseNumber_Agent' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I wait for '5' seconds
And I should see element 'Actions_Tab' present on page
#And I click by JS 'More_RelatedCase'
And I wait for '5' seconds
And I click by JS 'NewRelatedCase_CreateRelated'
And I should see text 'Are you sure you would like to Clone this Case? Click Next to Continue.' contained on page at 'Areyousure_Message_CreateRelated'
And I click by JS 'Next_Button_CreateRelated'
And I scroll till element 'RelatedCase_CreateRelated'
And I wait for '5' seconds
And I should see element 'NewRelatedCaseNumber_CreateRelated' present on page
And I get text from 'NewRelatedCaseNumber_CreateRelated' and store
And I should see element 'ParentCaseNumber_CreateRelated' present on page
And I get text from 'ParentCaseNumber_CreateRelated' and store
And I click by JS 'NewRelatedCaseNumber_CreateRelated'
And I refresh the WebPage
And I wait for '10' seconds
And I should see element 'ParentCase_field' present on page
And I should see text '$$ParentCaseNumber_CreateRelated' contained on page at 'ParentCase_field'
And I should see element 'Contact_Name_CreateRelated' present on page
#And I should see text 'Mark Ashton' contained on page at 'Contact_Name_CreateRelated'
And I should see text 'Phone' present on page at 'Case_Origin_CreateRelated'
And I should see text 'Phone' present on page at 'Source_CreateRelated'
And I should see text '120 Day Price Match Guarantee Request' present on page at 'SubRecordType_CreateRelated'
#And I should see text 'Inspection Not Required' present on page at 'SubType_CreateRelated'
#And I should see text 'Online Warranty Claim' present on page at 'Reason_CreateRelated'
#And I should see text 'Approved: Defective' present on page at 'CurrentState_CreateRelated'
And I should see text 'New' present on page at 'Status_CreateRelated'
And I click by JS 'Close_Agent' 


Scenario: 4. Creating a New Blank Related Service Case Using the Related Case Component - SP1_TC22
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I should see element 'New_Button_Agent' present on page
And I click 'New_Button_Agent'
And I click by JS 'ServiceDropdown_Agent'
And I click by JS 'NextButtonAgent'
Then I should see element 'NewCase_Text_Agent' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click by JS 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click 'SubRecordType_Dropdown_Agent'
And I click by JS '120DayPrice_Option'
And I click 'TypeOfProduct(s)_Dropdown'
And I click by JS 'CoreProduct_Option'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I click by JS 'Close_Agent' 
And I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see text '$$CaseNumber_Agent' contained on page at 'CaseNumber_Dropdown_Agent'
And I click 'CaseNumber_Dropdown_Agent'
And I wait for '5' seconds
And I scroll till element 'RelatedCase_CreateRelated'
And I wait for '5' seconds
And I scroll till element 'RelatedCase_CreateRelated'
And I wait for '5' seconds
And I should see element 'RelatedCase_CreateRelated' present on page
And I click by JS 'RelatedCases_Dropdown'
And I should see text 'No actions available' contained on page at 'NoActionsAvailable_Message'
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent'

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

@S5_ChatCustomQuickTextComponent_Agent
Feature: Chat(IS)-Custom QuickText Component-Agent
 
 1. Login to Mattress Firm Sales Force Application
1.1 To verify that Chat (IS) - Custom QuickText Component - SP5_TC2
 
Scenario: 1. Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'IS Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'IS Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 To verify that Chat (IS) - Custom QuickText Component - SP5_TC2
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
And I wait for '130' seconds
#And I refresh the WebPage
#And I wait for '15' seconds
And I should see element 'JoeTab' present on page 
And I scroll till element 'QuickText_Icon' 
And I wait for '5' seconds
And I should see element 'SettingsIcon_Custom' present on page 
And I click by JS 'SettingsIcon_Custom'
And I wait for '5' seconds
And I clear the text and enter 'Welcome' in field 'QuickTextNameField_Custom'
And Blur 'QuickTextNameField_Custom'
And I scroll till element 'Textarea_Custom'
And I clear the text and enter 'Hello' in field 'Textarea_Custom'
And Blur 'Textarea_Custom'
And I click by JS 'SaveButton_Custom'
And I wait for '5' seconds
And I should see element 'WelcomeText_Custom' present on page 
And I mouse over 'WelcomeText_Custom'
And I should see element 'HelloMessage_Custom' present on page 




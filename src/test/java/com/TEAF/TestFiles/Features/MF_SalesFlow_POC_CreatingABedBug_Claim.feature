@MF_SalesFlow_POC_Creating_a_BedBug_Claim
Feature: As an Agent Creating a bed bug claim service case

1. Login with Standard user Credentials
2. SP1_TC53 Creating a bed bug claim service case 
3. Verify logout of Sales Force application

Scenario: Login with Standard user Credentials
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: SP1_TC53 Creating a bed bug claim service case 
And I click by JS 'SearchTypeDD'
And I click by JS 'OrderHeaderGlobalSearch'
#for UAT
#And I enter '5165410544' in field 'GlobalSearchField'
#For QA
And I enter 'AX-S021442145' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '10' seconds
And I click by JS 'SearchTypeDD'
And I click by JS 'AllGlobalSearch'
And I clear field 'GlobalSearchField'
#for UAT
#And I enter '5165410544' in field 'GlobalSearchField'
#For QA
And I enter '9047448370' in field 'GlobalSearchField'

And I wait for '5' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '10' seconds
#And I should see element 'Contact_Header' present on page
#And I wait for '10' seconds
#And I scroll till element 'Contact_Header'
#And I wait for '10' seconds
And I scroll till element 'ChatTranscript_Header'
And I wait for '5' seconds
And I scroll till element 'Order_Header'
#And I scroll to 'element' - 'EmailMessages'
And I wait for '10' seconds
And I wait for visibility of element 'Order_Header'
And I should see element 'Order_Header' present on page
And I click by JS 'OrderHeaderOrderNumber'
And I wait for '5' seconds
And I click 'New_Case'
And I wait for '3' seconds
And I should see element 'NewCaseRecordTypeLabel' present on page
And I should see element 'NewCaseRecordTypeDD' present on page
And I click by JS 'NewCaseRecordTypeDD'
And I wait for '2' seconds
And I select option 'Service' in dropdown 'NewCaseRecordTypeDD' by 'text'
And I wait for '2' seconds
And I should see element 'NewCaseRecordTypeNext' present on page
And I click by JS 'NewCaseRecordTypeNext'
And I wait for '5' seconds
#And I refresh the WebPage
#And I should see element 'New_Case_Header' present on page
And I should see element 'New_Case_Header' present on page
And I should see element 'Case_Origin_Label' present on page
And I click 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click 'Source_DD'
And I click 'Source_DD_Value'
And I should see element 'TypeOfProduct' present on page
And I click by JS 'TypeOfProductDD'
And I click by JS 'TypeOfProductValue'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'BedBugSubRecordTypeValue'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I refresh the WebPage
And I wait for '3' seconds
And I should see element 'CaseNo' present on page
And I should see element 'CaseNumberValue' present on page
And I should see element 'OriginalOrderNumber' present on page_
And I should see element 'SubTabOriginalOrder' present on page
And I should see element 'SubTabOriginalOrderValueQA' present on page
And I wait for '2' seconds
And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'BedBugStatusDD_Value'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I wait for visibility of element 'CaseOwnerValue'
And I wait for visibility of element 'StoredCaseNumber'
And I get text from 'StoredCaseNumber' and store
And I should see element 'CaseOwner' present on page
And I should see element 'CaseOwnerValue' present on page
And I click by JS 'Close'
And I wait for '3' seconds


@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
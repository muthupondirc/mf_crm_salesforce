@MF_CreatingAttachedWarrantyCase
Feature: Creating Attached and UnAttached Warranty Case from MattressFirm Website
 
1. Creating Unattached Warranty E-claim case From Mattress firm Website -SP1_TC64
2. Creating Unattached Warranty Third Party Inspection Case From Mattress firm Website -SP1_TC65
3. Creating Attached Warranty E-claim case From Mattress firm Website -TC - SP1_TC66
4. Creating Attached Warranty Third Party Inspection Case From Mattress firm Website -SP1_TC67
 
Scenario: 1. Creating Unattached Warranty E-claim case From Mattress firm Website -SP1_TC64
Given My WebApp 'MF_SalesForce' is open 
#And I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/Home-Show' application
#And I wait for visibility of element 'MattressSite_CloseButton'
#And I click by JS 'MattressSite_CloseButton'
#---UAT temporary url-------------------------
#And I navigate to 'https://uat-mattressfirmcustomercommunity.cs165.force.com/s/warranty-claim' application
#---QA temporary url ------------------------
#And I navigate to 'https://qa-mattressfirmcustomercommunity.cs194.force.com/s/warranty-claim' application
And I navigate to 'https://demo-web-sleepys.demandware.net/s/Mattress-Firm/default/warranty-exclusions.html' application
#And I should see Window Authontication popup on page
And I wait for '10' seconds
#And I should see element 'HelpButton_Case' present on page
#And I mouse over 'HelpButton_Case'
#And I wait for '10' seconds
##And I should see element 'Popup_Dialog' present on page
##And I click popup close button
#And I should see element 'WarrantyClaims_Option' present on page
#And I click by JS 'WarrantyClaims_Option'
#And I should see element 'MattressFirmWarrantyInformation_Header' present on page
#And I scroll till element 'GetStarted_Button'
#And I should see element 'GetStarted_Button' present on page
#And I click by JS 'GetStarted_Button'
#And I scroll till element 'Continue_Button'
#And I wait for '5' seconds
And I should see element 'Continue_Button' present on page
And I click by JS 'Continue_Button'
And I should see element 'CustomerPortal_Header' present on page
And I should see element 'EmailField_Case' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_Case'
And I should see element 'PasswordField_Case' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_Case'
And I wait for '3' seconds
And Blur 'PasswordField_Case'
And I should see element 'LoginButton_Case' present on page
And I click by JS 'LoginButton_Case'
And I wait for '5' seconds
#And I should see element 'PopupClose_Button' present on page
#And I click by JS 'PopupClose_Button'
And I should see element 'Step1Select_Header' present on page
And I scroll till element 'Step1Select_NextButton'
And I should see element 'Step1Select_NextButton' present on page
And I click by JS 'Step1Select_NextButton'
And I should see element 'ContinueButton_Popup' present on page
And I click by JS 'ContinueButton_Popup'
And I should see element 'Step2TellUs_Header' present on page
And I should see element 'PartA_Header' present on page
And I should see element 'Brand_Dropdown_Case' present on page
And I select option 'Sealy' in dropdown 'Brand_Dropdown_Case' by 'value'
And I should see element 'ProductType_Dropdown_Case' present on page
And I select option 'Mattress and Box Spring' in dropdown 'ProductType_Dropdown_Case' by 'value'
And I should see element 'ProductSize_Dropdown_Case' present on page
And I select option 'Queen' in dropdown 'ProductSize_Dropdown_Case' by 'value'
And I should see element 'DefectReason_Dropdown_Case' present on page
And I select option 'My mattress is sagging' in dropdown 'DefectReason_Dropdown_Case' by 'value'
And I should see element 'FoundationType_Dropdown_Case' present on page
And I select option 'Metal' in dropdown 'FoundationType_Dropdown_Case' by 'value'
And I should see element 'legs_Dropdown_Case' present on page
And I select option '5' in dropdown 'legs_Dropdown_Case' by 'value'
And I should see element 'slats_Dropdown_Case' present on page
And I select option '1' in dropdown 'slats_Dropdown_Case' by 'value'
And I should see element 'Myproduct_Checkbox_Case' present on page
And I click by JS 'Myproduct_Checkbox_Case'
And I should see element 'Customerinformation_Textarea' present on page
And I clear the text and enter 'Mattress is sagging' in field 'Customerinformation_Textarea'
And I wait for '5' seconds
And I should see element 'uploadreceiptFiles' present on page
And I upload reciept files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds
And I should see element 'BestPhoneNumber_Field' present on page
And I clear the text and enter '516655784' in field 'BestPhoneNumber_Field'
And I should see element 'Any_Checkbox' present on page
And I click by JS 'Any_Checkbox'
And I should see element 'Street_Field' present on page
And I clear the text and enter '28 WoodBine DRS' in field 'Street_Field'
And I should see element 'City_Field' present on page
And I clear the text and enter 'Hicksville' in field 'City_Field'
And I should see element 'State_Field' present on page
And I clear the text and enter 'New York' in field 'State_Field'
And I should see element 'Zip_Field' present on page
And I clear the text and enter '11801' in field 'Zip_Field'
And I should see element 'Next_Button_PartB' present on page
And I click 'Next_Button_PartB'
And I wait for '5' seconds
And I should see element 'WarrantyeClaim_Header' present on page
And I should see element 'WarrantyeClaim_ContinueButton' present on page
And I click by JS 'WarrantyeClaim_ContinueButton'
And I should see element 'Step3_Header' present on page
And I should see element 'MainAffectedArea_1_Header' present on page
And I upload Mainarea files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds
And I should see element 'CloseUp_2_UploadFile' present on page
And I upload CloseUp Of Main Affected Area files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '3' seconds
And I should see element 'TopOfTheMattress_7_UploadFile' present on page
And I upload Photo Of Law Tag Attached files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '3' seconds
And I should see element 'PhotooftheSupport_11_UploadFile' present on page
And I upload Frame Support files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds 
And I click by JS 'SubmitButton_Step3'
And I wait for '5' seconds
And I should see element 'TrackStatus_Button' present on page
And I click by JS 'TrackStatus_Button'
And I wait for '10' seconds
And I get latest warranty case and store
And I mouse over 'MyAccount_Button'
And I click by JS 'SignOut_Link'
And I wait for '5' seconds

Scenario: 2. Creating Unattached Warranty Third Party Inspection Case From Mattress firm Website -SP1_TC65
Given My WebApp 'MF_SalesForce' is open 
#And I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/Home-Show' application
#And I wait for visibility of element 'MattressSite_CloseButton'
#And I click by JS 'MattressSite_CloseButton'
#And I wait for '10' seconds
#And I navigate to 'https://uat-mattressfirmcustomercommunity.cs165.force.com/s/warranty-claim' application
And I navigate to 'https://demo-web-sleepys.demandware.net/s/Mattress-Firm/default/warranty-exclusions.html' application
And I wait for '10' seconds
#And I should see element 'HelpButton_Case' present on page
#And I mouse over 'HelpButton_Case'
#And I wait for '10' seconds
##And I click popup close button
#And I should see element 'WarrantyClaims_Option' present on page
#And I click by JS 'WarrantyClaims_Option'
#And I should see element 'MattressFirmWarrantyInformation_Header' present on page
#And I scroll till element 'GetStarted_Button'
#And I should see element 'GetStarted_Button' present on page
#And I click by JS 'GetStarted_Button'
#And I scroll till element 'Continue_Button'
#And I wait for '5' seconds
And I should see element 'Continue_Button' present on page
And I click by JS 'Continue_Button'
And I should see element 'CustomerPortal_Header' present on page
And I should see element 'EmailField_Case' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_Case'
And I should see element 'PasswordField_Case' present on page
And Blur 'PasswordField_Case'
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_Case'
And I wait for '3' seconds
And I should see element 'LoginButton_Case' present on page
And I click by JS 'LoginButton_Case'
And I wait for '5' seconds
#qa not showing-------------------
#And I should see element 'PopupClose_Button' present on page
#And I click by JS 'PopupClose_Button'
And I should see element 'Step1Select_Header' present on page
And I scroll till element 'Step1Select_NextButton'
And I should see element 'Step1Select_NextButton' present on page
And I click by JS 'Step1Select_NextButton'
And I should see element 'ContinueButton_Popup' present on page
And I click by JS 'ContinueButton_Popup'
And I should see element 'Step2TellUs_Header' present on page
And I should see element 'PartA_Header' present on page
And I should see element 'Brand_Dropdown_Case' present on page
And I select option 'Sealy' in dropdown 'Brand_Dropdown_Case' by 'value'
And I should see element 'ProductType_Dropdown_Case' present on page
And I select option 'Mattress and Box Spring' in dropdown 'ProductType_Dropdown_Case' by 'value'
And I should see element 'ProductSize_Dropdown_Case' present on page
And I select option 'Queen' in dropdown 'ProductSize_Dropdown_Case' by 'value'
And I should see element 'DefectReason_Dropdown_Case' present on page
And I select option 'My mattress is sagging' in dropdown 'DefectReason_Dropdown_Case' by 'value'
And I should see element 'FoundationType_Dropdown_Case' present on page
And I select option 'Metal' in dropdown 'FoundationType_Dropdown_Case' by 'value'
And I should see element 'legs_Dropdown_Case' present on page
And I select option '5' in dropdown 'legs_Dropdown_Case' by 'value'
And I should see element 'slats_Dropdown_Case' present on page
And I select option '1' in dropdown 'slats_Dropdown_Case' by 'value'
And I should see element 'Myproduct_Checkbox_Case' present on page
And I click by JS 'Myproduct_Checkbox_Case'
And I should see element 'Customerinformation_Textarea' present on page
And I clear the text and enter 'Mattress is sagging' in field 'Customerinformation_Textarea'
And I wait for '5' seconds
And I should see element 'uploadreceiptFiles' present on page
And I upload reciept files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds
And I should see element 'BestPhoneNumber_Field' present on page
And I clear the text and enter '516655784' in field 'BestPhoneNumber_Field'
And I should see element 'Any_Checkbox' present on page
And I click by JS 'Any_Checkbox'
And I should see element 'Street_Field' present on page
And I clear the text and enter '28 WoodBine DRS' in field 'Street_Field'
And I should see element 'City_Field' present on page
And I clear the text and enter 'Hicksville' in field 'City_Field'
And I should see element 'State_Field' present on page
And I clear the text and enter 'New York' in field 'State_Field'
And I should see element 'Zip_Field' present on page
And I clear the text and enter '11801' in field 'Zip_Field'
And I should see element 'Next_Button_PartB' present on page
And I click 'Next_Button_PartB'
And I wait for '5' seconds
And I should see element 'ThirdPartyInspection_Header' present on page
And I should see element 'ThirdPartyInspection_ContinueButton' present on page
And I click by JS 'ThirdPartyInspection_ContinueButton'
And I wait for '5' seconds
And I should see element 'TrackStatus_Button' present on page
And I click by JS 'TrackStatus_Button'
And I wait for '10' seconds
And I get latest warranty case and store
And I mouse over 'MyAccount_Button'
And I click by JS 'SignOut_Link'
And I wait for '5' seconds

Scenario: 3. Creating Attached Warranty E-claim case From Mattress firm Website -SP1_TC66 
Given My WebApp 'MF_SalesForce' is open 
#And I navigate to 'https://uat-mattressfirmcustomercommunity.cs165.force.com/s/warranty-claim' application
And I navigate to 'https://demo-web-sleepys.demandware.net/s/Mattress-Firm/default/warranty-exclusions.html' application
#And I wait for '10' seconds
#And I should see element 'HelpButton_Case' present on page
#And I wait for '10' seconds
#And I mouse over 'HelpButton_Case'
#And I wait for '10' seconds
##And I click popup close button
#And I should see element 'WarrantyClaims_Option' present on page
#And I click by JS 'WarrantyClaims_Option'
#And I should see element 'MattressFirmWarrantyInformation_Header' present on page
#And I scroll till element 'GetStarted_Button'
#And I should see element 'GetStarted_Button' present on page
#And I click by JS 'GetStarted_Button'
#And I scroll till element 'Continue_Button'
#And I wait for '5' seconds
And I should see element 'Continue_Button' present on page
And I click by JS 'Continue_Button'
And I wait for '5' seconds
#-------------not in qa-------------
And I should see element 'CustomerPortal_Header' present on page
And I should see element 'EmailField_Case' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_Case'
And I should see element 'PasswordField_Case' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_Case'
And I wait for '3' seconds
And Blur 'PasswordField_Case'
And I should see element 'LoginButton_Case' present on page
And I click by JS 'LoginButton_Case'
And I wait for '5' seconds
#And I should see element 'PopupClose_Button' present on page
#And I click by JS 'PopupClose_Button'
And I should see element 'Step1Select_Header' present on page
#----------------uat---------------------------------
#And I should see element 'SeriesFirmMatt_Checkbox' present on page
#And I click by JS 'SeriesFirmMatt_Checkbox'
#----------------qa----------------------------------
And I should see element 'MattCheckboxQA_Attached' present on page
And I click by JS 'MattCheckboxQA_Attached'
#And I should see element 'SeriesBox_Checkbox' present on page
#And I click by JS 'SeriesBox_Checkbox'
And I scroll till element 'Step1Select_NextButton'
And I should see element 'Step1Select_NextButton' present on page
And I click by JS 'Step1Select_NextButton'
And I wait for '5' seconds
And I should see element 'Step2TellUs_Header' present on page
And I should see element 'PartA_Header' present on page
And I should see element 'Brand_Dropdown_Case' present on page
And I select option 'Sealy' in dropdown 'Brand_Dropdown_Case' by 'value'
And I should see element 'ProductType_Dropdown_Case' present on page
And I select option 'Mattress and Box Spring' in dropdown 'ProductType_Dropdown_Case' by 'value'
And I should see element 'ProductSize_Dropdown_Case' present on page
And I select option 'Queen' in dropdown 'ProductSize_Dropdown_Case' by 'value'
And I should see element 'DefectReason_Dropdown_Case' present on page
And I select option 'My mattress is sagging' in dropdown 'DefectReason_Dropdown_Case' by 'value'
And I should see element 'FoundationType_Dropdown_Case' present on page
And I select option 'Metal' in dropdown 'FoundationType_Dropdown_Case' by 'value'
And I should see element 'legs_Dropdown_Case' present on page
And I select option '5' in dropdown 'legs_Dropdown_Case' by 'value'
And I should see element 'slats_Dropdown_Case' present on page
And I select option '1' in dropdown 'slats_Dropdown_Case' by 'value'
And I should see element 'Myproduct_Checkbox_Case' present on page
And I click by JS 'Myproduct_Checkbox_Case'
And I should see element 'Customerinformation_Textarea' present on page
And I clear the text and enter 'Mattress is sagging' in field 'Customerinformation_Textarea'
And I wait for '5' seconds
And I should see element 'uploadreceiptFiles' present on page
And I upload reciept files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds
And I should see element 'BestPhoneNumber_Field' present on page
And I clear the text and enter '516655784' in field 'BestPhoneNumber_Field'
And I should see element 'Any_Checkbox' present on page
And I click by JS 'Any_Checkbox'
And I should see element 'Street_Field' present on page
And I clear the text and enter '28 WoodBine DRS' in field 'Street_Field'
And I should see element 'City_Field' present on page
And I clear the text and enter 'Hicksville' in field 'City_Field'
And I should see element 'State_Field' present on page
And I clear the text and enter 'New York' in field 'State_Field'
And I should see element 'Zip_Field' present on page
And I clear the text and enter '11801' in field 'Zip_Field'
And I should see element 'Next_Button_PartB' present on page
And I click 'Next_Button_PartB'
And I wait for '5' seconds
And I should see element 'WarrantyeClaim_Header' present on page
And I should see element 'WarrantyeClaim_ContinueButton' present on page
And I click by JS 'WarrantyeClaim_ContinueButton'
And I should see element 'Step3_Header' present on page
And I should see element 'MainAffectedArea_1_Header' present on page
And I upload Mainarea files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds
And I should see element 'CloseUp_2_UploadFile' present on page
And I upload CloseUp Of Main Affected Area files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds
And I should see element 'TopOfTheMattress_7_UploadFile' present on page
And I upload Photo Of Law Tag Attached files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds
And I should see element 'PhotooftheSupport_11_UploadFile' present on page
And I upload Frame Support files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds 
And I click by JS 'SubmitButton_Step3'
And I wait for '5' seconds
And I should see element 'TrackStatus_Button' present on page
And I click by JS 'TrackStatus_Button'
And I wait for '10' seconds
And I get latest warranty case and store
And I mouse over 'MyAccount_Button'
And I click by JS 'SignOut_Link'
And I wait for '5' seconds


Scenario: 4. Creating Attached Warranty Third Party Inspection Case From Mattress firm Website -SP1_TC67
Given My WebApp 'MF_SalesForce' is open 
#And I navigate to 'https://uat-mattressfirmcustomercommunity.cs165.force.com/s/warranty-claim' application
And I navigate to 'https://demo-web-sleepys.demandware.net/s/Mattress-Firm/default/warranty-exclusions.html' application
#And I wait for visibility of element 'MattressSite_CloseButton'
#And I click by JS 'MattressSite_CloseButton'
#And I wait for '10' seconds
#And I wait for '10' seconds
#And I should see element 'HelpButton_Case' present on page
#And I wait for '10' seconds
#And I mouse over 'HelpButton_Case'
#And I wait for '10' seconds
##And I click popup close button
#And I should see element 'WarrantyClaims_Option' present on page
#And I click by JS 'WarrantyClaims_Option'
#And I should see element 'MattressFirmWarrantyInformation_Header' present on page
#And I scroll till element 'GetStarted_Button'
#And I should see element 'GetStarted_Button' present on page
#And I click by JS 'GetStarted_Button'
#And I scroll till element 'Continue_Button'
#And I wait for '5' seconds
And I should see element 'Continue_Button' present on page
And I click by JS 'Continue_Button'
#-------------not in qa-------------
#And I wait for '5' seconds
And I should see element 'CustomerPortal_Header' present on page
And I should see element 'EmailField_Case' present on page
And I clear the text and enter 'jaredsteiner813+500@gmail.com' in field 'EmailField_Case'
And I should see element 'PasswordField_Case' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_Case'
And I wait for '3' seconds
And Blur 'PasswordField_Case'
And I should see element 'LoginButton_Case' present on page
And I click by JS 'LoginButton_Case'
And I wait for '5' seconds
#And I should see element 'PopupClose_Button' present on page
#And I click by JS 'PopupClose_Button'
And I should see element 'Step1Select_Header' present on page
#----------------uat---------------------------------
#And I should see element 'SeriesFirmMatt_Checkbox' present on page
#And I click by JS 'SeriesFirmMatt_Checkbox'
#And I should see element 'SeriesBox_Checkbox' present on page
#And I click by JS 'SeriesBox_Checkbox'
#----------------qa----------------------------------
And I should see element 'MattCheckboxQA_Attached' present on page
And I click by JS 'MattCheckboxQA_Attached'
And I scroll till element 'Step1Select_NextButton'
And I should see element 'Step1Select_NextButton' present on page
And I click by JS 'Step1Select_NextButton'
And I wait for '5' seconds
And I should see element 'Step2TellUs_Header' present on page
And I should see element 'PartA_Header' present on page
And I should see element 'Brand_Dropdown_Case' present on page
And I select option 'Sealy' in dropdown 'Brand_Dropdown_Case' by 'value'
And I should see element 'ProductType_Dropdown_Case' present on page
And I select option 'Mattress and Box Spring' in dropdown 'ProductType_Dropdown_Case' by 'value'
And I should see element 'ProductSize_Dropdown_Case' present on page
And I select option 'Queen' in dropdown 'ProductSize_Dropdown_Case' by 'value'
And I should see element 'DefectReason_Dropdown_Case' present on page
And I select option 'My mattress is sagging' in dropdown 'DefectReason_Dropdown_Case' by 'value'
And I should see element 'FoundationType_Dropdown_Case' present on page
And I select option 'Metal' in dropdown 'FoundationType_Dropdown_Case' by 'value'
And I should see element 'legs_Dropdown_Case' present on page
And I select option '5' in dropdown 'legs_Dropdown_Case' by 'value'
And I should see element 'slats_Dropdown_Case' present on page
And I select option '1' in dropdown 'slats_Dropdown_Case' by 'value'
And I should see element 'Myproduct_Checkbox_Case' present on page
And I click by JS 'Myproduct_Checkbox_Case'
And I should see element 'Customerinformation_Textarea' present on page
And I clear the text and enter 'Mattress is sagging' in field 'Customerinformation_Textarea'
And I wait for '5' seconds
And I should see element 'uploadreceiptFiles' present on page
And I upload reciept files
And I wait for '20' seconds
And I click by JS 'Done_Button'
And I wait for '5' seconds
And I should see element 'BestPhoneNumber_Field' present on page
And I clear the text and enter '516655784' in field 'BestPhoneNumber_Field'
And I should see element 'Any_Checkbox' present on page
And I click by JS 'Any_Checkbox'
And I should see element 'Street_Field' present on page
And I clear the text and enter '28 WoodBine DRS' in field 'Street_Field'
And I should see element 'City_Field' present on page
And I clear the text and enter 'Hicksville' in field 'City_Field'
And I should see element 'State_Field' present on page
And I clear the text and enter 'New York' in field 'State_Field'
And I should see element 'Zip_Field' present on page
And I clear the text and enter '11801' in field 'Zip_Field'
And I should see element 'Next_Button_PartB' present on page
And I click 'Next_Button_PartB'
And I wait for '5' seconds
And I should see element 'ThirdPartyInspection_Header' present on page
And I should see element 'ThirdPartyInspection_ContinueButton' present on page
And I click by JS 'ThirdPartyInspection_ContinueButton'
And I wait for '5' seconds
And I should see element 'TrackStatus_Button' present on page
And I click by JS 'TrackStatus_Button'
And I wait for '10' seconds
And I get latest warranty case and store
And I mouse over 'MyAccount_Button'
And I click by JS 'SignOut_Link'

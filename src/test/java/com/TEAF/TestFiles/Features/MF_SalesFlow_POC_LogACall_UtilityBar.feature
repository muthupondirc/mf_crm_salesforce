@MF_SalesFlow_POC_LogACall_Utility
Feature: As an agent Add a call through a Utility bar
1. Login with Standard user Credentials
2.SP1_TC55 Log a call using utility bar
3. Verify logout of Sales Force application

Scenario: Login with Standard user Credentials
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '3' seconds

Scenario: SP1_TC55 Log a call using utility bar 
Given I should see element 'Log_A_Callbtn' present on page
And I click 'Log_A_Callbtn'
And I wait for '2' seconds
And I should see element 'Log_Call_header' present on page
And I should see element 'Call_Type_label' present on page
And I click 'Call_Type_DD'
And I click 'Call_Type_Warranty_Process'
And I should see element 'Subject_Txt' present on page
And I clear the text and enter 'Bottom Panel' in field 'Subject_Txt'
And I should see element 'Comment_Txt_Field' present on page
And I enter 'Testing 1,2,3' in field 'Comment_Txt_Field'
And I click 'Next_btn'
And I click by JS 'Minimize_btn' 
And I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Cases' 
And I hit tab-key on element 'SearchTypeDD'
# for Uat DUMMY CASE 
And I enter '03099220' in field 'GlobalSearchField'
#fOR QA DUMMY CASE
#And I enter '03090524' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '2' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'ActivityHistory' present on page
And I should see element 'DummyCaseSubject' present on page
And I should see element 'ActivityDate' present on page
And I wait for '2' seconds
And I click by JS 'DummyCaseActivityView' 
And I wait for '3' seconds
And I switch to iFrame 'IframeContent'
And I wait for '3' seconds
And I should see element 'AllActivityHistory' present on page
And I should see element 'LastModified' present on page
And I should see element 'MFRMCallTypeActivity' present on page
And I should see element 'ActivityComments' present on page
And I switch back to Main Window
And I wait for '3' seconds
And I click by JS 'Close'

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page


@MF_SalesForce_Customer_Service_Display_number_of_Escalate_to_Leads
Feature: Sprint9_Customer Service - Display number of Escalate to Leads
1.Login with servicelead
2.SP9_1 Validate the number of escalate to leads status change in a service case
3.Verify logout of Sales Force application

Scenario: Creating a Login Issue Case from Mattress Firm Web Site
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'servicelead' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'servicelead_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: SP_16843 Validate the number of times the escalate to leads status change in a service case
And I click by JS 'ShowNavMenu'
And I wait for '2' seconds
And I click by JS 'NavCases'
And I wait for '5' seconds
And I click by JS 'lnkCases'
And I wait for '2' seconds
And I should see element 'CreateNewCase' present on page
And I click by JS 'CreateNewCase'
And I wait for '5' seconds
And I should see element 'NewCaseSelectionBtn' present on page
And I should see element 'NewCaseSelectionLabel' present on page
And I click by JS 'NewCaseSelectionBtn'
And I should see element 'NewCaseSelectionNxt' present on page
And I click by JS 'NewCaseSelectionNxt'
And I wait for '5' seconds
And I should see element 'New_Case_Header' present on page
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'SubRecordType_NeedOption_Value'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'CaseNo' present on page
And I should see element 'CaseNumberValue' present on page
And I get text from 'CaseNumberValue' and store
#And I should see element 'Contact_Label' present on page
And I wait for '2' seconds
And I should see element 'lbl_NumberOfTimesEscalated' present on page
And I should see text '0' present on page at 'txtValue_NumberOfTimesEscalated'
And I should see element 'lbl_EscalatedAgent' present on page
And I should see element 'lbl_EscalatedDate' present on page
And I click by JS 'EscalateEdit'
And I wait for '5' seconds
And I scroll to 'element' - 'Case_Classification'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_Escalate'
And I wait for '2' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I scroll till element 'EscalateOwner'
And I wait for '3' seconds
And I should see element 'EscalateToLeadCheckBox' present on page
And I should see element 'lbl_NumberOfTimesEscalated' present on page
And I should see text '1' present on page at 'txtValue_NumberOfTimesEscalated'
And I should see element 'lbl_EscalatedAgent' present on page
And I should see element 'lbl_EscalatedDate' present on page
And I should see element 'txtValue_EscalatedDate' present on page
And I wait for '2' seconds
And I click by JS 'EscalateEdit'
And I should see element 'TypeOfProduct' present on page
And I click by JS 'TypeOfProductDD'
And I click by JS 'TypeOfProductValue'
And I wait for '2' seconds
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I should see element 'lbl_NumberOfTimesEscalated' present on page
And I should see text '1' present on page at 'txtValue_NumberOfTimesEscalated'
And I wait for '2' seconds
And I click by JS 'EscalateEdit'
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_Payment'
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'ShowOptionButtonToAccept' present on page
And I click by JS 'ShowOptionButtonToAccept'
And I wait for '5' seconds
And I click by JS 'AcceptButton'
And I wait for '5' seconds
And I click by JS 'EscalateEdit'
And I wait for '2' seconds
And I should see element 'BedBugStatus' present on page
And I should see element 'BedBugStatusDD' present on page
And I click by JS 'BedBugStatusDD'
And I wait for '2' seconds
And I click by JS 'Status_Escalate'
And I click by JS 'BedBugSave'
And I should see element 'lbl_NumberOfTimesEscalated' present on page
And I should see text '2' present on page at 'txtValue_NumberOfTimesEscalated'
And I wait for '5' seconds
And I click by JS 'Close'
And I wait for '5' seconds

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

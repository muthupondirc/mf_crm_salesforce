@MF_SalesForce_Sprint3Cases
Feature: Sprint 3 cases.
1.
1.SP3_TC3 Track Escalated & Payment Cases to Leads
2.SP3_TC5 Service-Lead Service case
3.SP3_TC7 Route Vendor Inspections with Email/Chatter back to Warranty 3rd Party Inspections queue
4.SP3_TC4 Report for telesales user
5.Verify logout of Sales Force application
Scenario: Creating a Login Issue Case from Mattress Firm Web Site
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

#Scenario: SP3_TC3 Track Escalated & Payment Cases to Leads
#And I click by JS 'ShowNavMenu'
#And I wait for '2' seconds
#And I click by JS 'NavCases'
#And I wait for '5' seconds
#And I should see element 'CreateNewCase' present on page
#And I click by JS 'CreateNewCase'
#And I wait for '5' seconds
#And I should see element 'NewCaseSelectionBtn' present on page
#And I should see element 'NewCaseSelectionLabel' present on page
#And I click by JS 'NewCaseSelectionBtn'
#And I should see element 'NewCaseSelectionNxt' present on page
#And I click by JS 'NewCaseSelectionNxt'
#And I wait for '5' seconds
#And I should see element 'New_Case_Header' present on page
#And I should see element 'Case_Origin_Label' present on page
#And I click by JS 'Case_Origin_DD'
#And I click by JS 'Case_Origin_DD_Phone'
#And I wait for '2' seconds
#And I scroll to 'element' - 'Case_Classification'
#And I should see element 'Source_Label' present on page
#And I click by JS 'Source_DD'
#And I click by JS 'Source_DD_Value'
#And I should see element 'TypeOfProduct' present on page
#And I click by JS 'TypeOfProductDD'
#And I click by JS 'TypeOfProductValue'
#And I should see element 'SubRecordTypeDD' present on page
#And I click by JS 'SubRecordTypeDD'
#And I click by JS 'SubRecordType_NeedOption_Value'
#And I wait for '5' seconds
#And I click by JS 'BedBugSave'
#And I wait for '5' seconds
#And I should see element 'CaseNo' present on page
#And I should see element 'CaseNumberValue' present on page
#And I get text from 'CaseNumberValue' and store
##And I should see element 'Contact_Label' present on page
#And I wait for '2' seconds
#And I click by JS 'EscalateEdit'
#And I wait for '5' seconds
#And I scroll to 'element' - 'Case_Classification'
#And I wait for '2' seconds
#And I should see element 'BedBugStatus' present on page
#And I should see element 'BedBugStatusDD' present on page
#And I click by JS 'BedBugStatusDD'
#And I wait for '2' seconds
#And I click by JS 'Status_Escalate'
#And I wait for '2' seconds
#And I click by JS 'BedBugSave'
#And I wait for '5' seconds
#And I scroll till element 'EscalateOwner'
#And I wait for '3' seconds
#And I should see element 'EscalateToLeadCheckBox' present on page
#And I wait for '2' seconds
#And I click by JS 'Close'
#And I wait for '5' seconds
#And I should see element 'CreateNewCase' present on page
#And I click by JS 'CreateNewCase'
#And I wait for '5' seconds
#And I should see element 'NewCaseSelectionBtn' present on page
#And I should see element 'NewCaseSelectionLabel' present on page
#And I click by JS 'NewCaseSelectionBtn'
#And I should see element 'NewCaseSelectionNxt' present on page
#And I click by JS 'NewCaseSelectionNxt'
#And I wait for '5' seconds
#And I should see element 'New_Case_Header' present on page
#And I should see element 'Case_Origin_Label' present on page
#And I click by JS 'Case_Origin_DD'
#And I click by JS 'Case_Origin_DD_Phone'
#And I wait for '2' seconds
#And I scroll to 'element' - 'Case_Classification'
#And I should see element 'Source_Label' present on page
#And I click by JS 'Source_DD'
#And I click by JS 'Source_DD_Value'
#And I should see element 'TypeOfProduct' present on page
#And I click by JS 'TypeOfProductDD'
#And I click by JS 'TypeOfProductValue'
#And I should see element 'SubRecordTypeDD' present on page
#And I click by JS 'SubRecordTypeDD'
#And I click by JS 'SubRecordType_NeedOption_Value'
#And I wait for '5' seconds
#And I click by JS 'BedBugSave'
#And I wait for '5' seconds
#And I should see element 'CaseNo' present on page
#And I should see element 'CaseNumberValue' present on page
#And I get text from 'CaseNumberValue' and store
##And I should see element 'Contact_Label' present on page
#And I wait for '2' seconds
#And I click by JS 'EscalateEdit'
#And I wait for '5' seconds
#And I scroll to 'element' - 'Case_Classification'
#And I wait for '2' seconds
#And I should see element 'BedBugStatus' present on page
#And I should see element 'BedBugStatusDD' present on page
#And I click by JS 'BedBugStatusDD'
#And I wait for '2' seconds
#And I wait for '2' seconds
#And I click by JS 'Status_Payment'
#And I wait for '2' seconds
#And I click by JS 'BedBugSave'
#And I wait for '5' seconds
#And I scroll till element 'EscalateOwner'
#And I wait for '3' seconds
#And I should see element 'PaymentCheckBox' present on page
#And I wait for '2' seconds
#And I click by JS 'Close'
#And I wait for '5' seconds
#
#@Ignore
#Scenario: Verify logout of Sales Force application
#Given I click by JS 'UserImageButton'
#And I click link 'Log Out'
#And I should see element 'login_username' present on page
#
#
#Scenario: Creating a Login Issue Case from Mattress Firm Web Site
#Given My WebApp 'MF_SalesForce' is open
#And I wait for '5' seconds
#When I login to Mattress Firm Sales Force with valid 'agent_UAT' User Credentials
#And I close all the opened tabs 'iconClose'
#And I wait for '5' seconds
#And I click by JS 'UserImageButton'
#And I should see profile name 'agent_DisplayName' present on page at 'ProfileCardPopUp'
#And I click by JS 'UserImageButton'
#
#
#Scenario: SP3_TC5 Service-Lead Service case
#
#And I click by JS 'ShowNavMenu'
#And I wait for '2' seconds
#And I click by JS 'NavCases'
#And I wait for '5' seconds
#And I should see element 'CreateNewCase' present on page
#And I click by JS 'CreateNewCase'
#And I wait for '5' seconds
#And I should see element 'NewCaseSelectionBtn' present on page
#And I should see element 'NewCaseSelectionLabel' present on page
#And I click by JS 'NewCaseSelectionBtn'
#And I should see element 'NewCaseSelectionNxt' present on page
#And I click by JS 'NewCaseSelectionNxt'
#And I wait for '5' seconds
#And I should see element 'New_Case_Header' present on page
#And I should see element 'Case_Origin_Label' present on page
#And I click by JS 'Case_Origin_DD'
#And I click by JS 'Case_Origin_DD_Phone'
#And I wait for '2' seconds
#And I scroll to 'element' - 'Case_Classification'
#And I should see element 'Source_Label' present on page
#And I click by JS 'Source_DD'
#And I click by JS 'Source_DD_Value'
#And I should see element 'TypeOfProduct' present on page
#And I click by JS 'TypeOfProductDD'
#And I click by JS 'TypeOfProductValue'
#And I should see element 'SubRecordTypeDD' present on page
#And I click by JS 'SubRecordTypeDD'
#And I click by JS 'SubRecordType_NeedOption_Value'
#And I wait for '5' seconds
#And I click by JS 'BedBugSave'
#And I wait for '5' seconds
#And I should see element 'CaseNo' present on page
#And I should see element 'CaseNumberValue' present on page
#And I get text from 'CaseNumberValue' and store
##And I should see element 'Contact_Label' present on page
#And I wait for '2' seconds
#And I click by JS 'EscalateEdit'
#And I wait for '5' seconds
#And I scroll to 'element' - 'Case_Classification'
#And I wait for '2' seconds
#And I should see element 'BedBugStatus' present on page
#And I should see element 'BedBugStatusDD' present on page
#And I click by JS 'BedBugStatusDD'
#And I wait for '2' seconds
#And I click by JS 'Status_Escalate'
#And I wait for '2' seconds
#And I click by JS 'BedBugSave'
#And I wait for '5' seconds
#And I scroll till element 'EscalateOwner'
#And I wait for '3' seconds
#And I should see element 'EscalateToLeadCheckBox' present on page
#And I wait for '2' seconds
#And I click by JS 'Close'
#And I wait for '5' seconds
#And I should see element 'CreateNewCase' present on page
#And I click by JS 'CreateNewCase'
#And I wait for '5' seconds
#And I should see element 'NewCaseSelectionBtn' present on page
#And I should see element 'NewCaseSelectionLabel' present on page
#And I click by JS 'NewCaseSelectionBtn'
#And I should see element 'NewCaseSelectionNxt' present on page
#And I click by JS 'NewCaseSelectionNxt'
#And I wait for '5' seconds
#And I should see element 'New_Case_Header' present on page
#And I should see element 'Case_Origin_Label' present on page
#And I click by JS 'Case_Origin_DD'
#And I click by JS 'Case_Origin_DD_Phone'
#And I wait for '2' seconds
#And I scroll to 'element' - 'Case_Classification'
#And I should see element 'Source_Label' present on page
#And I click by JS 'Source_DD'
#And I click by JS 'Source_DD_Value'
#And I should see element 'TypeOfProduct' present on page
#And I click by JS 'TypeOfProductDD'
#And I click by JS 'TypeOfProductValue'
#And I should see element 'SubRecordTypeDD' present on page
#And I click by JS 'SubRecordTypeDD'
#And I click by JS 'SubRecordType_NeedOption_Value'
#And I wait for '5' seconds
#And I click by JS 'BedBugSave'
#And I wait for '5' seconds
#And I should see element 'CaseNo' present on page
#And I should see element 'CaseNumberValue' present on page
#And I get text from 'CaseNumberValue' and store
##And I should see element 'Contact_Label' present on page
#And I wait for '2' seconds
#And I click by JS 'EscalateEdit'
#And I wait for '5' seconds
#And I scroll to 'element' - 'Case_Classification'
#And I wait for '2' seconds
#And I should see element 'BedBugStatus' present on page
#And I should see element 'BedBugStatusDD' present on page
#And I click by JS 'BedBugStatusDD'
#And I wait for '2' seconds
#And I wait for '2' seconds
#And I click by JS 'Status_Payment'
#And I wait for '2' seconds
#And I click by JS 'BedBugSave'
#And I wait for '5' seconds
#And I scroll till element 'EscalateOwner'
#And I wait for '3' seconds
#And I should see element 'PaymentCheckBox' present on page
#And I wait for '2' seconds
#And I click by JS 'Close'
#And I wait for '5' seconds

Scenario: SP3_TC7 Route Vendor Inspections with Email/Chatter back to Warranty 3rd Party Inspections queue
#Given My WebApp 'MF_SalesForce' is open 
#ForQA
#And I Switch the Tab and enter the url 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/default/warranty-exclusions.html'
#And I navigate to 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/default/warranty-exclusions.html' application
##----------------
#ForUAT

And I Switch the Tab and enter the url 'MFWarrantyExclusions'
And I navigate to 'https://storefront:face@dev.mattressfirm.com/warranty-exclusions.html' application
#----------------

And I wait for '10' seconds
And I should see element 'EmailField_CaseMf' present on page
And I clear the text and enter 'jaredsteiner813+501@gmail.com' in field 'EmailField_CaseMf'
And I should see element 'PasswordField_CaseMf' present on page
And I clear the text and enter 'M@ttr3ss123' in field 'PasswordField_CaseMf'
And I wait for '5' seconds
And I should see element 'LoginButton_CaseMf' present on page
And I click by JS 'LoginButton_CaseMf'
And I wait for '5' seconds

#--------------

#And I should see element 'HelpButton_Mf' present on page
#And I wait for '10' seconds
#And I mouse over 'HelpButton_Mf'
#And I wait for '10' seconds
#And I click by JS 'PopupClose_ButtonMf'
#And I should see element 'WarrantyClaims_OptionMf' present on page
#And I click by JS 'WarrantyClaims_OptionMf'
#And I should see element 'MattressFirmWarrantyInformation_HeaderMf' present on page
#And I scroll till element 'GetStarted_ButtonMf'
#And I should see element 'GetStarted_ButtonMf' present on page
#And I click by JS 'GetStarted_ButtonMf'
#And I scroll till element 'Continue_ButtonMf'
#And I wait for '5' seconds
#And I should see element 'Continue_ButtonMf' present on page
#And I click by JS 'Continue_ButtonMf'
#And I wait for '5' seconds
#And I should see element 'CustomerPortal_HeaderMf' present on page
#And I should see element 'PopupClose_ButtonMf' present on page
#And I click by JS 'PopupClose_ButtonMf'
#------------

And I should see element 'Step1Select_HeaderMf' present on page
#And I should see element 'SeriesFirmMatt_Checkbox' present on page
#And I click by JS 'SeriesFirmMatt_Checkbox'
#And I should see element 'SeriesBox_Checkbox' present on page
#And I click by JS 'SeriesBox_Checkbox'
And I scroll till element 'Step1Select_NextButtonMf'
And I should see element 'Step1Select_NextButtonMf' present on page
And I click by JS 'Step1Select_NextButtonMf'
And I wait for '5' seconds
And I click by JS 'ContinuePopup'
And I should see element 'Step2TellUs_HeaderMf' present on page
And I should see element 'PartA_HeaderMf' present on page
And I should see element 'Brand_Dropdown_CaseMf' present on page
And I select option 'Sealy' in dropdown 'Brand_Dropdown_CaseMf' by 'value'
And I should see element 'ProductType_Dropdown_CaseMf' present on page
And I select option 'Mattress and Box Spring' in dropdown 'ProductType_Dropdown_CaseMf' by 'value'
And I should see element 'ProductSize_Dropdown_CaseMf' present on page
And I select option 'Queen' in dropdown 'ProductSize_Dropdown_CaseMf' by 'value'
And I should see element 'DefectReason_Dropdown_CaseMf' present on page
And I select option 'My mattress is sagging' in dropdown 'DefectReason_Dropdown_CaseMf' by 'value'
And I should see element 'FoundationType_Dropdown_CaseMf' present on page
And I select option 'Metal' in dropdown 'FoundationType_Dropdown_CaseMf' by 'value'
And I should see element 'legs_Dropdown_CaseMf' present on page
And I select option '5' in dropdown 'legs_Dropdown_CaseMf' by 'value'
And I should see element 'slats_Dropdown_CaseMf' present on page
And I select option '1' in dropdown 'slats_Dropdown_CaseMf' by 'value'
And I should see element 'Myproduct_Checkbox_CaseMf' present on page
And I click by JS 'Myproduct_Checkbox_CaseMf'
And I should see element 'Customerinformation_TextareaMf' present on page
And I clear the text and enter 'Mattress is sagging' in field 'Customerinformation_TextareaMf'
And I wait for '5' seconds
And I should see element 'uploadreceiptFilesMf' present on page
And I upload reciept files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'BestPhoneNumber_FieldMf' present on page
And I clear the text and enter '516655784' in field 'BestPhoneNumber_FieldMf'
And I should see element 'Any_CheckboxMf' present on page
And I click by JS 'Any_CheckboxMf'
And I should see element 'Street_FieldMf' present on page
And I clear the text and enter '28 WoodBine DRS' in field 'Street_FieldMf'
And I should see element 'City_FieldMf' present on page
And I clear the text and enter 'Hicksville' in field 'City_FieldMf'
And I should see element 'State_FieldMf' present on page
And I clear the text and enter 'New York' in field 'State_FieldMf'
And I should see element 'Zip_FieldMf' present on page
And I clear the text and enter '11801' in field 'Zip_FieldMf'
And I should see element 'Next_Button_PartBMf' present on page
And I click 'Next_Button_PartBMf'
And I wait for '5' seconds
And I should see element 'WarrantyeClaim_HeaderMf' present on page
And I should see element 'WarrantyeClaim_ContinueButtonMf' present on page
And I click by JS 'WarrantyeClaim_ContinueButtonMf'
And I should see element 'Step3_HeaderMf' present on page
And I should see element 'MainAffectedArea_1_HeaderMf' present on page
And I upload Mainarea files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'CloseUp_2_UploadFileMf' present on page
And I upload CloseUp Of Main Affected Area files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'TopOfTheMattress_7_UploadFileMf' present on page
And I upload Photo Of Law Tag Attached files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds
And I should see element 'PhotooftheSupport_11_UploadFileMf' present on page
And I upload Frame Support files
And I wait for '20' seconds
And I click by JS 'Done_ButtonMf'
And I wait for '5' seconds 
And I click by JS 'SubmitButton_Step3Mf'
And I wait for '5' seconds
And I should see element 'TrackStatus_ButtonMf' present on page
And I click by JS 'TrackStatus_ButtonMf'
And I wait for '10' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I get latest warranty case and store
And I click by JS latest warranty case
And I wait for '5' seconds
And I Switch between Tab '2'
And I wait for '5' seconds
And I should see element 'ShareUpdate' present on page
And I click by JS 'ShareUpdate'
And I enter 'New Post' in field 'ShareTextField'
And I should see element 'Share' present on page
And I click by JS 'Share'
And I wait for '5' seconds
#And I mouse over 'MyAccount_Button'
#And I click by JS 'SignOut_Link'
#And I wait for '5' seconds
And I Switch between Tab '0'
And I wait for '5' seconds
And I should see element 'ShowNavMenu' present on page
And I click by JS 'ShowNavMenu'
And I wait for '3' seconds
And I should see element 'HomeMenu' present on page
And I click by JS 'HomeMenu'
And I wait for '5' seconds
And I click by JS 'SearchTypeDD'
And I wait for '3' seconds
And I click by JS 'SearchType_Cases'
And I wait for '3' seconds
And I hit tab-key on element 'SearchTypeDD'
And I enter from stored variable '$$value1' into feild 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '2' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '5' seconds
And I should see element 'NewChatterCaseOwner' present on page
And I should see element 'NewChatter' present on page
And I click by JS 'NextToEdit_DD'
And I click by JS 'AcceptCaseDD'
And I should see element 'AcceptWarrantyCase' present on page
And I click by JS 'AcceptWarrantyCase'
#And I should see element 'AgentCaseOwner' present on page
And I should see CaseOwner name 'agent_DisplayName' present on page
And I should see element 'ChatterPost' present on page
And I click by JS 'ChatterPost'
And I wait for '2' seconds
And I should see element 'ChatterAddComments' present on page
And I click by JS 'ChatterAddComments'
And I should see element 'ChatterComments' present on page
And I click by JS 'ChatterComments'
And I should see element 'ChatterCommentsTextField' present on page
And I enter 'GoodBye' in field 'ChatterCommentsTextField'
And I click by JS 'CommentPostButton'
And I wait for '3' seconds
And I click by JS 'AgentActions'
And I click by JS 'RefreshDropdown'
And I should see element 'RefreshTab' present on page
And I click by JS 'RefreshTab'
And I should see element 'NewChatter' present on page
And I click by JS 'WarrantyEditBtn'
And I wait for '3' seconds
And I scroll till element 'WarrantyClassification'
And I wait for '3' seconds
And I should see element 'WarrantyEditStatus' present on page
And I click by JS 'WarrantyEditStatus'
And I should see element 'WarrantyEditStatusValue' present on page
And I click by JS 'WarrantyEditStatusValue'
And I should see element 'EditCurrentState' present on page
And I click by JS 'EditCurrentState'
And I should see element 'EditCurrentStateValue' present on page
And I click by JS 'EditCurrentStateValue'
And I should see element 'EditResolutionDD' present on page
And I click by JS 'EditResolutionDD'
And I should see element 'EditResolutionValue' present on page
And I click by JS 'EditResolutionValue'
And I wait for '2' seconds
And I click by JS 'WarrantyClaimSave'
And I wait for '5' seconds
And I should see element 'ChatterPost' present on page
And I click by JS 'Close'


@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

Scenario:  Login into Saleforce using Telesales user
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'telesales' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'telesales_displayname' present on page at 'ProfileCardPopUp'

Scenario: SP3_TC4 Report for telesales user
And I click by JS 'ShowNavMenu'
And I wait for '2' seconds
And I click by JS 'Reports'
And I wait for '5' seconds
And I should see element 'Recent' present on page
And I should see element 'SearchReports' present on page
And I click by JS 'SearchReports'
And I enter 'Leads - Store Referral' in field 'SearchReports'
And I hit enter-key on element 'SearchReports'
And I wait for '5' seconds
And I click by JS 'LeadsStoreReferralReport'
And I wait for '5' seconds
And I switch to iFrame 'ReportView'
And I wait for visibility of element 'StoreReferralReport'
And I should see element 'StoreReferralReport' present on page
And I wait for '5' seconds
#And I validate the table
And I switch back to Main Window
And I wait for '5' seconds
@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page







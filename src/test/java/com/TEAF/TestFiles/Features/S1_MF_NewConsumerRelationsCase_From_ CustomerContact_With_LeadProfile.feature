@MF_ConsumerRelationsCase_CustomeContact_Lead
Feature: Creating a new Consumer Relations Case from Custome Contact with Lead Profile

 1. Login to Mattress Firm Sales Force Application
 1.2  Add Warning to Contact (Lead and above) - SP1_TC33
 2. Remove Warning from Contact (Lead and above)  - SP1_TC34
 3. Creating a new Consumer Relations Case from Customer Contact (Lead and above) - SP1_TC35
  
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Sr Mgr User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Sr Mgr User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1. Add Warning to Contact (Lead and above) - SP1_TC33
Given I click 'All_Dropdown_Agent'
And I click by JS 'ContactsFilter_ContactLead'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I clear the text and enter 'Jared Steiner' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I should see element 'ContactNameLink_ContactLead' present on page
And I click 'ContactNameLink_ContactLead'
And I wait for '10' seconds
And I should see element 'Add/RemoveWarning_Button' present on page
And I wait for '10' seconds
And I click 'Add/RemoveWarning_Button'
And I wait for '3' seconds
And I click 'WarningReason_Dropdown'
And I click 'ContactLeadServices_Option'
And I click 'SaveButton_Warning'
And I wait for '3' seconds
And I scroll till element 'NewCase_ContactLead'
And I wait for '3' seconds
And I should see element 'HighlightPanel' present on page
And I should see element 'CUSTOMERWARNING' present on page
And I should see element 'RedFlag' present on page
And I mouse over 'RedFlag'
And I wait for '5' seconds
And I should see element 'ContactLeadServices_Text' present on page
And I scroll till element 'ContactHistory_Tab'
And I wait for '10' seconds
And I scroll till element 'ContactHistory_Tab'
And I wait for '10' seconds
Then I should see element - 'NewValue_Warning' contains text 'Contact Lead Services'
And I scroll till element 'NewCase_ContactLead'
And I wait for '5' seconds
And I should see element 'Details_Tab' present on page
And I click 'Details_Tab'
And I scroll till element 'HighlightPanel'
And I wait for '10' seconds
And I should see element 'HasWarning_Section' present on page
And I should see element 'RedFlag' present on page
Then I should see element - 'WarningReason_Section' contains text 'Contact Lead Services'
And I wait for '3' seconds
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent' 


Scenario: 2. Remove Warning from Contact (Lead and above)  - SP1_TC34
Given I click 'All_Dropdown_Agent'
And I click by JS 'ContactsFilter_ContactLead'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I clear the text and enter 'Jared Steiner' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '10' seconds
And I should see element 'ContactNameLink_ContactLead' present on page
And I click 'ContactNameLink_ContactLead'
And I wait for '10' seconds
And I should see element 'Add/RemoveWarning_Button' present on page
And I wait for '10' seconds
And I click 'Add/RemoveWarning_Button'
And I wait for '3' seconds
And I click 'WarningReason_Dropdown'
And I click 'NoneOption'
And I click 'SaveButton_Warning'
And I wait for '3' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I scroll till element 'NewCase_ContactLead'
And I wait for '5' seconds
And I scroll till element 'NewCase_ContactLead'
And I wait for '10' seconds
And I should see element 'HighlightPanel' present on page
#And I should see empty value in the Input feild 'CUSTOMERWARNING'
#And I wait for '3' seconds
And I should see empty value in the Input feild 'RedFlag'
And I scroll till element 'ContactHistory_Tab'
And I wait for '5' seconds
And I scroll till element 'ContactHistory_Tab'
And I wait for '10' seconds
And I should see empty value in the Input feild 'NewValueFieldValue'
And I scroll till element 'NewCase_ContactLead'
And I wait for '5' seconds
And I should see element 'Details_Tab' present on page
And I click by JS 'Details_Tab'
And I scroll till element 'HighlightPanel'
And I wait for '5' seconds
And I should see empty value in the Input feild 'HasWarning_Section'
And I should see empty value in the Input feild 'WarningReason_Section'
And I click by JS 'Close_Agent' 


Scenario: 3.Creating a new Consumer Relations Case from Customer Contact (Lead and above) - SP1_TC35
Given I click 'All_Dropdown_Agent'
And I click by JS 'ContactsFilter_ContactLead'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I clear the text and enter 'Jared Steiner' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '10' seconds
And I should see element 'ContactNameLink_ContactLead' present on page
And I click 'ContactNameLink_ContactLead'
And I should see element 'NewCase_ContactLead' present on page
And I click by JS 'NewCase_ContactLead'
And I should see element 'SelectRecordType_Dropdown_Order' present on page
And I click by JS 'SelectRecordType_Dropdown_Order'
And I click 'ConsumerRelations_Order'
And I click by JS 'Next_Order'
And I wait for '5' seconds
Then I should see element 'NewCaseConsumerRelations_Text_Creat' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click 'State_Dropdown_Creat'
And I click by JS 'NY_Text_Creat'
And I click 'Brand_Dropdown_Creat'
And I click 'MattressFirm_Text_Creat'
And I click 'Source_Dropdown_Agent'
And I click 'CustomerCare_Text_Creat'
And I click 'MerchandiseDisposition_Dropdown_Creat'
And I click 'Prime_Text_Creat'
And I click 'CRCaseType_Dropdown_Creat'
And I click 'Agency_Text_Creat'
And I click 'SubCaseType_Dropdown_Creat'
And I click 'BetterBusinessBureau(BBB)_Text_Creat'
And I click 'CurrentState_Dropdown_Creat'
And I click 'AccommodationOffer_Text_Creat'
And I click 'CR_Reason_Dropdown_Creat'
And I click 'Billing/Financing_Text_Creat'
And I click 'CR_ReasonDetails_Dropdown_Creat'
And I click 'CreditCard-Overcharged_Text_Creat'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I get text from 'CaseNumber_Agent' and store
And I scroll till element 'CaseOwner_ContactLead'
And I wait for '4' seconds
And I should see element 'CaseNumber_Order' present on page
And I should see element 'ContactName_ConsumerOrder' present on page
And I should see element - 'ContactName_ConsumerOrder' contains text 'Jared Steiner'
And I click by JS 'ContactName_ConsumerOrder'
And I refresh the WebPage
And I wait for '10' seconds
And I scroll till element 'Cases_ContactLead'
And I wait for '5' seconds
And I scroll till element 'Cases_ContactLead'
And I wait for '10' seconds
And I should see element 'Cases_ContactLead' present on page
And I should see text '$$CaseNumber_Agent' contained on page at 'NewNumber_Order'
And I should see text 'Consumer Relations' contained on page at 'CaseRecordType_RelatedConsumer'
And I click by JS 'Close_Agent' 


Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page
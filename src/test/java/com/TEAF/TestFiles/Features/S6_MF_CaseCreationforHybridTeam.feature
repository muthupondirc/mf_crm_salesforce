@S6_CaseCreationforHybridTeam
Feature: IS-Case creation for Hybrid team to handle inside sales order reschedules

1. IS-Case creation for Hybrid team to handle inside sales order reschedules in storefront- SP6_TC5
1.2 IS-Case creation for Hybrid team to handle inside sales order reschedules in SF Console- SP6_TC5.1

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: Omni Channel - make Service Agent as Available Chat
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'

Scenario: 1. IS-Case creation for Hybrid team to handle inside sales order reschedules in storefront - SP6_Tc
#-----------UAT URL--------------------------------------------------------------------------------

#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#-----------QA URL--------------------------------------------------------------------------------- 

And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' 
#And I should see element 'CookiesCloseButton_Hybrid' present on page
#And I click by JS 'CookiesCloseButton_Hybrid'
#And I wait for visibility of element 'MattressSite_CloseButton'
#And I click 'MattressSite_CloseButton'
#And I wait for '15' seconds
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'joesmith@test.com' in field 'Email_Field_Omni'
And I get text from 'Email_Field_Omni' and store
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I get text from 'HomePhone_Field_Omni' and store
And I should see element 'HelpWithExistingOrder_Option' present on page
And I click 'HelpWithExistingOrder_Option'
#--------------next line is for old chat bot step-----------------------------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'

And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds
#--------------next 2 lines are old chat bot steps---------------------------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
#-----------------------console-----------------------------

And I Switch between Tab '0'
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AwayButton_Omni'
And I should see element 'AwayText_Omni' present on page
And I click by JS 'Minimize_Button_Omni'
#------------------------------------------------------------------------

And I Switch between Tab '1'
And I should see element 'Textarea_Field_Omni' present on page
And I clear the text and enter 'Agent' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'NoAgentsText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I should see element 'ChatStartedText_Chat' present on page
And I click by JS 'YesButton_Chat'
And I click by JS 'DoNotHaveButton_Chat'
And I click by JS 'PhoneSalesAgentButton_Chat'
And I click by JS 'YesButton_Chat'
And I click by JS 'YesButton_Chat'
And I should see element 'DeliveryButton_Chat' present on page
And I click by JS 'DeliveryButton_Chat'
And I should see element 'RescheduleMyDeliverButton_Chat' present on page
And I click by JS 'RescheduleMyDeliverButton_Chat'
And I should see element 'Provideatesection_Chat' present on page
And I clear the text and enter 'phone' in field 'Textarea_Field_Omni'
And I click by JS 'EnterMessage_Field_Omni'
And I click by JS 'Textarea_Field_Omni'
And I hit enter-key on element 'Textarea_Field_Omni'
And I should see element 'CaseCreatedMessage_Chat' present on page
And I get Case Number from Chat 'CaseCreatedMessage_Chat' and store
And I Switch between Tab '0'

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'Retention Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'Retention Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.2 IS-Case creation for Hybrid team to handle inside sales order reschedules in SF Console- SP6_TC
And I click 'Cases_Dropdown_Agent'
And I click 'Cases_Icon_Agent'
And I click by JS 'SelectListViewDropdown_CR'
And I should see element 'SearchField_CR' present on page
And I clear the text and enter 'Service - MFRM Care' in field 'SearchField_CR'
And I hit enter-key on element 'SearchField_CR'
And I wait for '5' seconds
And I should see element 'NewEscalatedCaseNumber_CR' present on page
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'NewEscalatedCaseNumber_CR'
And I click by JS 'NewEscalatedCaseNumber_CR'
And I should see text 'Service - MFRM Care' contained on page at 'CaseOwnerField_Chat'
And I should see text '$$CaseCreatedMessage_Chat' contained on page at 'CaseNumberField_CR'
And I should see element 'ContactNameField_Hybrid' present on page
And I should see text 'Chatbot' contained on page at 'CaseOriginField_Hybrid'
And I scroll till element 'SourceField_Hybrid'
And I wait for '5' seconds
And I should see text 'phone' contained on page at 'SourceField_Hybrid'
And I should see text 'Reschedule My Delivery' contained on page at 'SubRecordTypeField_Hybrid'
And I scroll till element 'WebEmailField_Hybrid'
And I wait for '5' seconds
And I should see text 'joesmith@test.com' contained on page at 'WebEmailField_Hybrid'
And I should see text '5168618800' contained on page at 'WebPhoneField_Hybrid'
And I click by JS 'Close_Agent'

#----------------old steps----------------------------
#And I should see element 'OmniChannel_Button_Omni' present on page
#And I click 'OmniChannel_Button_Omni'
#And I click by JS 'Status_Dropdown_Omni'
#And I click by JS 'AvailableCase_Hybrid'
#And I should see element 'AvailableCaseText_Hybrid' present on page
#And I click by JS 'Minimize_Button_Omni'
#And I should see element 'CaseTab_Hybrid' present on page
#And I should see text 'Jarely Cruz' contained on page at 'CaseOwnerField_HybridOmni'
#And I click by JS 'Close_Agent'
#And I refresh the WebPage
#And I wait for '5' seconds
#---------------------------------------------

Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page






















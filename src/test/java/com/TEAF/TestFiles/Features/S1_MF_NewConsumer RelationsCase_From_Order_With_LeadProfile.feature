@S1_MF_ConsumerRelationsCase_Order_Lead
Feature: Creating a new Consumer Relations Case from Order with Lead Profile

 1. Login to Mattress Firm Sales Force Application
 1.2 Creating a new Consumer Relations Case from an Order (Lead and above) - SP1_TC24
 2. Closing a Consumer Relations Case (Lead and above) - SP1_TC25
 3. Creating a New Related Consumer Relations Case (Lead and above) - SP1_TC26
 4. Creating a New Blank Related Consumer Relations Case Using the Related Case Component -SP1_TC27
 5. Creating a New Blank Related Service Case Using the Related Case Component (CR) - SP1_TC28
 
Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'CCO Sr Mgr User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'CCO Sr Mgr User 2' present on page at 'ProfileCardPopUp' 
And I click by JS 'UserImageButton'

Scenario: 1.2 Creating a new Consumer Relations Case from an Order (Lead and above) -  SP1_TC24
#Given I click 'All_Dropdown_Agent'
#And I click by JS 'Cases_Filter_Agent'
#And I should see element 'Search_Field_Agent' present on page
#And I click by JS 'Search_Field_Agent'
#And I clear the text and enter 'AX-S015348301' in field 'Search_Field_Agent'
#And I hit enter-key on element 'Search_Field_Agent'
#And I refresh the WebPage
#And I wait for '5' seconds
#And I should see element 'Order_Number_AX' present on page
#And I click 'Order_Number_AX'
Given I click 'All_Dropdown_Agent'
And I click by JS 'ContactsFilter_ContactLead'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
#Order number example: SLP-9857045, AX-C005088650, SLP-9887857
And I clear the text and enter 'SLP-9857045' in field 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I wait for '10' seconds
And I should see element 'ContactName_Orderheader' present on page
And I click by JS 'ContactName_Orderheader'
And I wait for '5' seconds
And I should see element 'OrderNumber_Orderheader' present on page
And I click by JS 'OrderNumber_Orderheader'
And I wait for '5' seconds
And I should see element 'NewCase_Button_Order' present on page
And I click by JS 'NewCase_Button_Order'
And I click 'SelectRecordType_Dropdown_Order'
And I click 'ConsumerRelations_Order'
And I click 'Next_Order'
Then I should see element 'NewCaseConsumerRelations_Text_Creat' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click 'State_Dropdown_Creat'
And I click by JS 'NY_Text_Creat'
And I click 'Brand_Dropdown_Creat'
And I click 'MattressFirm_Text_Creat'
And I click 'Source_Dropdown_Agent'
And I click 'CustomerCare_Text_Creat'
And I click 'MerchandiseDisposition_Dropdown_Creat'
And I click 'Prime_Text_Creat'
And I click 'CRCaseType_Dropdown_Creat'
And I click 'Agency_Text_Creat'
And I click 'SubCaseType_Dropdown_Creat'
And I click 'BetterBusinessBureau(BBB)_Text_Creat'
And I click 'CurrentState_Dropdown_Creat'
And I click 'AccommodationOffer_Text_Creat'
And I click 'CR_Reason_Dropdown_Creat'
And I click 'Billing/Financing_Text_Creat'
And I click 'CR_ReasonDetails_Dropdown_Creat'
And I click 'CreditCard-Overcharged_Text_Creat'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I get text from 'CaseNumber_Agent' and store
And I should see element 'CaseNumber_Order' present on page
And I should see element 'ContactName_ConsumerOrder' present on page
And I should see element - 'ContactName_ConsumerOrder' contains text 'Jared Steiner'
And I should see element 'OriginalOrderNumberX_ConsumerOrder' present on page
And I should see element - 'OriginalOrderNumberX_ConsumerOrder' contains text 'SLP-105174834-1130'
And I should see element 'AccountName_ConsumerOrder' present on page
And I should see element - 'AccountName_ConsumerOrder' contains text 'CHARLES, JR MORROW'
And I click by JS 'ContactName_Order'
And I wait for '5' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I scroll till element 'Cases_Order'
And I wait for '10' seconds
And I scroll till element 'Cases_Order'
And I wait for '10' seconds
And I should see element 'Cases_Order' present on page
And I should see text '$$CaseNumber_Agent' contained on page at 'NewNumber_Order'
And I should see text 'Consumer Relations' contained on page at 'CaseRecordType_RelatedConsumer'
And I click by JS 'Close_Agent' 

Scenario: 2. Closing a Consumer Relations Case (Lead and above) -  SP1_TC25
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I click 'CaseNumber_Dropdown_Agent'
And I click 'Edit_Button_Close' Tab 
And I should see element 'Edit_Header' present on page
And I click 'CR_Resolution_Dropdown_Close'
And I click 'NoFurtherActionNeeded_Text_Close'
And I click 'Status_Dropdown_Close'
And I click by JS 'Closed_Text_Close'
And I click 'Save_Button_Close'
And I wait for '5' seconds
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent' 


Scenario:  3. Creating a New Related Consumer Relations Case (Lead and above) - SP1_TC26
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see element 'CaseNumber_Dropdown_Agent' present on page
And I click 'CaseNumber_Dropdown_Agent'
And I should see element 'Actions_Tab' present on page
And I click by JS 'More_RelatedCase'
And I click by JS 'NewRelatedCase_CreateRelated'
And I should see text 'Are you sure you would like to Clone this Case? Click Next to Continue.' contained on page at 'Areyousure_Message_NewCreateRelated'
And I click by JS 'Next_Button_CreateRelated'
And I scroll till element 'RelatedCase_CreateRelated'
And I should see element 'NewRelatedCaseNumber_CreateRelated' present on page
And I get text from 'NewRelatedCaseNumber_CreateRelated' and store
And I should see element 'ParentCaseNumber_CreateRelated' present on page
And I get text from 'ParentCaseNumber_CreateRelated' and store
And I click by JS 'NewRelatedCaseNumber_CreateRelated'
And I wait for '5' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I should see element 'ParentCase_field' present on page
And I should see text '$$ParentCaseNumber_CreateRelated' contained on page at 'ParentCase_field'
And I should see text 'Jared Steiner' contained on page at 'ContactName_RelatedConsumer'
And I should see text 'CHARLES, JR MORROW' contained on page at 'AccountName_RelatedConsumer'
And I should see text 'Phone' present on page at 'Case_Origin_NewCreateRelated'
And I should see text 'New' present on page at 'Status_RelatedConsumer'
And I should see text 'Customer Care' present on page at 'Source_RelatedConsumer'
And I click by JS 'Close_Agent' 


Scenario: 4. Creating a New Blank Related Consumer Relations Case Using the Related Case Component - SP1_TC27
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see element 'CaseNumber_Dropdown_Agent' present on page
And I click 'CaseNumber_Dropdown_Agent'
And I wait for '5' seconds
And I scroll till element 'RelatedCase_CreateRelated'
And I wait for '5' seconds
And I should see element 'RelatedCase_CreateRelated' present on page
And I click by JS 'RelatedCases_Dropdown'
And I should see element 'New_Text_Note' present on page
And I click 'New_Text_Note'
Then I should see element 'NewCase_Text_Lead' present on page
And I click 'ConsumerRelations_Button_Creat'
And I click 'Next_Button_Lead'
Then I should see element 'NewCaseConsumerRelations_Text_Creat' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click 'Email_Text_Creat'
And I click 'State_Dropdown_Creat'
And I click by JS 'NY_Text_Creat'
And I click 'Brand_Dropdown_Creat'
And I click 'MattressFirm_Text_Creat'
And I click 'Source_Dropdown_Agent'
And I click 'CustomerCare_Text_Creat'
And I click 'MerchandiseDisposition_Dropdown_Creat'
And I click 'Prime_Text_Creat'
And I click 'CRCaseType_Dropdown_Creat'
And I click 'Agency_Text_Creat'
And I click 'SubCaseType_Dropdown_Creat'
And I click 'BetterBusinessBureau(BBB)_Text_Creat'
And I click 'CurrentState_Dropdown_Creat'
And I click by JS 'AccommodationOffer_Text_Creat'
And I click 'CR_Reason_Dropdown_Creat'
And I click 'Billing/Financing_Text_Creat'
And I click 'CR_ReasonDetails_Dropdown_Creat'
And I click 'CreditCard-Overcharged_Text_Creat'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I should see element 'ParentCase_field' present on page
And I should see text '$$ParentCaseNumber_CreateRelated' contained on page at 'ParentCase_field'
And I click by JS 'Close_Agent' 
And I click by JS 'Close_Agent' 


Scenario:  5. Creating a New Blank Related Service Case Using the Related Case Component (CR) -- SP1_TC28
Given I click 'All_Dropdown_Agent'
And I click by JS 'Cases_Filter_Agent'
And I should see element 'Search_Field_Agent' present on page
And I click by JS 'Search_Field_Agent'
And I enter from stored variable '$$CaseNumber_Agent' into feild 'Search_Field_Agent'
And I hit enter-key on element 'Search_Field_Agent'
And I refresh the WebPage
And I wait for '5' seconds
And I should see element 'CaseNumber_Dropdown_Agent' present on page
And I click 'CaseNumber_Dropdown_Agent'
And I wait for '5' seconds
And I scroll till element 'RelatedCase_CreateRelated'
And I wait for '5' seconds
And I should see element 'RelatedCase_CreateRelated' present on page
And I click by JS 'RelatedCases_Dropdown'
And I click 'New_Text_Note'
Then I should see element 'NewCase_Text_Lead' present on page
And I click 'Service_Button_Lead'
And I click 'Next_Button_Lead'
Then I should see element 'NewCaseService_Text_Lead' present on page
And I click by JS 'CaseOrigin_Dropdown_Agent'
And I click by JS 'Phone_Text_Agent'
And I click 'Source_Dropdown_Agent'
And I click by JS 'SourcePhone_Option'
And I click 'SubRecordType_Dropdown_Agent'
And I click by JS '120DayPrice_Option'
And I click 'TypeOfProduct(s)_Dropdown'
And I click by JS 'CoreProduct_Option'
And I click 'Save_Button_Agent'
And I wait for '5' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I should see element 'ParentCase_field' present on page
And I should see text '$$ParentCaseNumber_CreateRelated' contained on page at 'ParentCase_field'
And I click by JS 'Close_Agent' 
 
Scenario: Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page

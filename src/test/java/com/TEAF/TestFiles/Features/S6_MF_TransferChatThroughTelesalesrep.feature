@S6_TransferChat
Feature: Transfer Chat through Telesales rep user to Hybrid Agent

1. Transfer Chat through Telesales rep user to Hybrid Agent - SP6_TC2.9

Scenario: Login to Mattress Firm Sales Force Application
Given My WebApp 'MF_SalesForce' is open 
And I wait for '3' seconds
When I login to Mattress Firm Sales Force with valid 'IS Agent User 2_QA' User Credentials 
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I wait for '5' seconds
And I should see text 'IS Agent User 2' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: 1.1 Omni Channel - Telesales rep user as Available Chat
Given I should see element 'OmniChannel_Button_Omni' present on page
And I click 'OmniChannel_Button_Omni'
And I click by JS 'Status_Dropdown_Omni'
And I click by JS 'AvailableChat_Button_Omni'
And I should see element 'AvailableChat_Text_Omni' present on page
And I click by JS 'Minimize_Button_Omni'

Scenario: 1. Transfer Chat through Telesales rep user to Hybrid Agent - SP6_TC
#-----------UAT URL--------------------------------------------------------------------------------
#And I Switch the Tab and enter the 'https://storefront:face@dev.mattressfirm.com/'
#And I should see Window Authontication popup on page
#-----------QA URL--------------------------------------------------------------------------------- 

And I Switch the Tab and enter the 'https://storefront:face@demo-web-sleepys.demandware.net/s/Mattress-Firm/shop-mattresses.html' 
And I should see element 'CookiesCloseButton_Hybrid' present on page
And I click by JS 'CookiesCloseButton_Hybrid'
And I wait for visibility of element 'MattressSite_CloseButton'
And I click 'MattressSite_CloseButton'
#And I wait for '15' seconds
And I should see element 'ChatwithanExpert_Button_Omni' present on page
And I click by JS 'ChatwithanExpert_Button_Omni'
And I wait for '5' seconds
And I should see element 'Chat_widget_Omni' present on page
And I wait for '5' seconds
And I click by JS 'FirstName_Field_Omni'
And I clear the text and enter 'Joe' in field 'FirstName_Field_Omni' by JS
And I click by JS 'LastName_Field_Omni'
And I clear the text and enter 'Smith' in field 'LastName_Field_Omni'
And I click by JS 'Email_Field_Omni'
And I clear the text and enter 'joesmith@test.com' in field 'Email_Field_Omni'
And I get text from 'Email_Field_Omni' and store
And I click by JS 'HomePhone_Field_Omni'
And I clear the text and enter '5168618800' in field 'HomePhone_Field_Omni'
And I should see element 'LiveShopping_Button' present on page
And I click 'LiveShopping_Button'
#--------------next line for old chat bot-----------------------------------------------------
#And I select option 'Service' in dropdown 'ChatWith_Dropdown_Omni' by 'value'

And I click by JS 'StartChat_Button_Omni'
And I wait for '10' seconds

#--------------next 2 lines for old chat bot---------------------------------------------------
#And I should see element 'NeedHelp_SecondOption' present on page
#And I click by JS 'NeedHelp_SecondOption'
And I should see element 'WorrySection_Live' present on page
And I should see element 'AnswerSection_Live' present on page
And I should see element 'WhatSection_Live' present on page
And I should see element 'TwinSize_Live' present on page
And I click by JS 'TwinSize_Live'
And I click by JS 'MattressOnlyOption'
And I should see element 'DesiredSection_Custom' present on page
And I click by JS 'SofterOption_Custom'
And I should see element 'ChatStartedSection_Custom' present on page
#And I Switch between Tab '1'
And I Switch between Tab '0'

#--------------------console steps---------------------

And I wait for '5' seconds
And I should see element 'Transfer_Icon' present on page 
And I click by JS 'Transfer_Icon'
And I wait for '3' seconds
And I should see element 'TransferChat_Header' present on page 
And I should see element 'SelectanItem_Field_Omni' present on page 
And I click 'SelectanItem_Field_Omni'
And I should see element 'RetentionAgent_Option_Omni' present on page 
And I click 'RetentionAgent_Option_Omni'
And I should see element 'Textarea_Field_Transfer' present on page 
And I clear the text and enter 'Hello' in field 'Textarea_Field_Transfer'
And I should see element 'Transfer_Button_Omni' present on page 
And I click 'Transfer_Button_Omni'

Scenario: 1.3 Verify logout of Sales Force application
Given I click 'UserImageButton'
And I wait for '5' seconds
And I click link 'Log Out'
And I wait for '5' seconds
And I should see element 'login_username' present on page
@MF_SalesForce_Customer_Service_Knowledge_Base 
Feature: Sprint9_Customer Service - Knowledge Base 
1.Login with serviceseniormanager
2.SP9_1 Display knowledge articles for Service case if customer warning is true for contact
3.SP9_2 Display knowledge articles for Service case if customer warning is false for contact and source is store or event
4.SP9_3 Display knowledge articles for Service case if customer warning is false for contact and source is NOT store or event
5.Verify logout of Sales Force application
@S
Scenario: Creating a Login Issue Case from Mattress Firm Web Site
Given My WebApp 'MF_SalesForce' is open
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'

Scenario: Display knowledge articles for Service case if customer warning is true for contact
And I click by JS 'ShowNavMenu'
And I wait for '2' seconds
And I click by JS 'NavCases'
And I wait for '5' seconds
And I click by JS 'lnkCases'
And I wait for '2' seconds
And I should see element 'CreateNewCase' present on page
And I click by JS 'CreateNewCase'
And I wait for '5' seconds
And I should see element 'NewCaseSelectionBtn' present on page
And I should see element 'NewCaseSelectionLabel' present on page
And I click by JS 'NewCaseSelectionBtn'
And I should see element 'NewCaseSelectionNxt' present on page
And I click by JS 'NewCaseSelectionNxt'
And I wait for '5' seconds
And I should see element 'New_Case_Header' present on page
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I should see element 'Search_Txt' present on page
And I enter 'Brodrick B Baptiste' in field 'Search_Txt'
And I wait for '3' seconds
And I hit down Arrow key on element 'Search_Txt'
And I wait for '3' seconds
And I hit enter-key on element 'Search_Txt'
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'SubRecordType_NeedOption_Value'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'CaseNo' present on page
And I should see element 'CaseNumberValue' present on page
And I should see element 'lblKnowledgeArticles' present on page
And I should see text 'Escalate' present on page at 'valueKnowledgeArticles'
And I should see element 'checkBoxCustomerWarning' present on page
And I should see text containing "escalate" in all the knowledge articles

Scenario Outline: Display knowledge articles for Service case if customer warning is false for contact and source is store or event
And I close all the opened tabs 'iconClose'
And I click by JS 'ShowNavMenu'
And I wait for '2' seconds
And I click by JS 'NavCases'
And I wait for '5' seconds
And I click by JS 'lnkCases'
And I wait for '2' seconds
And I should see element 'CreateNewCase' present on page
And I click by JS 'CreateNewCase'
And I wait for '5' seconds
And I should see element 'NewCaseSelectionBtn' present on page
And I should see element 'NewCaseSelectionLabel' present on page
And I click by JS 'NewCaseSelectionBtn'
And I should see element 'NewCaseSelectionNxt' present on page
And I click by JS 'NewCaseSelectionNxt'
And I wait for '5' seconds
And I should see element 'New_Case_Header' present on page
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS '<sourcevalue>'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'SubRecordType_NeedOption_Value'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'CaseNo' present on page
And I should see element 'CaseNumberValue' present on page
And I should see element 'lblKnowledgeArticles' present on page
And I should see text '<keywordText>' present on page at 'valueKnowledgeArticles'
And I should see element 'checkBoxCustomerWarning' present on page
And I should see text containing "<keywordText>" in all the knowledge articles

Examples:
|sourcevalue|keywordText|
|SourceValue_DD_Store|Store|
|SourceValue_DD_Event|Event|
@S
Scenario: Display knowledge articles for Service case if customer warning is false for contact and source is NOT store or event
And I close all the opened tabs 'iconClose'
And I click by JS 'ShowNavMenu'
And I wait for '2' seconds
And I click by JS 'NavCases'
And I wait for '5' seconds
And I click by JS 'lnkCases'
And I wait for '2' seconds
And I should see element 'CreateNewCase' present on page
And I click by JS 'CreateNewCase'
And I wait for '5' seconds
And I should see element 'NewCaseSelectionBtn' present on page
And I should see element 'NewCaseSelectionLabel' present on page
And I click by JS 'NewCaseSelectionBtn'
And I should see element 'NewCaseSelectionNxt' present on page
And I click by JS 'NewCaseSelectionNxt'
And I wait for '5' seconds
And I should see element 'New_Case_Header' present on page
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I wait for '2' seconds
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'SubRecordTypeDD' present on page
And I click by JS 'SubRecordTypeDD'
And I click by JS 'SubRecordType_NeedOption_Value'
And I should see element 'TypeOfProduct' present on page
And I click by JS 'TypeOfProductDD'
And I click by JS 'TypeOfProductValue'
And I wait for '5' seconds
And I click by JS 'BedBugSave'
And I wait for '5' seconds
And I should see element 'CaseNo' present on page
And I should see element 'CaseNumberValue' present on page
And I should see element 'lblKnowledgeArticles' present on page
#And I should see text 'Escalate' present on page at 'valueKnowledgeArticles'
#And I should see element 'checkBoxCustomerWarning' present on page
#And I should see text containing "core" in all the knowledge articles





@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page

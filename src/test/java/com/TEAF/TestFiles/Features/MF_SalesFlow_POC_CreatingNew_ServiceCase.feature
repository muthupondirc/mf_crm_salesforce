@CreatingNewServiceCase_CustomerContact_With_AgentProfile
Feature: Creating New Service Case from Customer Contact with Agent Profile

1.Login with Standard user Credentials
2. SP1_TC30 Add Warning to contact from 
3. SP1_TC31 Creating a new Service Case from Customer Contact 

Scenario: Login with Standard user Credentials
Given My WebApp 'MF_SalesForce' is open 
And I wait for '5' seconds
When I login to Mattress Firm Sales Force with valid 'serviceagent' User Credentials 
And I close all the opened tabs 'iconClose'
And I wait for '5' seconds
And I click by JS 'UserImageButton'
And I should see profile name 'serviceagent_displayname' present on page at 'ProfileCardPopUp'
And I click by JS 'UserImageButton'
And I wait for '2' seconds

Scenario: SP1_TC30 Add Warning to contact from agent
And I wait for '3' seconds
And I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Contact'
And I hit tab-key on element 'SearchTypeDD'
And I enter 'Jared Steiner' in field 'GlobalSearchField'
And I wait for '5' seconds
And I hit down Arrow key on element 'GlobalSearchField'
#And I hit down Arrow key on element 'GlobalSearchField'
And I hit enter-key on element 'GlobalSearchField'
And I wait for '3' seconds
And I should not see element 'AddRemoveWarning' present on page
And I click by JS 'Close'

Scenario: SP1_TC31 Creating a new Service Case from Customer Contact 
And I wait for '3' seconds
And I click by JS 'SearchTypeDD'
And I click by JS 'SearchType_Contact'
And I hit tab-key on element 'SearchTypeDD'
And I enter 'Jared Steiner' in field 'GlobalSearchField'
And I wait for '3' seconds
And I hit down Arrow key on element 'GlobalSearchField'
And I wait for '5' seconds
And I hit enter-key on element 'GlobalSearchField'
And I wait for '3' seconds
#And I should see element 'Contact_Header' present on page
#And I click by JS 'ContactName'
And I wait for visibility of element 'New_Case'
And I should see element 'New_Case' present on page
And I click by JS 'New_Case'
And I wait for '3' seconds
And I should see element 'NewCaseRecordTypeLabel' present on page
And I should see element 'NewCaseRecordTypeDD' present on page
And I click by JS 'NewCaseRecordTypeDD'
And I wait for '2' seconds
And I select option 'Service' in dropdown 'NewCaseRecordTypeDD' by 'text'
And I wait for '2' seconds
And I should see element 'NewCaseRecordTypeNext' present on page
And I click by JS 'NewCaseRecordTypeNext'
And I wait for '5' seconds
And I should see element 'New_Case_Header' present on page
And I should see element 'Case_Origin_Label' present on page
And I click by JS 'Case_Origin_DD'
And I click by JS 'Case_Origin_DD_Phone'
And I scroll to 'element' - 'Case_Classification'
And I should see element 'Source_Label' present on page
And I click by JS 'Source_DD'
And I click by JS 'Source_DD_Value'
And I should see element 'TypeOfProduct' present on page
And I click by JS 'TypeOfProductDD'
And I click by JS 'TypeOfProductValue'
And I should see element 'SubrecordTypeDropDown' present on page
And I click by JS 'SubrecordTypeDropDown'
And I click by JS 'SubRecordType_Value'
#And I should see element 'SubTypeDropDown' present on page
#And I should see element 'SubTypeDropDown' present on page
#And I click by JS 'SubTypeDropDown'
#And I click by JS 'Sub_Type_BillingChargesReview'
#And I should see element 'Reason' present on page
#And I click by JS 'ReasonDropDown'
#And I click by JS 'Reason_CreditCard_Payment'
#And I should see element 'Current_State' present on page
#And I click by JS 'CurrentStateDropDown'
#And I click by JS 'CurrentState_CreditCard'
And I click by JS 'CaseSaveBtn'
And I wait for '5' seconds
And I should see element 'CreatedCaseNumber' present on page
And I get text from 'CreatedCaseNumber' and store
And I wait for '3' seconds
And I click by JS 'MainTab'
And I wait for '3' seconds
And I should see element 'HighlightPanel' present on page
And I scroll to 'element' - 'BillingInfo'
And I wait for '5' seconds
And I scroll to 'element' - 'ActivityHistoryHeading'
And I wait for '5' seconds
And I should see element 'Case_Comp_Header' present on page
#For UAT
#And I should see element 'Case_Comp_FirstRecordUAT' present on page
#For QA
And I should see element 'Case_Comp_FirstRecordQA' present on page
And I wait for '2' seconds
And I click by JS 'Close'
And I wait for '2' seconds
#And I click by JS 'Close'

@Ignore
Scenario: Verify logout of Sales Force application
Given I click by JS 'UserImageButton'
And I click link 'Log Out'
And I should see element 'login_username' present on page
package com.TEAF.TestFiles.Runner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.TEAF.Hooks.Hooks;
import com.TEAF.framework.GenerateCustomReport;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.TestConfig;
import com.TEAF.framework.Utilities;
import com.galenframework.config.GalenConfig;
import com.galenframework.config.GalenProperty;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@CucumberOptions(dryRun = false, plugin = { "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
		"pretty",
		// "html:CukeNativeHTMLReport/",
		// "junit:src/test/java/com/TestResults/cucumber-report/cucumber_1.xml",
		"json:src/test/java/com/TestResults/cucumber-report/cucumber_1.json" }, strict = true, junit = "--step-notifications", features = {
				"src/test/java/com/TEAF/TestFiles/Features" }, glue = { "com.TEAF.Hooks",
						"com.TEAF.stepDefinitions" }, tags = {  "@MF_Sprint7_DifferentScreen_ResolutionsCase_ForChat"}, monochrome = true)

@RunWith(Cucumber.class)
public class BS_FirefoxRunner {

	
	public static String Platform = "desktop";
	public static String Browser = "browserstack-desktop-browser";
	public static String TestName = "RC TEAF";

	static String reportPath = null;
	static String appType = "webapp";
	static String appLocation;
	static Logger log = Logger.getLogger(BS_FirefoxRunner.class.getName());

	@BeforeClass
	public static void setUp() {
		try {
			System.setProperty("MF_SF_QA", "https://mattress--qa.my.salesforce.com/");
			System.setProperty("MF_SF_UAT", "https://test.salesforce.com");
			String envUrl = "MF_SF_UAT";
			System.setProperty("test_env_URL",envUrl);
	        PropertyConfigurator.configure(System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\TestFiles\\Config\\log4j.properties");

			// LoadConfigurations
			TestConfig.LoadAllConfig();
			// Platform = System.getProperty("PlatformName");
			Browser = System.getProperty("BrowserName", Browser);
			appType = System.getProperty("AppType", appType);
			appLocation = System.getProperty("AppLocation", "D:/Omega Syteline/SyteLine.application");

			// System.setProperty("test.platformName", Platform);
			System.setProperty("test.browserName", Browser);
			System.setProperty("test.appType", appType);
			System.setProperty("test.winDesktopApp", appLocation);
			String URL = System.getProperty("URL", System.getProperty(envUrl));
			System.setProperty("test.appUrl", URL);
			System.setProperty("test.TestName", TestName);

			// Report configurations
			DateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm_ss");
			Date date = new Date();
			String timeStamp = dateFormat.format(date);
			reportPath = GenerateCustomReport.reportPath;

			if (System.getProperty("test.disableExtentReport").equalsIgnoreCase("false")) {
				System.setProperty("screenshot.dir", reportPath + "/Screenshots/");
				System.setProperty("extent.reporter.html.start", "true");
				System.setProperty("extent.reporter.spark.start", "true");
				System.setProperty("extent.reporter.html.out", reportPath + "/ExtentTestReport.html");
				System.setProperty("extent.reporter.spark.out", reportPath + "/");

				// System.setProperty("extent.reporter.logger.start", "true");
				// System.setProperty("extent.reporter.html.config","src/test/java/com/Resources/extent-config.xml");
				// System.setProperty("extent.reporter.logger.out", "output/LoggerOutput/");
				// System.setProperty("extent.reporter.klov.start", "true");
				// System.setProperty("extent.reporter.klov.config", "klov.properties");
			}

			// Galen Property
			GalenConfig.getConfig().setProperty(GalenProperty.SCREENSHOT_FULLPAGE, "true");

			// ScreenVideoCapture
			if (System.getProperty("test.disableScreenVideoCapture").equalsIgnoreCase("false")) {
				Utilities.startVideoRecorder();
			}
			// SessionSetup
			log.info("Platform: " + Platform);
			log.info("Browser : " + Browser);
			if (Platform.equals("desktop") && !Browser.equals("REST Service")) {
				StepBase.browserStackStart(Browser, "Firefox", "latest");
			} else if (Platform.equals("mobile") && !Browser.equals("REST Service")) {
				// StepBase.appiumStart(); //Appium server Should be installed via node js for
				// this to work
				StepBase.setUp(Platform, Browser);	
			} else {
				// System.out.println("Enter valid platform choice: desktop / android / ios");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
	}

	@AfterClass
	public static void tearDown() throws Throwable {
		try {
		
			StepBase.tearDown();

			// Generation of Default Cucumber Reports
			if (!System.getProperty("test.disableCucumberReport").equalsIgnoreCase("true")) {
				// Cucumber Report Generation
				GenerateCustomReport.generateCustomeReport(Browser, Platform, "cucumber_1.json");
				HashMapContainer.ClearHM();
			}

			Collection<String> values = Hooks.scenarioStatus.values();
			String toMailId = null;
			String ccMailId = null;
			boolean flag = false;
			if ((values.contains("FAILED") || values.contains("SKIPPED"))
					&& !(Hooks.code == 520 || Hooks.code == 521)) {
				flag = true;
			}

			log.info("Failure Present: " + flag);
			if (flag == true) {
				// Mailing only internal team to review the failures
				toMailId = "swathin@royalcyber.com";
				ccMailId = "chandan.navara@royalcyber.com,premkumar.g@royalcyber.com";
			} else {
				// Mailing everyone as results look good
				toMailId = "swathin@royalcyber.com";
				ccMailId = "chandan.navara@royalcyber.com,premkumar.g@royalcyber.com";
			}

			// Prepare reports for Email
			if (System.getProperty("test.generateEmail").equalsIgnoreCase("true")) {
				String toMail = System.getProperty("ToMailID", toMailId);
				String ccMail = System.getProperty("CCMailID", ccMailId);
				Utilities.reportstoZipFile(GenerateCustomReport.reportPath, "TestExecution_ExtentReports");
				Utilities.reportstoZipFile("UITestReports", "TestExecution_UIReports");
				log.info("To email id : "+toMail);
				log.info("Cc email id : "+ccMail);
				Utilities.auto_generation_Email(toMail, ccMail);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

}
